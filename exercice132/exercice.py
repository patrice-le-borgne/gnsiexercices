# sommets :     G, J, Y, E, N, M, A, L
matrice_adj = [[0, 1, 1, 0, 1, 1, 0, 0], # G
               [1, 0, 1, 1, 0, 0, 0, 1], # J
               [1, 1, 0, 1, 1, 1, 1, 0], # Y
               [0, 1, 1, 0, 1, 0, 0, 0], # E 
               [1, 0, 1, 1, 0, 0, 0, 0], # N
               [1, 0, 1, 0, 0, 0, 1, 0], # M
               [0, 0, 1, 0, 0, 1, 0, 0], # A
               [0, 1, 0, 0, 0, 0, 0, 0]] # L

graphe = {
    'G': ['J', 'Y', 'N', 'M'],
    'J': ['G', 'Y', 'E', 'L'],
    'Y': ['G', 'J', 'E', 'N', 'M', 'A'],
    'E': ['J', 'Y', 'N'],
    'N': ['G', 'Y', 'E'],
    'M': ['G', 'Y', 'A'],
    'A': ['Y', 'M'],
    'L': ['J'] }

sommets= ['G', 'J', 'Y', 'E', 'N', 'M', 'A', 'L']
retour_fonction_Q_4 = 4
c = 'clé'
v = 'valeur'
cercle_amis_lou = ['L', 'J', 'G', 'N', 'Y', 'E']

graphe_facherie = {'G' : ['J', 'N'],
          'J' : ['G', 'Y', 'E', 'L'],
          'Y' : ['J', 'E', 'N'],
          'E' : ['J', 'Y', 'N'],
          'N' : ['G', 'Y', 'E'],
          'M' : ['A'],
          'A' : ['M'],
          'L' : ['J']
       }


def position(l, s):
    for i in range(len(l)):
        if s == l[i]:
            return i
    return None
    
    
def nb_amis(L, m, s):
    pos_s = position(L, s)
    if pos_s is None:
        return None
    amis = 0
    for i in range(len(m)):
        amis += m[pos_s][i]
    return amis

def nb_amis_d(d, s):
    return len(d[s])

def parcours_en_profondeur(d, s, visites = []):
    visites.append(s)
    for v in d[s]:
        if v not in visites:
            parcours_en_profondeur(d, v)
    return visites


