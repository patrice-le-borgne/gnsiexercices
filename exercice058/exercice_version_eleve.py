def c2m(x):
    """
    retourne l’entier entre 0 et 25 correspondant au caractère x.
    """
    pass

def m2c(x):
    """
    renvoie le caractère correspondant à l'entier x (compris entre 0 et 25).
    """
    pass

def code_caractere(x, k):
    """
    renvoie le caractère x décalé de k rang (modulo 26)
    """
    pass

def code_phrase(phrase,k):
    """
    renvoie la phrase codée par un décalage de César de valeur k.
    """
    pass

def decode_phrase(phrase,k):
    """
    décode la phrase codée par un décalage de César k.
    """
    pass


def attaque_brute(texte):
    """
    Renvoie toutes les combinaisons de texte avec la clé k
    """
    pass


