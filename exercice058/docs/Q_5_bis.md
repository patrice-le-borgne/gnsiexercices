    >>> from exercice import *
    >>> from random import randint

    >>> decode_phrase(code_phrase("OU EST PANORAMIX", 10), 10)
    'OU EST PANORAMIX'
    >>> decode_phrase(code_phrase("J AIME LA POTION MAGIQUE", -5), -5)
    'J AIME LA POTION MAGIQUE'
    >>> decode_phrase(code_phrase("TU PEUX REPETER",26),26)
    'TU PEUX REPETER'

 
