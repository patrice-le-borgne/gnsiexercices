def c2m(x):
    """
    retourne l’entier entre 0 et 25 correspondant au caractère x.
    """
    return ord(x)-ord('A')

def m2c(x):
    """
    renvoie le caractère correspondant à l'entier x (compris entre 0 et 25).
    """
    return chr(ord("A")+x)

def code_caractere(x, k):
    """
    renvoie le caractère x décalé de k rang (modulo 26)
    """
    code_utf8_x = c2m(x)
    nouveau_code = (code_utf8_x + k) % 26
    return m2c(nouveau_code)

def code_phrase(phrase,k):
    """
    renvoie la phrase codée par un décalage de César de valeur k.
    """
    resultat = ""
    for lettre in phrase:
        if lettre == " ":
            resultat = resultat + " "
        else:
            resultat = resultat + code_caractere(lettre, k)
    return resultat

def decode_phrase(phrase,k):
    """
    décode la phrase codée par un décalage de César k.
    """
    return code_phrase(phrase,26-k)


def attaque_brute(texte):
    """
    Renvoie toutes les combinaisons de texte avec la clé k
    """
    for i in range(26):
        print(str(i) , decode_phrase(texte,i))


