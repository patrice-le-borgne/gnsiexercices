~~~ {.hidden .meta}
classe : première
type : écrit
chapitre : sécurisation
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> from random import randint
~~~

Exercice
==========

Le *chiffre de César* est une façon simple de coder un message afin de conserver le secret du contenu jusqu’à son destinataire. Il s’agit tout simplement de décaler chaque lettre du message. Voyons l’exemple d’un décalage de trois lettres : A  devient D  ;  B devient E ; etc.

![decalage_principe.png](decalage_principe.png)

Par exemple le message : *CAPTUREZ IDEFIX* se chiffre en : *FDSWXUHC LGHILA*

Une autre façon de présenter le décalage est de placer les alphabets en clair (anneau extérieur) et codé (anneau intérieur) sur deux roues concentriques. À gauche un décalage avec $k=3$ et à droite un décalage avec $k=10$.

![decalage_3_et_10.png](decalage_3_et_10.png)

Pour chiffrer des messages tu passes des lettres à l’extérieur aux lettres à l’intérieur. Pour déchiffrer des messages il suffit de faire l’opération inverse

Déchiffre à la main les messages suivants :

- *EORTXHC DVWHULA* chiffré avec un décalage $k=3$.

- *YE OCD ZKXYBKWSH* chiffré avec un décalage $k=10$.


## Passer d’un caractère à un nombre et inversement

On préfère travailler avec des nombres qu’avec des lettres ! On supporera que les messages sont écrits en majuscules sans ponctuation. Pour la suite du TP, on rappelle que :
 - `ord(x)` retourne le code UTF-8 (un entier) du caractère `x` ;
 - `chr(x)` retourne le caractère dont le code UTF-8 est l’entier `x`.

### De la lettre au nombre et réciproquement

À chaque lettre de l'alphabet, on souhaite associer sa position dans l'alphabet en commençant à 0.

![alphabet.png](alphabet.png)

1. Écrire une fonction `c2m(x)` qui retourne l’entier entre 0 et 25 correspondant au caractère `x`. 

~~~ {.python .test .amc file="Q_1.md" bareme="1"}
>>> c2m("A")
0
>>> c2m("Z")
25
~~~

~~~ {.python .hidden .test file="Q_1_bis.md" bareme="1"}
>>> c2m("H")
7
>>> c2m("M")
12
>>> c2m("T")
19
~~~

2. Écrire une fonction `m2c(x)` qui retourne le caractère correspondant à l'entier $x$, $0 \leqslant  x \leqslant 25$. Par exemple, `m2c(10)` doit renvoyer "K".

~~~ {.python .test .amc file="Q_2.md" bareme="1"}
>>> m2c(10)
'K'
>>> m2c(16)
'Q'
~~~

~~~ {.python .hidden .test file="Q_2_bis.md" bareme="1"}
>>> m2c(7)
'H'
>>> m2c(12)
'M'
>>> m2c(19)
'T'
~~~

~~~ {.python .hidden .test file="Q_2_ter.md" bareme="1"}
>>> for i in range(10):
...    n = randint(0, 26)
...    assert c2m(m2c(n)) == n
~~~

3.  Programmer une fonction `code_caractere(x,k)` qui renvoie le caractère `x` décalé de $k$ rang (modulo $26$). 

~~~ {.python .test .amc file="Q_3.md" bareme="1"}
>>> code_caractere("A", -1)
'Z'
>>> code_caractere("A", 10)
'K'
~~~

~~~ {.python .hidden .test file="Q_3_bis.md" bareme="1"}
>>> code_caractere("G", -3)
'D'
>>> code_caractere("X", 7)
'E'
>>> code_caractere("H", 0)
'H'
~~~

4. Programmer une fonction `code_phrase(phrase,k)` qui renvoie la phrase codée par un décalage de César $k$.

~~~ {.python .test .amc file="Q_4.md" bareme="1"}
>>> code_phrase("J AIME LA POTION MAGIQUE",-5)
'E VDHZ GV KJODJI HVBDLPZ'
~~~

~~~ {.python .hidden .test file="Q_4_bis.md" bareme="1"}
>>> code_phrase("BLOQUEZ ASTERIX",3)
'EORTXHC DVWHULA'
>>> code_phrase("OU EST PANORAMIX",10)
'YE OCD ZKXYBKWSH'
>>> code_phrase("TU PEUX REPETER",26)
'TU PEUX REPETER'
~~~

5. Programmer une fonction `decode_phrase(phrase,k)` qui décode la phrase codée par un décalage de César k.

~~~ {.python .test .amc file="Q_5.md" bareme="1"}
>>> decode_phrase("EORTXHC DVWHULA", 3)
'BLOQUEZ ASTERIX'
~~~

~~~ {.python .hidden .test file="Q_5_bis.md" bareme="1"}
>>> decode_phrase(code_phrase("OU EST PANORAMIX", 10), 10)
'OU EST PANORAMIX'
>>> decode_phrase(code_phrase("J AIME LA POTION MAGIQUE", -5), -5)
'J AIME LA POTION MAGIQUE'
>>> decode_phrase(code_phrase("TU PEUX REPETER",26),26)
'TU PEUX REPETER'
~~~

6.  Vous vous placez dans la peau d’un espion qui a intercepté un message codé par un chiffre de César, mais qui ne connaît pas la clé $k$. Programmer une fonction `attaque_brute(texte)` qui affiche les déchiffrements, en testant toutes les clés $k$ possibles.



   Vous avez intercepté le message envoyé par le camp Babaorum à  Jules César: HSFGJSEAP F S HDMK VW HGLAGF
    
 Que va maintenant faire César ?


~~~ {.python .test .amc file="Q_6.md" bareme="1"}
>>> attaque_brute("HSFGJSEAP F S HDMK VW HGLAGF")
0 HSFGJSEAP F S HDMK VW HGLAGF
1 GREFIRDZO E R GCLJ UV GFKZFE
2 FQDEHQCYN D Q FBKI TU FEJYED
3 EPCDGPBXM C P EAJH ST EDIXDC
4 DOBCFOAWL B O DZIG RS DCHWCB
5 CNABENZVK A N CYHF QR CBGVBA
6 BMZADMYUJ Z M BXGE PQ BAFUAZ
7 ALYZCLXTI Y L AWFD OP AZETZY
8 ZKXYBKWSH X K ZVEC NO ZYDSYX
9 YJWXAJVRG W J YUDB MN YXCRXW
10 XIVWZIUQF V I XTCA LM XWBQWV
11 WHUVYHTPE U H WSBZ KL WVAPVU
12 VGTUXGSOD T G VRAY JK VUZOUT
13 UFSTWFRNC S F UQZX IJ UTYNTS
14 TERSVEQMB R E TPYW HI TSXMSR
15 SDQRUDPLA Q D SOXV GH SRWLRQ
16 RCPQTCOKZ P C RNWU FG RQVKQP
17 QBOPSBNJY O B QMVT EF QPUJPO
18 PANORAMIX N A PLUS DE POTION
19 OZMNQZLHW M Z OKTR CD ONSHNM
20 NYLMPYKGV L Y NJSQ BC NMRGML
21 MXKLOXJFU K X MIRP AB MLQFLK
22 LWJKNWIET J W LHQO ZA LKPEKJ
23 KVIJMVHDS I V KGPN YZ KJODJI
24 JUHILUGCR H U JFOM XY JINCIH
25 ITGHKTFBQ G T IENL WX IHMBHG
~~~
