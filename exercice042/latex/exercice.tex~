\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

L'objet de ce TP est d'écrire un programme qui permet de prévoir si un
texte est écrit en français ou en anglais. Voici les statistiques
concernant la fréquence d'apparition en pourcentage des lettres \emph{U}
et \emph{H}, dans 10 textes écrits soit en anglais, soit en français.

\begin{longtable}[]{@{}llll@{}}
\toprule
Texte & U & H & Langue\tabularnewline
\midrule
\endhead
1 & 5.61 & 0.5 & ``F''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
2 & 2.99 & 5.09 & ``A''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
3 & 5.28 & 0.52 & ``F''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
4 & 2.57 & 5.87 & ``A''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
5 & 6.91 & 0.59 & ``F''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
6 & 2.68 & 5.56 & ``A''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
7 & 5.06 & 0.63 & ``F''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
8 & 2.91 & 5.28 & ``A''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
9 & 3.09 & 4.9 & ``A''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
10 & 5.75 & 0.6 & ``F''\tabularnewline
------- & ------ & ------ & --------\tabularnewline
\bottomrule
\end{longtable}

Ce tableau sera stocké dans la variable \texttt{STATS} (sans la première
ligne contenant le nom des colonnes).

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ STATS }\OperatorTok{=}\NormalTok{ [[}\DecValTok{1}\NormalTok{, }\FloatTok{5.61}\NormalTok{, }\FloatTok{0.5}\NormalTok{, }\StringTok{"F"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{2}\NormalTok{, }\FloatTok{2.99}\NormalTok{, }\FloatTok{5.09}\NormalTok{, }\StringTok{"A"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{3}\NormalTok{, }\FloatTok{5.28}\NormalTok{, }\FloatTok{0.52}\NormalTok{, }\StringTok{"F"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{4}\NormalTok{, }\FloatTok{2.57}\NormalTok{, }\FloatTok{5.87}\NormalTok{, }\StringTok{"A"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{5}\NormalTok{, }\FloatTok{6.91}\NormalTok{, }\FloatTok{0.59}\NormalTok{, }\StringTok{"F"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{6}\NormalTok{, }\FloatTok{2.68}\NormalTok{, }\FloatTok{5.56}\NormalTok{, }\StringTok{"A"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{7}\NormalTok{, }\FloatTok{5.06}\NormalTok{, }\FloatTok{0.63}\NormalTok{, }\StringTok{"F"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{8}\NormalTok{, }\FloatTok{2.91}\NormalTok{, }\FloatTok{5.28}\NormalTok{, }\StringTok{"A"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{9}\NormalTok{, }\FloatTok{3.09}\NormalTok{, }\FloatTok{4.9}\NormalTok{, }\StringTok{"A"}\NormalTok{],}
\NormalTok{...         [}\DecValTok{10}\NormalTok{, }\FloatTok{5.75}\NormalTok{, }\FloatTok{0.6}\NormalTok{, }\StringTok{"F"}\NormalTok{]]}
\end{Highlighting}
\end{Shaded}

La représentation graphique suivante avec en abscisse la fréquence du
\emph{U} et en ordonnée la fréquence du \emph{H} montre que ces deux
lettres sont assez discriminantes.

\begin{figure}
\centering
\includegraphics{nuages_points_1_UH.png}
\caption{nuages\_1}
\end{figure}

L'objectif du reste du TP est d'établir la fréquence de \emph{U} et de
\emph{H} dans le texte dont on veut prédire la langue d'écriture pour
pouvoir chercher quels sont les k plus proches voisins parmi les 10
textes dont on connait les statistiques.

\begin{figure}
\centering
\includegraphics{nuages_points_2_UH.png}
\caption{nuages\_1}
\end{figure}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Écrire une fonction \texttt{distance(x\_1,\ y\_1,\ x\_2,\ y\_2)} qui
  calcule la distance euclidienne entre les points de coordonnées
  \texttt{(x\_1,\ y\_1)} et \texttt{(x\_2,\ y\_2)}.

  On rappelle que la formule de la distance séparant les points \(A\) et
  \(B\) est :

  \[ AB = \sqrt{(x_A - x_B)^2 + (y_A-y_B)^2}\]
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ distance(}\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{4}\NormalTok{, }\DecValTok{5}\NormalTok{)}
    \FloatTok{5.0}
    \OperatorTok{>>>}\NormalTok{ distance(}\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{0}\NormalTok{) }
    \FloatTok{1.0}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Écrire une fonction \texttt{frequence\_lettre(lettre,\ texte)} qui
  renvoie la fréquence d'apparition de la \texttt{lettre} dans le
  \texttt{texte}. La fréquence renvoyée doit être en pourcentage.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ t }\OperatorTok{=} \StringTok{"J'adore jouer avec mon ballon !"}
    \OperatorTok{>>>}\NormalTok{ isclose(frequence_lettre(}\StringTok{'a'}\NormalTok{, t), }\FloatTok{9.67741935483871}\NormalTok{, rel_tol}\OperatorTok{=}\FloatTok{1e-10}\NormalTok{)}
    \VariableTok{True}
    \OperatorTok{>>>}\NormalTok{ frequence_lettre(}\StringTok{'y'}\NormalTok{, t)}
    \FloatTok{0.0}
    \OperatorTok{>>>}\NormalTok{ isclose(frequence_lettre(}\StringTok{'j'}\NormalTok{, t), }\FloatTok{6.451612903225806}\NormalTok{, rel_tol}\OperatorTok{=}\FloatTok{1e-10}\NormalTok{)}
    \VariableTok{True}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  Écrire une fonction \texttt{liste\_distances(x,\ y,\ liste\_donnees)}
  qui renvoie la liste des distances séparant le point
  \texttt{(x\ ;\ y)} de tous les points présents dans la
  \texttt{liste\_donnees}.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ liste_distances(}\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, STATS)}
\NormalTok{    [[}\DecValTok{1}\NormalTok{, }\FloatTok{5.632237566012287}\NormalTok{, }\StringTok{'F'}\NormalTok{], [}\DecValTok{2}\NormalTok{, }\FloatTok{5.90323640048406}\NormalTok{, }\StringTok{'A'}\NormalTok{], ..., [}\DecValTok{10}\NormalTok{, }\FloatTok{5.7812195945146385}\NormalTok{, }\StringTok{'F'}\NormalTok{]]}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  Écrire une fonction \texttt{prediction(liste,\ k)} qui renvoie la
  valeur de l'étiquette la plus présente parmi les \texttt{k} premières
  valeurs de la \texttt{liste}. \emph{La liste doit être triée.}
\end{enumerate}

Ici l'étiquette vaut \texttt{\textquotesingle{}A\textquotesingle{}},
\texttt{\textquotesingle{}F\textquotesingle{}} ou
\texttt{\textquotesingle{}?\textquotesingle{}} si on ne peut savoir.

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ liste_triee }\OperatorTok{=}\NormalTok{ [[}\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{, }\StringTok{'F'}\NormalTok{], }
\NormalTok{    ...                [}\DecValTok{2}\NormalTok{, }\DecValTok{2}\NormalTok{,}\StringTok{'F'}\NormalTok{], }
\NormalTok{    ...                [}\DecValTok{3}\NormalTok{, }\DecValTok{3}\NormalTok{, }\StringTok{'A'}\NormalTok{], }
\NormalTok{    ...                [}\DecValTok{4}\NormalTok{, }\DecValTok{4}\NormalTok{, }\StringTok{'A'}\NormalTok{], }
\NormalTok{    ...                [}\DecValTok{5}\NormalTok{, }\DecValTok{5}\NormalTok{, }\StringTok{'F'}\NormalTok{]]}
    \OperatorTok{>>>}\NormalTok{ prediction(liste_triee, }\DecValTok{1}\NormalTok{)}
    \CommentTok{'F'}
    \OperatorTok{>>>}\NormalTok{ prediction(liste_triee, }\DecValTok{2}\NormalTok{) }
    \CommentTok{'F'}
    \OperatorTok{>>>}\NormalTok{ prediction(liste_triee, }\DecValTok{4}\NormalTok{)}
    \CommentTok{'?'}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Écrire une fonction \texttt{predire\_langue(texte,\ k)} qui tente de
  prédire la langue dans laquelle est écrit le \texttt{texte} en se
  basant sur les \texttt{k} plus proches voisins. La fonction doit
  renvoyer l'une des phrases suivantes :
\end{enumerate}

\begin{itemize}
\tightlist
\item
  « Je pense que le texte est écrit en français.»
\item
  « Je pense que le texte est écrit en anglais.»
\item
  « Je ne sais pas dans quelle langue est écrit le texte.»
\end{itemize}

\emph{Petit coup de pouce}

\texttt{sorted(ld,\ key=lambda\ valeur:\ valeur{[}1{]})} renvoie une
copie triée dans l'ordre croissant de la liste de listes \texttt{ld}
suivant les valeurs de la deuxième colonne.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ T }\OperatorTok{=}\NormalTok{ (}\StringTok{"Earlier releases have been removed because"}
\NormalTok{...      }\StringTok{"we can only support the current versions of the software. "}
\NormalTok{...      }\StringTok{"To update old code, read the changes page. "}
\NormalTok{...      }\StringTok{"Changes for each release can be found in revisions.txt. "}
\NormalTok{...      }\StringTok{"If you have problems with the current release, please file a bug "}
\NormalTok{...      }\StringTok{"so that we can fix it. Older releases can also be built "}
\NormalTok{...      }\StringTok{"from the source. Read More about the releases and their numbering. "}
\NormalTok{...      }\StringTok{"To use Android Mode, Processing 3 or later is required."}\NormalTok{)}
\OperatorTok{>>>}\NormalTok{ predire_langue(T, }\DecValTok{1}\NormalTok{)}
\CommentTok{'Je pense que le texte est écrit en anglais.'}
\OperatorTok{>>>}\NormalTok{ predire_langue(T, }\DecValTok{8}\NormalTok{)}
\CommentTok{'Je pense que le texte est écrit en anglais.'}
\OperatorTok{>>>}\NormalTok{ predire_langue(T, }\DecValTok{10}\NormalTok{)}
\CommentTok{'Je ne sais pas dans quelle langue est écrit le texte.'}
\end{Highlighting}
\end{Shaded}

Peut-on considérer l'algorithme des k plus proches voisins comme fiable
?

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ T }\OperatorTok{=} \StringTok{"L'hyperbole est un truc de hippie !"}
\OperatorTok{>>>}\NormalTok{ predire_langue(T, }\DecValTok{9}\NormalTok{)}
\CommentTok{'Je pense que le texte est écrit en anglais.'}
\OperatorTok{>>>}\NormalTok{ predire_langue(T, }\DecValTok{1}\NormalTok{)}
\CommentTok{'Je pense que le texte est écrit en anglais.'}
\end{Highlighting}
\end{Shaded}

