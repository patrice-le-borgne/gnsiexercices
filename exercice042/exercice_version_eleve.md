Exercice
========

L'objet de ce TP est d'écrire un programme qui permet de prévoir si un
texte est écrit en français ou en anglais. Voici les statistiques
concernant la fréquence d'apparition en pourcentage des lettres *U* et
*H*, dans 10 textes écrits soit en anglais, soit en français.

  Texte     U        H        Langue
  --------- -------- -------- ----------
  1         5.61     0.5      "F"
  -------   ------   ------   --------
  2         2.99     5.09     "A"
  -------   ------   ------   --------
  3         5.28     0.52     "F"
  -------   ------   ------   --------
  4         2.57     5.87     "A"
  -------   ------   ------   --------
  5         6.91     0.59     "F"
  -------   ------   ------   --------
  6         2.68     5.56     "A"
  -------   ------   ------   --------
  7         5.06     0.63     "F"
  -------   ------   ------   --------
  8         2.91     5.28     "A"
  -------   ------   ------   --------
  9         3.09     4.9      "A"
  -------   ------   ------   --------
  10        5.75     0.6      "F"
  -------   ------   ------   --------

Ce tableau sera stocké dans la variable `STATS` (sans la première ligne
contenant le nom des colonnes).

``` {.python}
>>> STATS = [[1, 5.61, 0.5, "F"],
...         [2, 2.99, 5.09, "A"],
...         [3, 5.28, 0.52, "F"],
...         [4, 2.57, 5.87, "A"],
...         [5, 6.91, 0.59, "F"],
...         [6, 2.68, 5.56, "A"],
...         [7, 5.06, 0.63, "F"],
...         [8, 2.91, 5.28, "A"],
...         [9, 3.09, 4.9, "A"],
...         [10, 5.75, 0.6, "F"]]
```

La représentation graphique suivante avec en abscisse la fréquence du
*U* et en ordonnée la fréquence du *H* montre que ces deux lettres sont
assez discriminantes.

![nuages\_1](nuages_points_1_UH.png)

L'objectif du reste du TP est d'établir la fréquence de *U* et de *H*
dans le texte dont on veut prédire la langue d'écriture pour pouvoir
chercher quels sont les k plus proches voisins parmi les 10 textes dont
on connait les statistiques.

![nuages\_1](nuages_points_2_UH.png)

1.  Écrire une fonction `distance(x_1, y_1, x_2, y_2)` qui calcule la
    distance euclidienne entre les points de coordonnées `(x_1, y_1)` et
    `(x_2, y_2)`.

    On rappelle que la formule de la distance séparant les points $A$ et
    $B$ est :

    $$ AB = \sqrt{(x_A - x_B)^2 + (y_A-y_B)^2}$$

``` {.python}
    >>> distance(1, 1, 4, 5)
    5.0
    >>> distance(1, 1, 1, 0) 
    1.0
```

2.  Écrire une fonction `frequence_lettre(lettre, texte)` qui renvoie la
    fréquence d'apparition de la `lettre` dans le `texte`. La fréquence
    renvoyée doit être en pourcentage.

``` {.python}
    >>> t = "J'adore jouer avec mon ballon !"
    >>> isclose(frequence_lettre('a', t), 9.67741935483871, rel_tol=1e-10)
    True
    >>> frequence_lettre('y', t)
    0.0
    >>> isclose(frequence_lettre('j', t), 6.451612903225806, rel_tol=1e-10)
    True
```

3.  Écrire une fonction `liste_distances(x, y, liste_donnees)` qui
    renvoie la liste des distances séparant le point `(x ; y)` de tous
    les points présents dans la `liste_donnees`.

``` {.python}
    >>> liste_distances(0, 0, STATS)
    [[1, 5.632237566012287, 'F'], [2, 5.90323640048406, 'A'], ..., [10, 5.7812195945146385, 'F']]
```

4.  Écrire une fonction `prediction(liste, k)` qui renvoie la valeur de
    l'étiquette la plus présente parmi les `k` premières valeurs de la
    `liste`. *La liste doit être triée.*

Ici l'étiquette vaut `'A'`, `'F'` ou `'?'` si on ne peut savoir.

``` {.python}
    >>> liste_triee = [[1, 1, 'F'], 
    ...                [2, 2,'F'], 
    ...                [3, 3, 'A'], 
    ...                [4, 4, 'A'], 
    ...                [5, 5, 'F']]
    >>> prediction(liste_triee, 1)
    'F'
    >>> prediction(liste_triee, 2) 
    'F'
    >>> prediction(liste_triee, 4)
    '?'
```

5.  Écrire une fonction `predire_langue(texte, k)` qui tente de prédire
    la langue dans laquelle est écrit le `texte` en se basant sur les
    `k` plus proches voisins. La fonction doit renvoyer l'une des
    phrases suivantes :

    -   « Je pense que le texte est écrit en français.»
    -   « Je pense que le texte est écrit en anglais.»
    -   « Je ne sais pas dans quelle langue est écrit le texte.»
    
    *Petit coup de pouce*
    
    `sorted(ld, key=lambda valeur: valeur[1])` renvoie une copie triée dans
    l'ordre croissant de la liste de listes `ld` suivant les valeurs de la
    deuxième colonne.
    
    ``` {.python}
    >>> T = ("Earlier releases have been removed because"
    ...      "we can only support the current versions of the software. "
    ...      "To update old code, read the changes page. "
    ...      "Changes for each release can be found in revisions.txt. "
    ...      "If you have problems with the current release, please file a bug "
    ...      "so that we can fix it. Older releases can also be built "
    ...      "from the source. Read More about the releases and their numbering. "
    ...      "To use Android Mode, Processing 3 or later is required.")
    >>> predire_langue(T, 1)
    'Je pense que le texte est écrit en anglais.'
    >>> predire_langue(T, 8)
    'Je pense que le texte est écrit en anglais.'
    >>> predire_langue(T, 10)
    'Je ne sais pas dans quelle langue est écrit le texte.'
    ```
    
    Peut-on considérer l'algorithme des k plus proches voisins comme fiable
    ?
    
    ``` {.python}
    >>> T = "L'hyperbole est un truc de hippie !"
    >>> predire_langue(T, 9)
    'Je pense que le texte est écrit en anglais.'
    >>> predire_langue(T, 1)
    'Je pense que le texte est écrit en anglais.'
    ```
