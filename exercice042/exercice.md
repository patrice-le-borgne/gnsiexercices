~~~ {.hidden .meta}
classe : première
type : oral1
chapitre : algorithmique
thème : kppv
~~~

~~~ {.python .hidden .all}
>>> from math import sqrt, isclose
>>> from exercice import *
~~~

Exercice
==========

L’objet de ce TP est d’écrire un programme qui permet de prévoir si un
texte est écrit en français ou en anglais. Voici les statistiques
concernant la fréquence d’apparition en pourcentage des lettres _U_ et
_H_, dans 10 textes écrits soit en anglais, soit en français.


| Texte | U    | H    | Langue |
|-------|------|------|--------|
| 1     | 5.61 | 0.5  | "F"    |
|-------|------|------|--------|
| 2     | 2.99 | 5.09 | "A"    |
|-------|------|------|--------|
| 3     | 5.28 | 0.52 | "F"    |
|-------|------|------|--------|
| 4     | 2.57 | 5.87 | "A"    |
|-------|------|------|--------|
| 5     | 6.91 | 0.59 | "F"    |
|-------|------|------|--------|
| 6     | 2.68 | 5.56 | "A"    |
|-------|------|------|--------|
| 7     | 5.06 | 0.63 | "F"    |
|-------|------|------|--------|
| 8     | 2.91 | 5.28 | "A"    |
|-------|------|------|--------|
| 9     | 3.09 | 4.9  | "A"    |
|-------|------|------|--------|
| 10    | 5.75 | 0.6  | "F"    |
|-------|------|------|--------|



Ce tableau sera stocké dans la variable `STATS` (sans la première ligne contenant le nom des colonnes).

~~~ {.python .all}
>>> STATS = [[1, 5.61, 0.5, "F"],
...         [2, 2.99, 5.09, "A"],
...         [3, 5.28, 0.52, "F"],
...         [4, 2.57, 5.87, "A"],
...         [5, 6.91, 0.59, "F"],
...         [6, 2.68, 5.56, "A"],
...         [7, 5.06, 0.63, "F"],
...         [8, 2.91, 5.28, "A"],
...         [9, 3.09, 4.9, "A"],
...         [10, 5.75, 0.6, "F"]]
~~~

La représentation graphique suivante avec en abscisse la fréquence du
_U_ et en ordonnée la fréquence du _H_ montre que ces deux lettres sont
assez discriminantes.


![nuages_1](nuages_points_1_UH.png)

L’objectif du reste du TP est d’établir la fréquence de _U_ et de _H_
dans le texte dont on veut prédire la langue d’écriture pour pouvoir
chercher quels sont les k plus proches voisins parmi les 10 textes dont
on connait les statistiques.

![nuages_1](nuages_points_2_UH.png)

1. Écrire une fonction `distance(x_1, y_1, x_2, y_2)` qui calcule la
   distance euclidienne entre les points de coordonnées `(x_1, y_1)`
   et `(x_2, y_2)`.
   
   On rappelle que la formule de la distance séparant les points $A$ et $B$ est :

   $$ AB = \sqrt{(x_A - x_B)^2 + (y_A-y_B)^2}$$

~~~ {.python .test .amc file="Q_1.md" bareme="1"}
    >>> distance(1, 1, 4, 5)
    5.0
    >>> distance(1, 1, 1, 0) 
    1.0
~~~

~~~ {.python .hidden .test .amc file="Q_1_c.md" bareme="1"}
>>> distance(1, 1, 1, 1)
0.0
>>> distance(-1, -1, 4, 11)
13.0
~~~


2. Écrire une fonction `frequence_lettre(lettre, texte)` qui renvoie
   la fréquence d’apparition de la `lettre` dans le `texte`. La
   fréquence renvoyée doit être en pourcentage.

~~~ {.python .test .amc file="Q_2.md" bareme="1"}
    >>> t = "J'adore jouer avec mon ballon !"
    >>> isclose(frequence_lettre('a', t), 9.67741935483871, rel_tol=1e-10)
    True
    >>> frequence_lettre('y', t)
    0.0
    >>> isclose(frequence_lettre('j', t), 6.451612903225806, rel_tol=1e-10)
    True
~~~

~~~ {.python .hidden .test .amc file="Q_2_c.md" bareme="1"}
>>> t = "La NSI, ça pique !"
>>> isclose(frequence_lettre('i', t), 11.111111111111, rel_tol=1e-10)
True
>>> frequence_lettre('z', t)
0.0
~~~

3. Écrire une fonction `liste_distances(x, y, liste_donnees)` qui
   renvoie la liste des distances séparant le point `(x ; y)` de tous
   les points présents dans la `liste_donnees`.

~~~ {.python .amc file="Q_3.md" bareme="2"}
    >>> liste_distances(0, 0, STATS)
    [[1, 5.632237566012287, 'F'], [2, 5.90323640048406, 'A'], ..., [10, 5.7812195945146385, 'F']]
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
>>> resultat = liste_distances(0, 0, STATS)
>>> solution = [[1, 5.632237566012287, 'F'], [2, 5.90323640048406, 'A'], [3, 5.305544269912372, 'F'], [4, 6.407948189553346, 'A'], [5, 6.935142392193545, 'F'], [6, 6.172195719515058, 'A'], [7, 5.099068542390855, 'F'], [8, 6.028805851907989, 'A'], [9, 5.792935352651538, 'A'], [10, 5.7812195945146385, 'F']]
>>> for i in range(len(resultat)):
...     assert resultat[i][0] == solution[i][0]
...     assert resultat[i][2] == solution[i][2]
...     assert isclose(resultat[i][1], solution[i][1], rel_tol=1e-10) 
~~~

~~~ {.python .hidden .test file="Q_3_bis.md" bareme="1"}
>>> resultat = liste_distances(3, 3, STATS)
>>> solution = [[1, 3.6141527361194905, 'F'], [2, 2.090023923308056, 'A'], [3, 3.368798005223822, 'F'], [4, 2.9020337696174385, 'A'], [5, 4.593059982190523, 'F'], [6, 2.5799224794555355, 'A'], [7, 3.1401433088316204, 'F'], [8, 2.2817756243767704, 'A'], [9, 1.9021303845951258, 'A'], [10, 3.65, 'F']]
>>> for i in range(len(resultat)):
...     assert resultat[i][0] == solution[i][0]
...     assert resultat[i][2] == solution[i][2]
...     assert isclose(resultat[i][1], solution[i][1], rel_tol=1e-10) 
~~~

4. Écrire une fonction `prediction(liste, k)` qui renvoie la valeur de l’étiquette la plus présente parmi les `k` premières valeurs de la `liste`. *La liste doit être triée.*

Ici l’étiquette vaut `'A'`, `'F'` ou `'?'` si on ne peut savoir.

~~~ {.python .amc .test file="Q_4.md" bareme="1"}
    >>> liste_triee = [[1, 1, 'F'], 
    ...                [2, 2,'F'], 
    ...                [3, 3, 'A'], 
    ...                [4, 4, 'A'], 
    ...                [5, 5, 'F']]
    >>> prediction(liste_triee, 1)
    'F'
    >>> prediction(liste_triee, 2) 
    'F'
    >>> prediction(liste_triee, 4)
    '?'
~~~

~~~ {.python .hidden .test .amc file="Q_4_c.md" bareme="1"}
>>> liste_triee = [[1, 1, 'F'], 
...                [2, 2,'A'], 
...                [3, 3, 'A'], 
...                [4, 4, 'A'], 
...                [5, 5, 'F']] 
>>> prediction(liste_triee, 1)
'F'
>>> prediction(liste_triee, 2)
'?'
>>> prediction(liste_triee, 3)
'A'
>>> prediction(liste_triee, 4)
'A'
~~~


5. Écrire une fonction `predire_langue(texte, k)` qui tente de prédire la langue dans laquelle est écrit le `texte` en se basant sur les `k` plus proches voisins. La fonction doit renvoyer l’une des phrases suivantes : 
- « Je pense que le texte est écrit en français.» 
- « Je pense que le texte est écrit en anglais.» 
- « Je ne sais pas dans quelle langue est écrit le texte.» 

_Petit coup de pouce_

`sorted(ld, key=lambda valeur: valeur[1])` renvoie une copie triée dans
l’ordre croissant de la liste de listes `ld` suivant les valeurs de la
deuxième colonne.

~~~ {.python .test .amc file="Q_5.md" bareme="1"}
>>> T = ("Earlier releases have been removed because"
...      "we can only support the current versions of the software. "
...      "To update old code, read the changes page. "
...      "Changes for each release can be found in revisions.txt. "
...      "If you have problems with the current release, please file a bug "
...      "so that we can fix it. Older releases can also be built "
...      "from the source. Read More about the releases and their numbering. "
...      "To use Android Mode, Processing 3 or later is required.")
>>> predire_langue(T, 1)
'Je pense que le texte est écrit en anglais.'
>>> predire_langue(T, 8)
'Je pense que le texte est écrit en anglais.'
>>> predire_langue(T, 10)
'Je ne sais pas dans quelle langue est écrit le texte.'
~~~

Peut-on considérer l'algorithme des k plus proches voisins comme fiable ?

~~~ {.python .test .amc file="Q_5_c.md" bareme="1"}
>>> T = "L'hyperbole est un truc de hippie !"
>>> predire_langue(T, 9)
'Je pense que le texte est écrit en anglais.'
>>> predire_langue(T, 1)
'Je pense que le texte est écrit en anglais.'
~~~



