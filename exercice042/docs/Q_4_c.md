    >>> from math import sqrt, isclose
    >>> from exercice import *

    >>> STATS = [[1, 5.61, 0.5, "F"],
    ...         [2, 2.99, 5.09, "A"],
    ...         [3, 5.28, 0.52, "F"],
    ...         [4, 2.57, 5.87, "A"],
    ...         [5, 6.91, 0.59, "F"],
    ...         [6, 2.68, 5.56, "A"],
    ...         [7, 5.06, 0.63, "F"],
    ...         [8, 2.91, 5.28, "A"],
    ...         [9, 3.09, 4.9, "A"],
    ...         [10, 5.75, 0.6, "F"]]

    >>> liste_triee = [[1, 1, 'F'], 
    ...                [2, 2,'A'], 
    ...                [3, 3, 'A'], 
    ...                [4, 4, 'A'], 
    ...                [5, 5, 'F']] 
    >>> prediction(liste_triee, 1)
    'F'
    >>> prediction(liste_triee, 2)
    '?'
    >>> prediction(liste_triee, 3)
    'A'
    >>> prediction(liste_triee, 4)
    'A'

 
