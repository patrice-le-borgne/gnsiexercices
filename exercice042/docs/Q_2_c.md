    >>> from math import sqrt, isclose
    >>> from exercice import *

    >>> STATS = [[1, 5.61, 0.5, "F"],
    ...         [2, 2.99, 5.09, "A"],
    ...         [3, 5.28, 0.52, "F"],
    ...         [4, 2.57, 5.87, "A"],
    ...         [5, 6.91, 0.59, "F"],
    ...         [6, 2.68, 5.56, "A"],
    ...         [7, 5.06, 0.63, "F"],
    ...         [8, 2.91, 5.28, "A"],
    ...         [9, 3.09, 4.9, "A"],
    ...         [10, 5.75, 0.6, "F"]]

    >>> t = "La NSI, ça pique !"
    >>> isclose(frequence_lettre('i', t), 11.111111111111, rel_tol=1e-10)
    True
    >>> frequence_lettre('z', t)
    0.0

 
