################
###   Pile   ###
################

def creer_pile_vide():
    """renvoie une pile vide"""
    return []


def est_vide(p):
    """renvoie True si p est vide, False sinon"""
    return p == []

def empiler(p, element):
    """ajoute element au sommet de p"""
    p.append(element)
    
def depiler(p):
    """retire l'élément au sommet de p et le renvoie"""
    return p.pop()

def sommet(p):
    """renvoie l'élément au sommet de p sans le retirer de p"""
    return p[-1]

def taille(p):
    """renvoie le nombre d'éléments de p"""
    return len(p)

####################
###   Exercice   ###
####################

resultats_1_a = [[2, 4, 7, 8, 9, 4], [2, 4, 8, 4], [2, 4, 4], [2, 4]]

resultat1_b = 'B'

taille_minimale = 3

def reduire_triplet_au_sommet(p) :
    a = depiler(p)
    b = depiler(p)
    c = sommet(p)
    if a%2 != c%2:
        empiler(p, b)
    empiler(p, a)

def parcourir_pile_en_reduisant(p):
    q = creer_pile_vide()
    while taille(p) >= 3:
        reduire_triplet_au_sommet(p)
        e = depiler(p)
        empiler(q, e)
    while not est_vide(q):
        e = depiler(q)
        empiler(p, e)
    return p

def jouer(p):
    q = parcourir_pile_en_reduisant(p[:])
    if taille(q) == taille(p):
         return q
    else:
        return jouer(q)
