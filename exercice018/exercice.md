~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Écrire une fonction `rechercheMinMax` qui prend en paramètre un tableau de nombres
non triés `tab`, et qui renvoie la plus petite et la plus grande valeur du tableau sous la
forme d’un dictionnaire à deux clés `min` et `max`. Les tableaux seront représentés sous
forme de liste Python.

Exemples :

~~~ {.python .amc .test file="Q_1.md" bareme="4"}
    >>> tableau = [0, 1, 4, 2, -2, 9, 3, 1, 7, 1]
    >>> resultat = rechercheMinMax(tableau)
    >>> resultat
    {'min': -2, 'max': 9}
    >>> tableau = []
    >>> resultat = rechercheMinMax(tableau)
    >>> resultat
    {'min': None, 'max': None}
~~~
