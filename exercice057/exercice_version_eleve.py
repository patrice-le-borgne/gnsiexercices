"""
Le cryptage par transposition
"""

##########################
## Préparer votre texte ##
##########################

def preparer(texte,cle):
    """
    Fonction que renvoie le texte sous forme de liste de phrases composées de
    'cle' caractères.
    """
    pass

#####################
## Codage du texte ##
#####################

def coder(texte,cle):
    """
    Renvoie le texte crypté.
    * Entrée :
    texte : string, le texte à coder
    cle : int, le nombre de colonnes du texte préparé au format rectangulaire
    * Sortie :
    String
    """
    pass

########################
## la clé de décodage ##
########################

def cle_de_decryptage(texte,cle):
    pass

###################
## Votre mission ##
###################

def liste_paragraphes(fichier):
        lp = []
        f = open(fichier,"r")
        for ligne in f:
            if len(ligne) != 1:
                lp.append(ligne[:-1])
        f.close()
        return lp

def paragraphe_a_dechiffrer(nom):
    n = 0
    for i in nom:
        n += ord(i)
    return n % 65

PRENOM = "" # rentrez votre prenom
np = paragraphe_a_dechiffrer(PRENOM)
lp = liste_paragraphes("texte_code.txt")
paragraphe_a_dechiffrer = lp[np] # vérifier cette valeur pour savoir quel paragraphe déchiffrer
cle_de_dechiffrage = 00 # précisez la clé que vous avez utilisée pour déchiffrer votre paragraphe


if __name__ == '__main__':
    """
    Dans la partie précédente, vous ne complétez que vos réponses.
    Tous vos tests et le code nécessaire pour compléter votre mission doit
    se faire ici dans le if_main !
    """

    texte_a_coder = "À dix-huit ans (1641), Pascal commence le développement de la pascaline, machine à calculer capable d’effectuer des additions et des soustractions20, afin d’aider son père dans son travail. Il en écrit le mode d’emploi : Avis nécessaire à ceux qui auront la curiosité de voir ladite machine et s’en servir. Plusieurs exemplaires sont conservés, en France, au Musée des arts et métiers à Paris et au musée de Clermont-Ferrand."

    solution = ['À dix-huit ans (1',
                '641), Pascal comm',
                'ence le développe',
                'ment de la pascal',
                'ine, machine à ca',
                'lculer capable d’',
                'effectuer des add',
                'itions et des sou',
                'stractions20, afi',
                'n d’aider son pèr',
                'e dans son travai',
                'l. Il en écrit le',
                ' mode d’emploi : ',
                'Avis nécessaire à',
                ' ceux qui auront ',
                'la curiosité de v',
                'oir ladite machin',
                'e et s’en servir.',
                ' Plusieurs exempl',
                'aires sont conser',
                'vés, en France, a',
                'u Musée des arts ',
                'et métiers à Pari',
                's et au musée de ',
                'Clermont-Ferrand.']
    
    assert preparer(texte_a_coder,17) == solution

    texte_code_1 = "À6emileisnel A loe avuesC 4nencftt  .mvcai Pié t ld1cneufirdd oie relrsM eei)et,leoa’aIdsuc tue,umtrx,   ecncanle xul ss sé m- ldmrtstis  n rasi eétaohPeea u id edéqid’esneiunua  cceeoesn’cuoieuo  e tisdlhartnro eeistnrnFdrm-tcéaip  s néms ie stresuF av nadd2s cpsat s  as sealepebee0otrlauémeecn àérn la lss,nrioir arxoca erscosàe    atirodcvenerP a opc  asapv  enehims,tadn(mpacddofèal: t irpe sred1mela’duirie à vn.lra i ."

    assert coder(texte_a_coder,17) == texte_code_1

    assert cle_de_decryptage(texte_a_coder,17) == 25
    tc = "À6emileisnel A loe avuesC 4nencftt  .mvcai Pié t ld1cneufirdd oie relrsM eei)et,leoa’aIdsuc tue,umtrx,   ecncanle xul ss sé m- ldmrtstis  n rasi eétaohPeea u id edéqid’esneiunua  cceeoesn’cuoieuo  e tisdlhartnro eeistnrnFdrm-tcéaip  s néms ie stresuF av nadd2s cpsat s  as sealepebee0otrlauémeecn àérn la lss,nrioir arxoca erscosàe    atirodcvenerP a opc  asapv  enehims,tadn(mpacddofèal: t irpe sred1mela’duirie à vn.lra i ."
     texte_decode = coder(tc,25)
    assert texte_decode == texte_a_coder
