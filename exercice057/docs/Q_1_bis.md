    >>> from exercice import *
    >>> lc = [23, 17, 12, 22, 29, 30, 25, 48, 17, 19, 15, 9, 8, 12, 44, 34, 56, 50, 27, 20, 24, 27, 17, 55, 28, 26, 13, 45, 22, 34, 68, 140, 43, 98, 17, 13, 129, 20, 18, 33, 29, 32, 127, 14, 22, 67, 70, 48, 166, 48, 42, 41, 28, 57, 202, 45, 54, 31, 144, 84, 58, 18, 50, 43, 28]

    >>> texte_a_coder = "À dix-huit ans (1641), Pascal commence le développement de la pascaline, machine à calculer capable d’effectuer des additions et des soustractions20, afin d’aider son père dans son travail. Il en écrit le mode d’emploi : Avis nécessaire à ceux qui auront la curiosité de voir ladite machine et s’en servir. Plusieurs exemplaires sont conservés, en France, au Musée des arts et métiers à Paris et au musée de Clermont-Ferrand."

    >>> solution_1_bis = ['À dix-huit ans (1641),', 
    ...                   ' Pascal commence le dé', 
    ...                   'veloppement de la pasc', 
    ...                   'aline, machine à calcu', 
    ...                   'ler capable d’effectue', 
    ...                   'r des additions et des', 
    ...                   ' soustractions20, afin', 
    ...                   ' d’aider son père dans', 
    ...                   ' son travail. Il en éc', 
    ...                   'rit le mode d’emploi :', 
    ...                   ' Avis nécessaire à ceu', 
    ...                   'x qui auront la curios', 
    ...                   'ité de voir ladite mac', 
    ...                   'hine et s’en servir. P', 
    ...                   'lusieurs exemplaires s', 
    ...                   'ont conservés, en Fran', 
    ...                   'ce, au Musée des arts ', 
    ...                   'et métiers à Paris et ', 
    ...                   'au musée de Clermont-F', 
    ...                   'errand.               ']
    >>> assert preparer(texte_a_coder,22) == solution_1_bis
    >>> solution_1_ter = ['À dix-huit', 
    ...                   ' ans (1641', 
    ...                   '), Pascal ', 
    ...                   'commence l', 
    ...                   'e développ', 
    ...                   'ement de l', 
    ...                   'a pascalin', 
    ...                   'e, machine', 
    ...                   ' à calcule', 
    ...                   'r capable ', 
    ...                   'd’effectue', 
    ...                   'r des addi', 
    ...                   'tions et d', 
    ...                   'es soustra', 
    ...                   'ctions20, ', 
    ...                   'afin d’aid', 
    ...                   'er son pèr', 
    ...                   'e dans son', 
    ...                   ' travail. ', 
    ...                   'Il en écri', 
    ...                   't le mode ', 
    ...                   'd’emploi :', 
    ...                   ' Avis néce', 
    ...                   'ssaire à c', 
    ...                   'eux qui au', 
    ...                   'ront la cu', 
    ...                   'riosité de', 
    ...                   ' voir ladi', 
    ...                   'te machine', 
    ...                   ' et s’en s', 
    ...                   'ervir. Plu', 
    ...                   'sieurs exe', 
    ...                   'mplaires s', 
    ...                   'ont conser', 
    ...                   'vés, en Fr', 
    ...                   'ance, au M', 
    ...                   'usée des a', 
    ...                   'rts et mét', 
    ...                   'iers à Par', 
    ...                   'is et au m', 
    ...                   'usée de Cl', 
    ...                   'ermont-Fer', 
    ...                   'rand.     ']
    >>> assert preparer(texte_a_coder,10) == solution_1_ter

 
