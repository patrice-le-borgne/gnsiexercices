    >>> from exercice import *
    >>> lc = [23, 17, 12, 22, 29, 30, 25, 48, 17, 19, 15, 9, 8, 12, 44, 34, 56, 50, 27, 20, 24, 27, 17, 55, 28, 26, 13, 45, 22, 34, 68, 140, 43, 98, 17, 13, 129, 20, 18, 33, 29, 32, 127, 14, 22, 67, 70, 48, 166, 48, 42, 41, 28, 57, 202, 45, 54, 31, 144, 84, 58, 18, 50, 43, 28]

    >>> texte_a_coder = "À dix-huit ans (1641), Pascal commence le développement de la pascaline, machine à calculer capable d’effectuer des additions et des soustractions20, afin d’aider son père dans son travail. Il en écrit le mode d’emploi : Avis nécessaire à ceux qui auront la curiosité de voir ladite machine et s’en servir. Plusieurs exemplaires sont conservés, en France, au Musée des arts et métiers à Paris et au musée de Clermont-Ferrand."

    >>> assert lc[np] == cle_de_dechiffrage

 
