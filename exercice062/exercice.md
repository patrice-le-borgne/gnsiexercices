~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes de listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
==========

Écrire une fonction `a_doublon` qui prend en paramètre une liste **triée** de nombres et
renvoie `True` si la liste contient au moins deux nombres identiques, `False` sinon.

Par exemple :

~~~ {.python .test .amc file="Q_1.md" bareme="2"}
    >>> a_doublon([])
    False
    >>> a_doublon([1])
    False
    >>> a_doublon([1, 2, 4, 6, 6])
    True
    >>> a_doublon([2, 5, 7, 7, 7, 9])
    True
    >>> a_doublon([0, 2, 3])
    False
~~~


