\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Écrire une fonction \texttt{enumere} qui prend en paramètre un tableau
\texttt{tab} (type \texttt{list}) et renvoie un dictionnaire \texttt{d}
dont les clés sont les éléments de \texttt{tab} avec pour valeur
associée la liste des indices de l'élément dans le tableau \texttt{tab}.

Exemple :

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ enumere([])}
\NormalTok{\{\}}
\OperatorTok{>>>}\NormalTok{ enumere([}\DecValTok{1}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{3}\NormalTok{])}
\NormalTok{\{}\DecValTok{1}\NormalTok{: [}\DecValTok{0}\NormalTok{], }\DecValTok{2}\NormalTok{: [}\DecValTok{1}\NormalTok{], }\DecValTok{3}\NormalTok{: [}\DecValTok{2}\NormalTok{]\}}
\OperatorTok{>>>}\NormalTok{ enumere([}\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{1}\NormalTok{])}
\NormalTok{\{}\DecValTok{1}\NormalTok{: [}\DecValTok{0}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{5}\NormalTok{], }\DecValTok{2}\NormalTok{: [}\DecValTok{2}\NormalTok{, }\DecValTok{4}\NormalTok{], }\DecValTok{3}\NormalTok{: [}\DecValTok{3}\NormalTok{]\}}
\end{Highlighting}
\end{Shaded}

