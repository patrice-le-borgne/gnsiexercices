~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : dictionnaires
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Écrire une fonction `enumere` qui prend en paramètre un tableau `tab` (type `list`) et renvoie
un dictionnaire `d` dont les clés sont les éléments de `tab` avec pour valeur associée la liste
des indices de l’élément dans le tableau `tab`.

Exemple :

~~~ {.python .amc file="Q_1.md" bareme="2"}
>>> enumere([])
{}
>>> enumere([1, 2, 3])
{1: [0], 2: [1], 3: [2]}
>>> enumere([1, 1, 2, 3, 2, 1])
{1: [0, 1, 5], 2: [2, 4], 3: [3]}
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="2"}
>>> d =  enumere([1, 1, 2, 3, 2, 1, 1, 9])
>>> d[1]
[0, 1, 5, 6]
>>> d[2]
[2, 4]
>>> d[3]
[3]
>>> d[9]
[7]
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> d =  enumere([])
>>> d
{}
~~~

