    >>> from exercice import *
    >>> import sqlite3 
    >>> conn = sqlite3.connect('cooperative.sqlite')


    >>> def requete_correcte(solution,proposition):
    ...     # Créer un cursor 
    ...     cur = conn.cursor() 
    ...     comparaison = "WITH TA AS ({}), TB AS ({}) SELECT * FROM TA EXCEPT SELECT * FROM TB UNION ALL SELECT * FROM TB EXCEPT SELECT * FROM TA;".format(solution, proposition)
    ...     # Exécution de la requete
    ...     try:
    ...         resultat = cur.execute(comparaison) 
    ...         row = cur.fetchone()
    ...         if row is None:
    ...             rc = True
    ...         else:
    ...             rc = False
    ...     except Exception:
    ...         print("Le nombre de colonnes est incorrect !")
    ...         rc = False
    ...     # Envoyer la requete 
    ...     conn.commit()
    ...     # Fermer la connexion 
    ...     conn.close
    ...     return rc

    >>> solution_Q1 = "SELECT isbn, editeur, titre FROM collections WHERE refniveau='Terminale'"
    >>> solution_Q2 = "SELECT isbn, editeur, titre FROM collections WHERE refniveau='Terminale' and etat='obsolète'"
    >>> solution_Q3 = "SELECT DISTINCT editeur FROM collections"
    >>> solution_Q4 = "SELECT collections.isbn, collections.titre FROM collections JOIN matieres ON collections.refmatiere=matieres.refmatiere WHERE matieres.nom='Espagnol'"
    >>> solution_Q5 = "SELECT DISTINCT collections.editeur FROM collections JOIN matieres ON collections.refmatiere = matieres.refmatiere WHERE matieres.nom='Histoire'"
    >>> solution_Q6 = "SELECT DISTINCT matieres.nom FROM matieres JOIN collections on collections.refmatiere = matieres.refmatiere WHERE collections.editeur='Nathan'"
    >>> solution_Q7 = "SELECT editeur, COUNT() AS effectif FROM collections GROUP BY editeur ORDER BY effectif DESC"
    >>> solution_Q8 = "SELECT COUNT() AS effectif FROM collections WHERE editeur='Hatier'"
    >>> solution_Q9 = "SELECT isbn, editeur, titre FROM collections WHERE substr(dateachat,1,4)='2005'"
    >>> solution_Q10 = "SELECT nom FROM matieres WHERE nom LIKE '%spécialité%'"

    >>> assert requete_correcte(solution_Q6, prop_Q6)

 
