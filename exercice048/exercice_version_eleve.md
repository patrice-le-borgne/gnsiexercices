Exercice
========

Une coopérative de livres distribue des manuels scolaires aux élèves
d'un lycée. Pour gérer ses stocks, elle utilise une base de données.
Nous utiliserons un extrait de cette base composée uniquement de 2
tables :

-   `collections` avec les attributs :

    -   `isbn` : un nombre (`INTEGER`) unique qui servira de clé
        primaire ;
    -   `editeur` : le nom de l'éditeur du manuel (`TEXT`);
    -   `titre` : le titre de l'ouvrage (`TEXT`, pas toujours original);
    -   `dateachat` : la date d'achat du manuel au format `TEXT` ;
    -   `refmatiere` : un entier (`INTEGER`) qui est une clé étrangère
        vers la table `matieres`;
    -   `refniveau` : le niveau dans lequel est utilisé le livre au
        format `TEXT`;
    -   `etat` : `TEXT` qui décrit l'état d'une collection. Elle peut
        être *active* si elle est utilisée ou *obsolète* si elle ne
        l'est plus.

-   `matieres` avec les attributs :

    -   `refmatiere` : un nombre (`INTEGER`), clé primaire de la table ;
    -   `nom` : le nom de la dicipline qui utilise le manuel (`TEXT`).

Tout est contenu dans le fichier `cooperative.sqlite` que vous pourrez
parcourir avec le logiciel `DB Browser for Sqlite`. Vous pourrez y
écrire vos requètes pour répondre aux questions posées.

Une fois que vous pensez que votre requète est correcte, vous la
collerez dans la variable texte qui vous est proposée dans la cellule.
**Attention**,

-   les attributs doivent impérativement être **dans l'ordre de la
    question** ;
-   n'utiliser que **des simples quotes '** et non des \" ;
-   surtout vous ne mettez **pas de point-virgule**.
-   **Ne changez pas le nom de la variable !**

**Question 1** : quels sont l'isbn, l'éditeur et le titre des
collections du niveau Terminale ?

Placer votre requète dans la variable `prop_Q1`.

**Question 2** : quels sont l'isbn, l'éditeur et le titre des
collections obsolètes du niveau Terminale ?

Placer votre requète dans la variable `prop_Q2`.

**Question 3** : quels sont les éditeurs ayant fourni des manuels au
lycée ?

Placer votre requète dans la variable `prop_Q3`.

**Question 4** : quels sont les isbn et les titres des collections
d'espagnol ?

Placer votre requète dans la variable `prop_Q4`.

**Question 5** : quels sont les éditeurs ayant fourni les collections
d'histoire ?

Placer votre requète dans la variable `prop_Q5`.

**Question 6** : dans quelles matières utilise t-on les livres de
l'éditeur *Nathan* ?

Placer votre requète dans la variable `prop_Q6`.

**Question 7** : combien chaque éditeur fournit-il de collections ?

Le résultat devra renvoyer l'éditeur et la quantité de collections
fournies. Cette dernière colonne devra s'appelée *effectif*. Les
résultats seront classés dans l'ordre décroissant, c'est-à-dire que
l'éditeur qui fournit le plus de collections doit apparaître en premier.
Placer votre requète dans la variable `prop_Q7`.

**Question 8** : combien l'éditeur *Hatier* fournit-il de collections ?
Le résultat sera donné dans une colonne qui s'appelle `effectif`. Placer
votre requète dans la variable `prop_Q8`.

**Question 9** : Quels sont les isbn, les éditeurs et les titres des
collections achetées en *2005* ?

Ici la date est fournie sous forme de texte. Pour extraire l'année, je
vous invite à utiliser la fonction *substr*.

Cette fonction peut s'utiliser dans une requête `SQL` en utilisant une
syntaxe comme celle-ci:

``` {.python}
SELECT SUBSTR(nom_colonne, 3, 10) FROM tableau
```

Dans cet exemple, le contenu de la colonne `nom_colonne` sera tronqué à
partir du 4ème caractère sur 10 caractères.

Placer votre requète dans la variable `prop_Q9`.

**Question 10** : quelles sont les matières correspondant à un
enseignement de *spécialité* ?

Vous pouvez utiliser le mot clé `LIKE`.

-   `LIKE ‘%a’` : le caractère "%" est un caractère joker qui remplace
    tous les autres caractères. Ainsi, ce modèle permet de rechercher
    toutes les chaines de caractère qui se termine par un "a".
-   `LIKE ‘a%’` : ce modèle permet de rechercher toutes les lignes de
    "colonne" qui commence par un "a".
-   `LIKE ‘%a%’` : ce modèle est utilisé pour rechercher tous les
    enregistrement qui utilisent le caractère "a".
-   `LIKE ‘pa%on’` : ce modèle permet de rechercher les chaines qui
    commence par "pa" et qui se terminent par "on", comme "pantalon" ou
    "pardon".

Placer votre requète dans la variable `prop_Q10`.
