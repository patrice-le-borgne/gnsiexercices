~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : structures linéaires
thème : dichotomie, récursivité
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
==========


Soit `T` un tableau non vide d'entiers triés dans l'ordre croissant et `n` un entier.
La fonction `chercher`, donnée ci-dessous, doit renvoyer un indice où la valeur `n`
apparaît éventuellement dans `T`, et `None` sinon. 

Les paramètres de la fonction sont :

- `T`, le tableau dans lequel s'effectue la recherche ;
- `n`, l'entier à chercher dans le tableau ;
- `i`, l'indice de début de la partie du tableau où s'effectue la recherche ;
- `j`, l'indice de fin de la partie du tableau où s'effectue la recherche.

La fonction `chercher` est une fonction récursive basée sur le principe « diviser pour régner ».

Le code de la fonction commence par vérifier que les paramètres `i` et
`j` sont corrects. Si ce n'est pas le cas, cela déclenche une
`AssertionError`.

Recopier et compléter le code de la fonction `chercher` proposée ci-dessous :

~~~ {.python .amc file="Q_1.md" bareme="4"}
def chercher(T, n, i, j):
    ???
    if i > j :
        return ???
    m = ???
    if T[m] < ??? :
        return chercher(T, n, ??? , ???)
    elif ??? :
        return chercher(T, n, ??? , ??? )
    else :
        return ???
~~~


~~~ {.python .test file="Q_1.md" bareme="1"}
>>> chercher([1, 5, 6, 6, 9, 12], 7, 0, 10)
Traceback (most recent call last):
...
AssertionError
~~~

~~~ {.python .test file="Q_2.md" bareme="1"}
>>> chercher([1, 5, 6, 6, 9, 12], 7, 0, 5)
>>> chercher([1, 5, 6, 6, 9, 12], 9, 0, 5)
4
>>> chercher([1, 5, 6, 6, 9, 12], 6, 0, 5)
2
~~~


~~~ {.python .hidden .test file="Q_3.md" bareme="2"}
>>> chercher([1, 5, 6, 6, 9, 12], 7, 5, 3)
>>> chercher([1, 5, 6, 9, 9, 9, 12], 1, 0, 5)
0
>>> chercher([1, 5, 6, 9, 9, 9, 12], 12, 0, 6)
6
~~~




