def chercher(T, n, i, j):
    assert i >= 0 and j < len(T)
    if i > j:
        return None
    m = (i + j) // 2
    if T[m] < n:
        return chercher(T, n, m + 1, j)
    elif T[m] > n:
        return chercher(T, n, i, m - 1)
    else:
        return m
