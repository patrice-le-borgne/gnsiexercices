~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> from unittest.mock import patch
~~~

Exercice
========

Écrire une fonction `maxi` qui prend en paramètre une liste `tab` de
nombres entiers et renvoie un couple donnant le plus grand élément de
cette liste, ainsi que l’indice de la première apparition de ce
maximum dans la liste.

Exemple :

~~~ {.python .amc .test file="Q_1.md" bareme="1"}
    >>> maxi([1, 5, 6, 9, 1, 2, 3, 7, 9, 8])
    (9, 3)
~~~

~~~ {.python .amc .hidden .test file="Q_2.md" bareme="1"}
	>>> with patch('builtins.max', return_value = 666):
	...     maxi([1, 5, 1, 1, 2, 3, 7, 9, 8, 9])
    (9, 7)
~~~

~~~ {.python .amc .hidden .test file="Q_3.md" bareme="1"}
	>>> with patch('builtins.max', return_value = 666):
    ...     maxi([-1, -5, -1, -1, -2, -3, -7, -9, -8, -9])
    (-1, 0)
~~~

~~~ {.python .amc .hidden .test file="Q_4.md" bareme="1"}
	>>> with patch('builtins.max', return_value = 666):
    ...     maxi([-10, -5, -11, -15, -21, -36, -7, -9, -8, -9])
    (-5, 1)
~~~
