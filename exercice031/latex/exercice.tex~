\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

On dispose de chaînes de caractères contenant uniquement des parenthèses
ouvrantes et fermantes.

Un parenthésage est correct si :

\begin{itemize}
\tightlist
\item
  le nombre de parenthèses ouvrantes de la chaîne est égal au nombre de
  parenthèses fermantes.
\item
  en parcourant la chaîne de gauche à droite, le nombre de parenthèses
  déjà ouvertes doit être, à tout moment, supérieur ou égal au nombre de
  parenthèses déjà fermées.
\end{itemize}

Ainsi, \texttt{((()())(()))} est un parenthésage correct.

Les parenthésages \texttt{())(()} et \texttt{(())(()} sont, eux,
incorrects.

On dispose du code de la classe \texttt{Pile} suivant :

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{class}\NormalTok{ Pile:}
    \CommentTok{""" Classe définissant une pile """}
    \KeywordTok{def} \FunctionTok{__init__}\NormalTok{(}\VariableTok{self}\NormalTok{, valeurs}\OperatorTok{=}\NormalTok{[]):}
        \VariableTok{self}\NormalTok{.valeurs }\OperatorTok{=}\NormalTok{ valeurs}

    \KeywordTok{def}\NormalTok{ est_vide(}\VariableTok{self}\NormalTok{):}
        \CommentTok{"""Renvoie True si la pile est vide, False sinon"""}
        \ControlFlowTok{return} \VariableTok{self}\NormalTok{.valeurs }\OperatorTok{==}\NormalTok{ []}

    \KeywordTok{def}\NormalTok{ empiler(}\VariableTok{self}\NormalTok{, c):}
        \CommentTok{"""Place l’élément c au sommet de la pile"""}
        \VariableTok{self}\NormalTok{.valeurs.append(c)}

    \KeywordTok{def}\NormalTok{ depiler(}\VariableTok{self}\NormalTok{):}
        \CommentTok{"""Supprime l’élément placé au sommet de la pile, à condition qu’elle soit non vide"""}
        \ControlFlowTok{if} \VariableTok{self}\NormalTok{.est_vide() }\OperatorTok{==} \VariableTok{False}\NormalTok{:}
            \VariableTok{self}\NormalTok{.valeurs.pop()}
\end{Highlighting}
\end{Shaded}

On souhaite programmer une fonction \texttt{parenthesage} qui prend en
paramètre une chaîne \texttt{ch} de parenthèses et renvoie \texttt{True}
si la chaîne est bien parenthésée et \texttt{False} sinon. Cette
fonction utilise une pile et suit le principe suivant : en parcourant la
chaîne de gauche à droite, si on trouve une parenthèse ouvrante, on
l'empile au sommet de la pile et si on trouve une parenthèse fermante,
on dépile (si possible !) la parenthèse ouvrante stockée au sommet de la
pile.

La chaîne est alors bien parenthésée si, à la fin du parcours, la pile
est vide.

Elle est, par contre, mal parenthésée :

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{-}\NormalTok{ si dans le parcours, on trouve une parenthèse fermante, alors que la pile est vide }\OperatorTok{;}
\OperatorTok{-}\NormalTok{ ou si, à la fin du parcours, la pile n’est pas vide.}
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
    \KeywordTok{def}\NormalTok{ parenthesage(ch):}
        \CommentTok{"""Renvoie True si la chaîne ch est bien parenthésée et False sinon"""}
\NormalTok{        p }\OperatorTok{=}\NormalTok{ Pile()}
        \ControlFlowTok{for}\NormalTok{ c }\KeywordTok{in}\NormalTok{ ch:}
            \ControlFlowTok{if}\NormalTok{ c }\OperatorTok{==}\NormalTok{ ...:}
\NormalTok{                p.empiler(c)}
            \ControlFlowTok{elif}\NormalTok{ c }\OperatorTok{==}\NormalTok{ ...:}
                \ControlFlowTok{if}\NormalTok{ p.est_vide():}
                    \ControlFlowTok{return}\NormalTok{ ...}
                \ControlFlowTok{else}\NormalTok{:}
\NormalTok{                    ...}
        \ControlFlowTok{return}\NormalTok{ p.est_vide()}
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
    \ControlFlowTok{assert}\NormalTok{ parenthesage(}\StringTok{"((()())(()))"}\NormalTok{) }\OperatorTok{==} \VariableTok{True}
    \ControlFlowTok{assert}\NormalTok{ parenthesage(}\StringTok{"())(()"}\NormalTok{) }\OperatorTok{==} \VariableTok{False}
    \ControlFlowTok{assert}\NormalTok{ parenthesage(}\StringTok{"(())(()"}\NormalTok{) }\OperatorTok{==} \VariableTok{False}
\end{Highlighting}
\end{Shaded}

Compléter le code de la fonction parenthesage.
