~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : poo
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On veut écrire une classe pour gérer une file à l’aide d’une liste chaînée. On dispose d’une classe `Maillon` permettant la création d’un maillon de la chaîne, celui-ci étant constitué d’une valeur et d’une référence au maillon suivant de la chaîne :

~~~ {.python}
class Maillon :
    def __init__(self, v, s) :
        self.valeur = v
        self.suivant = s
~~~

Compléter la classe `File` suivante où l’attribut `dernier_file` contient le maillon correspondant à l’élément arrivé en dernier dans la file :

~~~ {.python}

class File :
    def __init__(self) :
        self.dernier_file = None

    def enfile(self, element) :
        nouveau_maillon = Maillon(... , self.dernier_file)
        self.dernier_file = ...

    def est_vide(self) :
        return self.dernier_file == None

    def affiche(self) :
        maillon = self.dernier_file
        while maillon != ... :
            print(maillon.valeur)
            maillon = ...

    def defile(self) :
        if not self.est_vide() :
            if self.dernier_file.suivant == None :
                resultat = self.dernier_file.valeur
                self.dernier_file = None
                return resultat
            maillon = ...
            while maillon.suivant.suivant != None :
                maillon = maillon.suivant
            resultat = ...
            maillon.suivant = None
            return resultat
        return None

~~~

On pourra tester le fonctionnement de la classe en utilisant les commandes suivantes dans la console Python :

~~~ {.python}

    >>> F = File()
    >>> F.est_vide()
    True
    >>> F.enfile(2)
    >>> F.affiche()
    2
    >>> F.est_vide()
    False
    >>> F.enfile(5)
    >>> F.enfile(7)
    >>> F.affiche()
    7
    5
    2
    >>> F.defile()
    2
    >>> F.defile()
    5
    >>> F.affiche()
    7
	>>> F.defile()
	7
	>>> F.affiche()
	
~~~ 


~~~ {.python .hidden .test file="test1.md" bareme="1"}
    >>> F = File()
    >>> F.est_vide()
    True
~~~

~~~ {.python .hidden .test file="test2.md" bareme="1"}
    >>> F = File()
    >>> F.enfile(2)
    >>> F.affiche()
    2
    >>> F.est_vide()
    False
    >>> F.enfile(5)
    >>> F.enfile(7)
    >>> F.affiche()
    7
    5
    2
~~~

~~~ {.python .hidden .test file="test3.md" bareme="2"}
    >>> F = File()
	>>> F.enfile(2)
	>>> F.enfile(5)
	>>> F.enfile(7)
	>>> F.defile()
    2
    >>> F.defile()
    5
    >>> F.affiche()
    7
	>>> F.defile()
	7
	>>> F.affiche()
	
~~~


~~~ {.python .hidden .amc file="enfile.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .amc file="affiche.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .amc file="defile.md" bareme="2"}
>>> pass
~~~
