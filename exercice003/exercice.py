class Maillon :
    def __init__(self, v, s) :
        self.valeur = v
        self.suivant = s

class File :
    def __init__(self) :
        self.dernier_file = None

    def enfile(self, element) :
        ## on créé un nouveau maillon contenant la valeur,
        ## notée "element", à ajouter à la file
        nouveau_maillon = Maillon(element , self.dernier_file)
        ## ce maillon devient le dernier maillon de la file                                                
        self.dernier_file = nouveau_maillon 

    def est_vide(self) :
        return self.dernier_file == None

    def affiche(self) :
        maillon = self.dernier_file
        ## tant que l'on n'a pas déjà affiché l'élément le plus ancien dans la file
        while maillon != None :
            ## on affiche la valeur du maillon atteint
            print(maillon.valeur)
            ## on passe au maillon suivant (plus ancien dans la file)
            maillon = maillon.suivant 

    def defile(self) :
        ## on doit remonter la file depuis le dernier élément ajouté
        ## jusqu'au plus ancien ajouté
        if not self.est_vide() : ## sauf si la file est vide
             ## si la file ne contient qu'un élément
            if self.dernier_file.suivant == None :
                ## on note sauvegarde sa valeur sous le nom "resultat"
                resultat = self.dernier_file.valeur 
                self.dernier_file = None  ## on supprime l'unique maillon de la file
                return resultat
            maillon = self.dernier_file ## si la file contient plus d'un élément
            ## on teste si ce maillon est le second plus ancien
            while maillon.suivant.suivant != None :
                ## tant que le maillon atteint n'est pas le second plus ancien,
                ## on passe au suivant
                maillon = maillon.suivant
            ## on sauvegarde la valeur du plus ancien
            resultat = maillon.suivant.valeur
            ## on supprime la référence au maillon le plus ancien en la remplaçant 
            ## par None au niveau du second maillon le plus ancien
            maillon.suivant = None 

            ## on renvoie la valeur du maillon qui était le plus ancien dans la file
            return resultat
        ## si la file est vide, la fonction renvoie None
        return None 
