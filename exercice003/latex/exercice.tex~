\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

On veut écrire une classe pour gérer une file à l'aide d'une liste
chaînée. On dispose d'une classe \texttt{Maillon} permettant la création
d'un maillon de la chaîne, celui-ci étant constitué d'une valeur et
d'une référence au maillon suivant de la chaîne :

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{class}\NormalTok{ Maillon :}
    \KeywordTok{def} \FunctionTok{__init__}\NormalTok{(}\VariableTok{self}\NormalTok{, v, s) :}
        \VariableTok{self}\NormalTok{.valeur }\OperatorTok{=}\NormalTok{ v}
        \VariableTok{self}\NormalTok{.suivant }\OperatorTok{=}\NormalTok{ s}
\end{Highlighting}
\end{Shaded}

Compléter la classe \texttt{File} suivante où l'attribut
\texttt{dernier\_file} contient le maillon correspondant à l'élément
arrivé en dernier dans la file :

\begin{Shaded}
\begin{Highlighting}[]

\KeywordTok{class}\NormalTok{ File :}
    \KeywordTok{def} \FunctionTok{__init__}\NormalTok{(}\VariableTok{self}\NormalTok{) :}
        \VariableTok{self}\NormalTok{.dernier_file }\OperatorTok{=} \VariableTok{None}

    \KeywordTok{def}\NormalTok{ enfile(}\VariableTok{self}\NormalTok{, element) :}
\NormalTok{        nouveau_maillon }\OperatorTok{=}\NormalTok{ Maillon(... , }\VariableTok{self}\NormalTok{.dernier_file)}
        \VariableTok{self}\NormalTok{.dernier_file }\OperatorTok{=}\NormalTok{ ...}

    \KeywordTok{def}\NormalTok{ est_vide(}\VariableTok{self}\NormalTok{) :}
        \ControlFlowTok{return} \VariableTok{self}\NormalTok{.dernier_file }\OperatorTok{==} \VariableTok{None}

    \KeywordTok{def}\NormalTok{ affiche(}\VariableTok{self}\NormalTok{) :}
\NormalTok{        maillon }\OperatorTok{=} \VariableTok{self}\NormalTok{.dernier_file}
        \ControlFlowTok{while}\NormalTok{ maillon }\OperatorTok{!=}\NormalTok{ ... :}
            \BuiltInTok{print}\NormalTok{(maillon.valeur)}
\NormalTok{            maillon }\OperatorTok{=}\NormalTok{ ...}

    \KeywordTok{def}\NormalTok{ defile(}\VariableTok{self}\NormalTok{) :}
        \ControlFlowTok{if} \KeywordTok{not} \VariableTok{self}\NormalTok{.est_vide() :}
            \ControlFlowTok{if} \VariableTok{self}\NormalTok{.dernier_file.suivant }\OperatorTok{==} \VariableTok{None}\NormalTok{ :}
\NormalTok{                resultat }\OperatorTok{=} \VariableTok{self}\NormalTok{.dernier_file.valeur}
                \VariableTok{self}\NormalTok{.dernier_file }\OperatorTok{=} \VariableTok{None}
                \ControlFlowTok{return}\NormalTok{ resultat}
\NormalTok{            maillon }\OperatorTok{=}\NormalTok{ ...}
            \ControlFlowTok{while}\NormalTok{ maillon.suivant.suivant }\OperatorTok{!=} \VariableTok{None}\NormalTok{ :}
\NormalTok{                maillon }\OperatorTok{=}\NormalTok{ maillon.suivant}
\NormalTok{            resultat }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{            maillon.suivant }\OperatorTok{=} \VariableTok{None}
            \ControlFlowTok{return}\NormalTok{ resultat}
        \ControlFlowTok{return} \VariableTok{None}
\end{Highlighting}
\end{Shaded}

On pourra tester le fonctionnement de la classe en utilisant les
commandes suivantes dans la console Python :

\begin{Shaded}
\begin{Highlighting}[]

    \OperatorTok{>>>}\NormalTok{ F }\OperatorTok{=}\NormalTok{ File()}
    \OperatorTok{>>>}\NormalTok{ F.est_vide()}
    \VariableTok{True}
    \OperatorTok{>>>}\NormalTok{ F.enfile(}\DecValTok{2}\NormalTok{)}
    \OperatorTok{>>>}\NormalTok{ F.affiche()}
    \DecValTok{2}
    \OperatorTok{>>>}\NormalTok{ F.est_vide()}
    \VariableTok{False}
    \OperatorTok{>>>}\NormalTok{ F.enfile(}\DecValTok{5}\NormalTok{)}
    \OperatorTok{>>>}\NormalTok{ F.enfile(}\DecValTok{7}\NormalTok{)}
    \OperatorTok{>>>}\NormalTok{ F.affiche()}
    \DecValTok{7}
    \DecValTok{5}
    \DecValTok{2}
    \OperatorTok{>>>}\NormalTok{ F.defile()}
    \DecValTok{2}
    \OperatorTok{>>>}\NormalTok{ F.defile()}
    \DecValTok{5}
    \OperatorTok{>>>}\NormalTok{ F.affiche()}
    \DecValTok{7}
    \OperatorTok{>>>}\NormalTok{ F.defile()}
    \DecValTok{7}
    \OperatorTok{>>>}\NormalTok{ F.affiche()}
    
\end{Highlighting}
\end{Shaded}

