    >>> from exercice import *
    >>> R = [[1,2,3], [0,2,3], [0,1,3,4], [0,1,2,5], [2,5,6,7], [3,4,6,7], [4,5,7], [4,5,6]]

    >>> from unittest.mock import Mock, patch
    >>> m = Mock()
    >>> with patch('exercice.randrange', side_effect = [2, 1, 2, 3, 0, 3, 4, 2, 0, 3, 3, 1, 2, 4, 2, 1, 3]) as m:
    ...     nouveau_reseau =  generer_reseau_aleatoire(5, 3)
    ...     nouveau_reseau
    [[1, 2], [0, 3, 4], [0, 3], [1, 2, 4], [1, 3]]

 
