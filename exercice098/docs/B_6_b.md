    >>> from exercice import *
    >>> R = [[1,2,3], [0,2,3], [0,1,3,4], [0,1,2,5], [2,5,6,7], [3,4,6,7], [4,5,7], [4,5,6]]

    >>> from unittest.mock import Mock, patch
    >>> m = Mock()
    >>> with patch('exercice.inserer_ami', return_value=True) as m:
    ...     declarer_amis(R, 3, 4)
    ...     calls = m.mock_calls
    ...     assert calls != [] ,"La fonction inserer_ami n'est pas utilisée !"

 
