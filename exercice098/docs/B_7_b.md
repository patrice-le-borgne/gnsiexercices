    >>> from exercice import *
    >>> R = [[1,2,3], [0,2,3], [0,1,3,4], [0,1,2,5], [2,5,6,7], [3,4,6,7], [4,5,7], [4,5,6]]

    >>> from unittest.mock import Mock, patch
    >>> m1 = Mock()
    >>> m2 = Mock()
    >>> with patch('exercice.creer_reseau_vide', return_value=[[], [], [], [], []]) as m1:
    ...     with patch('exercice.declarer_amis', return_value = True) as m2:
    ...         nouveau_reseau =  generer_reseau_aleatoire(5, 3)
    ...         calls_1 = m1.mock_calls
    ...         assert calls_1 != [] ,"La fonction creer_reseau_vide()  n'est pas utilisée !"
    ...         calls_2 = m2.mock_calls
    ...         assert calls_2 != [] ,"La fonction declarer_amis()  n'est pas utilisée !"

 
