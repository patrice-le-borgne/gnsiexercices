from random import randrange

### Partie A ###

# Q.A.1.a.
A2 = [0, 1, 3]

# Q.A.1.b
A4 = []

# Q.A.2
def amis_de_quartier(i, j):
    Q = []
    for k in range(i, j + 1):
        Q.append(k)
    return Q

# Q.A.3
def star(s, n):
    S = []
    for k in range(n):
        if k != s :
            S.append(k)
    return S


# Q.A.4.a
type_retourne_par_est_ami_avec = "booléen"

# Q.A.4.b
def est_ami_avec(A, p) : 
    for k in range(len(A)):
        if p == A[k]:
            return True
    return False

# Q.A.5
etapes = [[2, 5, 0, 9, 4, 7, 1], [2, 5, 0, 9, 4, 7, 1], [0, 2, 5, 9, 4, 7, 1], [0, 2, 5, 9, 4, 7, 1], [0, 2, 4, 5, 9, 7, 1], [0, 2, 4, 5, 7, 9, 1], [0, 1, 2, 4, 5, 7, 9]]


# Q.A.6
def insertion_sort(t):
    for i in range(1, len(t)):
        to_insert = t[i]
        j = i
        while t[j - 1] > to_insert and j > 0:
            t[j] = t[j - 1]
            j -= 1
        t[j] = to_insert

## Partie B

# Q.B.1
Rb = [[1], [0, 2, 3], [1, 3, 4], [1, 2, 4], [2, 3]]

#Q.B.2
def creer_reseau_vide(n):
    V = []
    for _ in range(n):
        V.append([])
    return V


# Q.B.3
def inserer_ami(A, ami):
    A.append(ami)
    if len(A) > 1 and ami < A[-2]:
        j = len(A) - 1
        while A[j - 1] > ami and j > 0:
            A[j] = A[j - 1]
            j -= 1
        A[j] = ami

# Q.B.4
def rech_dicho(A, p):
    n = len(A)
    g = 0
    d = n - 1
    while g <= d:
        m = (g + d) // 2
        if A[m] == p:
            return True
        elif A[m] < p:
            g = m + 1
        else:
            d = m - 1
    return False     


# Q.B.6
def sont_amis(R, i, j):
    return rech_dicho(R[i], j) and rech_dicho(R[j], i)

# Q.B.6
def declarer_amis(R, i, j):
    if not sont_amis(R, i, j) and i != j:
        inserer_ami(R[i],j)
        inserer_ami(R[j],i)
        

# Q.B.7
def generer_reseau_aleatoire(n, na_max):
    Res = creer_reseau_vide(n)
    for k in range(n):
        na = randrange(na_max+1)
        for i in range(na):
            a = randrange(n)
            declarer_amis(Res, k, a)
    return Res

