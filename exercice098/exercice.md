~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : structures linéaires
thème : listes de listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> R = [[1,2,3], [0,2,3], [0,1,3,4], [0,1,2,5], [2,5,6,7], [3,4,6,7], [4,5,7], [4,5,6]]
~~~

Exercice
========

L’objectif de cet exercice est de programmer les fonctions nécessaires à
la création d’un logiciel permettant de gérer un réseau social. Les
individus du réseau social sont numérotés de 0 à `n-1` où `n` est le
nombre total d’individus. On dit que `n` est la taille du réseau. Chaque
individu possède un certain nombre de liens d’amitié qui sont
implémentés par une liste en Python. Si `j` est ami avec `i` alors il
existe une liste d’entiers des amis de `i` notée `Ai` qui contient au
moins `j` et une liste d’entiers `Aj` qui contient au moins `i`.

Un réseau social `R` entre `n` individus sera représenté par une liste
de listes d’amitiés `R` : la liste d’amitiés de l’individu de numéro
`i` est stockée en `R[i]`.  

Sur la figure 1, on a représenté le réseau social définie par la liste
:

`R = [[1,2,3], [0,2,3], [0,1,3,4], [0,1,2,5], [2,5,6,7], [3,4,6,7], [4,5,7], [4,5,6]]`

Les amis de 0 sont `[1, 2, 3]` et les amis de 6 sont `[4, 5, 7]`.


0---2---4---6
|\ /|   |\ /| 
| X |   | X |
|/ \|   |/ \|
1---3---5---7

Figure 1 – Réseau social à 8 individus. Dans chaque cercle figure un
individu, chaque trait représente un lien d’amitié


0---1     4
|\ /
| 2
|/
3

Figure 2 – Réseau social Ra

**Partie A**

1. Sur le réseau social de la figure 2, donner :
    1. La liste `A2` des amis de 2.

~~~ {.python .hidden .test .amc file="A_1_a.md" bareme="1"}
>>> A2
[0, 1, 3]
~~~

    2. La liste `A4` des amis de 4.

~~~ {.python .hidden .test .amc file="A_1_b.md" bareme="1"}
>>> A4
[]
~~~


2. Un ami de quartier est ami avec tous les individus de numéro
   compris dans l’intervalle entier $[i, j]$.  Écrire une fonction
   `amis_de_quartier(i, j)` qui renvoie la liste des amis d’un
   individu de quartier $[i, j]$.
   
~~~ {.python .hidden .test .amc file="A_2.md" bareme="1"}
>>> amis_de_quartier(4, 8)
[4, 5, 6, 7, 8]
>>> amis_de_quartier(0, 10)
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
~~~

3. Une star est amie avec tous les autres membres d’un réseau. Écrire
   une fonction `star(s, n)` qui renvoie la liste de tous les amis
   d’une star de numéro `s` pour un réseau comportant `n`
   individus.
   
~~~ {.python .hidden .test .amc file="A_3.md" bareme="1"}
>>> star(3, 10)
[0, 1, 2, 4, 5, 6, 7, 8, 9]
>>> star(0, 5)
[1, 2, 3, 4]
~~~
   
4. On considère la fonction `est_ami_avec(A, p)` où `A` est une liste
   d’amis et `p` un individu. Cette fonction statue sur le fait que
   `p` est présent ou pas dans la liste `A`.
   
    1. Quel est le type retourné par la fonction `est_ami_avec` ?

~~~ {.python .hidden .test .amc file="A_4_a.md" bareme="1"}
>>> assert "bool" in type_retourne_par_est_ami_avec
~~~
	
    2. Écrire cette fonction en effectuant une recherche séquentielle
       dans la liste. Il n’est pas autorisé d’utiliser la syntaxe
       `elem in liste` qui effectue déjà une recherche
       séquentielle.

~~~ {.python .hidden .test .amc file="A_4_b.md" bareme="1"}
>>> est_ami_avec([1, 5, 6], 6)
True
>>> est_ami_avec([1, 5, 6], 0)
False
~~~


5. On considère la liste d’amis suivante `[2,5,0,9,4,7,1]`. Trier
manuellement cette liste à l’aide de l’algorithme de tri par
insertion. Vous ferez apparaître les étapes nécessaires à la
compréhension de la méthode de tri.

~~~ {.python .hidden .test .amc file="A_5.md" bareme="1"}
>>> etapes
[[2, 5, 0, 9, 4, 7, 1], [2, 5, 0, 9, 4, 7, 1], [0, 2, 5, 9, 4, 7, 1], [0, 2, 5, 9, 4, 7, 1], [0, 2, 4, 5, 9, 7, 1], [0, 2, 4, 5, 7, 9, 1], [0, 1, 2, 4, 5, 7, 9]]
~~~


6. Écrire une fonction `insertion_sort(A)` où `A` est une liste d’amis qui trie `A` en utilisant l’algorithme du tri par insertion. L'usage des fonctions `sorted()` et `sort()` n'est pas autorisé.

~~~ {.python .hidden .test .amc file="A_6.md" bareme="2"}
>>> from unittest.mock import MagicMock, patch
>>> with patch('builtins.sorted', return_value=666):
...     m = MagicMock()
...     m.__aiter__.return_value = []
...     insertion_sort(m)
...     assert 'call.sort()' not in list(map(str, m.method_calls))
...     l = [2, 5, 0, 9, 4, 7, 1]
...     insertion_sort(l)
...     l
[0, 1, 2, 4, 5, 7, 9]
~~~


**Partie B**
1. Donner la représentation du réseau social `Rb`, dessiné ci-dessous, sous la forme d’une liste de listes nommée `Rb`.

   0---1---2
       |  /|
       | / |
       |/  |
       3---4

FIGURE 2 – Réseau social Ra

~~~ {.python .hidden .test .amc file="B_1.md" bareme="1"}
>>> Rb
[[1], [0, 2, 3], [1, 3, 4], [1, 2, 4], [2, 3]]
~~~

2. Écrire une fonction `creer_reseau_vide(n)` qui renvoie un réseau à
   `n` individus n’ayant aucun lien d’amitié entre eux.

~~~ {.python .hidden .test .amc file="B_2.md" bareme="1"}
>>> creer_reseau_vide(3)
[[], [], []]
>>> creer_reseau_vide(5)
[[], [], [], [], []]
~~~

3. Lors de la déclaration des liens d’amitié, on souhaite insérer les
   nouveaux numéros dans les listes d’amitié en faisant en sorte que
   la liste soit triée. Ceci est possible si à chaque fois qu’un
   élément est inséré, celui-ci est rangé à la bonne place. Écrire une
   fonction `inserer_ami(A, ami)` qui insère l’entier `ami` dans une
   liste d’amitié `A` à la bonne place, c’est-à-dire dans l’ordre
   croissant des entiers. Cette fonction travaille en place sur la
   liste `A`. Par exemple, les instructions `A = [0, 4, 9] ;
   inserer_ami(A, 7)` transforment la liste `A` en la liste `[0, 4, 7,
   9]`. L'usage des fonctions `sorted()` et `sort()` n'est pas autorisé.

~~~ {.python .hidden .test .amc file="B_3.md" bareme="2"}
>>> from unittest.mock import MagicMock, patch
>>> with patch('builtins.sorted', return_value=666):
...     m = MagicMock()
...     m.__aiter__.return_value = []
...     inserer_ami(m, 7)
...     assert 'call.sort()' not in list(map(str, m.method_calls))
...     l = [0, 4, 9]
...     inserer_ami(l, 7)
...     l
[0, 4, 7, 9]
>>> with patch('builtins.sorted', return_value=666):
...     m = MagicMock()
...     m.__aiter__.return_value = []
...     inserer_ami(m, 7)
...     assert 'call.sort()' not in list(map(str, m.method_calls))
...     l = [0, 4, 7, 9]
...     inserer_ami(l, 2)
...     l
[0, 2, 4, 7, 9]
~~~

**Pour toutes les questions suivantes, les listes d’amitiés d’un réseau sont donc triées de
manière ascendante.**

4. En utilisant l’algorithme de recherche d’un élément par dichotomie,
   écrire une fonction `rech_dicho(A, p)` qui statue sur la présence
   de `p` dans la liste `A` en renvoyant un booléen.
   
~~~ {.python .hidden .test .amc file="B_4.md" bareme="1"}
>>> l = [0, 2, 4, 7, 9]
>>> rech_dicho(l, 4)
True
>>> rech_dicho(l, 0)
True
>>> rech_dicho(l, 8)
False
~~~
   
5. Écrire une fonction `sont_amis(R, i, j)` qui statue sur le fait que
   `i` et `j` sont amis dans le réseau `R`. Ces deux individus sont
   amis si et seulement si `i` est ami et de `j` et `j` est ami de
   `i`. On veillera à coder de manière efficace.

~~~ {.python .hidden .test .amc file="B_5.md" bareme="1"}
>>> sont_amis(R, 0, 1)
True
>>> from unittest.mock import Mock, patch
>>> m = Mock()
>>> with patch('exercice.rech_dicho', return_value=True) as m:
...     resultat = sont_amis(R, 1, 0)
...     calls = m.mock_calls
...     assert calls != [] ,"La fonction rech_dicho n'est pas utilisée !"
~~~


6. Écrire une fonction `declarer_amis(R, i, j)` qui modifie le réseau
`R` afin d’enregistrer le lien d’amitié entre `i` et `j`. On fera
attention à ne pas déclarer un individu ami avec lui-même et à ne pas
déclarer plusieurs fois un lien d’amitié déjà existant. On garantira
également que les listes d’amitié restent triées après l’insertion des
nouveaux liens.

~~~ {.python .hidden .amc file="B_6.md" bareme="2"}
>>> sont_amis(R, 3, 4)
False
>>> declarer_amis(R, 3, 4)
>>> sont_amis(R, 3, 4)
True
~~~


~~~ {.python .hidden .test file="B_6_a.md" bareme="1"}
>>> sont_amis(R, 3, 4)
False
>>> declarer_amis(R, 3, 4)
>>> sont_amis(R, 3, 4)
True
~~~

~~~ {.python .hidden .test file="B_6_b.md" bareme="1"}
>>> from unittest.mock import Mock, patch
>>> m = Mock()
>>> with patch('exercice.inserer_ami', return_value=True) as m:
...     declarer_amis(R, 3, 4)
...     calls = m.mock_calls
...     assert calls != [] ,"La fonction inserer_ami n'est pas utilisée !"
~~~


7. Écrire une fonction `generer_reseau_aleatoire(n, na_max)` qui
renvoie un réseau de taille `n` généré aléatoirement dans lequel
chaque individu possède au plus `na_max` amis. On s’appuiera sur les
fonctions précédemment codées. Il est également nécessaire d’utiliser
la fonction `randrange` du module `random`. On rappelle que
`randrange(n)` génère un entier aléatoirement entre $0$ et $n-1$.

~~~ {.python .hidden .test file="B_7_1.md" bareme="1"}
>>> from unittest.mock import Mock, patch
>>> m = Mock()
>>> with patch('exercice.randrange', side_effect = [2, 1, 2, 3, 0, 3, 4, 2, 0, 3, 3, 1, 2, 4, 2, 1, 3]) as m:
...     nouveau_reseau =  generer_reseau_aleatoire(5, 3)
...     nouveau_reseau
[[1, 2], [0, 3, 4], [0, 3], [1, 2, 4], [1, 3]]
~~~

~~~ {.python .hidden .test file="B_7_b.md" bareme="1"}
>>> from unittest.mock import Mock, patch
>>> m1 = Mock()
>>> m2 = Mock()
>>> with patch('exercice.creer_reseau_vide', return_value=[[], [], [], [], []]) as m1:
...     with patch('exercice.declarer_amis', return_value = True) as m2:
...         nouveau_reseau =  generer_reseau_aleatoire(5, 3)
...         calls_1 = m1.mock_calls
...         assert calls_1 != [] ,"La fonction creer_reseau_vide()  n'est pas utilisée !"
...         calls_2 = m2.mock_calls
...         assert calls_2 != [] ,"La fonction declarer_amis()  n'est pas utilisée !"
~~~

~~~ {.python .hidden .amc file="B_7.md" bareme="2"}

~~~
