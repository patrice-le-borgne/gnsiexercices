\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

L'objectif de cet exercice est de programmer les fonctions nécessaires à
la création d'un logiciel permettant de gérer un réseau social. Les
individus du réseau social sont numérotés de 0 à \texttt{n-1} où
\texttt{n} est le nombre total d'individus. On dit que \texttt{n} est la
taille du réseau. Chaque individu possède un certain nombre de liens
d'amitié qui sont implémentés par une liste en Python. Si \texttt{j} est
ami avec \texttt{i} alors il existe une liste d'entiers des amis de
\texttt{i} notée \texttt{Ai} qui contient au moins \texttt{j} et une
liste d'entiers \texttt{Aj} qui contient au moins \texttt{i}.

Un réseau social \texttt{R} entre \texttt{n} individus sera représenté
par une liste de listes d'amitiés \texttt{R} : la liste d'amitiés de
l'individu de numéro \texttt{i} est stockée en \texttt{R{[}i{]}}.

Sur la figure 1, on a représenté le réseau social définie par la liste :

\texttt{R\ =\ {[}{[}1,2,3{]},\ {[}0,2,3{]},\ {[}0,1,3,4{]},\ {[}0,1,2,5{]},\ {[}2,5,6,7{]},\ {[}3,4,6,7{]},\ {[}4,5,7{]},\ {[}4,5,6{]}{]}}

Les amis de 0 sont \texttt{{[}1,\ 2,\ 3{]}} et les amis de 6 sont
\texttt{{[}4,\ 5,\ 7{]}}.

0---2---4---6 \textbar{}~/\textbar{} \textbar{}~/\textbar{} \textbar{} X
\textbar{} \textbar{} X \textbar{} \textbar{}/ \textbar{} \textbar{}/
\textbar{} 1---3---5---7

Figure 1 -- Réseau social à 8 individus. Dans chaque cercle figure un
individu, chaque trait représente un lien d'amitié

0---1 4 \textbar{}~/ \textbar{} 2 \textbar{}/ 3

Figure 2 -- Réseau social Ra

\textbf{Partie A}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Sur le réseau social de la figure 2, donner :

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    La liste \texttt{A2} des amis de 2.
  \end{enumerate}
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\FloatTok{2.}\NormalTok{ La liste `A4` des amis de }\FloatTok{4.}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Un ami de quartier est ami avec tous les individus de numéro compris
  dans l'intervalle entier \([i, j]\). Écrire une fonction
  \texttt{amis\_de\_quartier(i,\ j)} qui renvoie la liste des amis d'un
  individu de quartier \([i, j]\).
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  Une star est amie avec tous les autres membres d'un réseau. Écrire une
  fonction \texttt{star(s,\ n)} qui renvoie la liste de tous les amis
  d'une star de numéro \texttt{s} pour un réseau comportant \texttt{n}
  individus.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\item
  On considère la fonction \texttt{est\_ami\_avec(A,\ p)} où \texttt{A}
  est une liste d'amis et \texttt{p} un individu. Cette fonction statue
  sur le fait que \texttt{p} est présent ou pas dans la liste
  \texttt{A}.

  \begin{enumerate}
  \def\labelenumii{\arabic{enumii}.}
  \tightlist
  \item
    Quel est le type retourné par la fonction \texttt{est\_ami\_avec} ?
  \end{enumerate}
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\FloatTok{2.}\NormalTok{ Écrire cette fonction en effectuant une recherche séquentielle}
\NormalTok{   dans la liste. Il n’est pas autorisé d’utiliser la syntaxe}
\NormalTok{   `elem }\KeywordTok{in}\NormalTok{ liste` qui effectue déjà une recherche}
\NormalTok{   séquentielle.}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  On considère la liste d'amis suivante \texttt{{[}2,5,0,9,4,7,1{]}}.
  Trier manuellement cette liste à l'aide de l'algorithme de tri par
  insertion. Vous ferez apparaître les étapes nécessaires à la
  compréhension de la méthode de tri.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{5}
\tightlist
\item
  Écrire une fonction \texttt{insertion\_sort(A)} où \texttt{A} est une
  liste d'amis qui trie \texttt{A} en utilisant l'algorithme du tri par
  insertion. L'usage des fonctions \texttt{sorted()} et \texttt{sort()}
  n'est pas autorisé.
\end{enumerate}

\textbf{Partie B} 1. Donner la représentation du réseau social
\texttt{Rb}, dessiné ci-dessous, sous la forme d'une liste de listes
nommée \texttt{Rb}.

0---1---2 \textbar{} /\textbar{} \textbar{} / \textbar{} \textbar{}/
\textbar{} 3---4

FIGURE 2 -- Réseau social Ra

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Écrire une fonction \texttt{creer\_reseau\_vide(n)} qui renvoie un
  réseau à \texttt{n} individus n'ayant aucun lien d'amitié entre eux.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  Lors de la déclaration des liens d'amitié, on souhaite insérer les
  nouveaux numéros dans les listes d'amitié en faisant en sorte que la
  liste soit triée. Ceci est possible si à chaque fois qu'un élément est
  inséré, celui-ci est rangé à la bonne place. Écrire une fonction
  \texttt{inserer\_ami(A,\ ami)} qui insère l'entier \texttt{ami} dans
  une liste d'amitié \texttt{A} à la bonne place, c'est-à-dire dans
  l'ordre croissant des entiers. Cette fonction travaille en place sur
  la liste \texttt{A}. Par exemple, les instructions
  \texttt{A\ =\ {[}0,\ 4,\ 9{]}\ ;\ inserer\_ami(A,\ 7)} transforment la
  liste \texttt{A} en la liste \texttt{{[}0,\ 4,\ 7,\ 9{]}}. L'usage des
  fonctions \texttt{sorted()} et \texttt{sort()} n'est pas autorisé.
\end{enumerate}

\textbf{Pour toutes les questions suivantes, les listes d'amitiés d'un
réseau sont donc triées de manière ascendante.}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  En utilisant l'algorithme de recherche d'un élément par dichotomie,
  écrire une fonction \texttt{rech\_dicho(A,\ p)} qui statue sur la
  présence de \texttt{p} dans la liste \texttt{A} en renvoyant un
  booléen.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Écrire une fonction \texttt{sont\_amis(R,\ i,\ j)} qui statue sur le
  fait que \texttt{i} et \texttt{j} sont amis dans le réseau \texttt{R}.
  Ces deux individus sont amis si et seulement si \texttt{i} est ami et
  de \texttt{j} et \texttt{j} est ami de \texttt{i}. On veillera à coder
  de manière efficace.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{5}
\tightlist
\item
  Écrire une fonction \texttt{declarer\_amis(R,\ i,\ j)} qui modifie le
  réseau \texttt{R} afin d'enregistrer le lien d'amitié entre \texttt{i}
  et \texttt{j}. On fera attention à ne pas déclarer un individu ami
  avec lui-même et à ne pas déclarer plusieurs fois un lien d'amitié
  déjà existant. On garantira également que les listes d'amitié restent
  triées après l'insertion des nouveaux liens.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{6}
\tightlist
\item
  Écrire une fonction \texttt{generer\_reseau\_aleatoire(n,\ na\_max)}
  qui renvoie un réseau de taille \texttt{n} généré aléatoirement dans
  lequel chaque individu possède au plus \texttt{na\_max} amis. On
  s'appuiera sur les fonctions précédemment codées. Il est également
  nécessaire d'utiliser la fonction \texttt{randrange} du module
  \texttt{random}. On rappelle que \texttt{randrange(n)} génère un
  entier aléatoirement entre \(0\) et \(n-1\).
\end{enumerate}
