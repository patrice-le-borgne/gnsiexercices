>>> from exercice import *

Exercice
========

L’objectif de cet exercice est de programmer les fonctions nécessaires à
la création d’un logiciel permettant de gérer un réseau social. Les
individus du réseau social sont numérotés de 0 à `n-1` où `n` est le
nombre total d’individus. On dit que `n` est la taille du réseau. Chaque
individu possède un certain nombre de liens d’amitié qui sont
implémentés par une liste en Python. Si `j` est ami avec `i` alors il
existe une liste d’entiers des amis de `i` notée `Ai` qui contient au
moins `j` et une liste d’entiers `Aj` qui contient au moins `i`.

Un réseau social `R` entre `n` individus sera représenté par une liste
de listes d’amitiés `R` : la liste d’amitiés de l’individu de numéro
`i` est stockée en `R[i]`.

Sur la figure 1, on a représenté le réseau social définie par la liste :

`R = [[1,2,3], [0,2,3], [0,1,3,4], [0,1,2,5], [2,5,6,7], [3,4,6,7], [4,5,7], [4,5,6]]`

Les amis de 0 sont `[1, 2, 3]` et les amis de 6 sont `[4, 5, 7]`.


0---2---4---6
|\ /|   |\ /| 
| X |   | X |
|/ \|   |/ \|
1---3---5---7

Figure 1 – Réseau social à 8 individus. Dans chaque cercle figure un
individu, chaque trait représente un lien d’amitié


0---1     4
|\ /
| 2
|/
3

Figure 2 – Réseau social Ra

**Partie A**

1. Sur le réseau social de la figure 2, donner :
    1. La liste `A2` des amis de 2.
    2. La liste `A4` des amis de 4.

2. Un ami de quartier est ami avec tous les individus de numéro
   compris dans l’intervalle entier $[i, j]$.  Écrire une fonction
   `amis_de_quartier(i, j)` qui renvoie la liste des amis d’un
   individu de quartier $[i, j]$.

3. Une star est amie avec tous les autres membres d’un réseau. Écrire
   une fonction `star(s, n)` qui renvoie la liste de tous les amis
   d’une star de numéro `s` pour un réseau comportant `n`
   individus.
   
4. On considère la fonction `est_ami_avec(A, p)` où `A` est une liste
   d’amis et `p` un individu. Cette fonction statue sur le fait que
   `p` est présent ou pas dans la liste `A`.
   
    1. Quel est le type retourné par la fonction `est_ami_avec` ?
    2. Écrire cette fonction en effectuant une recherche séquentielle
       dans la liste. Il n’est pas autorisé d’utiliser la syntaxe
       `elem in liste` qui effectue déjà une recherche
       séquentielle.


5. On considère la liste d’amis suivante `[2,5,0,9,4,7,1]`. Trier
   manuellement cette liste à l’aide de l’algorithme de tri par
   insertion. Vous ferez apparaître les étapes nécessaires à la
   compréhension de la méthode de tri.

6. Écrire une fonction `insertion_sort(A)` où `A` est une liste d’amis
   qui trie `A` en utilisant l’algorithme du tri par
   insertion. L'usage des fonctions `sorted()` et `sort()` n'est pas
   autorisé.


**Partie B**

1. Donner la représentation du réseau social `Rb`, dessiné ci-dessous,
   sous la forme d’une liste de listes nommée `Rb`.

   0---1---2
       |  /|
       | / |
       |/  |
       3---4

FIGURE 3 – Réseau social R3

2. Écrire une fonction `creer_reseau_vide(n)` qui renvoie un réseau à
   `n` individus n’ayant aucun lien d’amitié entre eux.


3. Lors de la déclaration des liens d’amitié, on souhaite insérer les
   nouveaux numéros dans les listes d’amitié en faisant en sorte que
   la liste soit triée. Ceci est possible si à chaque fois qu’un
   élément est inséré, celui-ci est rangé à la bonne place. Écrire une
   fonction `inserer_ami(A, ami)` qui insère l’entier `ami` dans une
   liste d’amitié `A` à la bonne place, c’est-à-dire dans l’ordre
   croissant des entiers. Cette fonction travaille en place sur la
   liste `A`. Par exemple, les instructions `A = [0, 4, 9] ;
   inserer_ami(A, 7)` transforment la liste `A` en la liste `[0, 4, 7, 9]`. 
   L'usage des fonctions `sorted()` et `sort()` n'est pas autorisé.

**Pour toutes les questions suivantes, les listes d’amitiés d’un réseau sont donc triées de
manière ascendante.**

4. En utilisant l’algorithme de recherche d’un élément par dichotomie,
   écrire une fonction `rech_dicho(A, p)` qui statue sur la présence
   de `p` dans la liste `A` en renvoyant un booléen.
   
   
5. Écrire une fonction `sont_amis(R, i, j)` qui statue sur le fait que
   `i` et `j` sont amis dans le réseau `R`. Ces deux individus sont
   amis si et seulement si `i` est ami et de `j` et `j` est ami de
   `i`. On veillera à coder de manière efficace.

6. Écrire une fonction `declarer_amis(R, i, j)` qui modifie le réseau
   `R` afin d’enregistrer le lien d’amitié entre `i` et `j`. On fera
   attention à ne pas déclarer un individu ami avec lui-même et à ne
   pas déclarer plusieurs fois un lien d’amitié déjà existant. On
   garantira également que les listes d’amitié restent triées après
   l’insertion des nouveaux liens.

7. Écrire une fonction `generer_reseau_aleatoire(n, na_max)` qui
   renvoie un réseau de taille `n` généré aléatoirement dans lequel
   chaque individu possède au plus `na_max` amis. On s’appuiera sur
   les fonctions précédemment codées. Il est également nécessaire
   d’utiliser la fonction `randrange` du module `random`. On rappelle
   que `randrange(n)` génère un entier aléatoirement entre $0$ et
   $n-1$.
