Exercice
========

On considère le grpahe ci-dessous :

b---e---f /  /  / a d g  / / c h

1.  Partant du sommet **b**, proposer deux parcours en largueur de ce
    graphe.
2.  Partant du sommet **b**, proposer deux parcours en profondeur de ce
    graphe.
