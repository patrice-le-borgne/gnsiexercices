~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : graphes
thème : dictionnaires
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> g1 = Sudoku([[3, 0, 0, 0], [1, 0, 3, 0], [4, 3, 1, 0], [2, 0, 0, 0]])
>>> g2 = Sudoku([[0, 0, 4, 9, 0, 0, 0, 8, 0],
...	[0, 0, 5, 0, 1, 8, 4, 0, 0],
... [0, 8, 0, 0, 0, 0, 0, 5, 0],
... [0, 0, 2, 0, 8, 5, 0, 1, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 9, 0, 6, 4, 0, 2, 0, 0],
... [0, 6, 0, 0, 0, 0, 0, 2, 0],
... [0, 0, 7, 2, 9, 0, 5, 0, 0],
... [0, 1, 0, 0, 0, 3, 8, 0, 0]])
>>> gv3 = [[0, 0, 0, 0, 0, 0, 0, 0, 0],
...	[0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0]]

>>> def graphe_vide(n):
...     """
...     renvoie un graphe vide correspondant aux dimensions
...     self.niveau
...     """
...     g = {}
...     # on ajoute les sommets
...     for i in range(n**2):
...         for j in range(n**2):
...             g[(i,j)] = {"valeur":0, "voisins": set()}
...     # On ajoute les voisins
...     for i in range(n**2):
...         for j in range(n**2):
...             carre_ligne = (i // n ) * n
...             carre_colonne =(j // n) * n
...             for k in range(n**2):
...                 # on ajoute la colonne
...                 if (i,j) != (i,k):
...                     g[(i,j)]["voisins"].add((i,k))
...                 # on ajoute la ligne
...                 if (i,j) != (k,j):
...                     g[(i,j)]["voisins"].add((k,j))
...                 # on ajoute le carre
...                 for k in range(n):
...                     for l in range(n):
...                         if (i,j) != (carre_ligne + k, carre_colonne + l):
...                             g[(i,j)]["voisins"].add((carre_ligne + k,carre_colonne + l))
...     return g
~~~

Exercice
========

Le Sudoku, est un jeu en forme de grille défini en 1979 par
l’Américain Howard Garns, inspiré du carré latin, ainsi que du
problème des 36 officiers du mathématicien suisse Leonhard Euler.

Le but du jeu est de remplir la grille avec une série de chiffres (ou
de lettres ou de symboles) tous différents, qui ne se trouvent jamais
plus d’une fois sur une même ligne, dans une même colonne ou dans une
même région (également appelée « bloc », « groupe », « secteur » ou «
sous-grille »).

La plupart du temps, les symboles sont des chiffres allant de 1 à 9,
les régions étant alors des carrés de $3 \times 3$.

Quelques symboles sont déjà disposés dans la grille, ce qui autorise
une résolution progressive du problème complet.

![grille_incomplète](exo_115_1.png)
![grille_complète](exo_115_2.png)

Il existe une version mini-Sudoku avec une grille composée de 4 carrés
2x2. (les symboles étant les chiffres de 1 à 4).

![mini_sudoku](exo_115_3.png)


**Modélisation**

Nous allons utiliser une modélisation du jeu du Sudoku utilisant un graphe coloré.

**Structure de données modélisant une grille de Sudoku**

La grille du jeu peut être représentée par une matrice carrée en Python :

Une matrice $9\times9$ pour le Sudoku classique ;

Une matrice $4\times4$ pour le mini-Sudoku.

*Exemple :*

La grille du mini-Sudoku présentée précédemment peut être représentée par la liste suivante :

`[[0,0,0,0],[1,0,3,0],[4,3,1,0],[2,0,0,0]]`

*Remarque :* Les cases vides sont "codées" par le chiffre 0.

**Modélisation d'une grille de Sudoku avec un graphe**

On peut modéliser une grille de Sudoku avec un graphe défini comme suit :

Les sommets du graphe sont les cases de la grille.

Pour un Sudoku classique le nombre de sommets est $9\times9=81$.

Pour un mini-Sudoku le nombre de sommets est $4\times4=16$.

Les arêtes représentent les contraintes d'une grille de Sudoku.

*Exemple :* Pour une grille de mini-Sudoku, le sommet représenté en rouge
(à l'intersection de la ligne 3 et de la colonne 2) est lié à tous les
sommets en bleus.

![mini_sudoku](exo_115_4.png)

C'est-à-dire à tous les sommets sur la même ligne que lui, sur la même
colonne que lui et dans la même sous-grille que lui.

**Structure de données pour représenter le graphe associé à une grille de Sudoku**

Il est possible d'utiliser un dictionnaire représentant la liste
d'adjacence associée au graphe.

Les **clefs** du dictionnaire sont les **sommets** du graphe.

On peut réprésenter chaque sommet par un tuple du type `(ligne,colonne)`
déterminant la ligne et la colonne de la case de la grille du associé
au sommet.

Les **valeurs** du dictionnaire sont :
- la valeur écrite dans la case (0 pour une case vide) ;
- la liste des voisins d'un sommet donné.

*Exemple :*

Le dictionnaire ainsi défini pour une grille de Mini-Sudoku précédente est le suivant :

~~~ {.python}
{(0,1) : {'valeur': 3, 'voisins' : {(0, 1), (1, 1), (0, 3), (2, 0), (3, 0), (0, 2), (1, 0)},
 (0,1) : {'valeur': 0, 'voisins' : {(2, 1), (0, 0), (3, 1), (1, 1), (0, 3), (0, 2), (1, 0)},
 (0,2) : {'valeur': 0, 'voisins' : {(0, 1), (1, 2), (0, 0), (0, 3), (2, 2), (3, 2), (1, 3)},
 (0,3) : {'valeur': 0, 'voisins' : {(0, 1), (1, 2), (0, 0), (2, 3), (0, 2), (3, 3), (1, 3)},
 (1,0) : {'valeur': 1, 'voisins' : {(0, 1), (1, 2), (0, 0), (1, 1), (2, 0), (3, 0), (1, 3)},
 (1,1) : {'valeur': 0, 'voisins' : {(0, 1), (1, 2), (2, 1), (0, 0), (3, 1), (1, 0), (1, 3)},
 (1,2) : {'valeur': 3, 'voisins' : {(1, 1), (0, 3), (0, 2), (2, 2), (1, 0), (3, 2), (1, 3)},
 (1,3) : {'valeur': 0, 'voisins' : {(1, 2), (1, 1), (0, 3), (2, 3), (0, 2), (3, 3), (1, 0)},
 (2,0) : {'valeur': 4, 'voisins' : {(2, 1), (0, 0), (3, 1), (3, 0), (2, 3), (2, 2), (1, 0)},
 (2,1) : {'valeur': 3, 'voisins' : {(0, 1), (3, 1), (1, 1), (2, 0), (3, 0), (2, 3), (2, 2)},
 (2,2) : {'valeur': 1, 'voisins' : {(1, 2), (2, 1), (2, 0), (2, 3), (0, 2), (3, 3), (3, 2)},
 (2,3) : {'valeur': 0, 'voisins' : {(2, 1), (0, 3), (2, 0), (3, 3), (2, 2), (3, 2), (1, 3)},
 (3,0) : {'valeur': 2, 'voisins' : {(2, 1), (0, 0), (3, 1), (2, 0), (3, 3), (1, 0), (3, 2)},
 (3,1) : {'valeur': 0, 'voisins' : {(0, 1), (2, 1), (1, 1), (2, 0), (3, 0), (3, 3), (3, 2)},
 (3,2) : {'valeur': 0, 'voisins' : {(1, 2), (3, 1), (3, 0), (2, 3), (0, 2), (3, 3), (2, 2)},
 (3,3) : {'valeur': 0, 'voisins' : {(3, 1), (0, 3), (3, 0), (2, 3), (2, 2), (3, 2), (1, 3)}}
~~~

La classe `Sudoku` proposera l'affichage suivant :

~~~ {.python}
(0,0) : 3 -> {(0, 1), (1, 1), (0, 3), (2, 0), (3, 0), (0, 2), (1, 0)}
(0,1) : 0 -> {(2, 1), (0, 0), (3, 1), (1, 1), (0, 3), (0, 2), (1, 0)}
(0,2) : 0 -> {(0, 1), (1, 2), (0, 0), (0, 3), (2, 2), (3, 2), (1, 3)}
(0,3) : 0 -> {(0, 1), (1, 2), (0, 0), (2, 3), (0, 2), (3, 3), (1, 3)}
(1,0) : 1 -> {(0, 1), (1, 2), (0, 0), (1, 1), (2, 0), (3, 0), (1, 3)}
(1,1) : 0 -> {(0, 1), (1, 2), (2, 1), (0, 0), (3, 1), (1, 0), (1, 3)}
(1,2) : 3 -> {(1, 1), (0, 3), (0, 2), (2, 2), (1, 0), (3, 2), (1, 3)}
(1,3) : 0 -> {(1, 2), (1, 1), (0, 3), (2, 3), (0, 2), (3, 3), (1, 0)}
(2,0) : 4 -> {(2, 1), (0, 0), (3, 1), (3, 0), (2, 3), (2, 2), (1, 0)}
(2,1) : 3 -> {(0, 1), (3, 1), (1, 1), (2, 0), (3, 0), (2, 3), (2, 2)}
(2,2) : 1 -> {(1, 2), (2, 1), (2, 0), (2, 3), (0, 2), (3, 3), (3, 2)}
(2,3) : 0 -> {(2, 1), (0, 3), (2, 0), (3, 3), (2, 2), (3, 2), (1, 3)}
(3,0) : 2 -> {(2, 1), (0, 0), (3, 1), (2, 0), (3, 3), (1, 0), (3, 2)}
(3,1) : 0 -> {(0, 1), (2, 1), (1, 1), (2, 0), (3, 0), (3, 3), (3, 2)}
(3,2) : 0 -> {(1, 2), (3, 1), (3, 0), (2, 3), (0, 2), (3, 3), (2, 2)}
(3,3) : 0 -> {(3, 1), (0, 3), (3, 0), (2, 3), (2, 2), (3, 2), (1, 3)}
~~~

1. Écrire la méthode `_graphe_vide(self, n)` qui renvoie un graphe vide. `n` est la taille de la sous-grille. Cette méthode est appelée par la méthode `__init__(self, grille = None)`.

~~~ {.python .test .amc file="Q_1_1.md" bareme="1"}
>>> gv = Sudoku()
>>> print(gv)
+--+--+
|..|..|
|..|..|
+--+--+
|..|..|
|..|..|
+--+--+
~~~ 

~~~ {.python .hidden .test .amc file="Q_1_2.md" bareme="1"}
>>> gv = Sudoku(gv3)
>>> print(gv)
+---+---+---+
|...|...|...|
|...|...|...|
|...|...|...|
+---+---+---+
|...|...|...|
|...|...|...|
|...|...|...|
+---+---+---+
|...|...|...|
|...|...|...|
|...|...|...|
+---+---+---+
~~~

2. Compléter le méthode `__init(self, grille = None)` pour qu'elle remplisse le graphe à partir de la `grille`.

~~~ {.python .test .amc file="Q_2.md" bareme="1"}
>>> g1 = Sudoku([[3, 0, 0, 0], [1, 0, 3, 0], [4, 3, 1, 0], [2, 0, 0, 0]])
>>> print(g1)
+--+--+
|3.|..|
|1.|3.|
+--+--+
|43|1.|
|2.|..|
+--+--+
~~~

3. Écrire la méthode `valeurs_possibles(self,case)` qui renvoie la liste des valeurs possibles pour la `case (i,j)`.

~~~ {.python .test .amc file="Q_3_1.md" bareme="1"}
>>> print(g1)
+--+--+
|3.|..|
|1.|3.|
+--+--+
|43|1.|
|2.|..|
+--+--+
>>> g1.valeurs_possibles((0, 1))
[2, 4]
>>> g1.valeurs_possibles((3, 3))
[3, 4]
~~~

~~~ {.python .hidden .test .amc file="Q_3_2.md" bareme="1"}
>>> g2 = Sudoku([[0, 0, 4, 9, 0, 0, 0, 8, 0],
...	[0, 0, 5, 0, 1, 8, 4, 0, 0],
... [0, 8, 0, 0, 0, 0, 0, 5, 0],
... [0, 0, 2, 0, 8, 5, 0, 1, 0],
... [0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 9, 0, 6, 4, 0, 2, 0, 0],
... [0, 6, 0, 0, 0, 0, 0, 2, 0],
... [0, 0, 7, 2, 9, 0, 5, 0, 0],
... [0, 1, 0, 0, 0, 3, 8, 0, 0]])
>>> print(g2)
+---+---+---+
|..4|9..|.8.|
|..5|.18|4..|
|.8.|...|.5.|
+---+---+---+
|..2|.85|.1.|
|...|...|...|
|.9.|64.|2..|
+---+---+---+
|.6.|...|.2.|
|..7|29.|5..|
|.1.|..3|8..|
+---+---+---+
>>> g2.valeurs_possibles((0, 0))
[1, 2, 3, 6, 7]
>>> g2.valeurs_possibles((8, 8))
[4, 6, 7, 9]
~~~

4. Écrire la méthode `case_a_remplir(self)` qui renvoie une liste avec la case à remplir de préférence (c'est celle qui a le moins de possibilités) et la liste des valeurs possibles.

~~~ {.python .test .amc file="Q_4.md" bareme="1"}
>>> print(g1)
+--+--+
|3.|..|
|1.|3.|
+--+--+
|43|1.|
|2.|..|
+--+--+
>>> g1.case_a_remplir()
[(2, 3), [2]]
~~~

5. Écrire `est_plein(self)` renvoie `True` si le graphe est plein. `False` sinon.

~~~ {.python .test .amc file="Q_5_1.md" bareme="1"}
>>> print(g1)
+--+--+
|3.|..|
|1.|3.|
+--+--+
|43|1.|
|2.|..|
+--+--+
>>> g1.est_plein()
False
>>> g4 = Sudoku([[3, 4, 2, 1], [1, 2, 3, 4], [4, 3, 1, 2], [2, 1, 4, 3]])
>>> g4.est_plein()
True
~~~

6. Écrire la méthode `resolution(self)` qui résous, si possible la grille de sudoku.

~~~ {.python .test .amc file="Q_6_1.md" bareme="1"}
>>> print(g1)
+--+--+
|3.|..|
|1.|3.|
+--+--+
|43|1.|
|2.|..|
+--+--+
>>> g1.resolution()
>>> print(g1)
+--+--+
|34|21|
|12|34|
+--+--+
|43|12|
|21|43|
+--+--+
~~~

~~~ {.python .test .amc file="Q_6_2.md" bareme="1"}
>>> print(g2)
+---+---+---+
|..4|9..|.8.|
|..5|.18|4..|
|.8.|...|.5.|
+---+---+---+
|..2|.85|.1.|
|...|...|...|
|.9.|64.|2..|
+---+---+---+
|.6.|...|.2.|
|..7|29.|5..|
|.1.|..3|8..|
+---+---+---+
>>> g2.resolution()
>>> print(g2)
+---+---+---+
|724|956|381|
|635|718|492|
|981|432|657|
+---+---+---+
|472|385|916|
|356|129|748|
|198|647|235|
+---+---+---+
|563|874|129|
|847|291|563|
|219|563|874|
+---+---+---+
~~~

~~~ {.python .hidden .test .amc file="Q_6_3.md" bareme="1"}
>>> g3 = Sudoku([[0, 0, 0, 0, 0, 0, 0, 0, 0],
... [0, 4, 5, 0, 0, 0, 3, 8, 0],
... [0, 8, 0, 2, 0, 6, 0, 7, 0],
... [0, 0, 9, 0, 1, 0, 4, 0, 0],
... [0, 0, 1, 7, 0, 3, 8, 0, 0],
... [0, 0, 0, 0, 6, 0, 0, 0, 0],
... [7, 0, 0, 0, 0, 0, 0, 0, 5],
... [0, 0, 0, 4, 0, 9, 0, 0, 0],
... [0, 5, 0, 0, 0, 0, 0, 6, 0]])
>>> print(g3)
+---+---+---+
|...|...|...|
|.45|...|38.|
|.8.|2.6|.7.|
+---+---+---+
|..9|.1.|4..|
|..1|7.3|8..|
|...|.6.|...|
+---+---+---+
|7..|...|..5|
|...|4.9|...|
|.5.|...|.6.|
+---+---+---+
>>> g3.resolution()
>>> print(g3)
+---+---+---+
|217|385|694|
|645|971|382|
|983|246|571|
+---+---+---+
|569|812|437|
|421|793|856|
|378|564|219|
+---+---+---+
|792|638|145|
|136|459|728|
|854|127|963|
+---+---+---+
~~~






