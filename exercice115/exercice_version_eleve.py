from math import sqrt

class Sudoku:
    """La classe Sudoku qui stocke la grille sous forme de graphe.
    - l'attribut graphe est un dictionnaire dont la clé est un tuple
    qui correspond à une case et la valeur est également un dictionnaire.
    - l'attribut niveau correspond au dimension des sous-cases. Le sudoku classique est de niveau 3.

    """
    def __init__(self, grille = None):
        """
        La grille est une liste de listes.  Si la grille n'est pas
        fournie, on déclare que la grille est de niveau 2 et
        self.grille est un graphe qui correspond à une grille vide.
        """
        if grille is None:
            self.niveau = 2
        else:
            self.niveau = int(sqrt(len(grille)))
        self.graphe = self._graphe_vide(self.niveau)
        if grille is not None:
            # à compléter pour la question 2
                    
    def _graphe_vide(self, n):
        """
        renvoie un graphe vide correspondant aux dimensions
        self.niveau
        """
        pass
    
    def valeurs_possibles(self,case):
        """
        Renvoie la liste des valeurs possibles pour la case (i,j)
        """
        pass

    def case_a_remplir(self):
        """Renvoie la case à remplir de préférence : c'est celle qui
        a le moins de possibilités.
        """
        pass

    def est_plein(self):
        """
        Renvoie True si le graphe est plein. False sinon.
        """
        pass
    
    def resolution(self):
        """
        Résous la grille de sudoku.
        """
        pile = []
        ... à compléter
  
    def __repr__(self):
        n = self.niveau
        texte = ""
        for i in range(n**2):
            for j in range(n**2):
                texte += f"({i},{j}) : {self.graphe[(i,j)]['valeur']} -> "
                texte += str(self.graphe[(i,j)]['voisins'])
                texte += "\n"
        return texte

    def __str__(self):
        n = self.niveau
        ligne_horizontale = "+"
        for i in range(n):
            ligne_horizontale += "-"*(n) + "+"
        ligne_horizontale += "\n"
        texte = ligne_horizontale
        for i in range(n**2):
            ligne = ""
            for j in range(n**2):
                if self.graphe[i,j]['valeur'] == 0:
                    v = "."
                else:
                    v = str(self.graphe[i,j]['valeur'])
                if j % n == 0:
                    ligne += "|"
                ligne += v
            ligne += "|\n"
            texte += ligne
            if (i+1) % n == 0:
                texte += ligne_horizontale
        return texte
    
