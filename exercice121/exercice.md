~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : structures linéaires
thème : tri
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On considère l'algorithme de tri de tableau suivant : à chaque étape,
on parcourt le sous-tableau des éléments non rangés et on place le
plus petit élément en première position de ce sous-tableau.

Exemple avec le tableau : `t = [41, 55, 21, 18, 12, 6, 25]`. 

- Étape 1 : on parcourt tous les éléments du tableau, on permute le plus petit élément avec le premier. Le tableau devient `t = [6, 55, 21, 18, 12, 41, 25]`

- Étape 2 : on parcourt tous les éléments **sauf le premier**, on permute le plus petit élément trouvé avec le second.  
Le tableau devient : `t = [6, 12, 21, 18, 55, 41, 25]` 

Et ainsi de suite. 

Le programme ci-dessous implémente cet algorithme.


~~~ {.python}
def echange(tab, i, j):
    '''Echange les éléments d'indice i et j dans le tableau tab.'''
    temp = ... 
    tab[i] = ... 
    tab[j] = ... 

def tri_selection(tab):
    '''Trie le tableau tab dans l'ordre croissant
    par la méthode du tri par sélection.'''
    N = len(tab)
    for k in range(...): 
        imin = ... 
        for i in range(..., N): 
            if tab[i] < ...: 
                imin = i
        echange(tab, ..., ...)
~~~

Compléter le code de cette fonction de façon à obtenir :

~~~ {.python}
>>> liste = [41, 55, 21, 18, 12, 6, 25]
>>> tri_selection(liste)
>>> liste
[6, 12, 18, 21, 25, 41, 55]
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> l = [2, 3, 6, 9]
>>> echange(l, 0, 1)
>>> l
[3, 2, 6, 9]
>>> echange(l, 3, 1)
>>> l
[3, 9, 6, 2]
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="2"}
>>> liste = [41, 55, 21, 18, 12, 6, 25]
>>> tri_selection(liste)
>>> liste
[6, 12, 18, 21, 25, 41, 55]
>>> liste2 = [1, 2, 3]
>>> tri_selection(liste2)
>>> liste2
[1, 2, 3]
~~~

~~~ {.python .hidden .amc file="Q_1.md" bareme="4"}
>>> pass
~~~





