~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> from unittest.mock import patch
~~~

Exercice
========

On a relevé les valeurs moyennes annuelles des températures à Paris pour la période
allant de 2013 à 2019. Les résultats ont été récupérés sous la forme de deux listes : l’une pour les températures, l’autre pour les années :

~~~ {.python .all}
t_moy = [14.9, 13.3, 13.1, 12.5, 13.0, 13.6, 13.7]
annees = [2013, 2014, 2015, 2016, 2017, 2018, 2019]
~~~

Écrire la fonction `mini` qui prend en paramètres le tableau `releve` des relevés et le
tableau `date` des dates et qui renvoie la plus petite valeur relevée au cours de la
période et l’année correspondante.

Exemple :

~~~ {.python .amc file="Q_1.md" bareme="4"}
    >>> mini(t_moy, annees)
    (12.5, 2016)
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="2"}
>>> with patch('builtins.min', return_value = 666):
...     t_moy = [14.9, 13.3, 13.1, 12.5, 13.0, 13.6, 13.7]
...     annees = [2013, 2014, 2015, 2016, 2017, 2018, 2019]
...     mini(t_moy, annees)
(12.5, 2016)
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="2"}
>>> with patch('builtins.min', return_value = 666):
...     t_moy = [-14.9, -13.3, -13.1, -12.5, -13.0, -13.6, -13.7]
...     annees = [2013, 2014, 2015, 2016, 2017, 2018, 2019]
...     mini(t_moy, annees)
(-14.9, 2013)
~~~



