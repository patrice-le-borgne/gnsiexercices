#+begin_src plantuml :file arbre_exo_129.png
@startuml
digraph calc {
  graph[nodesep=0.1, ranksep=0.35, splines=line];
  node [fontname = "Cousine", fontsize=14, shape=circle];

  // layout all nodes 1 row at a time
  // order matters on each line, but not the order of lines
  "5";
  "2" , am, bm "7";
  "cm", dm, "3" ;
   em, fm, gm;
  
  // make 'mid' nodes invisible
  am, bm, cm, dm, em, fm, gm, hm [style=invis, label=""];

  // layout all visible edges as parent -> left_child, right_child
  "5"-> "2", "7";
  "2"-> "3" ;

  // link mid nodes with a larger weight:
  edge [style=invis, weight=10];
  "5" -> am, bm;
  "2" -> cm, dm;
  "7" -> em, fm, gm;
}
@enduml
#+end_src

#+RESULTS:
[[file:arbre_exo_129.png]]
