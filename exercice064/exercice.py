import random

def lancer(n):
    """
    renvoie un tableau de type `list` de `n` entiers obtenus
    aléatoirement entre 1 et 6 (1 et 6 inclus)
    """
    return [random.randint(1, 6) for _ in range(n)]

def paire_6(tab):
    """
    renvoie un booléen égal à `True` si le
    nombre de 6 est supérieur ou égal à 2, `False` sinon
     - tab, un tableau de type `list` de `n` entiers entre 1 et 6
    obtenus aléatoirement,
    """
    nb = 0
    for elt in tab:
        if elt == 6:
            nb += 1
    if nb >= 2:
        return True
    else:
        return False
