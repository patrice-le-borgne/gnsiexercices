~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from unittest.mock import patch
>>> from exercice import *
~~~

Exercice
==========

Écrire en python deux fonctions :

- `lancer` de paramètre `n`, un entier positif, qui renvoie un tableau de type `list` de
`n` entiers obtenus aléatoirement entre 1 et 6 (1 et 6 inclus) ;

- `paire_6` de paramètre `tab`, un tableau de type `list` de `n` entiers entre 1 et
6 obtenus aléatoirement, qui renvoie un booléen égal à `True` si le nombre de 6
est supérieur ou égal à 2, `False` sinon.

On pourra utiliser la fonction `randint(a,b)` du module `random` pour laquelle la
documentation officielle est la suivante : `Renvoie un entier aléatoire N tel que a <=N <= b.`


**Démarche à suivre impérativement** : importer le module `random` puis appeler la fonction `random.randint`.


~~~ {.python}
    import random # pour importer le module random
    N = random.randint(1, 6) # pour lancer un dé
~~~


Exemples :

~~~ {.python .amc file="Q_1.md" bareme="3"}
    >>> lancer1 = lancer(5)
    >>> lancer1
    [5, 6, 6, 2, 2]
    >>> paire_6(lancer1)
    True
    >>> lancer2 = lancer(5)
    >>> lancer2
    [6, 5, 1, 6, 6]
    >>> paire_6(lancer2)
    True
    >>> lancer3 = lancer(3)
    >>> lancer3
    [2, 2, 6]
    >>> paire_6(lancer3)
    False
    >>> lancer4 = lancer(0)
    >>> lancer4
    []
    >>> paire_6(lancer4)
    False
~~~


~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> with patch('random.randint', side_effect = [5, 6, 6, 2, 2, 6, 5, 1, 6, 6, 2, 2, 6]):
...    lancer1 = lancer(5)
...    lancer2 = lancer(5)
...    lancer3 = lancer(3)
...    lancer4 = lancer(0)
...    print(lancer1)
...    print(lancer2)
...    print(lancer3)
...    print(lancer4)
[5, 6, 6, 2, 2]
[6, 5, 1, 6, 6]
[2, 2, 6]
[]
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> with patch('random.randint', side_effect = [5, 6, 6, 2, 2, 6, 5, 1, 6, 6, 2, 2, 6]):
...    lancer1 = lancer(5)
...    lancer2 = lancer(5)
...    lancer3 = lancer(3)
...    lancer4 = lancer(0)
...    print(paire_6(lancer1))
...    print(paire_6(lancer2))
True
True
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
>>> with patch('random.randint', side_effect = [5, 6, 6, 2, 2, 6, 5, 1, 6, 6, 2, 2, 6]):
...    lancer1 = lancer(5)
...    lancer2 = lancer(5)
...    lancer3 = lancer(3)
...    lancer4 = lancer(0)
...    print(paire_6(lancer3))
...    print(paire_6(lancer4))
False
False
~~~




