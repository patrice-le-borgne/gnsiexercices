    >>> from random import randint
    >>> from exercice import *

    >>> for _ in range(5):
    ...    n = randint(0, 100)
    ...    nbin = bin(n)[2:]
    ...    nsol = bin_to_dec(nbin)
    ...    assert n == nsol, f"{nsol} != {nbin}"

 
