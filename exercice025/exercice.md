
~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : structures linéaires
thème : conversion
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Pour rappel, la conversion d’un nombre entier positif en binaire peut s’effectuer à l’aide
des divisions successives comme illustré ici :

![image](img21_2_ep2021.png){: .center}

Voici une fonction Python basée sur la méthode des divisions successives permettant de
convertir un nombre entier positif en binaire :

~~~ {.python}
def binaire(a):
    bin_a = str(...)
    a = a // 2
    while a ... :
        bin_a = ...(a%2) + ...
        a = ...
    return bin_a
~~~

Compléter la fonction binaire.

Exemples :

~~~ {.python .amc file="Q_1.md" bareme="4"}
    >>> binaire(0)
    '0'
    >>> binaire(77)
    '1001101'
~~~


~~~ {.python .hidden .test file="Q_1.md" bareme="2"}
    >>> binaire(0)
    '0'
    >>> binaire(77)
    '1001101'
~~~


~~~ {.python .hidden .test file="Q_2.md" bareme="2"}
    >>> binaire(255)
    '11111111'
    >>> binaire(256)
    '100000000'
~~~
