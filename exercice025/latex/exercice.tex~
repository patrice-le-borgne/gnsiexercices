\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Pour rappel, la conversion d'un nombre entier positif en binaire peut
s'effectuer à l'aide des divisions successives comme illustré ici :

\includegraphics{img21_2_ep2021.png}\{: .center\}

Voici une fonction Python basée sur la méthode des divisions successives
permettant de convertir un nombre entier positif en binaire :

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ binaire(a):}
\NormalTok{    bin_a }\OperatorTok{=} \BuiltInTok{str}\NormalTok{(...)}
\NormalTok{    a }\OperatorTok{=}\NormalTok{ a }\OperatorTok{//} \DecValTok{2}
    \ControlFlowTok{while}\NormalTok{ a ... :}
\NormalTok{        bin_a }\OperatorTok{=}\NormalTok{ ...(a}\OperatorTok{%}\DecValTok{2}\NormalTok{) }\OperatorTok{+}\NormalTok{ ...}
\NormalTok{        a }\OperatorTok{=}\NormalTok{ ...}
    \ControlFlowTok{return}\NormalTok{ bin_a}
\end{Highlighting}
\end{Shaded}

Compléter la fonction binaire.

Exemples :

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ binaire(}\DecValTok{0}\NormalTok{)}
    \CommentTok{'0'}
    \OperatorTok{>>>}\NormalTok{ binaire(}\DecValTok{77}\NormalTok{)}
    \CommentTok{'1001101'}
\end{Highlighting}
\end{Shaded}

