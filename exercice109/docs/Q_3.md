    >>> from exercice import *

    >>> lab = Labyrinthe(3, 4)
    >>> lab.creer_passage(1,1,2,1)
    >>> lab.grille[1][1].murs["S"]
    False
    >>> lab.grille[2][1].murs["N"]
    False
    >>> lab.grille[1][1].murs["E"]
    True
    >>> lab.grille[1][1].murs["O"]
    True
    >>> lab.grille[2][1].murs["E"]
    True
    >>> lab.grille[2][1].murs["O"]
    True

 
