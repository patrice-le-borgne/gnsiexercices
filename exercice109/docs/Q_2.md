    >>> from exercice import *

    >>> lab = Labyrinthe(3, 4)
    >>> len(lab.grille)
    3
    >>> for i in range(len(lab.grille)):
    ...     assert len(lab.grille[i]) == 4
    >>> for i in range(len(lab.grille)):
    ...    for j in range(len(lab.grille[0])):
    ...        assert lab.grille[i][j].murs["N"] == True
    ...        assert lab.grille[i][j].murs["E"] == True
    ...        assert lab.grille[i][j].murs["S"] == True
    ...        assert lab.grille[i][j].murs["O"] == True

 
