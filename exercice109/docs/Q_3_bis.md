    >>> from exercice import *

    >>> lab = Labyrinthe(3, 4)
    >>> lab.creer_passage(2,1,1,1)
    >>> lab.grille[1][1].murs["S"]
    False
    >>> lab.grille[2][1].murs["N"]
    False
    >>> lab.grille[1][1].murs["E"]
    True
    >>> lab.grille[1][1].murs["O"]
    True
    >>> lab.grille[2][1].murs["E"]
    True
    >>> lab.grille[2][1].murs["O"]
    True
    >>> lab.creer_passage(2,3,2,2)
    >>> lab.grille[2][2].murs["E"]
    False
    >>> lab.grille[2][3].murs["O"]
    False
    >>> lab.grille[2][2].murs["N"]
    True
    >>> lab.grille[2][2].murs["S"]
    True
    >>> lab.grille[2][3].murs["N"]
    True
    >>> lab.grille[2][3].murs["S"]
    True

 
