~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : diviser_pour_regner
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========


Un labyrinthe est composé de cellules possédant chacune quatre murs (voir ci-dessous). La cellule en haut à gauche du labyrinthe est de coordonnées (0, 0).

![Le labyrinthe](lab.png){ .autolight width=75%}

On définit la classe `Cellule` ci-dessous. Le constructeur possède un attribut murs de type `#!py dict` dont les clés sont `'N'`, `'E'`, `'S'` et `'O'` et dont les valeurs sont des booléens (`#!py True` si le mur est présent et `#!py False` sinon).

~~~ {.python}
class Cellule:
    def __init__(self, murNord, murEst, murSud, murOuest):
        self.murs = {
            "N": murNord,
            "E": murEst,
            "S": murSud,
            "O": murOuest
            }
~~~

1. Recopier et compléter sur la copie l'instruction Python suivante
   permettant de créer une instance `cellule1` de la classe `Cellule`
   possédant tous ses murs sauf le mur Est.

~~~ {.python}
cellule1 = Cellule(...)
~~~

~~~ {.python .hidden .test .amc file="Q_1.md" bareme="1"}
>>> cellule1.murs["N"]
True
>>> cellule1.murs["E"]
False
>>> cellule1.murs["S"]
True
>>> cellule1.murs["O"]
True
~~~


2.Le constructeur de la classe `Labyrinthe` ci-dessous possède un seul attribut `grille`.

Cette grille est un tableau à deux dimensions `hauteur` et `largeur` contenant des cellules possédant chacune ses quatre murs.

Recopier et compléter sur la copie les lignes 4 à 8 de la classe `Labyrinthe`.

~~~ {.python linenums="1"}
class Labyrinthe:
    def __init__(self, hauteur, largeur):
        self.grille = []
        for i in range(...):
            ligne = []
            for j in range(...):
                cellule = ...
                ligne.append(...)
            self.grille.append(ligne)
~~~


~~~ {.python .hidden .test .amc file="Q_2.md" bareme="1"}
>>> lab = Labyrinthe(3, 4)
>>> len(lab.grille)
3
>>> for i in range(len(lab.grille)):
...     assert len(lab.grille[i]) == 4
>>> for i in range(len(lab.grille)):
...    for j in range(len(lab.grille[0])):
...        assert lab.grille[i][j].murs["N"] == True
...        assert lab.grille[i][j].murs["E"] == True
...        assert lab.grille[i][j].murs["S"] == True
...        assert lab.grille[i][j].murs["O"] == True
~~~

3. Pour générer un labyrinthe, on munit la classe `Labyrinthe` d'une
   méthode `creer_passage` permettant de supprimer des murs entre deux
   cellules ayant un côté commun afin de créer un passage.
   
   Cette méthode prend en paramètres les coordonnées `i1`, `j1` d'une
   cellule notée `cellule1` et les coordonnées `i2`, `j2` d'une
   cellule notée `cellule2` et crée un passage entre `cellule1` et
   `cellule2`.
   
   Recopier et compléter sur la copie le code Python suivant :

~~~ {.python linenums="10"}
def creer_passage(self, i1, j1, i2, j2):
        cellule1 = self.grille[i1][j1]
        cellule2 = self.grille[i2][j2]
        # cellule2 au Nord de cellule1
        if i1 - i2 == 1 and j1 == j2:
            cellule1.murs["N"] = False
            ...
            # cellule2 à l'Ouest de cellule1
        elif ...:
            ...
            ...
		...
~~~

~~~ {.python .amc file="Q_3.md" bareme="2"}
>>> pass
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
>>> lab = Labyrinthe(3, 4)
>>> lab.creer_passage(1,1,2,1)
>>> lab.grille[1][1].murs["S"]
False
>>> lab.grille[2][1].murs["N"]
False
>>> lab.grille[1][1].murs["E"]
True
>>> lab.grille[1][1].murs["O"]
True
>>> lab.grille[2][1].murs["E"]
True
>>> lab.grille[2][1].murs["O"]
True
~~~

~~~ {.python .hidden .test file="Q_3_bis.md" bareme="1"}
>>> lab = Labyrinthe(3, 4)
>>> lab.creer_passage(2,1,1,1)
>>> lab.grille[1][1].murs["S"]
False
>>> lab.grille[2][1].murs["N"]
False
>>> lab.grille[1][1].murs["E"]
True
>>> lab.grille[1][1].murs["O"]
True
>>> lab.grille[2][1].murs["E"]
True
>>> lab.grille[2][1].murs["O"]
True
>>> lab.creer_passage(2,3,2,2)
>>> lab.grille[2][2].murs["E"]
False
>>> lab.grille[2][3].murs["O"]
False
>>> lab.grille[2][2].murs["N"]
True
>>> lab.grille[2][2].murs["S"]
True
>>> lab.grille[2][3].murs["N"]
True
>>> lab.grille[2][3].murs["S"]
True
~~~

4. Pour créer un labyrinthe, on utilise la méthode diviser pour régner
   en appliquant récursivement l'algorithme `creer_labyrinthe` sur des
   sous-grilles obtenues en coupant la grille en deux puis en reliant
   les deux sous-labyrinthes en créant un passage entre eux.

   ![Création récursive](algo.png){ .autolight width=90%}

   Les cas de base correspondent à la situation où la grille est de
   hauteur 1 ou de largeur 1. Il suffit alors de supprimer tous les
   murs intérieurs de la grille.

   ![Cas de base](chemins.png){ .autolight width=40%}

   Recopier et compléter sur la copie les lignes 22 à 27 de la méthode
   `creer_labyrinthe` traitant le cas de base.

~~~ {.python linenums="21"}
def creer_labyrinthe(self, i, j, hauteur, largeur):
        if hauteur == 1:  # Cas de base
            for k in range(...):
                self.creer_passage(i, j + k, i, j + k + 1)
        elif largeur == 1:  # Cas de base
            for k in range(...):
                self.creer_passage(...)
        else:   # Appels récursifs
                # Code non étudié (Ne pas compléter)
~~~

~~~ {.python .hidden .test .amc file="Q_4.md" bareme="1"}
>>> lab = Labyrinthe(1, 4)
>>> for i in range(3):
...     assert lab.grille[0][i].murs["E"] == True
...     assert lab.grille[0][i+1].murs["O"] == True
>>> lab.creer_labyrinthe(0,0,1,4)
>>> for i in range(3):
...     assert lab.grille[0][i].murs["E"] == False
...     assert lab.grille[0][i+1].murs["O"] == False
>>> lab2 = Labyrinthe(5, 1)
>>> for i in range(4):
...     assert lab2.grille[i][0].murs["S"] == True
...     assert lab2.grille[i+1][0].murs["N"] == True
>>> lab2.creer_labyrinthe(0,0,5,1)
>>> for i in range(4):
...     assert lab2.grille[i][0].murs["S"] == False
...     assert lab2.grille[i+1][0].murs["N"] == False
~~~


5. Dans cette question, on considère une grille de hauteur `hauteur =
   4` et de longueur `largeur = 8` dont chaque cellule possède tous
   ses murs.

   On fixe les deux contraintes supplémentaires suivantes sur la
   méthode `creer_labyrinthe` :

   * Si `hauteur` est supérieure ou égale à `largeur`, on coupe
     horizontalement la grille en deux sous-labyrinthes de même
     dimension ;
   * Si `hauteur` est strictement inférieure à `largeur`, on coupe
     verticalement la grille en deux sous-labyrinthes de même
     dimension.

   L'ouverture du passage entre les deux sous-labyrinthes se fait le
   plus au Nord pour une coupe verticale et le plus à l'Ouest pour une
   coupe horizontale.

   Dessiner le labyrinthe obtenu suite à l'exécution complète de
   l'algorithme `creer_labyrinthe` sur cette grille.


~~~ {.python .hidden .test .amc file="Q_5.md" bareme="2"}
>>> reponse_5
'c'
~~~


