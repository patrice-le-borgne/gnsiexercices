~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : bases de données
thème : bases de données
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> import sqlite3 
>>> conn = sqlite3.connect('cooperative.sqlite')
>>> def initialisation():
...     """
...     Fonction qui vide la table des coureurs si elle existe puis en crée une nouvelle.
...     """
... 
...     curseur = conn.cursor()
... 
...     curseur.executescript("""
...     DROP TABLE IF EXISTS ModeleVelo;
...     CREATE TABLE `ModeleVelo` (
...     	`idModele` INT,
...     	`nomModele`	TEXT,
...     	`idFabriquant` INT,
...     	`Stock`	INT,
...     	`PrixUnitaire` INT	
...     );
...     INSERT INTO `ModeleVelo` (idModele,nomModele,idFabriquant,Stock,PrixUnitaire) VALUES (10,'Bovelo',3000,1,1990);
...     INSERT INTO `ModeleVelo` (idModele,nomModele,idFabriquant,Stock,PrixUnitaire) VALUES (11,'Jolivelo',3001,0,1990);
...     INSERT INTO `ModeleVelo` (idModele,nomModele,idFabriquant,Stock,PrixUnitaire) VALUES (12,'Rapidvelo',3002,7,1500);
...     DROP TABLE IF EXISTS Fabriquant;
...     CREATE TABLE "Fabriquant" (
...     	`idFabriquant`	INT,
...     	`nom`	TEXT,
...     	`adresse`	TEXT
...     );
...     INSERT INTO `Fabriquant` (idFabriquant,nom,adresse) VALUES (3000,'Raleygh','Londres');
...     INSERT INTO `Fabriquant` (idFabriquant,nom,adresse) VALUES (3001,'Mercier','Lyon');
...     INSERT INTO `Fabriquant` (idFabriquant,nom,adresse) VALUES (3002,'Peugeot','Sochaux');
...     DROP TABLE IF EXISTS Commande;
...     CREATE TABLE "Commande" (
...     	`numeroCommande`	INT,
...     	`idClient`	INT,
...     	`idModele`	INT,
...     	`date`	TEXT,
...     	`quantite`	INT,
...     	`totalCde`	INT
...     );
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (100,1,10,'2021-12-22',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (101,2,10,'2021-12-22',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (102,3,10,'2021-12-22',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (103,4,10,'2022-01-01',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (104,5,11,'2022-01-23',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (105,6,11,'2022-01-24',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (106,7,11,'2022-01-25',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (107,8,11,'2022-01-26',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (108,9,11,'2022-01-27',1,2100);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (109,10,12,'2022-01-28',1,1700);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (110,11,12,'2022-01-29',1,1700);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (111,12,12,'2022-01-30',1,1700);
...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (112,13,12,'2022-01-31',1,1700);
...     DROP TABLE IF EXISTS Client;
...     CREATE TABLE `Client` (
...     	`idClient` INT	,
...     	`nom`	TEXT,
...     	`prenom` TEXT	,
...     	`adresse`	TEXT
...     );
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (1,'DHENNIN','Anaelle','Brest');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (2,'LE BUREL','Léna','Brest');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (3,'PENNEC','Marion','Brest');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (4,'FRAPPAT','Marie','Brest');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (5,'LE MOAL','Manon','Brest');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (6,'PEDEMONTE','Margot','Brest');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (7,'CHABLE','Diane','Le conquet');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (8,'PETTON-PRIGENT','Lilou','Le conquet');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (9,'GEORGELIN','Aline','Le conquet');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (10,'LE DOARE','Alexia','Le conquet');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (11,'LE MAREC','Enora','Le conquet');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (12,'QUEMENER','Maelle','Le conquet');
...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (13,'TONDUT','Roger','Le conquet');
...     """)
...     conn.commit()

>>> def requete_correcte(solution,proposition):
...     # Créer un cursor 
...     cur = conn.cursor() 
...     comparaison = "WITH TA AS ({}), TB AS ({}) SELECT * FROM TA EXCEPT SELECT * FROM TB UNION ALL SELECT * FROM TB EXCEPT SELECT * FROM TA;".format(solution, proposition)
...     # Exécution de la requete
...     try:
...         resultat = cur.execute(comparaison) 
...         row = cur.fetchone()
...         if row is None:
...             rc = True
...         else:
...             rc = False
...     except Exception:
...         print("Le nombre de colonnes est incorrect !")
...         rc = False
...     # Envoyer la requete 
...     conn.commit()
...     # Fermer la connexion 
...     conn.close()
...     return rc

>>> solution_Q1 = "UPDATE ModeleVelo SET stock=0 WHERE nomModele='Bovelo'"
>>> solution_Q3a = "SELECT nomModele, idFabriquant FROM ModeleVelo WHERE stock = 0"
>>> solution_Q3b = "SELECT COUNT(*) FROM Commande WHERE date>='2022-01-01'"
>>> solution_Q3c = "SELECT DISTINCT f.nom FROM Fabriquant AS f JOIN ModeleVelo AS m ON m.idFabriquant=f.idFabriquant WHERE m.Stock > 0"
~~~

Exercice
========

Cet exercice porte sur les bases de données.
Une entreprise vend en ligne des vélos électriques. Afin de gérer le stock des vélos
disponibles à la vente, le gestionnaire du site a créé une base de données contenant les
quatre relations du schéma relationnel ci-dessous.

~~~ {.ditaa}
+-------------+               +------------------+
|Client       |		          | Commande         |
+-------------+		          +------------------+
|idClient INT |<------\       |numeroCommande INT|
|nom     CHAR |	      \-------+#idClient    INT  |
|prenom  CHAR |	      /-------+#idModele    INT  |
|adresse CHAR |	      |	      |date         DATE |
+-------------+	      |	      |quantite     INT  |
                      |	      |totalCde     INT  |
+------------------+  |	      +------------------+
|ModeleVelo        |  |
+------------------+  |	      +----------------+
|idModele   INT id |<-/	      |Fabriquant      |
|nomModele  CHAR   |  	      +----------------+
|#idFabricant INT  +--------->|idFabricant INT |
|Stock        INT  |	      |nom       CHAR  |
|prixUnitaire INT  |	      |adresse   CHAR  |
+------------------+	      +----------------+
~~~

Dans le schéma relationnel précédent, un attribut souligné indique qu’il s’agit d’une clé
primaire. Un attribut précédé du symbole # indique qu’il s’agit d’une clé étrangère et la
flèche associée indique l’attribut référencé. Ainsi, par exemple, l'attribut idFabricant de
la relation ModeleVelo est une clé étrangère qui fait référence à l'attribut idFabricant
de la relation Fabricant.

Dans la suite, les mots clés suivants du langage SQL pourront être utilisés dans les
requêtes : `SELECT`, `FROM`, `WHERE`, `JOIN`, `ON`, `DELETE`, `UPDATE`, `SET`,
`INSERT`, `AND`, `OR`.

Les fonctions d’agrégation `MIN(att)`, `MAX(att)` et `COUNT(att)` renvoient
respectivement la plus petite valeur, la plus grande valeur et le nombre d'enregistrements
de l’attribut att pour les enregistrements sélectionnés. Ainsi la requête `SELECT
MAX(totalCde) FROM Commande` renvoie la plus grande valeur de l'attribut totalCde
de la table Commande.

1.  Quand l’entreprise vend le dernier exemplaire en stock d’un modèle,
    l’attribut Stock de la table ModeleVelo du modèle correspondant
    doit être mis à jour pour contenir la valeur 0.  L’entreprise vient
    de vendre le dernier exemplaire en stock du modèle 'Bovelo'.
    Compléter la requête SQL suivante afin de mettre à jour la relation
    ModeleVelo : `UPDATE ... SET ... WHERE ...;`

~~~ {.python .hidden .test .amc file="Q_1.md" bareme="1"}
>>> initialisation()
>>> cur = conn.cursor() 
>>> r1 = cur.execute(prop_Q1)
>>> r2 = cur.execute("SELECT stock FROM Modelevelo WHERE nomModele='Bovelo'")
>>> resultat = cur.fetchone()
>>> resultat[0]
0
>>> conn.commit()
>>> conn.close()
~~~

2.  Ravel, un nouveau fabricant, a livré à l’entreprise 10 vélos du
    modèle 'Bovelo'. Parmi les cinq requêtes numérotées ci-dessous, en
    sélectionner deux qui permettent d'intégrer cette nouvelle
    information et indiquer l'ordre dans lequel elles doivent être
    saisies.
    
    ****Requête 1**** :
    `UPDATE ModeleVelo SET quantite=10 WHERE nomModele='Bovelo';`
    ****Requête 2**** :
    `INSERT INTO ModeleVelo VALUES (6298, 'Bovelo', 3127, 10, 1990);`
    ****Requête 3**** :
    `UPDATE ModeleVelo SET quantite=10 WHERE nomModele='Bovelo' AND nomFabricant='Ravel';`
    ****Requête 4**** :
    `INSERT INTO Fabricant VALUES (3127, 'Ravel', '89 cours de Vincennes, 75020 Paris');`
    ****Requête 5**** :
    `UPDATE ModeleVelo SET quantite=10 WHERE idFabricant=3127;`

~~~ {.python .hidden .test .amc file="Q_2.md" bareme="1"}
>>> reponse_Q2
'R4-R2'
~~~


3.  Écrire des requêtes SQL permettant d'obtenir les informations suivantes :
    a. les noms des modèles en rupture de stock et l'identifiant de leur fabricant respectif ;

~~~ {.python .hidden .test .amc file="Q_3_a.md" bareme="1"}
>>> assert requete_correcte(solution_Q3a, prop_Q3a)
~~~


	b. le nombre de commandes passées depuis le '2022-01-01' inclus.
    On précise que le type DATE permet de représenter une date par une
    chaîne de caractère au format 'AAAA-MM-JJ' et que ces dates peuvent
    être comparées à l’aide des opérateurs usuels (=, !=, >, <, <=, >=);
	
~~~ {.python .hidden .test .amc file="Q_3_b.md" bareme="1"}
>>> assert requete_correcte(solution_Q3b, prop_Q3b)
~~~
	
    c. les noms des fabricants dont le stock de vélos est strictement positif.

~~~ {.python .hidden .test .amc file="Q_3_c.md" bareme="1"}
>>> assert requete_correcte(solution_Q3c, prop_Q3c)
~~~

4.  Que permet d’obtenir la requête SQL suivante ?

~~~ {.sql}
    SELECT DISTINCT Client.nom FROM Client 
       JOIN Commande ON Client.idClient = Commande.idClient 
       JOIN ModeleVelo ON ModeleVelo.idModele = Commande.idModele 
       WHERE ModeleVelo.nomModele = 'Bovelo';
~~~

~~~ {.python .hidden .test .amc file="Q_4.md" bareme="1"}
>>> reponse_Q4
'a'
~~~

