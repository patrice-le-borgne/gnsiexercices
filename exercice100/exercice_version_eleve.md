Exercice
========

Cet exercice porte sur les bases de données. Une entreprise vend en
ligne des vélos électriques. Afin de gérer le stock des vélos
disponibles à la vente, le gestionnaire du site a créé une base de
données contenant les quatre relations du schéma relationnel ci-dessous.

``` {.ditaa}
+-------------+               +------------------+
|Client       |               | Commande         |
+-------------+               +------------------+
|idClient INT |<------\       |numeroCommande INT|
|nom     CHAR |       \-------+#idClient    INT  |
|prenom  CHAR |       /-------+#idModele    INT  |
|adresse CHAR |       |       |date         DATE |
+-------------+       |       |quantite     INT  |
                      |       |totalCde     INT  |
+------------------+  |       +------------------+
|ModeleVelo        |  |
+------------------+  |       +----------------+
|idModele   INT id |<-/       |Fabriquant      |
|nomModele  CHAR   |          +----------------+
|#idFabricant INT  +--------->|idFabricant INT |
|Stock        INT  |          |nom       CHAR  |
|prixUnitaire INT  |          |adresse   CHAR  |
+------------------+          +----------------+
```

Dans le schéma relationnel précédent, un attribut souligné indique qu'il
s'agit d'une clé primaire. Un attribut précédé du symbole \# indique
qu'il s'agit d'une clé étrangère et la flèche associée indique
l'attribut référencé. Ainsi, par exemple, l'attribut idFabricant de la
relation ModeleVelo est une clé étrangère qui fait référence à
l'attribut idFabricant de la relation Fabricant.

Dans la suite, les mots clés suivants du langage SQL pourront être
utilisés dans les requêtes : `SELECT`, `FROM`, `WHERE`, `JOIN`, `ON`,
`DELETE`, `UPDATE`, `SET`, `INSERT`, `AND`, `OR`.

Les fonctions d'agrégation `MIN(att)`, `MAX(att)` et `COUNT(att)`
renvoient respectivement la plus petite valeur, la plus grande valeur et
le nombre d'enregistrements de l'attribut att pour les enregistrements
sélectionnés. Ainsi la requête `SELECT MAX(totalCde) FROM Commande`
renvoie la plus grande valeur de l'attribut totalCde de la table
Commande.

1.  Quand l'entreprise vend le dernier exemplaire en stock d'un modèle,
    l'attribut Stock de la table ModeleVelo du modèle correspondant doit
    être mis à jour pour contenir la valeur 0. L'entreprise vient de
    vendre le dernier exemplaire en stock du modèle 'Bovelo'. Compléter
    la requête SQL suivante afin de mettre à jour la relation ModeleVelo
    : `UPDATE ... SET ... WHERE ...;`



2.  Ravel, un nouveau fabricant, a livré à l'entreprise 10 vélos du
    modèle 'Bovelo'. Parmi les cinq requêtes numérotées ci-dessous, en
    sélectionner deux qui permettent d'intégrer cette nouvelle
    information et indiquer l'ordre dans lequel elles doivent être
    saisies.

    **Requête 1** :
    `UPDATE ModeleVelo SET quantite=10 WHERE nomModele='Bovelo';`
    **Requête 2** :
    `INSERT INTO ModeleVelo VALUES (6298, 'Bovelo', 3127, 10, 1990);`
    **Requête 3** :
    `UPDATE ModeleVelo SET quantite=10 WHERE nomModele='Bovelo' AND nomFabricant='Ravel';`
    **Requête 4** :
    `INSERT INTO Fabricant VALUES (3127, 'Ravel', '89 cours de Vincennes, 75020 Paris');`
    **Requête 5** :
    `UPDATE ModeleVelo SET quantite=10 WHERE idFabricant=3127;`


3.  Écrire des requêtes SQL permettant d'obtenir les informations
    suivantes :
    a.  les noms des modèles en rupture de stock et l'identifiant de
        leur fabricant respectif ;
    
    b. le nombre de commandes passées depuis le '2022-01-01' inclus.
    On précise que le type DATE permet de représenter une date par une
    chaîne de caractère au format 'AAAA-MM-JJ' et que ces dates peuvent
    être comparées à l’aide des opérateurs usuels (=, !=, >, <, <=, >=);
    
    c. les noms des fabricants dont le stock de vélos est strictement positif.
    

4.  Que permet d'obtenir la requête SQL suivante ?

``` {.sql}
    SELECT DISTINCT Client.nom FROM Client 
       JOIN Commande ON Client.idClient = Commande.idClient 
       JOIN ModeleVelo ON ModeleVelo.idModele = Commande.idModele 
       WHERE ModeleVelo.nomModele = 'Bovelo';
```
