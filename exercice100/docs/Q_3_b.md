    >>> from exercice import *
    >>> import sqlite3 
    >>> conn = sqlite3.connect('cooperative.sqlite')
    >>> def initialisation():
    ...     """
    ...     Fonction qui vide la table des coureurs si elle existe puis en crée une nouvelle.
    ...     """
    ... 
    ...     curseur = conn.cursor()
    ... 
    ...     curseur.executescript("""
    ...     DROP TABLE IF EXISTS ModeleVelo;
    ...     CREATE TABLE `ModeleVelo` (
    ...         `idModele` INT,
    ...         `nomModele` TEXT,
    ...         `idFabriquant` INT,
    ...         `Stock` INT,
    ...         `PrixUnitaire` INT  
    ...     );
    ...     INSERT INTO `ModeleVelo` (idModele,nomModele,idFabriquant,Stock,PrixUnitaire) VALUES (10,'Bovelo',3000,1,1990);
    ...     INSERT INTO `ModeleVelo` (idModele,nomModele,idFabriquant,Stock,PrixUnitaire) VALUES (11,'Jolivelo',3001,0,1990);
    ...     INSERT INTO `ModeleVelo` (idModele,nomModele,idFabriquant,Stock,PrixUnitaire) VALUES (12,'Rapidvelo',3002,7,1500);
    ...     DROP TABLE IF EXISTS Fabriquant;
    ...     CREATE TABLE "Fabriquant" (
    ...         `idFabriquant`  INT,
    ...         `nom`   TEXT,
    ...         `adresse`   TEXT
    ...     );
    ...     INSERT INTO `Fabriquant` (idFabriquant,nom,adresse) VALUES (3000,'Raleygh','Londres');
    ...     INSERT INTO `Fabriquant` (idFabriquant,nom,adresse) VALUES (3001,'Mercier','Lyon');
    ...     INSERT INTO `Fabriquant` (idFabriquant,nom,adresse) VALUES (3002,'Peugeot','Sochaux');
    ...     DROP TABLE IF EXISTS Commande;
    ...     CREATE TABLE "Commande" (
    ...         `numeroCommande`    INT,
    ...         `idClient`  INT,
    ...         `idModele`  INT,
    ...         `date`  TEXT,
    ...         `quantite`  INT,
    ...         `totalCde`  INT
    ...     );
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (100,1,10,'2021-12-22',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (101,2,10,'2021-12-22',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (102,3,10,'2021-12-22',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (103,4,10,'2022-01-01',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (104,5,11,'2022-01-23',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (105,6,11,'2022-01-24',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (106,7,11,'2022-01-25',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (107,8,11,'2022-01-26',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (108,9,11,'2022-01-27',1,2100);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (109,10,12,'2022-01-28',1,1700);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (110,11,12,'2022-01-29',1,1700);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (111,12,12,'2022-01-30',1,1700);
    ...     INSERT INTO `Commande` (numeroCommande,idClient,idModele,date,quantite,totalCde) VALUES (112,13,12,'2022-01-31',1,1700);
    ...     DROP TABLE IF EXISTS Client;
    ...     CREATE TABLE `Client` (
    ...         `idClient` INT  ,
    ...         `nom`   TEXT,
    ...         `prenom` TEXT   ,
    ...         `adresse`   TEXT
    ...     );
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (1,'DHENNIN','Anaelle','Brest');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (2,'LE BUREL','Léna','Brest');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (3,'PENNEC','Marion','Brest');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (4,'FRAPPAT','Marie','Brest');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (5,'LE MOAL','Manon','Brest');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (6,'PEDEMONTE','Margot','Brest');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (7,'CHABLE','Diane','Le conquet');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (8,'PETTON-PRIGENT','Lilou','Le conquet');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (9,'GEORGELIN','Aline','Le conquet');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (10,'LE DOARE','Alexia','Le conquet');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (11,'LE MAREC','Enora','Le conquet');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (12,'QUEMENER','Maelle','Le conquet');
    ...     INSERT INTO `Client` (idClient,nom,prenom,adresse) VALUES (13,'TONDUT','Roger','Le conquet');
    ...     """)
    ...     conn.commit()

    >>> def requete_correcte(solution,proposition):
    ...     # Créer un cursor 
    ...     cur = conn.cursor() 
    ...     comparaison = "WITH TA AS ({}), TB AS ({}) SELECT * FROM TA EXCEPT SELECT * FROM TB UNION ALL SELECT * FROM TB EXCEPT SELECT * FROM TA;".format(solution, proposition)
    ...     # Exécution de la requete
    ...     try:
    ...         resultat = cur.execute(comparaison) 
    ...         row = cur.fetchone()
    ...         if row is None:
    ...             rc = True
    ...         else:
    ...             rc = False
    ...     except Exception:
    ...         print("Le nombre de colonnes est incorrect !")
    ...         rc = False
    ...     # Envoyer la requete 
    ...     conn.commit()
    ...     # Fermer la connexion 
    ...     conn.close()
    ...     return rc

    >>> solution_Q1 = "UPDATE ModeleVelo SET stock=0 WHERE nomModele='Bovelo'"
    >>> solution_Q3a = "SELECT nomModele, idFabriquant FROM ModeleVelo WHERE stock = 0"
    >>> solution_Q3b = "SELECT COUNT(*) FROM Commande WHERE date>='2022-01-01'"
    >>> solution_Q3c = "SELECT DISTINCT f.nom FROM Fabriquant AS f JOIN ModeleVelo AS m ON m.idFabriquant=f.idFabriquant WHERE m.Stock > 0"

    >>> assert requete_correcte(solution_Q3b, prop_Q3b)

 
