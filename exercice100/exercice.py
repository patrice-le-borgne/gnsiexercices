# Question 1
prop_Q1 = "UPDATE ModeleVelo SET stock=0 WHERE nomModele='Bovelo'"

# Question 2
reponse_Q2 = "R4-R2"

# Question 3.a.
prop_Q3a = "SELECT nomModele, idFabriquant FROM ModeleVelo WHERE stock = 0"

# Question 3.b.
prop_Q3b = "SELECT COUNT(*) FROM Commande WHERE date>='2022-01-01'"

# Question 3.c.
prop_Q3c = "SELECT DISTINCT f.nom FROM Fabriquant AS f JOIN ModeleVelo AS m ON m.idFabriquant=f.idFabriquant WHERE m.Stock > 0"

# Question 4
reponse_Q4 = "a"
