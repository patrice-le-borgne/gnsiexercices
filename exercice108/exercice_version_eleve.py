from arbrebinaire import *

def fichier_vers_liste(fichier):
    """
    lit le fichier txt et range les lignes non vides 
    et qui ne sont pas des commentaires dans une liste
    """
    pass

def charger_dico_caracteres_speciaux(fichier):
    """
    Charge le fichier qui donne les associations
    caractere special -> caractere normal
    """
    pass

def corriger_mot(mot, association):
    """
    Ote tous les caracteres speciaux d'un mot en utilisant le dictionnaire
    des associations.
    """
    pass

def fichier_vers_arbre(fichier):
    """
    Transforme une liste de mots en trie tout en ôtant les
    caracteres spéciaux.
    """
    pass

def est_dans_dictionnaire(arbre_a_mots, mot , r = False) :
    """
    Précise si me mot est dans l'arbre à mot.
    """
    pass

def est_une_racine(arbre_a_mots, racine):
    """
    Précise si racine est la racine d'un mot.
    """
    return arbre_a_mots.est_racine(racine)
