from arbrebinaire import *

def fichier_vers_liste(fichier):
    """
    lit le fichier txt et range les lignes non vides 
    et qui ne sont pas des commentaires dans une liste
    """
    liste = []
    f = open(fichier, 'r')
    for ligne in f:
         if ligne[0] != "%" and ligne != "\n":
            liste.append(ligne[:-1])
    f.close()
    return liste


def charger_dico_caracteres_speciaux(fichier):
    """
    Charge le fichier qui donne les associations
    caractere special -> caractere normal
    """
    association = dict()
    f = open(fichier, "r")
    for ligne in f:
        ligne = ligne[:-1]
        liste = ligne.split(',')
        association[liste[0]] = liste[1]
    f.close()
    return association


def corriger_mot(mot, association):
    """
    Ote tous les caracteres speciaux d'un mot en utilisant le dictionnaire
    des associations.
    """
    listeCS = association.keys()
    nouveau_mot = ""
    for lettre in mot:
        if lettre in listeCS:
            nouveau_mot += association[lettre]
        else:
            nouveau_mot += lettre
    return nouveau_mot


def fichier_vers_arbre(fichier):
    """
    Transforme une liste de mots en trie tout en ôtant les
    caracteres spéciaux.
    """
    dico_cs = charger_dico_caracteres_speciaux("listeAccents.txt")
    lm = fichier_vers_liste(fichier)
    arbre = ArbreBinaire()
    for i in lm:
        arbre.ajoute(corriger_mot(i, dico_cs))
    return arbre

def est_dans_dictionnaire(arbre_a_mots, mot , r = False) :
    return arbre_a_mots.cherche_mot(mot)
    """
    resultat = False
    if arbre_a_mots is not None:
        if mot == "" :
            if arbre_a_mots.noeud.fin:
                r = True
        else :
            if mot[0] in arbre_a_mots.dictFils.keys():
                r =  est_dans_dictionnaire(arbre_a_mots.dictFils[mot[0]], mot[1:], r)
            else :
                r = False
    resultat = resultat or r
    return resultat
    """
    
def est_une_racine(arbre_a_mots, racine):
    """
    Précise si racine est la racine d'un mot.
    """
    return arbre_a_mots.est_racine(racine)
