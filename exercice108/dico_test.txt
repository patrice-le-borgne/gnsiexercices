%  6. Ce copyright s'applique obligatoirement à toute amélioration
%     par simple correction d'erreurs ou d'oublis mineurs (orthographe,
%     phrase manquante, ...), c'est-à-dire ne correspondant pas à
%     l'adjonction d'une autre variante connue du texte, qui devra donc
%     comporter la présente notice.
%  ----------------------- FIN DE LA LICENCE ABU --------------------------------

a
à
abaca
abacule
abaissa
abaissable
abaissai
abaissaient
abaissais
abaissait
abaissâmes
abaissant
abaissant
abaissante
