Exercice
========

Dans le cadre du Ruzzle, on veut charger dans un arbre de binaire un
dictionnaire de la langue française afin de rechercher l'existence d'un
mot le plus rapidement possible. Le dictionnaire est le fichier
`dico.txt`. Le jeu affichera les lettres sans accents. Nous allons donc
les retirer avant d'ajouter les mots à l'arbre de recherche.

*Pour pouvoir effectuer les tests avec un fichier court, on dispose du
fichier `dico_test.txt`.*

1.  Écrire une fonction `fichier_vers_liste(fichier)` qui lit le
    `fichier` texte qui contient le dictionnaire et renvoie une liste
    contenant les mots. Les lignes vides et celles commençant par un "%"
    doivent être ignorées.

``` {.python}
>>> liste_mots = fichier_vers_liste('dico_test.txt')
>>> len(liste_mots)
14
>>> liste_mots[0]
'a'
>>> liste_mots[-1]
'abaissante'
```

2.  Écrire une fonction `charger_dico_caracteres_speciaux(fichier)` qui
    charge le `fichier` qui donne les associations
    `caractere special -> caractere normal` et le renvoie sous forme de
    dictionnaire.

``` {.python}
>>> d = charger_dico_caracteres_speciaux("listeAccents.txt")
>>> d['à']
'a'
>>> d['ç']
'c'
```

3.  Écrire une fonction `corriger_mot(mot, association)` qui renvoie le
    `mot` sans caractère spécial. `association` est un dictionaire qui
    les associations `caractere special -> caractere normal`.

``` {.python}
>>> dico_cs = charger_dico_caracteres_speciaux("listeAccents.txt")
>>> corriger_mot("àbaçêtéèï",dico_cs)
'abaceteei'
```

4.  Écrire une fonction `fichier_vers_arbre(fichier)` qui range le
    `fichier`contenant de mots dans un arbre tout en ôtant les
    caracteres spéciaux.

``` {.python}
>>> arbre = fichier_vers_arbre('dico_test.txt')
>>> arbre.afficher_arbre()
    a
       b
          a
                i
                   s
                      s
                         a
                                     n
                                        t
                                           e
                                  m
                                     e
                                        s
                               i
                                        t
                                     s
                                  e
                                     n
                                        t
                            b
                               l
                                  e
             c
                   u
                      l
                         e
                a
```

5.  Écrire une fonction
    `est_dans_dictionnaire(arbre_a_mots, mot , r = False)` qui teste si
    `mot` est présent dans l'`arbre_a_mots`.

``` {.python}
>>> arbre = fichier_vers_arbre('dico_test.txt')
>>> est_dans_dictionnaire(arbre, 'abaca')
True
>>> est_dans_dictionnaire(arbre, 'abaissa')
True
>>> est_dans_dictionnaire(arbre, 'abaissable')
True
>>> est_dans_dictionnaire(arbre, 'abaissai')
True
>>> est_dans_dictionnaire(arbre, 'abaissaient')
True
>>> est_dans_dictionnaire(arbre, 'abaissait')
True
>>> est_dans_dictionnaire(arbre, 'dictionnaire')
False
```
