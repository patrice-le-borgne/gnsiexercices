~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : bases de données
thème : cours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

*Cet exercice porte sur les bases de données et le langage SQL.*

L’énoncé de cet exercice utilise les mots du langage SQL suivants :
`SELECT FROM, WHERE, JOIN ON, INSERT INTO VALUES, UPDATE, SET, DELETE, COUNT, AND,OR.`

Pour la gestion des réservations clients, on dispose d’une base de données nommée « `gare` » dont le schéma relationnel est le suivant :

~~~
Train (numT, provenance, destination, horaireArrivee, horaireDepart)
Reservation (numR, nomClient, prenomClient, prix, #numT)
~~~

Les attributs `numT` et `numR` sont respectivement les clés primaires des tables `Train` et `Reservation`. L’attribut précédé de `#` est une clé étrangère. La clé étrangère `Reservation.numT` fait référence à la clé primaire `Train.numT`.

Les attributs `horaireDepart` et `horaireArrivee` sont de type `TIME` et s’écrivent selon le format `"hh:mm"`, où `hh` représente les heures et `mm` les minutes.

~~~ {.python .hidden .amc file="Q_1.md" bareme="1"}
>>> pass
~~~


1. Quel nom générique donne-t-on aux logiciels qui assurent, entre autres, la persistance des données, l’efficacité de traitement des requêtes et la sécurisation des accès pour les bases de données ?


~~~ {.python .hidden .amc file="Q_2.md" bareme="2"}
>>> pass
~~~

2. a. On considère les requêtes SQL suivantes :

~~~ {.sql}
    DELETE FROM Train WHERE numT = 1241 ;
    DELETE FROM Reservation WHERE numT = 1241 ;
~~~

   Sachant que le train n°1241 a été enregistré dans la table `Train` et que des réservations
   pour ce train ont été enregistrées dans la table `Reservation`, expliquer pourquoi cette
   suite d’instructions renvoie une erreur.
   
   b. Citer un cas pour lequel l’insertion d’un enregistrement dans la table `Reservation` n’est pas possible.

~~~ {.python .hidden .amc file="Q_3.md" bareme="2"}
>>> pass
~~~

3. Écrire des requêtes SQL correspondant à chacune des instructions suivantes :
   a. Donner tous les numéros des trains dont la destination est « Lyon ».
   b. Ajouter une réservation n°1307 de 33 € pour M. Alan Turing dans le train n°654.
   c. Suite à un changement, l’horaire d’arrivée du train n°7869 est programmé à 08 h 11. Mettre à jour la base de données en conséquence.

~~~ {.python .hidden .amc file="Q_4.md" bareme="1"}
>>> pass
~~~

4. Que permet de déterminer la requête suivante ?

~~~ {.sql}
    SELECT COUNT(*) FROM Reservation
    WHERE nomClient = "Hopper" AND prenomClient = "Grace";
~~~

~~~ {.python .hidden .amc file="Q_5.md" bareme="2"}
>>> pass
~~~

5. Écrire la requête qui renvoie les destinations et les prix des réservations effectuées par Grace Hopper.
