# Exercice

## Q1

SGBDR : Système de Gestion des Bases de Données Relationnel

## Q2

### Q2.a

La première requête efface de la table Train le train n°1241. Or la deuxième requête fait référence
(Reservation.numT=1241) à une entrée qui n’existe plus dans la base Train.

### Q2.b

Si un ° de train n’existe pas, Il est impossible d’effectuer une réservation pour ce train.

## Q3

### Q3.a

~~~ {.sql}
    SELECT numT
    FROM Train
    WHERE Destination = ‘Lyon’
    ORDER BY numT
~~~

### Q3.b

~~~ {.sql}
    INSERT INTO Reservation VALUES(1307, 'Turing', 'Alan', 33, 7869)
~~~

### Q3.c

~~~ {.sql}
    UPDATE Train SET horaireArrivee = ‘08:11’ WHERE numT = 7869
~~~

## Q4

Dénombre les réservations faites au nom de Grace Hopper

## Q5

~~~ {.sql}
    SELECT Train.destination, Reservation.prix
    FROM Train, Reservation
    WHERE Train.nomClient = 'Hopper' AND Train.prenomClient = 'Grace'
    AND Train.numT = Reservation.numT
    ORDER BY Train.destination
~~~
