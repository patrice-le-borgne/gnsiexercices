\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

\emph{Cet exercice porte sur les bases de données et le langage SQL.}

L'énoncé de cet exercice utilise les mots du langage SQL suivants :
\texttt{SELECT\ FROM,\ WHERE,\ JOIN\ ON,\ INSERT\ INTO\ VALUES,\ UPDATE,\ SET,\ DELETE,\ COUNT,\ AND,OR.}

Pour la gestion des réservations clients, on dispose d'une base de
données nommée « \texttt{gare} » dont le schéma relationnel est le
suivant :

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{Train (numT, provenance, destination, horaireArrivee, horaireDepart)}
\NormalTok{Reservation (numR, nomClient, prenomClient, prix, }\CommentTok{#numT)}
\end{Highlighting}
\end{Shaded}

Les attributs \texttt{numT} et \texttt{numR} sont respectivement les
clés primaires des tables \texttt{Train} et \texttt{Reservation}.
L'attribut précédé de \texttt{\#} est une clé étrangère. La clé
étrangère \texttt{Reservation.numT} fait référence à la clé primaire
\texttt{Train.numT}.

Les attributs \texttt{horaireDepart} et \texttt{horaireArrivee} sont de
type \texttt{TIME} et s'écrivent selon le format \texttt{"hh:mm"}, où
\texttt{hh} représente les heures et \texttt{mm} les minutes.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Quel nom générique donne-t-on aux logiciels qui assurent, entre
  autres, la persistance des données, l'efficacité de traitement des
  requêtes et la sécurisation des accès pour les bases de données ?
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\item
  \begin{enumerate}
  \def\labelenumii{\alph{enumii}.}
  \tightlist
  \item
    On considère les requêtes SQL suivantes :
  \end{enumerate}
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{    DELETE FROM Train WHERE numT }\OperatorTok{=} \DecValTok{1241} \OperatorTok{;}
\NormalTok{    DELETE FROM Reservation WHERE numT }\OperatorTok{=} \DecValTok{1241} \OperatorTok{;}
\end{Highlighting}
\end{Shaded}

Sachant que le train n°1241 a été enregistré dans la table
\texttt{Train} et que des réservations pour ce train ont été
enregistrées dans la table \texttt{Reservation}, expliquer pourquoi
cette suite d'instructions renvoie une erreur.

\begin{enumerate}
\def\labelenumi{\alph{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Citer un cas pour lequel l'insertion d'un enregistrement dans la table
  \texttt{Reservation} n'est pas possible.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  Écrire des requêtes SQL correspondant à chacune des instructions
  suivantes :

  \begin{enumerate}
  \def\labelenumii{\alph{enumii}.}
  \tightlist
  \item
    Donner tous les numéros des trains dont la destination est « Lyon ».
  \item
    Ajouter une réservation n°1307 de 33 € pour M. Alan Turing dans le
    train n°654.
  \item
    Suite à un changement, l'horaire d'arrivée du train n°7869 est
    programmé à 08 h 11. Mettre à jour la base de données en
    conséquence.
  \end{enumerate}
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  Que permet de déterminer la requête suivante ?
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{    SELECT COUNT(}\OperatorTok{*}\NormalTok{) FROM Reservation}
\NormalTok{    WHERE nomClient }\OperatorTok{=} \StringTok{"Hopper"}\NormalTok{ AND prenomClient }\OperatorTok{=} \StringTok{"Grace"}\OperatorTok{;}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Écrire la requête qui renvoie les destinations et les prix des
  réservations effectuées par Grace Hopper.
\end{enumerate}
