Exercice
========

Compléter le code de la fonction `g(x)` qui correspond à la fonction
$g(x) = -2x^2 -3x+ 1$.

``` {.python}
def g(x):
    return .........
```

**Exemple d'appels de la fonction**

``` {.python}
>>> g(0)
1
>>> g(3)
-26
>>> g(-1)
-4
```
