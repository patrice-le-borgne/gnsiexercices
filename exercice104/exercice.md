~~~ {.hidden .meta}
classe : seconde
type : oral2
chapitre : fonction
thème : fonction
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Compléter le code de la fonction `g(x)` qui correspond à la fonction $g(x) = -2x^2 -3x+ 1$.

~~~ {.python}
def g(x):
    return .........
~~~


**Exemple d'appels de la fonction**

~~~ {.python .test .amc file="Q_1.md" bareme="1"}
>>> g(0)
1
>>> g(3)
-26
>>> g(-1)
2
~~~
