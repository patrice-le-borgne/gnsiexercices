~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : moyenne
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Programmer la fonction `moyenne` prenant en paramètre un tableau d'entiers `tab` (type `list`) qui renvoie la moyenne de ses éléments si le tableau est non vide et affiche `'erreur'` si le tableau est vide.

Exemples :

~~~ {.python .hidden .amc file="Q_1.md" bareme="4"}
>>> pass 
~~~

~~~ {.python .test file="Q_1.md" bareme="2"}
    >>> moyenne([5, 3, 8])
    5.333333333333333
    >>> moyenne([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    5.5
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
    >>> moyenne([1, 2, 3, -3, -2, -1])
    0.0
~~~


~~~ {.python .test file="Q_3.md" bareme="1"}
    >>> moyenne([])
    'erreur'
~~~
