``` {.python}
classe : terminale
type : écrit
chapitre : structures linéaires
thème : parcours
```

``` {.python}
>>> from exercice import *
```

Exercice
========

On dispose de la liste jours suivante et du dictionnaire mois suivant :

``` {.python}
jours=["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]

mois={1 :("janvier",31), 
      2 :("février",28),
      3 :("mars",31), 
      4 :("avril",30) , 
      5 :("mai",31), 
      6 :("juin",30),
      7 :("juillet",31),
      8 :("aout",31),
      9 :("septembre",30),
      10 :("octobre",31),
      11 :("novembre",30),
      12 :("décembre",31)}
```

1.  2.  À partir de la liste jours, comment obtenir l'élément 'lundi' ?

<!-- -->

1.  Que renvoie l'instruction `jours[18%7]` ?

2.  Écrire le code de la fonction `n_jours_plus_tard(liste_jours, x, n)`
    qui renvoie le jour situé `n` jours après le jour `x`.

Par exemple :

``` {.python}
>>> n_jours_plus_tard(jours, 1, 3)
'jeudi'
>>> n_jours_plus_tard(jours, 3, 9)
'vendredi'
```

3.  4.  À partir du dictionnaire `mois`, comment obtenir le nombre de
    jours du mois de mars ?

<!-- -->

2.  Écrire le code de la fonction
    `n_mois_plus_tard( dico_mois, numero_mois, n)` permettant d'obtenir
    le nom du mois qu'il sera `n` mois après le mois `numero_mois`.

``` {.python}
>>> n_mois_plus_tard( mois, 4, 5)
'septembre'
>>> n_mois_plus_tard( mois, 10, 3)
'janvier'
```

4.  On définit une date comme un tuple :
    `(nom_jour,numero_jour, numero_mois,annee)`.

5.  Sachant que `date = ("samedi", 21, 10, 1995)`, que renvoie
    `mois[date[2]][1]` ?

<!-- -->

2.  Écrire une fonction `jour_suivant(date)` qui prend en paramètre une
    date sous forme de tuple et qui renvoie un tuple désignant la date
    du lendemain.

*On ne tient pas compte des années bissextiles et on considère que le
mois de février comporte toujours 28 jours.*

Par exemple :

``` {.python}
>>>jour_suivant( ("samedi", 21, 10, 1995) )
("dimanche", 22, 10, 1995)
>>> jour_suivant( ("mardi", 31, 10, 1995) )
("mercredi", 1, 11, 1995)
```
