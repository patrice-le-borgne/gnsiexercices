\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

On souhaite programmer une fonction donnant la distance la plus courte
entre un point de départ et une liste de points. Les points sont tous à
coordonnées entières.

Les points sont donnés sous la forme d'un tuple de deux entiers. La
liste des points à traiter est donc un tableau de tuples. On rappelle
que la distance entre deux points du plan de coordonnées \((x ; y)\) et
\((x' ; y')\) est donnée par la formule :
\(d = \sqrt{(x − x')^2 + (y − y')^2}\).

On importe pour cela la fonction racine carrée (\texttt{sqrt}) du module
\texttt{math} de Python. On dispose d'une fonction \texttt{distance} et
d'une fonction \texttt{plus\_courte\_distance} :

\begin{Shaded}
\begin{Highlighting}[]
\ImportTok{from}\NormalTok{ math }\ImportTok{import}\NormalTok{ sqrt}

\CommentTok{# import de la fonction racine carrée}
\KeywordTok{def}\NormalTok{ distance(point1, point2):}
    \CommentTok{""" Calcule et renvoie la distance entre deux points. """}
    \ControlFlowTok{return}\NormalTok{ sqrt((...)}\OperatorTok{**}\DecValTok{2} \OperatorTok{+}\NormalTok{ (...)}\OperatorTok{**}\DecValTok{2}\NormalTok{)}

\KeywordTok{def}\NormalTok{ plus_courte_distance(tab, depart):}
    \CommentTok{""" Renvoie le point du tableau tab se trouvant à la plus}
\CommentTok{    courte distance du point depart."""}
\NormalTok{    point }\OperatorTok{=}\NormalTok{ tab[}\DecValTok{0}\NormalTok{]}
\NormalTok{    min_dist }\OperatorTok{=}\NormalTok{ ...}
    \ControlFlowTok{for}\NormalTok{ i }\KeywordTok{in} \BuiltInTok{range}\NormalTok{ (}\DecValTok{1}\NormalTok{, ...):}
        \ControlFlowTok{if}\NormalTok{ distance(tab[i], depart)...:}
\NormalTok{            point }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{            min_dist }\OperatorTok{=}\NormalTok{ ...}
    \ControlFlowTok{return}\NormalTok{ point}
\end{Highlighting}
\end{Shaded}

Voila un exemple de ce que doivent retourner les fonctions précédentes :

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ distance((}\DecValTok{1}\NormalTok{, }\DecValTok{0}\NormalTok{), (}\DecValTok{5}\NormalTok{, }\DecValTok{3}\NormalTok{)) }
    \FloatTok{5.0}
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ plus_courte_distance([(}\DecValTok{7}\NormalTok{, }\DecValTok{9}\NormalTok{), (}\DecValTok{2}\NormalTok{, }\DecValTok{5}\NormalTok{), (}\DecValTok{5}\NormalTok{, }\DecValTok{2}\NormalTok{)], (}\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{))}
\NormalTok{    (}\DecValTok{2}\NormalTok{, }\DecValTok{5}\NormalTok{)}
\end{Highlighting}
\end{Shaded}

Recopier sous Python (sans les commentaires) ces deux fonctions puis
compléter leur code et ajouter une ou des déclarations (assert) à la
fonction distance permettant de vérifier la ou les préconditions.
\textbf{Faites des assertions simples, sans rajouter de message
d'erreur.}
