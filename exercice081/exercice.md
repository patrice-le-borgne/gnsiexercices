~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : arbres
thème : récursivité
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
==========

Un arbre binaire est implémenté par la classe `Arbre` donnée ci-dessous.  
Les attributs `fg` et `fd` prennent pour valeurs des instances de la classe `Arbre` ou `None`.

~~~ {.python}
    class Arbre:
        def __init__(self, etiquette):
            self.v = etiquette
            self.fg = None
            self.fd = None
    
    def parcours(arbre, liste):
        if arbre != None:
            parcours(arbre.fg, liste)
            liste.append(arbre.v)
            parcours(arbre.fd, liste)
        return liste
~~~

La fonction récursive `parcours` renvoie la liste des étiquettes des
nœuds de l’arbre implémenté par l’instance arbre dans l’ordre du
parcours en profondeur infixe à partir d’une liste vide passée en
argument.

Compléter le code de la fonction `insere` qui insère un nœud d’étiquette
`cle` en feuille de l’arbre implémenté par l’instance `arbre` selon la
spécification indiquée et de façon que l’arbre ainsi complété soit
encore un arbre binaire de recherche.

Tester ensuite ce code en utilisant la fonction parcours et en
insérant successivement des nœuds d’étiquette 1, 4, 6 et 8 dans
l’arbre binaire de recherche représenté ci- dessous :

![arbre_binaire](arbre081.png)

~~~ {.python .amc file="Q_1.md" bareme="3"}
    def insere(arbre, cle):
        """ arbre est une instance de la classe Arbre qui implémente
            un arbre binaire de recherche.
        """
        if ...:
            if ...:
                insere(arbre.fg, cle)
            else:
                arbre.fg = Arbre(cle)
        else:
            if ...:
                insere(arbre.fd, cle)
            else:
                arbre.fd = Arbre(cle)
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> a = Arbre(5)
>>> insere(a, 2)
>>> parcours(a, [])
[2, 5]
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> a = Arbre(5)
>>> insere(a, 2)
>>> insere(a, 7)
>>> insere(a, 3)
>>> parcours(a, [])
[2, 3, 5, 7]
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="2"}
>>> a = Arbre(5)
>>> insere(a, 2)
>>> insere(a, 7)
>>> insere(a, 3)
>>> insere(a, 1)
>>> insere(a, 4)
>>> insere(a, 6)
>>> insere(a, 8)
>>> parcours(a, [])
[1, 2, 3, 4, 5, 6, 7, 8]
~~~
