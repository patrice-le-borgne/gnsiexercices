        >>> from exercice import *

        >>> lab1 = [[1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1],
        ... [1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
        ... [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
        ... [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
        ... [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
        ... [2, 0, 1, 0, 0, 0, 1, 0, 1, 0, 3],
        ... [1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1],
        ... [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
        ... [1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1],
        ... [1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1],
        ... [1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1]]

        >>> lab2 = [[1, 1, 1, 1, 1, 1, 1],
        ... [2, 0, 0, 0, 0, 0, 1],
        ... [1, 1, 1, 1, 1, 0, 1],
        ... [1, 0, 1, 0, 0, 0, 1],
        ... [1, 0, 1, 0, 1, 0, 1],
        ... [1, 0, 0, 0, 1, 0, 1],
        ... [1, 1, 1, 1, 1, 3, 1]]

        >>> lab3 = [[1, 1, 1, 1, 1, 1],
        ...         [2, 0, 0, 0, 0, 3],
        ...         [1, 0, 1, 0, 1, 1],
        ...         [1, 1, 1, 0, 0, 1]]

             >>> solution(lab2)
             [(1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5), (6, 5)]
             >>> solution(lab1)
             [(5, 0), (5, 1), (4, 1), (3, 1), (2, 1), (1, 1), (1, 2), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3), (5, 4), (5, 5), (4, 5), (3, 5), (2, 5), (1, 5), (0, 5), (0, 6), (0, 7), (0, 8), (0, 9), (1, 9), (2, 9), (3, 9), (4, 9), (5, 9), (5, 10)]

 
