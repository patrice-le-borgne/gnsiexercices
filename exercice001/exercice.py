"""
Correction de l'exercice sur les listes
"""
# pylint: disable=C0103
import logging
logger = logging.getLogger()


def est_valide(i: int, j: int, n: int, m: int) -> bool:
    """
    Vérifie que les indices pour accéder à ne case sont conformes.
    """
    return 0 <= i < n and 0 <= j < m


def depart(lab: list):
    """
    Retourne la case de départ du labyrinthe.
    """
    n = len(lab)
    m = len(lab[0])
    for i in range(n):
        for j in range(m):
            if lab[i][j] == 2:
                return (i, j)
    return None


def arrivee(lab: list) -> tuple:
    """
    Retourne la case de sortie du labyrinthe.
    """
    n = len(lab)
    m = len(lab[0])
    for i in range(n):
        for j in range(m):
            if lab[i][j] == 3:
                return (i, j)
    return None

def nb_cases_vides(lab: list) -> int:
    """
    Renvoie les nombres de cases vides (pas des murs) du labyrinthe.
    """
    n = len(lab)
    m = len(lab[0])
    s = 0
    for i in range(n):
        for j in range(m):
            if lab[i][j] != 1:
                s += 1
    return s


def voisines(i, j, lab):
    """
    Renvoie la liste des cases adjacents non visitées qu'ils restent à explorer.
    """
    positions_relatives = [(1, 0), (-1, 0), (0, -1), (0, 1)]
    l = []
    for k, m in positions_relatives:
        if est_valide(i + k, j + m, len(lab), len(lab[0])):
            if lab[i + k][j + m] not in (1, 4):
                l.append((i + k, j + m))
    return l


def solution(lab):
    """
    Renvoie le chemin menant du départ vers la sortie.
    """
    case_depart = depart(lab)
    logger.info("Case de départ : %s", case_depart)
    chemin = [case_depart]
    case_arrivee = arrivee(lab)
    logger.info("Case d'arrivée : %s", case_arrivee)
    while chemin[-1] != case_arrivee:
        i, j = chemin[-1]
        logger.info("Case de active : %s", (i, j))
        lab[i][j] = 4
        logger.info("\tOn déclare la case active comme visitée.")
        liste_des_voisines = voisines(i, j, lab)
        logger.info(
            "\tListe des cases voisine : %s", liste_des_voisines)
        if liste_des_voisines != []:
            case_a_visiter = liste_des_voisines.pop()
            logger.info("\tOn choisit la case %s.", case_a_visiter)
            chemin.append(case_a_visiter)
        else:
            logger.info("\tC'est une impasse. On supprime la case(%s, %s) du chemin", i, j)
            chemin.pop()

    return chemin


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    LAB2 = [[1, 1, 1, 1, 1, 1, 1],
            [2, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 0, 1],
            [1, 0, 1, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 1],
            [1, 0, 0, 0, 1, 0, 1],
            [1, 1, 1, 1, 1, 3, 1]]
    print(solution(LAB2))
    LAB1 = [[1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
            [2, 0, 1, 0, 0, 0, 1, 0, 1, 0, 3],
            [1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1],
            [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1],
            [1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1]]
    print(solution(LAB1))
