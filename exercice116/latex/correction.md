Correction
==========

1. La pyramide représentée par la liste de listes `ex2 = [[3],[1,2],[4,5,9],[3,6,2,1]]` est la suivante :

~~~ {.dot}
digraph G {
    trois[label="3"]
    trois -> {un_1[label="1"] deux_1[label="2"]}
    un_1 -> {quatre_2[label="4"] cinq_2[label="5"]}
    deux_1 -> {cinq_2 neuf_2[label="9"]}
    quatre_2 -> {trois_3[label="3"] six_3[label="6"]}
    cinq_2 -> {six_3 deux_3[label="2"]}
    neuf_2 -> {deux_3 un_3[label="1"]}
}
~~~

2. Un conduit de score de confiance maximal dans la pyramide `ex2` est celui suivant : `3 + 1 + 4 + 6 = 14`.

3. Pour la pyramide de trois niveaux représentée sur la figure 2, les conduits possibles sont :
   - Gauche-Gauche : 2 + 5 + 3 = 10
   - Gauche-Droite : 2 + 1 + 6 = 9
   - Droite-Gauche : 2 + 7 + 3 = 12
   - Droite-Droite : 2 + 7 + 2 = 11

4. Le nombre de conduits dans une pyramide de `n` niveaux est égal à $2^{n-1}$.

5. Tester tous les conduits possibles pour calculer le score de confiance maximal d'une pyramide n'est pas raisonnable car le nombre de conduits augmente exponentiellement avec le nombre de niveaux de la pyramide, rendant ce calcul peu efficace pour des pyramides de taille importante.

6. La fonction `score_max` peut être implémentée en Python de la manière suivante :

```python
def score_max(i, j, p):
    if i == len(p) - 1:
        return p[i][j]
    else:
        return p[i][j] + max(score_max(i+1, j, p), score_max(i+1, j+1, p))
```

7. La fonction `pyramide_nulle` qui construit une pyramide remplie de 0 à `n` niveaux peut être implémentée comme suit :

```python
def pyramide_nulle(n):
    return [[0] * (i+1) for i in range(n)]
```

8. La fonction `prog_dyn` complétée est la suivante :

```python
def prog_dyn(p):
    n = len(p)
    s = pyramide_nulle(n)
    
    for j in range(len(p[n-1])):
        s[n-1][j] = p[n-1][j]
    
    for i in range(n-2, -1, -1):
        for j in range(len(p[i])):
            s[i][j] = p[i][j] + max(s[i+1][j], s[i+1][j+1])
    
    return s[0][0]
```

9. Le coût d'exécution de la fonction `prog_dyn` est quadratique en
   `n` car chaque élément de la pyramide `s` est calculé une fois
   seulement, ce qui donne un coût proportionnel au nombre total de
   nombres dans la pyramide.

10. Pour adapter la fonction `score_max` afin d'éviter la redondance
    des calculs et obtenir un coût quadratique, on peut utiliser une
    approche de programmation dynamique en mémorisant les résultats
    intermédiaires. Une implémentation de cette approche permettrait
    de stocker les résultats déjà calculés dans une table pour éviter
    de les recalculer à chaque fois.
