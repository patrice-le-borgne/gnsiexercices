

#+begin_src dot :file arbre.png 
     digraph G {
     quatre[label="4"]
     quatre -> {six_1[label="6"] deux_1[label="2"]}
     six_1 -> {trois_2[label="3"] cinq_2[label="5"]} 
     deux_1 -> {cinq_2 sept_2[label="7"]} 
     trois_2 -> {cinq_3[label="5"] un_3[label="1"]}
     cinq_2 -> {un_3[label="1"] six_3[label="6"]}
     sept_2 -> {six_3 deux_3[label="2"]}
     cinq_3 -> {quatre_4[label="4"] sept_4[label="7"]}
     un_3 -> {sept_4 trois_4[label="3"]}
     six_3 ->{trois_4 cinq_4[label="5"] }
     deux_3 -> {cinq_4  deux_4[label="2"]}
     }
#+end_src

#+RESULTS:
[[file:arbre.png]]


#+begin_src dot :file arbre_2.png 
  digraph G {
  size ="4,5";
  quatre[label="4",style=filled,color="lightgray"]			
  quatre -> {six_1[label="6"] deux_1[label="2",style=filled,color="lightgray"]}
  six_1 -> {trois_2[label="3"] cinq_2[label="5",style=filled,color="lightgray"]} 
  deux_1 -> {cinq_2 sept_2[label="7"]} 
  trois_2 -> {cinq_3[label="5"] un_3[label="1",style=filled,color="lightgray"]}
  cinq_2 -> {un_3[label="1"] six_3[label="6"]}
  sept_2 -> {six_3 deux_3[label="2"]}
  cinq_3 -> {quatre_4[label="4"] sept_4[label="7"]}
  un_3 -> {sept_4 trois_4[label="3",style=filled,color="lightgray"]}
  six_3 ->{trois_4 cinq_4[label="5"] }
  deux_3 -> {cinq_4  deux_4[label="2"]}
  }
#+end_src

#+RESULTS:
[[file:arbre_2.png]]


#+begin_src dot :file arbre_3.png 
     digraph G {
     deux[label="2"]
     deux -> {cinq_1[label="5"] un_1[label="1"]}
     cinq_1 -> {deux_2[label="2"] trois_2[label="3"]} 
     un_1 -> {trois_2 neuf_2[label="9"]} 
     }
#+end_src

#+RESULTS:
[[file:arbre_3.png]]

#+begin_src dot :file arbre_4.png 
    digraph G {
    size ="4,5";
    six_1[label="6",style=filled,color="lightgray"]
    six_1 -> {trois_2[label="3"] cinq_2[label="5",style=filled,color="lightgray"]} 
    trois_2 -> {cinq_3[label="5"] un_3[label="1"]}
    cinq_2 -> {un_3[label="1"] six_3[label="6",style=filled,color="lightgray"]}
    cinq_3 -> {quatre_4[label="4"] sept_4[label="7"]}
    un_3 -> {sept_4 trois_4[label="3"]}
    six_3 ->{trois_4 cinq_4[label="5",style=filled,color="lightgray"] }
    }
#+end_src

#+RESULTS:
[[file:arbre_4.png]]


#+begin_src dot :file arbre_5.png 
   digraph G {
   size ="4,5";
   deux_1[label="2",style=filled,color="lightgray"]
   deux_1 -> {cinq_2[label="5"] sept_2[label="7",style=filled,color="lightgray"]}
   cinq_2 -> {un_3[label="1"] six_3[label="6",style=filled,color="lightgray"]}
   sept_2 -> {six_3 deux_3[label="2"]}
   un_3 -> {sept_4[label="7"] trois_4[label="3"]}
   six_3 ->{trois_4 cinq_4[label="5",style=filled,color="lightgray"] }
   deux_3 -> {cinq_4  deux_4[label="2"]}
   }
#+end_src

#+RESULTS:
[[file:arbre_5.png]]



#+begin_src dot :file arbre_6.png 
  digraph G {
  size ="4,5";
  quatre[label="4",style=filled,color="lightgray"]			
  quatre -> {six_1[label="6",style=filled,color="lightgray"] deux_1[label="2"]}
  six_1 -> {trois_2[label="3"] cinq_2[label="5",style=filled,color="lightgray"]} 
  deux_1 -> {cinq_2 sept_2[label="7"]} 
  trois_2 -> {cinq_3[label="5"] un_3[label="1"]}
  cinq_2 -> {un_3[label="1"] six_3[label="6",style=filled,color="lightgray"]}
  sept_2 -> {six_3 deux_3[label="2"]}
  cinq_3 -> {quatre_4[label="4"] sept_4[label="7"]}
  un_3 -> {sept_4 trois_4[label="3"]}
  six_3 ->{trois_4 cinq_4[label="5",style=filled,color="lightgray"] }
  deux_3 -> {cinq_4  deux_4[label="2"]}
  }
#+end_src

#+RESULTS:
[[file:arbre_6.png]]

