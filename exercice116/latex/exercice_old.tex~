\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

\emph{Cet exercice porte sur la notion de listes, la récursivité et la
programmation dynamique.}

Pour extraire de l'eau dans des zones de terrain instable, on souhaite
forer un conduit dans le sol pour réaliser un puits tout en préservant
l'intégrité du terrain. Pour représenter cette situation, on va
considérer qu'en forant à partir d'une position en surface, on s'enfonce
dans le sol en allant à gauche ou à droite à chaque niveau, jusqu'à
atteindre le niveau de la nappe phréatique.

Le sol pourra donc être représenté par une pyramide d'entiers où chaque
entier est le score de confiance qu'on a dans le forage de la zone
correspondante. Une telle pyramide est présentée sur la figure 1, à
gauche, les flèches indiquant les différents déplacements possibles
d'une zone à une autre au cours du forage.

Un conduit doit partir du sommet de la pyramide et descendre jusqu'au
niveau le plus bas, où se situe l'eau, en suivant des déplacements
élémentaires, c'est-à-dire en choisissant à chaque niveau de descendre
sur la gauche ou sur la droite. Le score de confiance d'un conduit est
la somme des nombres rencontrés le long de ce conduit. Le conduit gris
représenté à droite sur la figure 1 a pour score de confiance
\texttt{4\ +\ 2\ +\ 5\ +\ 1\ +\ 3\ =\ 15}.

\begin{center}
  \begin{minipage}{.80\linewidth}
    \begin{center}
      \includegraphics[width=.45\linewidth]{arbre.png}
      \hfill
      \includegraphics[width=.45\linewidth]{arbre_2.png}

      Figure 1 
    \end{center}
  \end{minipage}
\end{center}

On va utiliser un ordinateur pour chercher à résoudre ce problème. Pour
cela, on représente chaque niveau par la liste des nombres de ce niveau
et une pyramide par une liste de niveaux.

La pyramide ci-dessus est donc représentée par la liste de listes

\texttt{ex1\ =\ {[}{[}4{]},{[}6,2{]},{[}3,5,7{]},{[}5,1,6,2{]},{[}4,7,3,5,2{]}{]}}.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Dessiner la pyramide représentée par la liste de listes
  \texttt{ex2\ =\ {[}{[}3{]},{[}1,2{]},{[}4,5,9{]},{[}3,6,2,1{]}{]}}.
\item
  Déterminer un conduit de score de confiance maximal dans la pyramide
  ex2 et donner son score.

\item On souhaite déterminer le score de confiance maximal pouvant être
atteint pour une pyramide quelconque. Une première idée consiste à
énumérer tous les conduits et à calculer leur score pour déterminer les
meilleurs.

  Énumérer les conduits dans la pyramide de trois niveaux représentée
  sur la figure 2.


  \begin{center}
    \begin{minipage}{0.3\linewidth}
      \begin{center}
        \includegraphics[width=\linewidth]{arbre_3.png}
        
        Figure 2
      \end{center}
    \end{minipage}
  \end{center}
  
Afin de compter le nombre de conduits pour une pyramide de \texttt{n}
niveaux, on remarque qu'un conduit est uniquement représenté par une
séquence de \texttt{n} déplacements \texttt{gauche} ou \texttt{droite}.

\item En considérant un codage binaire d'un tel conduit, où \texttt{gauche}
  est représenté par 0 et \texttt{droite} par 1, déterminer le nombre de
  conduits dans une pyramide de \texttt{n} niveaux.
\item Justifier que la solution qui consiste à tester tous les conduits
  possibles pour calculer le score de confiance maximal d'une pyramide
  n'est pas raisonnable.

\noindent On dira dans la suite qu'un conduit est maximal si son score de
confiance est maximal. Afin de pouvoir calculer efficacement le score
maximal, on peut analyser la structure des conduits maximaux.


\begin{center}
  \begin{minipage}{\linewidth}
    \begin{center}
      \includegraphics[width=.28\linewidth]{arbre_4.png}
      \hfill
      \includegraphics[width=.28\linewidth]{arbre_5.png}
      \hfill
      \includegraphics[width=.35\linewidth]{arbre_6.png}

      Figure 3
    \end{center}
  \end{minipage}
\end{center}

\begin{itemize}
\item
  \textbf{Première observation} : si on a des conduits maximaux
  \texttt{cm1} et \texttt{cm2} (représentés en gris dans la figure 3)
  pour les deux pyramides obtenues en enlevant le sommet de
  \texttt{ex1}, on obtient un conduit maximal en ajoutant le sommet 4
  devant le conduit de plus grand score parmi cm1 et \texttt{cm2}. Ici
  le score de \texttt{cm1} est \texttt{6+5+6+5=22} et le score de
  \texttt{cm2} est \texttt{2+7+6+5=20} donc le conduit maximal dans
  \texttt{ex1} est celui obtenu à partir de \texttt{cm1} et dessiné à
  droite dans la figure 3.
\item
  \textbf{Deuxième observation} : si la pyramide n'a qu'un seul niveau,
  il n'y a que le sommet, dans ce cas, il n'y a pas de choix à faire, le
  seul conduit possible est celui qui contient le sommet et le nombre de
  ce sommet est le score maximal que l'on peut obtenir.
\end{itemize}

Avec ces deux observations, on peut calculer le score maximal possible
pour un conduit dans une pyramide \texttt{p} par récurrence. Posons
\texttt{score\_max(i,j)} le score maximal possible depuis le nombre
d'indice \texttt{j} du niveau \texttt{i}, c'est-à-dire dans la petite
pyramide issue de ce nombre. On a alors les relations suivantes :

\begin{itemize}
\tightlist
\item
  \texttt{score\_max(len(p)-1,j,p)\ =\ p{[}len(p)-1{]}{[}j{]}} ;
\item
  \texttt{score\_max(i,j,p)\ =\ p{[}i{]}{[}j{]}\ +\ max(score\_max(i+1,j,p),score\_max(i+1,j+1,p))}.
\end{itemize}

Le score maximal possible pour \texttt{p} toute entière sera alors
\texttt{score\_max(0,0,p)}.

\item
  Écrire la fonction récursive \texttt{score\_max} qui implémente les
  règles précédentes.

Si on suit à la lettre la définition de \texttt{score\_max}, on obtient
une résolution dont le coût est prohibitif à cause de la redondance des
calculs. Par exemple \texttt{score\_max(3,1,p)} va être calculé pour
chaque appel à \texttt{score\_max(2,0,p)} et \texttt{score\_max(2,1,p)}.
Pour éviter cette redondance, on décide de mettre en place une approche
par programmation dynamique. Pour cela, on va construire une pyramide
\texttt{s} dont le nombre à l'indice \texttt{j} du niveau \texttt{i}
correspond à \texttt{score\_max(i,j,p)}, c'est-à-dire au score maximal
pour un conduit à partir du nombre correspondant dans \texttt{p}.

\item
  Écrire une fonction \texttt{pyramide\_nulle} qui prend en paramètre un
  entier \texttt{n} et construit une pyramide remplie de 0 à \texttt{n}
  niveaux.
\item
  Compléter la fonction \texttt{prog\_dyn} ci-dessous qui prend en
  paramètre une pyramide \texttt{p}, et qui renvoie le score maximal
  pour un conduit dans \texttt{p}. Pour cela, on construit une pyramide
  \texttt{s} remplie de 0 de la même taille et la remplit avec les
  valeurs de \texttt{score\_max} en commençant par le dernier niveau et
  en appliquant petit à petit les relations données ci-dessus.

\newpage
  
  \begin{Shaded}
\begin{Highlighting}[]
    \KeywordTok{def}\NormalTok{ prog_dyn(p):}
    \NormalTok{    n }\OperatorTok{=} \BuiltInTok{len}\NormalTok{(p)}
    \NormalTok{    s }\OperatorTok{=}\NormalTok{ ...}
        \CommentTok{# remplissage du dernier niveau}
        \ControlFlowTok{for}\NormalTok{ j }\KeywordTok{in}\NormalTok{ ...}
    \NormalTok{        s[n}\DecValTok{-1}\NormalTok{][j] }\OperatorTok{=}\NormalTok{ ...}
        \CommentTok{# remplissage des autres niveaux}
        \ControlFlowTok{for}\NormalTok{ i }\KeywordTok{in}\NormalTok{ ...}
            \ControlFlowTok{for}\NormalTok{ j }\KeywordTok{in}\NormalTok{ ...}
    \NormalTok{            s[i][j] }\OperatorTok{=}\NormalTok{ ...}
        \CommentTok{# renvoie du score maximal}
        \ControlFlowTok{return}\NormalTok{ s[}\DecValTok{0}\NormalTok{][}\DecValTok{0}\NormalTok{]}
\end{Highlighting}
\end{Shaded}


\item
  Montrer que le coût d'exécution de cette fonction est quadratique en
  \texttt{n} pour une pyramide à \texttt{n} niveaux.
\item
  Expliquer comment adapter la fonction \texttt{score\_max} pour éviter
  la redondance des calculs afin d'obtenir également un coût
  quadratique, tout en gardant une approche récursive.
\end{enumerate}
