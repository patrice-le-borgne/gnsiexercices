"""
Module plateau
"""

from rangee import Rangee
from carte import Carte

class Plateau:
    """
    La classe plateau qui gère les 4 rangées du jeu.
    Un seul attribut :
      - rangees = une liste de 4 rangees
    """

    def __init__(self, l):
        """
        Il faut fournir une liste avec 4 cartes pour initialiser
        le plateau.
        rangees : contient une liste de 4 rangées initialisées
        grâce à la liste de 4 cartes fournies en argument.
        """
        pass

    def _get_rangee_la_plus_faible(self):
        """
        Méthode privée.
        Renvoie l'indice de la rangée avec le plus faible nombre de têtes de
        bœuf.
        """
        pass
        
        
    def _get_rangee_ou_ajouter(self, c):
        """
        Méthode privée.
        Renvoie l'indice de la rangée où il faut ajouter la carte c.
        Si la carte est trop petite pour être ajouter à une rangée,
        la valeur -1 est renvoyée.
        """
        pass

    def ajouter(self, c):
        """
        Ajouter la carte c au plateau.
        """
        pass
    
    def __str__(self):
        """
        Affichage en 2 parties :
        [nombre de TdB] : liste des cartes
        """
        pass
                       
if __name__ == '__main__':
    import nose
    nose.run()
