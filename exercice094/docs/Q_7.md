    >>> from exercice import *
    >>> from rangee import Rangee
    >>> from carte import Carte
    >>> p = Plateau([Carte(12), Carte(69), Carte(55), Carte(11)])
    >>> p1 = Plateau([Carte(17), Carte(69), Carte(55), Carte(11)])

        >>> p.ajouter(Carte(13))
        (0, [])
        >>> p.ajouter(Carte(14))
        (0, [])
        >>> p.ajouter(Carte(15))
        (0, [])
        >>> p.ajouter(Carte(16))
        (0, [])
        >>> print(p)
        [6] : 12-13-14-15-16
        [1] : 69
        [7] : 55
        [5] : 11
        <BLANKLINE>
        >>> p.ajouter(Carte(17))
        (0, [Carte n°12 - 1 TdB, Carte n°13 - 1 TdB, Carte n°14 - 1 TdB, Carte n°15 - 2 TdB, Carte n°16 - 1 TdB])
        >>> print(p)
        [1] : 17
        [1] : 69
        [7] : 55
        [5] : 11
        <BLANKLINE>

 
