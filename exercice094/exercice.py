"""
Module plateau
"""

from rangee import Rangee
from carte import Carte

class Plateau:
    """
    La classe plateau qui gère les 4 rangées du jeu.
    Un seul attribut :
      - rangees = une liste de 4 rangees
    """

    def __init__(self, l):
        """
        Il faut fournir une liste avec 4 cartes pour initialiser
        le plateau.
        rangees : contient une liste de 4 rangées initialisées
        grâce à la liste de 4 cartes fournies en argument.
        """
        assert len(l) == 4
        for c in l:
            assert isinstance(c, Carte)
        self.rangees = [ Rangee(c) for c in l]

    def _get_rangee_la_plus_faible(self):
        """
        Méthode privée.
        Renvoie l'indice de la rangée avec le plus faible nombre de têtes de
        bœuf.
        """
        l = [r.get_nombre_tdb() for r in self.rangees]
        return l.index(min(l))
        
        
    def _get_rangee_ou_ajouter(self, c):
        """
        Méthode privée.
        Renvoie l'indice de la rangée où il faut ajouter la carte c.
        Si la carte est trop petite pour être ajouter à une rangée,
        la valeur -1 est renvoyée.
        """
        l = []
        for r in self.rangees:
            l.append(c.get_numero()-r.get_derniere_carte().get_numero())
        peut_poser = False
        for v in l:
            if v > 0:
                peut_poser = True
        if not peut_poser:
            return -1
        for i, v in enumerate(l):
            if v < 0:
                l[i] = 1000
        return l.index(min(l))

    def ajouter(self, c):
        """
        Ajouter la carte c au plateau.
        """
        i = self._get_rangee_ou_ajouter(c)
        if i < 0:
            i = self._get_rangee_la_plus_faible()
            l = self.rangees[i].get_cartes()
            self.rangees[i].set_cartes([c])
        else:
            l = self.rangees[i].ajouter(c)
        return i, l
    
    def __str__(self):
        """
        Affichage en 2 parties :
        [nombre de TdB] : liste des cartes
        """
        texte = ""
        for i in range(len(self.rangees)):
            texte += "[{}] : {}\n".format( self.rangees[i].get_nombre_tdb(), self.rangees[i])
        return texte
                       
if __name__ == '__main__':
    import nose
    nose.run()
