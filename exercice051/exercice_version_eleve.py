class Noeud:
    def __init__(self, racine=None):
        self.r = racine
        self.gauche = None
        self.droit = None

"""
# Partie 1

1. Combien d’attributs possèdent la classe Nœud ?
"""
nb_attributs = -1
"""
2. Soit  l’arbre obtenu avec les instructions suivantes :

    >>> a = Noeud(4)
    >>> a.gauche = Noeud(5)
    >>> a.droit = Noeud(14)
    >>> a.gauche.droit = Noeud(8)

Choisir la représentation de l'arbre a. La réponse est un string: soit "a", "b", "c", "d" ou "e".
          a.     4      b.     4       c.     4       d.     4      e.     4   
                / \           / \            / \            / \           / \  
               5   14        5   14         5   14         5   8         5   8
              /               \                  \          \                 \    
             8                 8                  8          14               14   
"""
representation_arbre_a = ""

"""
3. a. . Quelle est la profondeur du nœud d’étiquette 12 ?
"""
profondeur_etiquette_12 = -1

"""
3.b. Quelle est la taille de l’arbre ?
"""
taille_arbre_a = -1

"""
3.c. Dans quel ordre sont parcourus les nœuds lors d'un parcours préfixe en profondeur ? On visite le nœud gauche avant le nœud droit. La réponse est à écrire dans une liste. Attention, il faut un espace après la virgule. En revanche, il n'est faut pas après et avant le crochet. Exemple correct : [1, 2, 3]. Exemple incorrect : [ 1,2,3].
"""
parcours_prefixe = []

"""
# Partie 2

1. Quelle est la distance entre les nœuds d’étiquette 5 et d’étiquette 1 ?
"""
distance_entre_5_et_1 = -1

"""
2.
"""

def contient(arbre, x):
    """
    renvoie True si le nœud de valeur x est dans l’arbre, False sinon.
    """
    pass


def chemin(arbre, n):
    """
    Renvoie la liste des nœuds compris entre la racine et le nœud compris.
    """
    if arbre is None:
        return .....
    else:
        if arbre.r == n:
            return [n]
        elif contient(arbre.gauche, n):
            return [arbre.r] + ...............
        elif .....................:
            return ...........................
        else:
            return []


def ppac(arbre, n1, n2):
    """
    Renvoie le plus petit ancêtre commun aux nœud n1 et n2.
    """
    pass
        

def distance(arbre, n1, n2):
    """
    Renvoie la distance entre deux nœuds.
    """
    pass
        
"""
Partie 3
"""

def somme(arbre):
    """
    Renvoie la somme des valeurs des nœuds de l’arbre.
    """
    pass

def est_arbre_somme(arbre):
    """
    Renvoie True sur arbre est un arbre_somme, False sinon.
    """
    def est_arbre_somme(arbre):
    """
    Renvoie True sur arbre est un arbre_somme, False sinon.
    """
    if arbre is None:
        return True
    elif ..........................................: # Tester si le noeud est une feuille
        return True
    else:
        return .................. and ................. and  ........................
