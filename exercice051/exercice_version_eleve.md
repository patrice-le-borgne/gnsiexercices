Exercice
========

Cet exercice est composé de 3 parties indépendantes. La partie 1 porte
sur quelques concepts fondamentaux sur les arbres binaires, la partie 2
s'intéresse à la distance entre deux nœuds d'un arbre binaire et la
partie 3 présente les Arbre\_Somme.

Partie 1
--------

On implémente les arbres binaires en utilisant la programmation objet
avec la classe Nœud ci-dessous :

``` {.python}
class Noeud:
   def __init__(self, racine=None):
       self.r = racine
       self.gauche = None
       self.droit = None
```

1.  Combien d'attributs possèdent la classe Nœud ?

<!-- -->

2.  Représenter l'arbre obtenu avec les instructions suivantes :

``` {.python}
    >>> a = Noeud(4)
    >>> a.gauche = Noeud(5)
    >>> a.droit = Noeud(14)
    >>> a.gauche.droit = Noeud(8)
```

3.  On considère l'arbre de la Figure 1 :

    ![Figure 1](arbre_exo_51_1.png)

    1.  Quelle est la profondeur du nœud d'étiquette 12 ?

<!-- -->

2.  Quelle est la taille de l'arbre ?

<!-- -->

3.  Dans quel ordre sont parcourus les nœuds lors d'un parcours préfixe
    en profondeur ? On visite le nœud gauche avant le nœud droit.

Partie 2
--------

Dans toute cette partie, on prendra pour exemple l'arbre de la Figure 1.

**On appelle distance entre deux nœuds, le nombre minimal de branches
qui sépare les deux nœuds. On notera d la distance.**

Ainsi, pour l'arbre de la Figure 1, on a `d(12, 4) = 2` et
`d(8, 9) = 4`.

1.  Quelle est la distance entre les nœuds d'étiquette 5 et d'étiquette
    1 ?

<!-- -->

2.  Soient `n1` et `n2`, deux nœuds d'un arbre binaire, on admettra la
    relation suivante :

    `d(n1,n2)= d(n1,r) + d(n2,r) – 2 × d(r, ppac)`

    où `r` représente la racine de l'arbre et `ppac` le plus petit
    ancêtre commun aux nœuds `n1`et `n2`.
    1.  Écrire une fonction `contient(arbre, x)` qui renvoie `True` si
        le nœud de valeur `x` est dans l'arbre, `False` sinon.

<!-- -->

2.  Compléter une fonction `chemin(arbre, n)` qui renvoie la liste des
    nœuds compris entre la racine et le nœud compris.

``` {.python}
def chemin(arbre, n):
    """
    Renvoie la liste des nœuds compris entre la racine et le nœud compris.
    """
    if arbre is None:
        return .....
    else:
        if arbre.r == n:
            return [n]
        elif contient(arbre.gauche, n):
            return [arbre.r] + ...............
        elif .....................:
            return ...........................
        else:
            return []
```

3.  Pour trouver le plus petit ancêtre commun à deux nœuds `n1` et `n2`,
    on procède de la manière suivante :
    -   On détermine le chemin entre la racine et le nœud `n1` ;
    -   On détermine le chemin entre la racine le le nœud `n2` ;
    -   On compare les deux listes ci-dessus et le premier élément
        différent entre les deux listes est l'élément qui suit le plus
        petit ancêtre commun. Écrire une fonction `ppac(arbre, n1,n2)`
        qui renvoie le plus petit ancêtre commun aux nœud `n1` et `n2`.

<!-- -->

3.  Déduire des fonctions précédentes, une fonction
    `distance(arbre,n1,n2)` qui renvoie la distance entre deux nœuds.
    *(Remarque : On aura remarqué que la longueur de la liste du chemin
    entre la racine et le nœud renvoie le nombre de nœud et non la
    distance. On admettra que la formule donnée en début de partie
    appliquée aux chemins renvoie effectivement la distance)*

Partie 3
--------

On appelle `arbre_somme` un arbre binaire dont la valeur d'un nœud est
égale à la somme des nœuds de ses sous-arbres gauche et droit.

Ainsi, l'arbre suivant est un `arbre_somme`.

![Figure 2](arbre_exo_51_2.png)

En effet, $26 = (10 + 6 + 4) + (3 + 2 +1)$ et $10 = 4 + 6$ et
$3 = 1 + 2$.

On se propose dans cette partie de déterminer si un arbre binaire est un
`arbre_somme`

1.  Écrire une fonction `somme(arbre)` qui renvoie la somme des valeurs
    des nœuds de l'arbre.

<!-- -->

2.  Compléter la fonction `est_arbre_somme(arbre)` qui renvoie un
    booléen.

``` {.python}
def est_arbre_somme(arbre):
    """
    Renvoie True sur arbre est un arbre_somme, False sinon.
    """
    if arbre is None:
        return True
    elif ..........................................: # Tester si le noeud est une feuille
        return True
    else:
        return .................. and ................. and  ........................
```
