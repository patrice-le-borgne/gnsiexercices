class Noeud:
    def __init__(self, racine=None):
        self.r = racine
        self.gauche = None
        self.droit = None

"""
# Partie 1

1. Combien d’attributs possèdent la classe Nœud ?
"""
nb_attributs = 3
"""
2. Soit  l’arbre obtenu avec les instructions suivantes :

    >>> a = Noeud(4)
    >>> a.gauche = Noeud(5)
    >>> a.droit = Noeud(14)
    >>> a.gauche.droit = Noeud(8)

Choisir la représentation de l'arbre a. La réponse est un string: soit "a", "b", "c", "d" ou "e".
          a.     4      b.     4       c.     4       d.     4      e.     4   
                / \           / \            / \            / \           / \  
               5   14        5   14         5   14         5   8         5   8
              /               \                  \          \                 \    
             8                 8                  8          14               14   
"""
representation_arbre_a = "b"

"""
3. a. . Quelle est la profondeur du nœud d’étiquette 12 ?
"""
profondeur_etiquette_12 = 2

"""
3.b. Quelle est la taille de l’arbre ?
"""
taille_arbre_a= 8

"""
3.c. Dans quel ordre sont parcourus les nœuds lors d'un parcours préfixe en profondeur ? On visite le nœud gauche avant le nœud droit. La réponse est à écrire dans une liste. Attention, il faut un espace après la virgule. En revanche, il n'est faut pas après et avant le crochet. Exemple correct : [1, 2, 3]. Exemple incorrect : [ 1,2,3].
"""
parcours_prefixe = [7, 5, 12, 8, 4, 9, 1, 3]

"""
# Partie 2

1. Quelle est la distance entre les nœuds d’étiquette 5 et d’étiquette 1 ?
"""
distance_entre_5_et_1 = 3

"""
2.
"""

def contient(arbre, x):
    """
    renvoie True si le nœud de valeur x est dans l’arbre, False sinon.
    """
    if arbre is None:
        return False
    elif arbre.r == x:
        return True
    else:
        return contient(arbre.gauche, x) or contient(arbre.droit, x)

def chemin(arbre, n):
    """
    Renvoie la liste des nœuds compris entre la racine et le nœud compris.
    """
    if arbre is None:
        return []
    else:
        if arbre.r == n:
            return [n]
        elif contient(arbre.gauche, n):
            return [arbre.r] + chemin(arbre.gauche, n)
        elif contient(arbre.droit, n):
            return [arbre.r] + chemin(arbre.droit, n)
        else:
            return []
        
def ppac(arbre, n1, n2):
    """
    Renvoie le plus petit ancêtre commun aux nœud n1 et n2.
    """
    l1 = chemin(arbre, n1)[::-1]
    l2 = chemin(arbre, n2)[::-1]
    for v1 in l1:
        for v2 in l2:
            if v1 == v2:
                return v1
    return None
        

def distance(arbre, n1, n2):
    """
    Renvoie la distance entre deux nœuds.
    """
    dn1_r = len(chemin(arbre, n1))
    dn2_r = len(chemin(arbre, n2))
    nppac = ppac(arbre, n1, n2)
    dnppac_r = len(chemin(arbre, nppac))
    return dn1_r + dn2_r - 2*dnppac_r
        
"""
Partie 3
"""

def somme(arbre):
    """
    Renvoie la somme des valeurs des nœuds de l’arbre.
    """
    if arbre is None:
        return 0
    else:
        return arbre.r + somme(arbre.gauche) + somme(arbre.droit)

def est_arbre_somme(arbre):
    """
    Renvoie True sur arbre est un arbre_somme, False sinon.
    """
    if arbre is None:
        return True
    elif arbre.gauche is None and arbre.droit is None: # Tester si le noeud est une feuille
        return True
    else:
        return arbre.r == (somme(arbre.gauche) + somme(arbre.droit)) and est_arbre_somme(arbre.gauche) and est_arbre_somme(arbre.droit)
