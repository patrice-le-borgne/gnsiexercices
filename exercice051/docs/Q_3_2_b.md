    >>> from exercice import *
    >>> i= Noeud(8)
    >>> e = Noeud(12)
    >>> e.gauche = i
    >>> c = Noeud(5)
    >>> c.gauche = e
    >>> c.droit = Noeud(4)
    >>> d = Noeud(9)
    >>> d.gauche = Noeud(1)
    >>> d.droit = Noeud(3)
    >>> b = Noeud(7)
    >>> b.gauche = c
    >>> b.droit = d

        >>> a = Noeud(4)
        >>> a.gauche = Noeud(5)
        >>> a.droit = Noeud(14)
        >>> a.gauche.droit = Noeud(8)

    >>> l = Noeud(26)
    >>> m = Noeud(10)
    >>> m.gauche = Noeud(4)
    >>> m.droit = Noeud(6)
    >>> n = Noeud(3)
    >>> n.gauche = Noeud(1)
    >>> n.droit = Noeud(2)
    >>> l.gauche = m
    >>> l.droit = n
    >>> est_arbre_somme(l)
    True

 
