from math import sqrt

class Point:
    """
    La classe Point dans un plan (O, I, J)
    Les attributs sont :
    * nom : str, le nom du point ;
    * x : float, l'abscisse du point
    * y : float : l'ordonnée du point
    """
    def __init__(self, nom, x=0, y=0):
        """
        Le constructeur.
        Si les coordonnées ne sont pas précisées,
        elles seront initialisées à (0; 0).
        """
        self.nom = nom
        self.x = x
        self.y = y

    def __repr__(self):
        """
        Pour avoir une description lisible de l'objet Point.
        """
        return f"Point {self.nom}({self.x}, {self.y})"

    def distance(self, p2):
        """
        Calcule la distance entre le point courant et le point p2.
        """
        xA = self.x
        yA = self.y
        xB = p2.x
        yB = p2.y
        return sqrt((xA-xB)**2 + (yA-yB)**2)

def milieu(p1, p2, nom):
    """
    Renvoie un objet Point, milieu du segment [p1p2]
    """
    x = (p1.x + p2.x) / 2
    y = (p1.y + p2.y) / 2
    return Point(nom, x, y)
