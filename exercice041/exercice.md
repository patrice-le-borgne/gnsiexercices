~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : poo
thème : poo
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> A = Point("A", 1, 2)
>>> B = Point("B", 4, 6)
~~~

Exercice
==========

L'objectif est d'implémenter une classe `Point` et une fonction `milieu()` qui pourraient être utilisées pour développer un logiciel de géométrie.

Nous aurons besoin de la racine carrée pour calculer des longueurs.

~~~ {.python .all}
>>> from math import sqrt 
~~~

1. Implémenter la classe `Point` correspondant au schéma suivant :

![point](point.png)

~~~ {.python .amc .test file="Q_1_a.md" bareme="1"}
    >>> A = Point("A", 1, 2)
    >>> A.nom
    'A'
    >>> A.x
    1
    >>> A.y
    2
~~~

On rappelle que la dunder méthode  `__repr__`  renvoie un texte qui décrit l'objet. Ici, on souhaite obtenir le résultat suivant (attention aux espaces ! ):

~~~ {.python .amc .test file="Q_1_b.md" bareme="1"}
    >>> A
    Point A(1, 2)
~~~

On rappelle que la formule de la distance séparant les points $A$ et $B$ est :

$$ AB = \sqrt{(x_A - x_B)^2 + (y_A-y_B)^2}$$

~~~ {.python .amc .test  file="Q_1_c.md" bareme="1"}
    >>> B = Point("B", 4, 6)
    >>> A.distance(B)
    5.0
    >>> B.distance(A)
    5.0
~~~

2. Écrire une fonction `milieu(p1, p2, nom)` qui prend comme paramètres deux points `p1` et `p2` et qui renvoie un nouveau point appelé `nom`, milieu du segment `[p1p2]`.

On rappelle que le milieu $I$ du segment $AB$ a pour coordonnées $x_I = \dfrac{x_A+x_B}{2}$ et  $y_I = \dfrac{y_A+y_B}{2}$.


~~~ {.python .amc  file="Q_2.md" bareme="2"}
    >>> C = milieu(A, B, "C")
    >>> C
    Point C(2.5, 4)
~~~

~~~ {.python .test .hidden file="Q_2_a.md" bareme="1"}
    >>> C = milieu(A, B, "C")
    >>> C
    Point C(2.5, 4.0)
~~~

~~~ {.python .test .hidden file="Q_2_b.md" bareme="1"}
>>> R = Point("R", 0, 0)
>>> T = Point("T", -4, 0)
>>> R.distance(T)
4.0
>>> L = milieu(R, T, "L")
>>> L
Point L(-2.0, 0.0)
~~~






