Exercice
========

L'objectif est d'implémenter une classe `Point` et une fonction
`milieu()` qui pourraient être utilisées pour développer un logiciel de
géométrie.

Nous aurons besoin de la racine carrée pour calculer des longueurs.

``` {.python}
>>> from math import sqrt 
```

1.  Implémenter la classe `Point` correspondant au schéma suivant :

![point](point.png)

``` {.python}
    >>> A = Point("A", 1, 2)
    >>> A.nom
    'A'
    >>> A.x
    1
    >>> A.y
    2
```

On rappelle que la dunder méthode `__repr__` renvoie un texte qui décrit
l'objet. Ici, on souhaite obtenir le résultat suivant (attention aux
espaces ! ):

``` {.python}
    >>> A
    Point A(1, 2)
```

On rappelle que la formule de la distance séparant les points $A$ et $B$
est :

$$ AB = \sqrt{(x_A - x_B)^2 + (y_A-y_B)^2}$$

``` {.python}
    >>> B = Point("B", 4, 6)
    >>> A.distance(B)
    5.0
    >>> B.distance(A)
    5.0
```

2.  Écrire une fonction `milieu(p1, p2, nom)` qui prend comme paramètres
    deux points `p1` et `p2` et qui renvoie un nouveau point appelé
    `nom`, milieu du segment `[p1p2]`.

On rappelle que le milieu $I$ du segment $AB$ a pour coordonnées
$x_I = \dfrac{x_A+x_B}{2}$ et $y_I = \dfrac{y_A+y_B}{2}$.

``` {.python}
    >>> C = milieu(A, B, "C")
    >>> C
    Point C(2.5, 4)
```
