\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

On considère un tableau d'entiers \texttt{tab} (type \texttt{list} dont
les éléments sont des 0 ou des 1). On se propose de trier ce tableau
selon l'algorithme suivant : à chaque étape du tri, le tableau est
constitué de trois zones consécutives, la première ne contenant que des
0, la seconde n'étant pas triée et la dernière ne contenant que des 1.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{Zone de }\DecValTok{0} \OperatorTok{|}\NormalTok{  Zone non triée }\OperatorTok{|}\NormalTok{  Zone de }\DecValTok{1}
\end{Highlighting}
\end{Shaded}

Tant que la zone non triée n'est pas réduite à un seul élément, on
regarde son premier élément :

\begin{itemize}
\tightlist
\item
  si cet élément vaut 0, on considère qu'il appartient désormais à la
  zone ne contenant que des 0 ;
\item
  si cet élément vaut 1, il est échangé avec le dernier élément de la
  zone non triée et on considère alors qu'il appartient à la zone ne
  contenant que des 1.
\end{itemize}

Dans tous les cas, à chaque étape du tri, la longueur de la zone non
triée diminue de 1.

Recopier sous Python en la complétant la fonction tri suivante :

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ tri(tab):}
    \CommentTok{#i est le premier indice de la zone non triee, j le dernier indice.}
    \CommentTok{#Au debut, la zone non triee est le tableau entier.}
\NormalTok{    i }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{    j }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{    point }\OperatorTok{=}\NormalTok{ tab[}\DecValTok{0}\NormalTok{]}
    \ControlFlowTok{while}\NormalTok{ i }\OperatorTok{<}\NormalTok{ j :}
        \ControlFlowTok{if}\NormalTok{ tab[i] }\OperatorTok{==} \DecValTok{0}\NormalTok{:}
\NormalTok{            i }\OperatorTok{=}\NormalTok{ ...}
        \ControlFlowTok{else}\NormalTok{:}
\NormalTok{            valeur }\OperatorTok{=}\NormalTok{ tab[j]}
\NormalTok{            tab[j] }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{            ...}
\NormalTok{            j }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{    ...}
\end{Highlighting}
\end{Shaded}

Exemple :

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ tri([}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{])}
\NormalTok{    [}\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{]}
\end{Highlighting}
\end{Shaded}

