def tri(tab):
    #i est le premier indice de la zone non triee, j le dernier indice.
    #Au debut, la zone non triee est le tableau entier.
    i = 0
    j = len(tab) - 1
    while i < j : ## on a remplacé le test i != j par le test i <j plus parlant car toujours i <= j
        if tab[i] == 0:
            i = i + 1 ## la zone de zéros est agrandie de 1
        else :
            valeur = tab[j]##sauvegarde de la valeur en tab[j] afin de préparer l'échange de valeurs tab[i]<->tab[j]
            tab[j] = 1 ##la valeur 1 en tab[i] est copiée en tab[j]
            tab[i] = valeur##la valeur qui se trouvait en tab[j], sauvegardée dans valeur, est recopiée en tab[i]
            j = j - 1 ## la zone de 1 est agrandie de 1
    return tab ## renvoi de la référence au tableau reçu en entrée
