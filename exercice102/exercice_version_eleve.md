Exercice
========

Compléter la fonction suivante `etat_eau(t)` qui renvoie l'état de l'eau
(liquide, gazeux ou solide) en fonction de la température `t`.

``` {.python}
def etat_eau(t):
    if t ..... 0:
        return "solide"
    elif .......:
        return "......."
    else:
        return "......."
```

**Exemple d'appel de la fonction**

``` {.python}
>>> etat_eau(50)
'liquide'
>>> etat_eau(-5)
'solide'
>>> etat_eau(120)
'gazeux'
```
