def etat_eau(t):
    if t <=0:
        return "solide"
    elif t >= 100:
        return "gazeux"
    else:
        return "liquide"
