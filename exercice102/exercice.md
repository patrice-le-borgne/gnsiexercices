~~~ {.hidden .meta}
classe : seconde
type : oral2
chapitre : test
thème : test
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Compléter la fonction suivante `etat_eau(t)` qui renvoie l'état de l'eau (liquide, gazeux ou solide) en fonction de la température `t`.

~~~ {.python}
def etat_eau(t):
    if t ..... 0:
        return "solide"
    elif .......:
        return "......."
    else:
        return "......."
~~~

**Exemple d'appel de la fonction**

~~~ {.python}
>>> etat_eau(50)
'liquide'
>>> etat_eau(-5)
'solide'
>>> etat_eau(120)
'gazeux'
~~~

~~~ {.python .hidden .amc file="Q_1.md" bareme="2"}
>>> pass
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> etat_eau(-5)
'solide'
>>> etat_eau(-15)
'solide'
>>> etat_eau(-1)
'solide'
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> etat_eau(35)
'liquide'
>>> etat_eau(1)
'liquide'
>>> etat_eau(99)
'liquide'
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
>>> etat_eau(135)
'gazeux'
>>> etat_eau(101)
'gazeux'
~~~

~~~ {.python .hidden .test file="Q_4.md" bareme="1"}
>>> etat_eau(0)
'solide'
>>> etat_eau(100)
'gazeux'
~~~



