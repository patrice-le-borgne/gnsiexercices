"""
1.a. from math import sqrt permet d'import la fonction racine carrée de la bibliothèque math
b. Cette instruction renvoie une False à cause des erreurs d’arrondis sur les nombres à virgule ﬂottante. En eﬀet, les nombres à virgule ﬂottante sont représentés par une somme de puissance de 2. Comme 0,1, 0,2 et 0,3 ne peuvent pas s’exprimer comme une somme ﬁnie de puissance de 2, l’opération booléenne précédente renvoie False
c. point_A est un tuple qui n'est pas mutable d'où l'erreur.
"""

# 2a.

from math import sqrt
class Segment:
    def __init__(self,point1,point2):
        self.p1=point1
        self.p2=point2
        self.longueur= sqrt((point1[0]-point2[0])**2+(point1[1]-point2[1])**2)

#2b.

def liste_segments(liste_points):
    n = len(liste_points)
    segments = []
    for i in range(n-1):
        for j in range(i+1, n):
            # On construit le segment à partir des points i et j.
            seq = Segment(liste_points[i],liste_points[j])
            segments.append(seg) # On l'ajoute à la liste
    return segments    

"""
2. c. Pour liste de points de longueur n, on aura (n-1) + (n-2) + ... +1 =n(n-1)/2 segments

d. La complexité est donc de l'ordre de O(n^2)
"""
.

# 3

def plus_court_segment(liste_segments):
    if len(liste_segments)==1:
        return liste_segments[0]
    else:
        seg_gauche=plus_court_segment(moitie_gauche(liste_segments))
        seg_droite=plus_court_segment(moitie_droite(liste_segments))
    if seg_gauche.longueur>seg_droite.longueur:
        return seg_droite
    else:
        return seg_gauche

# 4.a.

point_A=(3,4)
point_B=(2,3)
point_C=(-3,-1)

nuage_points=[point_A,point_B,point_C]

# 4.b.

segment=plus_court_segment(liste_segments(nuage_points))

print((segment.p1[0],segment.p1[1]),(segment.p2[0],segment.p2[1]))
