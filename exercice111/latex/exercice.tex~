\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

La classe \texttt{Ruzzle} est la classe principale du jeu. En
particulier, elle gère la grille. Dans cet exercice, nous nous
intéressons juste aux fonctions qui permettent de faire l'inventaire de
tous les mots contenus dans la grille. Par défaut, les dimensions d'une
grille de Ruzzle sont \(4\times4\).

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  La recherche des mots dans la grille s'apparente à un parcours en
  profondeur de graphe (même si ici, il s'agit de grille.) Pour ne pas
  tourner en rond, il faut tenir à jour la liste des cases par
  lesquelles on est passé. Le moyen le plus simple ici est de stocker
  cette information dans une grille de même taille avec des booléens. On
  a donné ici le nom \texttt{masque} à cette grille en référence au jeu
  du démineur. Les cases explorées sont découvertes donc elles ne sont
  pas masquées (\texttt{False}). Les cases à explorer sont masquées
  (\texttt{True}).

  Écrire la méthode \texttt{generer\_masque()} qui renvoie une liste de
  listes de même dimension que \texttt{self.grille} remplie avec de
  \texttt{True}.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ grille }\OperatorTok{=}\NormalTok{ [[}\StringTok{'f'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'o'}\NormalTok{], [}\StringTok{'m'}\NormalTok{, }\StringTok{'u'}\NormalTok{, }\StringTok{'r'}\NormalTok{, }\StringTok{'l'}\NormalTok{], [}\StringTok{'n'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'i'}\NormalTok{, }\StringTok{'c'}\NormalTok{], [}\StringTok{'i'}\NormalTok{, }\StringTok{'o'}\NormalTok{, }\StringTok{'c'}\NormalTok{, }\StringTok{'e'}\NormalTok{]]}
\OperatorTok{>>>}\NormalTok{ ruzzle }\OperatorTok{=}\NormalTok{ Ruzzle(grille)}
\OperatorTok{>>>}\NormalTok{ ruzzle.generer_masque()}
\NormalTok{[[}\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{], [}\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{], [}\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{], [}\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{, }\VariableTok{True}\NormalTok{]] }
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\item
  Dans un parcours en profondeur, on a nécessairement des retours en
  arrière. Pour gérer ce problème et retrouver une situation précédente
  on fera une copie du masque au début de chaque appel récursif.

  Écrire la methode \texttt{copier\_grille(self,\ g)} qui renvoie une
  copie de la grille \texttt{g}.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ grille }\OperatorTok{=}\NormalTok{ [[}\StringTok{'f'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'o'}\NormalTok{], [}\StringTok{'m'}\NormalTok{, }\StringTok{'u'}\NormalTok{, }\StringTok{'r'}\NormalTok{, }\StringTok{'l'}\NormalTok{], [}\StringTok{'n'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'i'}\NormalTok{, }\StringTok{'c'}\NormalTok{], [}\StringTok{'i'}\NormalTok{, }\StringTok{'o'}\NormalTok{, }\StringTok{'c'}\NormalTok{, }\StringTok{'e'}\NormalTok{]]}
\OperatorTok{>>>}\NormalTok{ ruzzle }\OperatorTok{=}\NormalTok{ Ruzzle(grille)}
\OperatorTok{>>>}\NormalTok{ ruzzle.copier_grille(ruzzle.grille)}
\NormalTok{[[}\StringTok{'f'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'o'}\NormalTok{], [}\StringTok{'m'}\NormalTok{, }\StringTok{'u'}\NormalTok{, }\StringTok{'r'}\NormalTok{, }\StringTok{'l'}\NormalTok{], [}\StringTok{'n'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'i'}\NormalTok{, }\StringTok{'c'}\NormalTok{], [}\StringTok{'i'}\NormalTok{, }\StringTok{'o'}\NormalTok{, }\StringTok{'c'}\NormalTok{, }\StringTok{'e'}\NormalTok{]]}
\OperatorTok{>>>}\NormalTok{ ruzzle2 }\OperatorTok{=}\NormalTok{ Ruzzle(grille)}
\OperatorTok{>>>}\NormalTok{ ruzzle2.copier_grille([[}\DecValTok{1}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{3}\NormalTok{], [}\DecValTok{2}\NormalTok{, }\DecValTok{4}\NormalTok{, }\DecValTok{5}\NormalTok{], [}\DecValTok{4}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{9}\NormalTok{]])}
\NormalTok{[[}\DecValTok{1}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{3}\NormalTok{], [}\DecValTok{2}\NormalTok{, }\DecValTok{4}\NormalTok{, }\DecValTok{5}\NormalTok{], [}\DecValTok{4}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{9}\NormalTok{]]}
\OperatorTok{>>>}\NormalTok{ ruzzle2.copier_grille([[}\DecValTok{1}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{3}\NormalTok{], [}\DecValTok{4}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{9}\NormalTok{]])}
\NormalTok{[[}\DecValTok{1}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{3}\NormalTok{], [}\DecValTok{4}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{9}\NormalTok{]]}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\item
  Dans un premier temps, on va chercher la liste des mots dont on
  connait la premier case. Écrire la méthode récursive
  \texttt{liste\_possibilites\_avec\_position\_depart(self,\ l,\ c,\ masque,\ dico,\ listeP={[}{]},\ prefixe="")}
  qui renvoie les mots dont on connait la premier case \texttt{(l,c)}.

  \begin{itemize}
  \tightlist
  \item
    \texttt{l} : l'indice de la ligne
  \item
    \texttt{c} : l'indice de la colonne
  \item
    \texttt{masque} : une grille de booléens. \texttt{True} : la case
    est à visiter. \texttt{False}, on est déja passé par la case.
  \item
    \texttt{dico} : l'arbre qui contient tous les mots du dictionnaire.
  \item
    \texttt{listeP} : contient les mots possibles.
  \item
    \texttt{prefixe} : le début du mot que l'on est en train de
    parcourir.
  \end{itemize}

  Dans l'algorithme suivant, \texttt{case\ initiale} correspond à la
  case que l'on traite au début de la fonction. Ensuite
  \texttt{case\ courante} correspond à une case voisine de la
  \texttt{case\ initiale} traitée au sein d'une boucle.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{   liste_possibilites(case initiale)}
\NormalTok{       on copie la grille de masque}\OperatorTok{;}
\NormalTok{       on modifie la case initiale du masque}\OperatorTok{;}
\NormalTok{       on ajoute la lettre de la case initiale à prefixe}\OperatorTok{;}
\NormalTok{       si le prefixe est un mot):}
\NormalTok{           on le rajoute à la liste}\OperatorTok{;}
\NormalTok{       si le prefixe est la racine d}\StringTok{'un mot :}
\StringTok{           on parcourt les cases autour de la case initiale;}
\StringTok{           si la case courante est dans la grille}
\StringTok{               si la case courante n'}\NormalTok{est pas la case initiale et qu}\StringTok{'elle n'}\NormalTok{a pas déjà été explorée :}
\NormalTok{                   liste_possibilites(case courante)}\OperatorTok{;} 
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ grille }\OperatorTok{=}\NormalTok{ [[}\StringTok{'f'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'o'}\NormalTok{], [}\StringTok{'m'}\NormalTok{, }\StringTok{'u'}\NormalTok{, }\StringTok{'r'}\NormalTok{, }\StringTok{'l'}\NormalTok{], [}\StringTok{'n'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'i'}\NormalTok{, }\StringTok{'c'}\NormalTok{], [}\StringTok{'i'}\NormalTok{, }\StringTok{'o'}\NormalTok{, }\StringTok{'c'}\NormalTok{, }\StringTok{'e'}\NormalTok{]]}
\OperatorTok{>>>}\NormalTok{ ruzzle }\OperatorTok{=}\NormalTok{ Ruzzle(grille)}
\OperatorTok{>>>}\NormalTok{ masque }\OperatorTok{=}\NormalTok{ ruzzle.generer_masque()}
\OperatorTok{>>>}\NormalTok{ possibilites }\OperatorTok{=}\NormalTok{ ruzzle.liste_possibilites_avec_position_depart(}\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, masque, dico)}
\OperatorTok{>>>} \StringTok{'fee'} \KeywordTok{in}\NormalTok{ possibilites}
\VariableTok{True}
\OperatorTok{>>>} \StringTok{'feerie'} \KeywordTok{in}\NormalTok{ possibilites}
\VariableTok{True}
\OperatorTok{>>>} \StringTok{'ee'} \KeywordTok{in}\NormalTok{ possibilites}
\VariableTok{False}
\OperatorTok{>>>} \BuiltInTok{sorted}\NormalTok{(}\BuiltInTok{list}\NormalTok{(}\BuiltInTok{set}\NormalTok{(possibilites)))}
\NormalTok{[}\StringTok{'fee'}\NormalTok{, }\StringTok{'feerie'}\NormalTok{, }\StringTok{'femur'}\NormalTok{, }\StringTok{'fer'}\NormalTok{, }\StringTok{'ferie'}\NormalTok{, }\StringTok{'ferle'}\NormalTok{, }\StringTok{'feru'}\NormalTok{, }\StringTok{'ferue'}\NormalTok{, }\StringTok{'feu'}\NormalTok{, }\StringTok{'feue'}\NormalTok{, }\StringTok{'feuil'}\NormalTok{, }\StringTok{'fuel'}\NormalTok{, }\StringTok{'fui'}\NormalTok{, }\StringTok{'fuie'}\NormalTok{, }\StringTok{'fuir'}\NormalTok{, }\StringTok{'fume'}\NormalTok{, }\StringTok{'fumee'}\NormalTok{, }\StringTok{'fumer'}\NormalTok{, }\StringTok{'fumerie'}\NormalTok{, }\StringTok{'fur'}\NormalTok{, }\StringTok{'furie'}\NormalTok{, }\StringTok{'furole'}\NormalTok{]}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  Écrire la méthode \texttt{liste\_toutes\_les\_possibilites()} qui
  renvoie la liste de tous les mots présents dans la grille. Les mots ne
  devront apparaitre qu'une fois et être rangés dans l'ordre
  alphabétique. Il y une solution dans les tests de la question
  précédente.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ grille }\OperatorTok{=}\NormalTok{ [[}\StringTok{'f'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'o'}\NormalTok{], [}\StringTok{'m'}\NormalTok{, }\StringTok{'u'}\NormalTok{, }\StringTok{'r'}\NormalTok{, }\StringTok{'l'}\NormalTok{], [}\StringTok{'n'}\NormalTok{, }\StringTok{'e'}\NormalTok{, }\StringTok{'i'}\NormalTok{, }\StringTok{'c'}\NormalTok{], [}\StringTok{'i'}\NormalTok{, }\StringTok{'o'}\NormalTok{, }\StringTok{'c'}\NormalTok{, }\StringTok{'e'}\NormalTok{]]}
\OperatorTok{>>>}\NormalTok{ ruzzle }\OperatorTok{=}\NormalTok{ Ruzzle(grille)}
\OperatorTok{>>>}\NormalTok{ ruzzle.liste_toutes_les_possibilites()}
\NormalTok{[}\StringTok{'cc'}\NormalTok{, }\StringTok{'ce'}\NormalTok{, }\StringTok{'ceci'}\NormalTok{, }\StringTok{'cenure'}\NormalTok{, }\StringTok{'cerce'}\NormalTok{, }\StringTok{'cercle'}\NormalTok{, }\StringTok{'cerclee'}\NormalTok{, }\StringTok{'cerium'}\NormalTok{, }\StringTok{'cicero'}\NormalTok{, }\StringTok{'cil'}\NormalTok{, }\StringTok{'cire'}\NormalTok{, }\StringTok{'ciree'}\NormalTok{, }\StringTok{'cl'}\NormalTok{, }\StringTok{'cle'}\NormalTok{, }\StringTok{'clic'}\NormalTok{, }\StringTok{'clore'}\NormalTok{, }\StringTok{'coenure'}\NormalTok{, }\StringTok{'coeur'}\NormalTok{, }\StringTok{'coi'}\NormalTok{, }\StringTok{'coin'}\NormalTok{, }\StringTok{'con'}\NormalTok{, }\StringTok{'cone'}\NormalTok{, }\StringTok{'cree'}\NormalTok{, }\StringTok{'creme'}\NormalTok{, }\StringTok{'cri'}\NormalTok{, }\StringTok{'cric'}\NormalTok{, }\StringTok{'crie'}\NormalTok{, }\StringTok{'cru'}\NormalTok{, }\StringTok{'crue'}\NormalTok{, }\StringTok{'cruel'}\NormalTok{, }\StringTok{'ec'}\NormalTok{, }\StringTok{'eclore'}\NormalTok{, }\StringTok{'ecoeure'}\NormalTok{, }\StringTok{'ecoeuree'}\NormalTok{, }\StringTok{'ecoin'}\NormalTok{, }\StringTok{'ecreme'}\NormalTok{, }\StringTok{'ecremee'}\NormalTok{, }\StringTok{'ecrie'}\NormalTok{, }\StringTok{'ecru'}\NormalTok{, }\StringTok{'ecrue'}\NormalTok{, }\StringTok{'el'}\NormalTok{, }\StringTok{'elier'}\NormalTok{, }\StringTok{'elire'}\NormalTok{, }\StringTok{'emeri'}\NormalTok{, }\StringTok{'emeu'}\NormalTok{, }\StringTok{'emu'}\NormalTok{, }\StringTok{'emue'}\NormalTok{, }\StringTok{'en'}\NormalTok{, }\StringTok{'enumere'}\NormalTok{, }\StringTok{'eolien'}\NormalTok{, }\StringTok{'eon'}\NormalTok{, }\StringTok{'ere'}\NormalTok{, }\StringTok{'eu'}\NormalTok{, }\StringTok{'eue'}\NormalTok{, }\StringTok{'fee'}\NormalTok{, }\StringTok{'feerie'}\NormalTok{, }\StringTok{'femur'}\NormalTok{, }\StringTok{'fer'}\NormalTok{, }\StringTok{'ferie'}\NormalTok{, }\StringTok{'ferle'}\NormalTok{, }\StringTok{'feru'}\NormalTok{, }\StringTok{'ferue'}\NormalTok{, }\StringTok{'feu'}\NormalTok{, }\StringTok{'feue'}\NormalTok{, }\StringTok{'feuil'}\NormalTok{, }\StringTok{'fuel'}\NormalTok{, }\StringTok{'fui'}\NormalTok{, }\StringTok{'fuie'}\NormalTok{, }\StringTok{'fuir'}\NormalTok{, }\StringTok{'fume'}\NormalTok{, }\StringTok{'fumee'}\NormalTok{, }\StringTok{'fumer'}\NormalTok{, }\StringTok{'fumerie'}\NormalTok{, }\StringTok{'fur'}\NormalTok{, }\StringTok{'furie'}\NormalTok{, }\StringTok{'furole'}\NormalTok{, }\StringTok{'icone'}\NormalTok{, }\StringTok{'il'}\NormalTok{, }\StringTok{'ile'}\NormalTok{, }\StringTok{'in'}\NormalTok{, }\StringTok{'ion'}\NormalTok{, }\StringTok{'ire'}\NormalTok{, }\StringTok{'le'}\NormalTok{, }\StringTok{'leu'}\NormalTok{, }\StringTok{'leur'}\NormalTok{, }\StringTok{'li'}\NormalTok{, }\StringTok{'lice'}\NormalTok{, }\StringTok{'lie'}\NormalTok{, }\StringTok{'lien'}\NormalTok{, }\StringTok{'lier'}\NormalTok{, }\StringTok{'lieu'}\NormalTok{, }\StringTok{'lieue'}\NormalTok{, }\StringTok{'lieur'}\NormalTok{, }\StringTok{'lion'}\NormalTok{, }\StringTok{'lire'}\NormalTok{, }\StringTok{'liure'}\NormalTok{, }\StringTok{'lori'}\NormalTok{, }\StringTok{'me'}\NormalTok{, }\StringTok{'mec'}\NormalTok{, }\StringTok{'men'}\NormalTok{, }\StringTok{'menu'}\NormalTok{, }\StringTok{'menue'}\NormalTok{, }\StringTok{'menure'}\NormalTok{, }\StringTok{'mer'}\NormalTok{, }\StringTok{'merci'}\NormalTok{, }\StringTok{'mere'}\NormalTok{, }\StringTok{'merl'}\NormalTok{, }\StringTok{'merle'}\NormalTok{, }\StringTok{'meunerie'}\NormalTok{, }\StringTok{'meunier'}\NormalTok{, }\StringTok{'meuniere'}\NormalTok{, }\StringTok{'meure'}\NormalTok{, }\StringTok{'mn'}\NormalTok{, }\StringTok{'mu'}\NormalTok{, }\StringTok{'mue'}\NormalTok{, }\StringTok{'muee'}\NormalTok{, }\StringTok{'muer'}\NormalTok{, }\StringTok{'muni'}\NormalTok{, }\StringTok{'munie'}\NormalTok{, }\StringTok{'mur'}\NormalTok{, }\StringTok{'mure'}\NormalTok{, }\StringTok{'muree'}\NormalTok{, }\StringTok{'muri'}\NormalTok{, }\StringTok{'murie'}\NormalTok{, }\StringTok{'ne'}\NormalTok{, }\StringTok{'neo'}\NormalTok{, }\StringTok{'neroli'}\NormalTok{, }\StringTok{'neuf'}\NormalTok{, }\StringTok{'neume'}\NormalTok{, }\StringTok{'neur'}\NormalTok{, }\StringTok{'ni'}\NormalTok{, }\StringTok{'nie'}\NormalTok{, }\StringTok{'niece'}\NormalTok{, }\StringTok{'nieme'}\NormalTok{, }\StringTok{'nier'}\NormalTok{, }\StringTok{'no'}\NormalTok{, }\StringTok{'noce'}\NormalTok{, }\StringTok{'noceur'}\NormalTok{, }\StringTok{'noie'}\NormalTok{, }\StringTok{'noir'}\NormalTok{, }\StringTok{'noire'}\NormalTok{, }\StringTok{'nu'}\NormalTok{, }\StringTok{'nue'}\NormalTok{, }\StringTok{'nuee'}\NormalTok{, }\StringTok{'nuer'}\NormalTok{, }\StringTok{'nui'}\NormalTok{, }\StringTok{'nuire'}\NormalTok{, }\StringTok{'numero'}\NormalTok{, }\StringTok{'oc'}\NormalTok{, }\StringTok{'occire'}\NormalTok{, }\StringTok{'oeil'}\NormalTok{, }\StringTok{'oeuf'}\NormalTok{, }\StringTok{'oie'}\NormalTok{, }\StringTok{'oil'}\NormalTok{, }\StringTok{'ole'}\NormalTok{, }\StringTok{'oleum'}\NormalTok{, }\StringTok{'on'}\NormalTok{, }\StringTok{'or'}\NormalTok{, }\StringTok{'oree'}\NormalTok{, }\StringTok{'orle'}\NormalTok{, }\StringTok{'re'}\NormalTok{, }\StringTok{'recoin'}\NormalTok{, }\StringTok{'ree'}\NormalTok{, }\StringTok{'reel'}\NormalTok{, }\StringTok{'ref'}\NormalTok{, }\StringTok{'refui'}\NormalTok{, }\StringTok{'refuie'}\NormalTok{, }\StringTok{'rein'}\NormalTok{, }\StringTok{'relie'}\NormalTok{, }\StringTok{'rem'}\NormalTok{, }\StringTok{'remue'}\NormalTok{, }\StringTok{'remuee'}\NormalTok{, }\StringTok{'reuni'}\NormalTok{, }\StringTok{'reunie'}\NormalTok{, }\StringTok{'ri'}\NormalTok{, }\StringTok{'rie'}\NormalTok{, }\StringTok{'rien'}\NormalTok{, }\StringTok{'role'}\NormalTok{, }\StringTok{'ru'}\NormalTok{, }\StringTok{'rue'}\NormalTok{, }\StringTok{'ruee'}\NormalTok{, }\StringTok{'ruile'}\NormalTok{, }\StringTok{'rumen'}\NormalTok{, }\StringTok{'un'}\NormalTok{, }\StringTok{'une'}\NormalTok{, }\StringTok{'uni'}\NormalTok{, }\StringTok{'unie'}\NormalTok{, }\StringTok{'unieme'}\NormalTok{, }\StringTok{'ure'}\NormalTok{, }\StringTok{'uree'}\NormalTok{]}
\end{Highlighting}
\end{Shaded}

