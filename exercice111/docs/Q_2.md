    >>> from exercice import *

    >>> grille = [['f', 'e', 'e', 'o'], ['m', 'u', 'r', 'l'], ['n', 'e', 'i', 'c'], ['i', 'o', 'c', 'e']]
    >>> ruzzle = Ruzzle(grille)
    >>> ruzzle.copier_grille(ruzzle.grille)
    [['f', 'e', 'e', 'o'], ['m', 'u', 'r', 'l'], ['n', 'e', 'i', 'c'], ['i', 'o', 'c', 'e']]
    >>> ruzzle.copier_grille([[1, 2, 3], [2, 4, 5], [4, 5, 9]])
    [[1, 2, 3], [2, 4, 5], [4, 5, 9]]
    >>> ruzzle.copier_grille([[1, 2, 3], [4, 5, 9]])
    [[1, 2, 3], [4, 5, 9]]

 
