from arbrebinaire import *
from dictionnaire import *

dico = fichier_vers_arbre("dico_test.txt")

class Ruzzle:
    def __init__(self, grille = None):
        """La classe qui gère la grille du Ruzzle. Pour les besoins
        des tests, on peut passer une grille comme argument.

        En l'absence d'argument, une grille sera générée
        aléatoirement. Ce n'est pas à faire au cours de cet
        exercice. Vous devrez le faire la prochaine fois quand vous
        programmerez l'interface du jeu.
        """
        if grille is None:
            # Pas évaluez cette fois-ci.
            pass
        else:
            self.grille = grille
            
    def __str__(self):
        ligne_horizontale = "+"+ "-+"*len(self.grille)+"\n"
        texte = ligne_horizontale
        for i in range(len(self.grille)):
            ligne = "|"
            for j in range(len(self.grille[0])):
                ligne = ligne + self.grille[i][j]+"|"
            texte += ligne + "\n" + ligne_horizontale
        return texte

    def generer_masque(self):
        """
        Renvoie une grille de même dimension que self.grille
        remplie avec des valeur True. Ce masque nous servira lors du
        parcours de la grille pour savoir les cases par lesquelles on
        est déjà passé.
        """
        pass

    def copier_grille(self, g):
        """
        Méthode générique qui permet de copier une liste de
        liste. On l'utilisera pour copier le masque de grille lors de
        l'exploration récursive de la grille.
        """
        pass

    def liste_possibilites_avec_position_depart(self, l, c, masque, dico, listeP=[], prefixe=""):
        """Méthode récursive qui recherche tous les mots que l'on peut
        former en partant de la case (l, c).
        - l : l'indice de la
        ligne
        - c : l'indice de la colonne
        - masque : une grille de booléens.
          True : la case est à visiter. False, on est déja
          passé par la case.
        - dico : l'arbre qui contient tous les mots du dictionnaire.
        - listeP : contient les mots possibles
        - prefixe : le début du mot que l'on est en train de parcourir.
        """
        pass
    
    def liste_toutes_les_possibilites(self):
        """
        Renvoie la liste de tous les mots présents dans la grille.
        """
        pass

    def main(self):
        """
        Le corps du jeu à implémenter ulrérieurement.
        """
        pass
