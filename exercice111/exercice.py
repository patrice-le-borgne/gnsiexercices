from arbrebinaire import *
from dictionnaire import *

dico = fichier_vers_arbre("dico_test.txt")

class Ruzzle:
    def __init__(self, grille = None):
        """La classe qui gère la grille du Ruzzle. Pour les besoins
        des tests, on peut passer une grille comme argument.

        En l'absence d'argument, une grille sera générée
        aléatoirement. Ce n'est pas à faire au cours de cet
        exercice. Vous devrez le faire la prochaine fois quand vous
        programmerez l'interface du jeu.
        """
        if grille is None:
            # Pas évaluez cette fois-ci.
            pass
        else:
            self.grille = grille
            
    def __str__(self):
        ligne_horizontale = "+"+ "-+"*len(self.grille)+"\n"
        texte = ligne_horizontale
        for i in range(len(self.grille)):
            ligne = "|"
            for j in range(len(self.grille[0])):
                ligne = ligne + self.grille[i][j]+"|"
            texte += ligne + "\n" + ligne_horizontale
        return texte

    def generer_masque(self):
        """
        Renvoie une grille de même dimension que self.grille
        remplie avec des valeur True. Ce masque nous servira lors du
        parcours de la grille pour savoir les cases par lesquelles on
        est déjà passé.
        """
        n = len(self.grille)
        return ([[True for i in range(n)] for j in range(n)])

    def copier_grille(self, g):
        """
        Méthode générique qui permet de copier une liste de
        liste. On l'utilisera pour copier le masque de grille lors de
        l'exploration récursive de la grille.
        """
        m = []
        for i in range(len(g)):
            ligne = []
            for valeur in g[i]:
                ligne.append(valeur)
            m.append(ligne)
        return m

    def liste_possibilites_avec_position_depart(self, l, c, masque, dico, listeP=[], prefixe=""):
        """Méthode récursive qui recherche tous les mots que l'on peut
        former en partant de la case (l, c).
        - l : l'indice de la
        ligne
        - c : l'indice de la colonne
        - masque : une grille de booléens.
          True : la case est à visiter. False, on est déja
          passé par la case.
        - dico : l'arbre qui contient tous les mots du dictionnaire.
        - listeP : contient les mots possibles
        - prefixe : le début du mot que l'on est en train de parcourir.
        """
        m = self.copier_grille(masque) # on conserve la case où on est passé
        m[l][c] = False # on change l'état de la case
        prefixe = prefixe + self.grille[l][c] # on conserve le début du mot
        # si le mot est dans le dico, on l'ajoute à la liste
        #print(prefixe)
        if est_dans_dictionnaire(dico,prefixe) and len(prefixe) > 1 : 
            listeP.append(prefixe)
        # on continue à explorer pour trouver des mots avec la même racine
        if est_une_racine(dico, prefixe):
            # on parcourt les cases adjacentes 
            for i in range(-1, 2):
                for j in range(-1, 2):
                    # on évite de sortir de la grille
                    if 0 <= l+i < len(self.grille) and 0 <= c+j < len(self.grille[0]):
                        # on évite la case sur laquelle on se trouve
                        if m[l+i][c+j] and (i,j) != (0,0):
                            self.liste_possibilites_avec_position_depart(l+i, c+j, m, dico, listeP, prefixe)
        return listeP

    def liste_toutes_les_possibilites(self):
        """
        Renvoie la liste de tous les mots présents dans la grille.
        """
        l = []
        for i in range(len(self.grille)):
            for j in range(len(self.grille[0])):
                masque = self.generer_masque()
                l += self.liste_possibilites_avec_position_depart(i, j, masque, dico)      
        return sorted(list(set(l)))

    def main(self):
        """
        Le corps du jeu à implémenter ulrérieurement.
        """
        pass

    if __name__ == '__main__':
        
        grille = [['f', 'e', 'e', 'o'], ['m', 'u', 'r', 'l'], ['n', 'e', 'i', 'c'], ['i', 'o', 'c', 'e']]
        ruzzle = Ruzzle(grille)
        print(ruzzle)
        masque = ruzzle.generer_masque()
        print(ruzzle.liste_possibilites_avec_position_depart(0,0,masque,dico))      
        print(ruzzle.liste_toutes_les_possibilites())      
