Exercice
========

>>> from exercice import *

La classe `Ruzzle` est la classe principale du jeu. En particulier, elle
gère la grille. Dans cet exercice, nous nous intéressons juste aux
fonctions qui permettent de faire l'inventaire de tous les mots contenus
dans la grille. Par défaut, les dimensions d'une grille de Ruzzle sont
$4\times4$.

1.  La recherche des mots dans la grille s'apparente à un parcours en
    profondeur de graphe (même si ici, il s'agit de grille.) Pour ne pas
    tourner en rond, il faut tenir à jour la liste des cases par
    lesquelles on est passé. Le moyen le plus simple ici est de stocker
    cette information dans une grille de même taille avec des booléens.
    On a donné ici le nom `masque` à cette grille en référence au jeu du
    démineur. Les cases explorées sont découvertes donc elles ne sont
    pas masquées (`False`). Les cases à explorer sont masquées (`True`).

    Écrire la méthode `generer_masque()` qui renvoie une liste de listes
    de même dimension que `self.grille` remplie avec de `True`.


    >>> grille = [['f', 'e', 'e', 'o'], ['m', 'u', 'r', 'l'], ['n', 'e', 'i', 'c'], ['i', 'o', 'c', 'e']]
    >>> ruzzle = Ruzzle(grille)
    >>> ruzzle.generer_masque()
    [[True, True, True, True], [True, True, True, True], [True, True, True, True], [True, True, True, True]] 


2.  Dans un parcours en profondeur, on a nécessairement des retours en
    arrière. Pour gérer ce problème et retrouver une situation
    précédente on fera une copie du masque au début de chaque appel
    récursif.

    Écrire la methode `copier_grille(self, g)` qui renvoie une copie de
    la grille `g`.


    >>> ruzzle.copier_grille(ruzzle.grille)
    [['f', 'e', 'e', 'o'], ['m', 'u', 'r', 'l'], ['n', 'e', 'i', 'c'], ['i', 'o', 'c', 'e']]
    >>> ruzzle2 = Ruzzle(grille)
    >>> ruzzle2.copier_grille([[1, 2, 3], [2, 4, 5], [4, 5, 9]])
    [[1, 2, 3], [2, 4, 5], [4, 5, 9]]
    >>> ruzzle2.copier_grille([[1, 2, 3], [4, 5, 9]])
    [[1, 2, 3], [4, 5, 9]]


3.  Dans un premier temps, on va chercher la liste des mots dont on
    connait la premier case. Écrire la méthode récursive
    `liste_possibilites_avec_position_depart(self, l, c, masque, dico, listeP=[], prefixe="")`
    qui renvoie les mots dont on connait la premier case `(l,c)`.

    -   `l` : l'indice de la ligne
    -   `c` : l'indice de la colonne
    -   `masque` : une grille de booléens. `True` : la case est à
        visiter. `False`, on est déja passé par la case.
    -   `dico` : l'arbre qui contient tous les mots du dictionnaire.
    -   `listeP` : contient les mots possibles.
    -   `prefixe` : le début du mot que l'on est en train de parcourir.

    Dans l'algorithme suivant, `case initiale` correspond à la case que
    l'on traite au début de la fonction. Ensuite `case courante`
    correspond à une case voisine de la `case initiale` traitée au sein
    d'une boucle.


   liste_possibilites(case initiale)
       on copie la grille de masque;
       on modifie la case initiale du masque;
       on ajoute la lettre de la case initiale à prefixe;
       si le prefixe est un mot):
           on le rajoute à la liste;
       si le prefixe est la racine d'un mot :
           on parcourt les cases autour de la case initiale;
           si la case courante est dans la grille
               si la case courante n'est pas la case initiale et qu'elle n'a pas déjà été explorée :
                   liste_possibilites(case courante); 



    >>> masque = ruzzle.generer_masque()
    >>> possibilites = ruzzle.liste_possibilites_avec_position_depart(0, 0, masque, dico)
    >>> 'fee' in possibilites
    True
    >>> 'feerie' in possibilites
    True
    >>> 'ee' in possibilites
    False
    >>> sorted(list(set(possibilites)))
    ['fee', 'feerie', 'femur', 'fer', 'ferie', 'ferle', 'feru', 'ferue', 'feu', 'feue', 'feuil', 'fuel', 'fui', 'fuie', 'fuir', 'fume', 'fumee', 'fumer', 'fumerie', 'fur', 'furie', 'furole']


4.  Écrire la méthode `liste_toutes_les_possibilites()` qui renvoie la
    liste de tous les mots présents dans la grille. Les mots ne devront
    apparaitre qu'une fois et être rangés dans l'ordre alphabétique. Il
    y une solution dans les tests de la question précédente.


    >>> ruzzle.liste_toutes_les_possibilites()
    ['cc', 'ce', 'ceci', 'cenure', 'cerce', 'cercle', 'cerclee', 'cerium', 'cicero', 'cil', 'cire', 'ciree', 'cl', 'cle', 'clic', 'clore', 'coenure', 'coeur', 'coi', 'coin', 'con', 'cone', 'cree', 'creme', 'cri', 'cric', 'crie', 'cru', 'crue', 'cruel', 'ec', 'eclore', 'ecoeure', 'ecoeuree', 'ecoin', 'ecreme', 'ecremee', 'ecrie', 'ecru', 'ecrue', 'el', 'elier', 'elire', 'emeri', 'emeu', 'emu', 'emue', 'en', 'enumere', 'eolien', 'eon', 'ere', 'eu', 'eue', 'fee', 'feerie', 'femur', 'fer', 'ferie', 'ferle', 'feru', 'ferue', 'feu', 'feue', 'feuil', 'fuel', 'fui', 'fuie', 'fuir', 'fume', 'fumee', 'fumer', 'fumerie', 'fur', 'furie', 'furole', 'icone', 'il', 'ile', 'in', 'ion', 'ire', 'le', 'leu', 'leur', 'li', 'lice', 'lie', 'lien', 'lier', 'lieu', 'lieue', 'lieur', 'lion', 'lire', 'liure', 'lori', 'me', 'mec', 'men', 'menu', 'menue', 'menure', 'mer', 'merci', 'mere', 'merl', 'merle', 'meunerie', 'meunier', 'meuniere', 'meure', 'mn', 'mu', 'mue', 'muee', 'muer', 'muni', 'munie', 'mur', 'mure', 'muree', 'muri', 'murie', 'ne', 'neo', 'neroli', 'neuf', 'neume', 'neur', 'ni', 'nie', 'niece', 'nieme', 'nier', 'no', 'noce', 'noceur', 'noie', 'noir', 'noire', 'nu', 'nue', 'nuee', 'nuer', 'nui', 'nuire', 'numero', 'oc', 'occire', 'oeil', 'oeuf', 'oie', 'oil', 'ole', 'oleum', 'on', 'or', 'oree', 'orle', 're', 'recoin', 'ree', 'reel', 'ref', 'refui', 'refuie', 'rein', 'relie', 'rem', 'remue', 'remuee', 'reuni', 'reunie', 'ri', 'rie', 'rien', 'role', 'ru', 'rue', 'ruee', 'ruile', 'rumen', 'un', 'une', 'uni', 'unie', 'unieme', 'ure', 'uree']
    
    
