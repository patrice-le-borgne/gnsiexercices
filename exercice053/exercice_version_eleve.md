Exercice
========

Partie A
--------

``` {.python}
 Cet exercice porte sur les systėmes d'exploitation et la gestion des processus par un système d'exploitation.
```

Cet exercice pourra utiliser des commandes de systèmes d'exploitation de
type UNIX telles que `cd`, `1s` , `mkdir`, `rm` ,`rmd`, `mv`, `cat`.

1.  Dans un système d'exploitation de type UNIX, on considėre
    l'arborescence des fichiers suivante :

![arborescence](exo053_arborescence.png)

On souhaite, grâce à l'utilisation du terminal de commande, explorer et
modifier les répertoires et fichiers présents.

On suppose qu'on se trouve actuellement dans le dossier `/home/morgane`

1.  Parmi les 4 propositions suivantes, donner celle correspondant à
    l'affichage obtenu lors de l'utilisation de la commande `ls` :

-   **proposition 1** : lycee francais NSI info.txt image1.jpg perso
-   **proposition 2** : lycee perso
-   **proposition 3** : morgane
-   **proposition 4** : bin etc home tmp

2.  Écrire la commande qui permet, à partir de cet emplacement,
    d'atteindre le répertoire `lycee`.

<!-- -->

2.  On suppose maintenant qu'on se trouve dans le répertoire
    `/home/morgane/lycee/NSI`.

3.  Écrire la commande qui permet de créer, à cet emplacement, le
    dossier nommé `algorithmique`.

<!-- -->

2.  Écrire la commande qui permet, à partir de cet emplacement, de
    suppimer le fichier `image1.jpg`.

Partie B
--------

On rappelle qu'un processus est une instance d'application. Un processus
peut être démarré par l'utilisateur, par un périphérique ou par un autre
processus parent.

La commande UNIX `ps` présente un cliché instantanée des processus en
cours d'exécution.

![processus](exo053_processus.png)

On a exécuté la commande `ps` (avec quelques options qu'il n'est pas
nécessaire de connaitre pour la réussite de cet exercice). Un extrait du
résultat de la commande est présenté ci-dessous :

On rappelle : - l'`UID` est l'identifiant de l'utilisateur propriétaire
du processus ; - le `PID` est l'identifiant du procesus ; - le `PPID`
est l'identifiant du processus parent ; - `C` indique l'utilisation du
processus ; - `STIME` est l'heure de démarrage du processus ; - `TTY`
est le nom du terminal de commande auquel le processus est attaché ; -
`TIME` est la durée d'utilisation du processeur par le processus ; -
`CMD` le nom de commande utilisé pour démarrer le processus.

1.  Donner le `PID` du parent du processus démarré par la commande `vi`.

<!-- -->

2.  Donner le `PID` d'un processus enfant du processus démarré par la
    commande `xfce4-terminal`.

<!-- -->

3.  Citer le `PID` de deux processus qui ont le même parent.

<!-- -->

4.  Parmi tous les processus affichés, citer le `PID` des deux qui ont
    consommé le plus de temps du processus.

Partie C
--------

On considère le réseau suivant compososé de sept routeurs.

![réseau](exo_53_reseaux.png)

On donne les tables de routage préalablement construites ci-dessous avec
le protocole RIP (Routing information Protocole). Le protocole RIP
permet de construire les tables de routage des différerents routeurs, en
indiquant pour chaque routeur, la distance, en nombre de sauts, qui le
sépare d'un autre routeur.

![Tables de routage](exo_53_tables_de_routage.png)

1.  Le routeur R2 doit envoyer un paquet de données au routeur R7 qui en
    accuse réception. Déterminer le chemin parcouru par le paquet de
    données ainsi que celui parcouru par l'accusé de réception.

<!-- -->

2.  1.Indiquer la faiblesse que présente ce réseau en cas de panne du
    routeur R4.
    2.  Proposer une solution pour y remédier.

<!-- -->

3.  **Dans cette question uniquement**, on décide de rajouter un routeur
    R8 qui sera relié aux routeurs R2 et R6.
    -   Donner une table de routage pour R8 qui minimise le nombre de
        saut.
    -   Donner une nouvelle table de routage de R2,

<!-- -->

4.  **Pour la suite de l'exercice on considèrere le réseau sans le
    routeur R8.** Il a été décidé de modifier les règles de routage de
    ce réseau en appliquant dorénavant le protocole de routage OSPF qui
    prend en compte la bande passante.

Ce protocole attribue un coût à chaque liaison afin de priviligier le
choix de certaines routes plus rapides. Plus le coût est faible, plus le
lien est intéressant.

Le coût d'une liaison est calcul par la formule :

$$\text{coût} = \dfrac{10^8 \text{bit/s}}{\text{bande passante du lien en bit/s}}$$

Voici le tableau rérérençant les coûts de liaisons en fonction du type
deliaison entre 2 routeurs :

![couts](exo_53_couts.png)

On rappelle que 1 Mb/s = 1000 kb/s = $10^6$ bit/s.

``` {.python}
- Déterminer la bande passante du FastEthernet (FE) et justifier que le coût du reseau de type Ethernet (E) est de 10.

-  On précise sur le graphe ci-dessous les types de liaison dans notre réseau:
```

![liaisons](exo_53_liaisons.png)

Le coût d'un chemin est la somme des coûts des liaisons rencontrés.
Donner en justifiant le chemin le moins coûteux pour relier R2 à R5.
Préciser le coût.
