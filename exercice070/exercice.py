class ABR:
    def __init__(self, g0, v0, d0):
        self.gauche = g0
        self.cle = v0
        self.droit = d0

    def __repr__(self):
        if self is None:
            return ''
        else:
            return '(' + (self.gauche).__repr__() + ', ' + str(self.cle) + ', ' +(self.droit).__repr__() + ')'


def ajoute(cle, a): 
    if a is None:
        a = ABR(None, cle, None)
    elif cle > a.cle:
        a.droit = ajoute(cle, a.droit)
    elif cle < a.cle:
        a.gauche = ajoute(cle, a.gauche)
    return a
