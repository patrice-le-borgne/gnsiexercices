\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Dans cet exercice, un arbre binaire de caractères est stocké sous la
forme d'un dictionnaire où les clefs sont les caractères des nœuds de
l'arbre et les valeurs, pour chaque clef, la liste des caractères des
fils gauche et droit du nœud.

On utilise la valeur \texttt{\textquotesingle{}\textquotesingle{}} pour
représenter un fils vide.

Par exemple, l'arbre

``` \{.dot\}

digraph calc \{ graph{[}nodesep=0.1, ranksep=0.35, splines=line{]}; node
{[}fontname = ``Cousine'', fontsize=14, shape=circle{]};

// layout all nodes 1 row at a time // order matters on each line, but
not the order of lines ``F''; ``B'' , am, ``G''; ``A'', bm, ``D''; dm,
em, ``I''; ``C'', cm, ``E''; fm, gm, ``H'' hm, im, jm;

// make `mid' nodes invisible am, bm, cm, dm, em, fm, gm, hm, im, jm
{[}style=invis, label=""{]};

// layout all visible edges as parent -\textgreater{} left\_child,
right\_child ``F''-\textgreater{}``B'', ``G'';
``B''-\textgreater{}``A'', ``D''; ``D''-\textgreater{}``C'', ``E'';
``G'' -\textgreater{} ``I''; ``I'' -\textgreater{} ``H'';

// link mid nodes with a larger weight: edge {[}style=invis,
weight=10{]}; ``F'' -\textgreater{} am; ``B'' -\textgreater{} bm; ``D''
-\textgreater{} cm; ``G'' -\textgreater{} dm, em; ``I'' -\textgreater{}
fm, gm; dm -\textgreater{} hm, im, jm; \}

\includegraphics{exo120_arbre.png}\{: .center\}

est stocké dans

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{a }\OperatorTok{=}\NormalTok{ \{}\StringTok{'F'}\NormalTok{:[}\StringTok{'B'}\NormalTok{,}\StringTok{'G'}\NormalTok{], }\StringTok{'B'}\NormalTok{:[}\StringTok{'A'}\NormalTok{,}\StringTok{'D'}\NormalTok{], }\StringTok{'A'}\NormalTok{:[}\StringTok{''}\NormalTok{,}\StringTok{''}\NormalTok{], }
     \StringTok{'D'}\NormalTok{:[}\StringTok{'C'}\NormalTok{,}\StringTok{'E'}\NormalTok{], }\StringTok{'C'}\NormalTok{:[}\StringTok{''}\NormalTok{,}\StringTok{''}\NormalTok{], }\StringTok{'E'}\NormalTok{:[}\StringTok{''}\NormalTok{,}\StringTok{''}\NormalTok{], }
     \StringTok{'G'}\NormalTok{:[}\StringTok{''}\NormalTok{,}\StringTok{'I'}\NormalTok{], }\StringTok{'I'}\NormalTok{:[}\StringTok{''}\NormalTok{,}\StringTok{'H'}\NormalTok{], }\StringTok{'H'}\NormalTok{:[}\StringTok{''}\NormalTok{,}\StringTok{''}\NormalTok{]\}}
\end{Highlighting}
\end{Shaded}

Écrire une fonction récursive \texttt{taille} prenant en paramètres un
arbre binaire \texttt{arbre} non vide sous la forme d'un dictionnaire et
un caractère \texttt{lettre} qui est la valeur du sommet de l'arbre, et
qui renvoie la taille de l'arbre à savoir le nombre total de nœuds.

On observe que, par exemple, \texttt{arbre{[}lettre{]}{[}0{]}},
respectivement \texttt{arbre{[}lettre{]}{[}1{]}}, permet d'atteindre la
clé du sous-arbre gauche, respectivement droit, de l'arbre
\texttt{arbre} de sommet \texttt{lettre}.

Exemple :

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ taille(a, }\StringTok{'F'}\NormalTok{)}
\DecValTok{9}
\OperatorTok{>>>}\NormalTok{ taille(a, }\StringTok{'B'}\NormalTok{)}
\DecValTok{5}
\OperatorTok{>>>}\NormalTok{ taille(a, }\StringTok{'I'}\NormalTok{)}
\DecValTok{2}
\end{Highlighting}
\end{Shaded}

