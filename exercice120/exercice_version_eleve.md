Exercice
========

Dans cet exercice, un arbre binaire de caractères est stocké sous la
forme d'un dictionnaire où les clefs sont les caractères des nœuds de
l'arbre et les valeurs, pour chaque clef, la liste des caractères des
fils gauche et droit du nœud.

On utilise la valeur `''` pour représenter un fils vide.

Par exemple, l'arbre

\`\`\` {.dot}

digraph calc { graph\[nodesep=0.1, ranksep=0.35, splines=line\]; node
\[fontname = "Cousine", fontsize=14, shape=circle\];

// layout all nodes 1 row at a time // order matters on each line, but
not the order of lines "F"; "B" , am, "G"; "A", bm, "D"; dm, em, "I";
"C", cm, "E"; fm, gm, "H" hm, im, jm;

// make 'mid' nodes invisible am, bm, cm, dm, em, fm, gm, hm, im, jm
\[style=invis, label=\"\"\];

// layout all visible edges as parent -\> left\_child, right\_child
"F"-\>"B", "G"; "B"-\>"A", "D"; "D"-\>"C", "E"; "G" -\> "I"; "I" -\>
"H";

// link mid nodes with a larger weight: edge \[style=invis, weight=10\];
"F" -\> am; "B" -\> bm; "D" -\> cm; "G" -\> dm, em; "I" -\> fm, gm; dm
-\> hm, im, jm; }

![image](exo120_arbre.png){: .center}

est stocké dans

``` {.python}
a = {'F':['B','G'], 'B':['A','D'], 'A':['',''], 
     'D':['C','E'], 'C':['',''], 'E':['',''], 
     'G':['','I'], 'I':['','H'], 'H':['','']}
```

Écrire une fonction récursive `taille` prenant en paramètres un arbre
binaire `arbre` non vide sous la forme d'un dictionnaire et un caractère
`lettre` qui est la valeur du sommet de l'arbre, et qui renvoie la
taille de l'arbre à savoir le nombre total de nœuds.

On observe que, par exemple, `arbre[lettre][0]`, respectivement
`arbre[lettre][1]`, permet d'atteindre la clé du sous-arbre gauche,
respectivement droit, de l'arbre `arbre` de sommet `lettre`.

Exemple :

``` {.python}
>>> taille(a, 'F')
9
>>> taille(a, 'B')
5
>>> taille(a, 'I')
2
```
