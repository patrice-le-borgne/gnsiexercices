~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
==========

Écrire une fonction `enumere` qui prend en paramètre une liste `L` et renvoie un
dictionnaire `d` dont les clés sont les éléments de `L` avec pour valeur associée la liste des
indices de l’élément dans la liste `L`.

Exemple :

~~~ {.python .amc file="Q_1.md" bareme="2"}
    >>> enumere([1, 1, 2, 3, 2, 1])
    {1: [0, 1, 5], 2: [2, 4], 3: [3]}
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> enumere([1, 1, 2, 3, 2, 1])
{1: [0, 1, 5], 2: [2, 4], 3: [3]}
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> enumere([1, 10, 2, 3, 2, 10, 3])
{1: [0], 10: [1, 5], 2: [2, 4], 3: [3, 6]}
~~~




