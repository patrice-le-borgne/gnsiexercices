~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : structures linéaires
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

La fonction `recherche` prend en paramètres deux chaines de caractères `gene` et
`seq_adn` et renvoie `True` si on retrouve `gene` dans `seq_adn` et `False` sinon.
Compléter le code Python ci-dessous pour qu’il implémente la fonction `recherche`.

~~~ {.python .amc file="Q_1.md" bareme="4"}
def recherche(gene, seq_adn):
    n = len(seq_adn)
    g = len(gene)
    i = ...
    trouve = False
    while i < ... and trouve == ... :
        j = 0
        while j < g and gene[j] == seq_adn[i+j]:
            ...
        if j == g:
            trouve = True
        ...
    return trouve
~~~

Exemples :

~~~ {.python}
    >>> recherche("AATC", "GTACAAATCTTGCC")
    True
    >>> recherche("AGTC", "GTACAAATCTTGCC")
    False
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="2"}
>>> recherche("AATC", "GTACAAATCTTGCC")
True
>>> recherche("AGTC", "GTACAAATCTTGCC")
False
~~~

~~~ {.python .all .hidden .amc .test file="Q_2.md" bareme="2"}
>>> recherche("AATC", "GTA")
False
>>> recherche("GTACAAATCTTGCC", "GTACAAATCTTGCC")
True
>>> recherche("GTACAAATCTTGCCT", "GTACAAATCTTGCC")
False
~~~
