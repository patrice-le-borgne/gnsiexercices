    >>> from exercice import *

        >>> recherche([1, 2, 7, 5])
        [(1, 2)]
        >>> recherche([1, 4, 10, 3])
        []
        >>> recherche([7, 1, 2, 5, 3, 4])
        [(1, 2), (3, 4)]
        >>> recherche([5, 6, 7, 3, 8, -5, -4, 7])
        [(5, 6), (6, 7), (-5, -4)]

 
