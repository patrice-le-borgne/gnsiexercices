~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Écrire une fonction `recherche` qui prend en paramètre un tableau de nombres entiers
`tab`, et qui renvoie la liste (éventuellement vide) des couples d'entiers consécutifs
successifs qu'il peut y avoir dans `tab`.

Exemples :

~~~ {.python .amc file="Q_1.md" bareme="2"}
    >>> recherche([1, 4, 3, 5])
    []
    >>> recherche([1, 4, 5, 3])
    [(4, 5)]
    >>> recherche([7, 1, 2, 5, 3, 4])
    [(1, 2), (3, 4)]
    >>> recherche([5, 1, 2, 3, 8, -5, -4, 7])
    [(1, 2), (2, 3), (-5, -4)]
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="2"}
    >>> recherche([1, 2, 7, 5])
    [(1, 2)]
    >>> recherche([1, 4, 10, 3])
    []
    >>> recherche([7, 1, 2, 5, 3, 4])
    [(1, 2), (3, 4)]
    >>> recherche([5, 6, 7, 3, 8, -5, -4, 7])
    [(5, 6), (6, 7), (-5, -4)]
~~~
