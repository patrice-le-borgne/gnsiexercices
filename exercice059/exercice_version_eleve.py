
def alphabet():
    """
    renvoie une liste avec les lettres de l'alphabet en majuscules.
    """
    pass

def nouveau_dico_alphabet():
    """renvoie un dictionnaire associant chaque lettre de l'alphabet
    à un point (`'.'`). Le dictionnaire ainsi crée pourra nous être
    utile pour conserver l'association entre deux lettres. Par
    exemple, `d['A'] = 'E'` signifiera que le `'E'` est codé par un
    `'A'`. En revanche, `d['D'] = '.'` signifie qu'on ne sait pas à
    quelle lettre correspond le `'D'`.
    """
    pass

def index_max(l):
    """
    renvoie l'index du maximum de la liste `l`, le premier s'il y
    le maximum apparaît plusieurs fois.
    """
    pass

def effectifs(texte):
    """
    renvoie une liste avec le nombre d'apparition d'une lettre
    dans `texte`. Par exemple, si `e = effectif(texte)`, `e[0]`
    correspond au nombre de `'A'` dans le `texte`, `e[1]` correspond
    au nombre de `'B'` dans le `texte`, etc...
    """
    pass

def lettre_les_plus_frequentes(liste):
    """
    à partir d'une liste correspondant aux effectifs des lettres
    d'une texte, renvoie les 10 lettres les plus fréquentes dans un
    texte, dans l'ordre décroissant de fréquence d'apparition.
    """
    pass
 

def traduire(texte,dico):
    """
    renvoie un texte avec les lettres connues. Les lettres
    inconnues sont remplacées par des points.
    """
    pass


