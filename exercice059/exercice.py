
def alphabet():
    """
    renvoie une liste avec les lettres de l'alphabet en majuscules.
    """
    texte = []
    for i in range(65, 65+26):
        texte.append(chr(i))
    return texte

def nouveau_dico_alphabet():
    """renvoie un dictionnaire associant chaque lettre de l'alphabet
    à un point (`'.'`). Le dictionnaire ainsi crée pourra nous être
    utile pour conserver l'association entre deux lettres. Par
    exemple, `d['A'] = 'E'` signifiera que le `'E'` est codé par un
    `'A'`. En revanche, `d['D'] = '.'` signifie qu'on ne sait pas à
    quelle lettre correspond le `'D'`.
    """
    dico = {}
    for i in range(65, 65+26):
        dico[chr(i)] = "."
    return dico

def index_max(l):
    """
    renvoie l'index du maximum de la liste `l`, le premier s'il y
    le maximum apparaît plusieurs fois.
    """
    maxi = l[0]
    pos = 0
    for i in range(len(l)):
        if l[i] > maxi:
            maxi = l[i]
            pos = i
    return pos

def effectifs(texte):
    """
    renvoie une liste avec le nombre d'apparition d'une lettre
    dans `texte`. Par exemple, si `e = effectif(texte)`, `e[0]`
    correspond au nombre de `'A'` dans le `texte`, `e[1]` correspond
    au nombre de `'B'` dans le `texte`, etc...
    """
    resultat = [0]*26
    for i in texte:
        if i != " ":
            n = ord(i)-ord('A')
            resultat[n] += 1
    return resultat

def lettre_les_plus_frequentes(liste):
    """
    à partir d'une liste correspondant aux effectifs des lettres
    d'une texte, renvoie les 10 lettres les plus fréquentes dans un
    texte, dans l'ordre décroissant de fréquence d'apparition.
    """
    l1 = liste[:]
    l = []
    for i in range(10):
        n = index_max(l1)
        l1[n] = 0
        l.append(chr(ord("A")+n))
    return l
 

def traduire(texte,dico):
    """
    renvoie un texte avec les lettres connues. Les lettres
    inconnues sont remplacées par des points.
    """
    t = ""
    alpha = alphabet()
    for i in texte:
        if i in alpha:
            t = t + dico[i]
        else:
            t = t + " "
    return t


