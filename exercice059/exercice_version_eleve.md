Exercice
========

L'objectif de cet exercice est d'écrire un petit programme qui aide à
décrypté un message codé suivant le codage de César évolué. C'est-à-dire
que les lettres ne sont pas justes décalées mais mélangées
aléatoirement. Pour simplifier le problème, les messages sont écrits en
majuscules sans ponctuation.

A. Quelques fonctions utiles
----------------------------

1.  Écrire une fonction `alphabet()` qui renvoie une liste avec les
    lettres de l'alphabet en majuscules.

``` {.python}
>>> a = alphabet()
>>> type(a)
<class 'list'>
>>> len(a)
26
```

2.  Écrire une fonction `nouveau_dico_alphabet()` qui renvoie un
    dictionnaire associant chaque lettre de l'alphabet à un point
    (`'.'`). Le dictionnaire ainsi crée pourra nous être utile pour
    conserver l'association entre deux lettres. Par exemple,
    `d['A'] = 'E'` signifiera que le `'E'` est codé par un `'A'`. En
    revanche, `d['D'] = '.'` signifie qu'on ne sait pas à quelle lettre
    correspond le `'D'`.

``` {.python}
>>> d = nouveau_dico_alphabet()
>>> type(d)
<class 'dict'>
>>> len(d)
26
>>> d['A']
'.'
```

3.  Écrire une fonction `index_max(l)` qui renvoie l'index du maximum de
    la liste `l`, le premier s'il y le maximum apparaît plusieurs fois.

``` {.python}
>>> index_max([12, 5, 6, 15, 8, 9])
3
>>> index_max([12, 5, 6, 5, 8, 9])
0
```

B. Quelques statistiques
------------------------

Cette méthode de codage peut être cassée grâce aux statistiques. En
effet, la fréquence d'apparition des lettres dépend de la langue dans
laquelle a été écrite le message. Voila les statistiques du français à
partir du roman *Germinal* d'Émile Zola. ![](Selection_001.png)

Pour décoder un texte, nous allons donc étudier la fréquence
d'apparition de chaque lettre. Le tableau précédent nous permettra
ensuite d'orienter nos recherches.

1.  Écrire une fonction `effectif(texte)` qui renvoie une liste avec le
    nombre d'apparition d'une lettre dans `texte`. Par exemple, si
    `e = effectif(texte)`, `e[0]` correspond au nombre de `'A'` dans le
    `texte`, `e[1]` correspond au nombre de `'B'` dans le `texte`,
    etc...

``` {.python}
>>> t = "ABRACADABRA"
>>> e = effectifs(t)
>>> e[0]
5
>>> e[1]
2
>>> e[2]
1
>>> e[3]
1
>>> e[4]
0
```

2.  Écrire une fonction `lettre_les_plus_frequentes(liste)` qui, à
    partir d'une liste correspondant aux effectifs des lettres d'une
    texte, renvoie les 10 lettres les plus fréquentes dans un texte,
    dans l'ordre décroissant de fréquence d'apparition.

``` {.python}
>>> t = "MCXZ JC AJCQXT ICZT  ZSBZ JC XBQL ZCXZ TLSQJTZ  M BXT SGZOBIQLT TL M BXT TACQZZTBI M TXOIT  BX PSHHT ZBQECQL ZTBJ JC VICXMT ISBLT MT HCIOPQTXXTZ C HSXLZSB  MQF YQJSHTLITZ MT ACET OSBACXL LSBL MISQL  C LICETIZ JTZ OPCHAZ MT GTLLTICETZ  MTECXL JBQ  QJ XT ESRCQL HTHT ACZ JT ZSJ XSQI  TL QJ X CECQL JC ZTXZCLQSX MT J QHHTXZT PSIQKSX AJCL NBT ACI JTZ ZSBUUJTZ MB ETXL MT HCIZ  MTZ ICUCJTZ JCIVTZ OSHHT ZBI BXT HTI  VJCOTTZ M CESQI GCJCRT MTZ JQTBTZ MT HCICQZ TL MT LTIITZ XBTZ  CBOBXT SHGIT M CIGIT XT LCOPCQL JT OQTJ  JT ACET ZT MTISBJCQL CETO JC ITOLQLBMT M BXT WTLTT  CB HQJQTB MT J THGIBX CETBVJCXL MTZ LTXTGITZ"
>>> e = effectifs(t)
>>> lettre_les_plus_frequentes(e)
['T', 'C', 'Z', 'L', 'I', 'J', 'B', 'X', 'Q', 'M']
```

C. Vers un assistant de décodage
--------------------------------

On veut fabriquer un outil qui nous aide à décoder un message. Pour
cela, on va utiliser un dictionnaire qui va garder en mémoire toutes les
lettres déjà trouvées et qui va nous afficher le texte avec ce niveau de
connaissance.

Par exemple, si le texte codé est `"ZNTZVZFZNTZ"`. Si on pense que le
`'Z'` correspond à un `'A'`, et que c'est la seule lettre que l'on a
découvert, alors on veut afficher `'A..A.A.A..A'`.

1.  Écrire une fonction `traduire(texte,dico)` qui renvoie seulement les
    lettres connues du `texte`.

``` {.python}
>>> t = "ZNTZVZFZNTZ"
>>> d = nouveau_dico_alphabet()
>>> d['Z'] = 'A'
>>> traduire(t,d)
'A..A.A.A..A'
```

Je pense que maintenant vous avez assez d'outils pour programmer un
assistant de décodage qui vous aidera à décoder le message de l'exercice
Moodle.

**Bon courage ! Soyez persévérant !**
