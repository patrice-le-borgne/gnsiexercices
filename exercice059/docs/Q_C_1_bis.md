    >>> from exercice import *

    >>> t = 'MCXZ JC AJCQXT'
    >>> d = nouveau_dico_alphabet()
    >>> d['C'] = 'A'
    >>> traduire(t,d)
    '.A.. .A ..A...'
    >>> d['Z'] = 'S'
    >>> d['J'] = 'L'
    >>> traduire(t,d)
    '.A.S LA .LA...'

 
