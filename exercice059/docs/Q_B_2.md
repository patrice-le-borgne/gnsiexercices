    >>> from exercice import *

    >>> t = "MCXZ JC AJCQXT ICZT  ZSBZ JC XBQL ZCXZ TLSQJTZ  M BXT SGZOBIQLT TL M BXT TACQZZTBI M TXOIT  BX PSHHT ZBQECQL ZTBJ JC VICXMT ISBLT MT HCIOPQTXXTZ C HSXLZSB  MQF YQJSHTLITZ MT ACET OSBACXL LSBL MISQL  C LICETIZ JTZ OPCHAZ MT GTLLTICETZ  MTECXL JBQ  QJ XT ESRCQL HTHT ACZ JT ZSJ XSQI  TL QJ X CECQL JC ZTXZCLQSX MT J QHHTXZT PSIQKSX AJCL NBT ACI JTZ ZSBUUJTZ MB ETXL MT HCIZ  MTZ ICUCJTZ JCIVTZ OSHHT ZBI BXT HTI  VJCOTTZ M CESQI GCJCRT MTZ JQTBTZ MT HCICQZ TL MT LTIITZ XBTZ  CBOBXT SHGIT M CIGIT XT LCOPCQL JT OQTJ  JT ACET ZT MTISBJCQL CETO JC ITOLQLBMT M BXT WTLTT  CB HQJQTB MT J THGIBX CETBVJCXL MTZ LTXTGITZ"
    >>> e = effectifs(t)
    >>> lettre_les_plus_frequentes(e)
    ['T', 'C', 'Z', 'L', 'I', 'J', 'B', 'X', 'Q', 'M']

 
