~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
==========

Programmer la fonction `fusion` prenant en paramètres deux tableaux non vides `tab1` et `tab2`
(type `list`) d'entiers, chacun dans l’ordre croissant, et renvoyant un tableau trié dans l’ordre
croissant et contenant l’ensemble des valeurs de `tab1` et `tab2`.

Exemples :

~~~ {.python .amc file="Q_1.md" bareme="3"}
    >>> fusion([3, 5], [2, 5])
    [2, 3, 5, 5]
    >>> fusion([-2, 4], [-3, 5, 10])
    [-3, -2, 4, 5, 10]
    >>> fusion([4], [2, 6])
    [2, 4, 6]
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> fusion([], [1, 3])
[1, 3]
>>> fusion([1, 3], [])
[1, 3]
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="2"}
>>> fusion([3, 5], [2, 5])
[2, 3, 5, 5]
>>> fusion([-2, 4], [-3, 5, 10])
[-3, -2, 4, 5, 10]
>>> fusion([6], [2, 4])
[2, 4, 6]
~~~



