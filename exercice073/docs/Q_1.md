    >>> from exercice import *
    >>> resultats = {'Dupont': {
    ...                         'DS1': [15.5, 4],
    ...                         'DM1': [14.5, 1],
    ...                         'DS2': [13, 4],
    ...                         'PROJET1': [16, 3],
    ...                         'DS3': [14, 4]
    ...                     },
    ...             'Durand': {
    ...                         'DS1': [6 , 4],
    ...                         'DM1': [14.5, 1],
    ...                         'DS2': [8, 4],
    ...                         'PROJET1': [9, 3],
    ...                         'IE1': [7, 2],
    ...                         'DS3': [8, 4],
    ...                         'DS4':[15, 4]
    ...                     }
    ...             }

    >>> moyenne('Dupont', resultats)
    14.5
    >>> resultats['Dupont']['DS4'] = [5, 2]
    >>> moyenne('Dupont', resultats)
    13.5

 
