\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

On considère dans cet exercice la suite de nombre suivante : 1, 11, 21,
1211, 111221, \ldots{}

Cette suite est construite ainsi : pour passer d'une valeur à la
suivante, on la lit et on l'écrit sous la forme d'un nombre. Ainsi, pour
1211 :

\begin{itemize}
\tightlist
\item
  on lit \emph{un 1, un 2, deux 1} ;
\item
  on écrit donc en nombre \emph{1 1, 1 2, 2 1} ;
\item
  puis on concatène \emph{111221}.
\end{itemize}

Compléter la fonction \texttt{nombre\_suivant} qui prend en entrée un
nombre sous forme de chaine de caractères et qui renvoie le nombre
suivant par ce procédé, encore sous forme de chaîne de caractères.

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ nombre_suivant(s):}
    \CommentTok{'''Renvoie le nombre suivant de celui representé par s}
\CommentTok{    en appliquant le procédé de lecture.'''}
\NormalTok{    resultat }\OperatorTok{=} \StringTok{''}
\NormalTok{    chiffre }\OperatorTok{=}\NormalTok{ s[}\DecValTok{0}\NormalTok{]}
\NormalTok{    compte }\OperatorTok{=} \DecValTok{1}
    \ControlFlowTok{for}\NormalTok{ i }\KeywordTok{in} \BuiltInTok{range}\NormalTok{(...): }
        \ControlFlowTok{if}\NormalTok{ s[i] }\OperatorTok{==}\NormalTok{ chiffre:}
\NormalTok{            compte }\OperatorTok{=}\NormalTok{ ... }
        \ControlFlowTok{else}\NormalTok{:}
\NormalTok{            resultat }\OperatorTok{+=}\NormalTok{ ... }\OperatorTok{+}\NormalTok{ ... }
\NormalTok{            chiffre }\OperatorTok{=}\NormalTok{ ... }
\NormalTok{            ...}
\NormalTok{    lecture_... }\OperatorTok{=}\NormalTok{ ... }\OperatorTok{+}\NormalTok{ ... }
\NormalTok{    resultat }\OperatorTok{+=}\NormalTok{ lecture_chiffre}
    \ControlFlowTok{return}\NormalTok{ resultat}
\end{Highlighting}
\end{Shaded}

Exemples :

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ nombre_suivant(}\StringTok{'1211'}\NormalTok{)}
\CommentTok{'111221'}
\OperatorTok{>>>}\NormalTok{ nombre_suivant(}\StringTok{'311'}\NormalTok{)}
\CommentTok{'1321'}
\end{Highlighting}
\end{Shaded}

