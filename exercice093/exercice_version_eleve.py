"""
Module rangée.
"""


from carte import Carte

class Rangee:
    """
    La classe Rangee se comporte comme une file avec au minimum une carte et au
    maximum 6 cartes.
    Les attributs :
      - cartes : une liste avec au moins une carte ;
    Les méthodes :
     - __init__(self, c) : une carte c doit être fournie à la création de la rangée.
                          Si c n'est pas une carte, cela provoque une AssertionError.
     - get_derniere_carte(self): Renvoie la dernière carte de la rangée             
     - get_cartes(self): Renvoie la liste de cartes.                                
     - set_cartes(self, l): remplace la liste de cartes par la liste l.             
     - ajouter(self, c):Ajoute le carte c à la liste. Si c est la sixième           
     -                carte, les 5 premières sont renvoyées dans une liste.         
     - get_nombre_tdb(self): renvoie le nombre de têtes de bœufs                    
     -                     contenues dans la rangée.                                
     - __len(self)__:renvoie la longueur de la rangée.                              
     - __getitem(self, i)__ : renvoie la i-ème carte de la rangée                   
     - __str__(self): pour afficher la liste de cartes séparées par un tiret.       
    `
    """

    def __init__(self, c):
        """
        Une carte doit être fournie à la création de la rangée.
        Si c n'est pas une carte, cela provoque une AssertionError.
        """
        pass

    def __len__(self):
        """
        Renvoie la longueur de la rangée
        """
        pass

    def get_cartes(self):
        """
        Renvoie la liste de cartes.
        """
        pass

    def get_derniere_carte(self):
        """
        Renvoie la dernière pile de la rangée
        """
        pass


    def set_cartes(self, l):
        """
        Change la liste de cartes.
        """
        pass
    
    def ajouter(self, c):
        """
        Ajoute le carte c à la liste. Si c est la sixième
        carte, les 5 premières sont renvoyées dans une liste.
        """
        pass

    def get_nombre_tdb(self):
        """
        renvoie le nombre de têtes de bœufs
        contenues dans la rangée.
        """
        pass

    
    def __str__(self):
        """
        Méthode string pour affichage texte.
        """
        pass
