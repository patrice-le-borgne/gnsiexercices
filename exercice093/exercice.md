~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> r2 = Rangee(Carte(10))
~~~

Exercice
========

Une `Rangee` est une `liste` qui contient au minimum une carte et au maximum 6 cartes.

,-------------------------------------------------------------------------------.
|Rangee                                                                         |
|-------------------------------------------------------------------------------|
|+ cartes : la liste des cartes                                                 |
|                                                                               |
|+ __init__(self, c) : une carte c doit être fournie à la création de la rangée.|
|                     Si c n'est pas une carte, cela provoque une AssertionError|
|+ __len(self)__:renvoie la longueur de la rangée.                              |
|+ get_derniere_carte(self): Renvoie la dernière carte de la rangée             |
|+ get_cartes(self): Renvoie la liste de cartes.                                |
|+ set_cartes(self, l): remplace la liste de cartes par la liste l.             |
|+ ajouter(self, c):Ajoute le carte c à la liste. Si c est la sixième           |
|                 carte, les 5 premières sont renvoyées dans une liste.         |
|+ get_nombre_tdb(self): renvoie le nombre de têtes de bœufs                    |
|                      contenues dans la rangée.                                |
|+ __str__(self): pour afficher la liste de cartes séparées par un tiret.       |
`-------------------------------------------------------------------------------'


1. Ne nous trompons pas entre la liste et les éléments de la liste....

~~~ {.python  .test .amc file="Q_1.md" bareme="1"}
    >>> r = Rangee(Carte(3))
	>>> len(r)
	1
    >>> r.get_derniere_carte()
    Carte n°3 - 1 TdB
    >>> r.get_cartes()
    [Carte n°3 - 1 TdB]
~~~

~~~ {.python .hidden .test file="Q_1_bis.md" bareme="1"}
	>>> r = Rangee(3)
	Traceback (most recent call last):
    ...
    AssertionError
~~~

2. Tant que la rangée n'est pas pleine, elle renvoie une liste vide à chaque ajout.

~~~ {.python .test .amc file="Q_2.md" bareme="1"}
    >>> r = Rangee(Carte(3))
    >>> r.ajouter(Carte(55))
    []
    >>> r.get_derniere_carte()
    Carte n°55 - 7 TdB
    >>> r.get_nombre_tdb()
    8
    >>> print(r)
    3-55
    >>> r.ajouter(Carte(63))
    []
    >>> r.get_derniere_carte()
    Carte n°63 - 1 TdB
    >>> print(r)
    3-55-63
    >>> r.get_nombre_tdb()
    9
    >>> r.ajouter(Carte(76))
    []
    >>> r.ajouter(Carte(82))
    []
    >>> r.get_nombre_tdb()
    11 
~~~

~~~ {.python .hidden .test file="Q_2_bis.md" bareme="1"}
>>> r2.ajouter(Carte(11))
[]
>>> r2.ajouter(Carte(21))
[]
>>> len(r2)
3
>>> r2.ajouter(Carte(31))
[]
>>> r2.ajouter(Carte(41))
[]
>>> len(r2)
5
>>> r2.get_derniere_carte()
Carte n°41 - 1 TdB
~~~



3. Quand la sixième carte est ajoutée, les 5 premières sont renvoyées
dans une liste et la liste des cartes de la rangées est réduite à la
dernière carte ajoutée.

~~~ {.python .test .amc file="Q_3.md" bareme="1"}
    >>> r = Rangee(Carte(3))
    >>> r.ajouter(Carte(55))
	[]
	>>> r.ajouter(Carte(63))
	[]
	>>> r.ajouter(Carte(76))
    []
    >>> r.ajouter(Carte(82))
    []

    >>> print(r)
    3-55-63-76-82
    >>> r.ajouter(Carte(85))
    [Carte n°3 - 1 TdB, Carte n°55 - 7 TdB, Carte n°63 - 1 TdB, Carte n°76 - 1 TdB, Carte n°82 - 1 TdB]
    >>> print(r)
    85
    >>> r.cartes
    [Carte n°85 - 2 TdB]
~~~

~~~ {.python .hidden .test file="Q_3_bis.md" bareme="1"}
>>> r2.ajouter(Carte(11))
[]
>>> r2.ajouter(Carte(21))
[]
>>> r2.ajouter(Carte(31))
[]
>>> r2.ajouter(Carte(41))
[]
>>> r2.ajouter(Carte(51))
[Carte n°10 - 3 TdB, Carte n°11 - 5 TdB, Carte n°21 - 1 TdB, Carte n°31 - 1 TdB, Carte n°41 - 1 TdB]
>>> len(r2)
1
~~~



4. On peut remplacer la liste des cartes.

~~~ {.python .test .amc file="Q_4.md" bareme="1"}
    >>> r = Rangee(Carte(3))
	>>> r.set_cartes([Carte(100)])
    >>> r.cartes
    [Carte n°100 - 3 TdB]
~~~	

~~~ {.python .hidden .test file="Q_4_bis.md" bareme="1"}
>>> r2.set_cartes([Carte(1)])
>>> r2.get_derniere_carte()
Carte n°1 - 1 TdB
~~~

