    >>> from exercice import *
    >>> r2 = Rangee(Carte(10))

    >>> r2.ajouter(Carte(11))
    []
    >>> r2.ajouter(Carte(21))
    []
    >>> r2.ajouter(Carte(31))
    []
    >>> r2.ajouter(Carte(41))
    []
    >>> r2.ajouter(Carte(51))
    [Carte n°10 - 3 TdB, Carte n°11 - 5 TdB, Carte n°21 - 1 TdB, Carte n°31 - 1 TdB, Carte n°41 - 1 TdB]
    >>> len(r2)
    1

 
