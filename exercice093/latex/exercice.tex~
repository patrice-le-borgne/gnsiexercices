\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Une \texttt{Rangee} est une \texttt{liste} qui contient au minimum une
carte et au maximum 6 cartes.

,-------------------------------------------------------------------------------.
\textbar{}Rangee \textbar{}
\textbar{}-------------------------------------------------------------------------------\textbar{}
\textbar{}+ cartes : la liste des cartes \textbar{} \textbar{}
\textbar{} \textbar{}+ \textbf{init}(self, c) : une carte c doit être
fournie à la création de la rangée.\textbar{} \textbar{}+
\textbf{len(self)}:renvoie la longueur de la rangée. \textbar{}
\textbar{}+ get\_derniere\_carte(self): Renvoie la dernière carte de la
rangée \textbar{} \textbar{}+ get\_cartes(self): Renvoie la liste de
cartes. \textbar{} \textbar{}+ set\_cartes(self, l): remplace la liste
de cartes par la liste l. \textbar{} \textbar{}+ ajouter(self, c):Ajoute
le carte c à la liste. Si c est la sixième \textbar{} \textbar{} carte,
les 5 premières sont renvoyées dans une liste. \textbar{} \textbar{}+
get\_nombre\_tdb(self): renvoie le nombre de têtes de bœufs \textbar{}
\textbar{} contenues dans la rangée. \textbar{} \textbar{}+
\textbf{str}(self): pour afficher la liste de cartes séparées par un
tiret. \textbar{}
`-------------------------------------------------------------------------------'

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Ne nous trompons pas entre la liste et les éléments de la
  liste\ldots{}.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ r }\OperatorTok{=}\NormalTok{ Rangee(Carte(}\DecValTok{3}\NormalTok{))}
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(r)}
    \DecValTok{1}
    \OperatorTok{>>>}\NormalTok{ r.get_derniere_carte()}
\NormalTok{    Carte n°}\DecValTok{3} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB}
    \OperatorTok{>>>}\NormalTok{ r.get_cartes()}
\NormalTok{    [Carte n°}\DecValTok{3} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB]}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Tant que la rangée n'est pas pleine, elle renvoie une liste vide à
  chaque ajout.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ r.ajouter(Carte(}\DecValTok{55}\NormalTok{))}
\NormalTok{    []}
    \OperatorTok{>>>}\NormalTok{ r.get_derniere_carte()}
\NormalTok{    Carte n°}\DecValTok{55} \OperatorTok{-} \DecValTok{7}\NormalTok{ TdB}
    \OperatorTok{>>>}\NormalTok{ r.get_nombre_tdb()}
    \DecValTok{8}
    \OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(r)}
    \DecValTok{3-55}
    \OperatorTok{>>>}\NormalTok{ r.ajouter(Carte(}\DecValTok{63}\NormalTok{))}
\NormalTok{    []}
    \OperatorTok{>>>}\NormalTok{ r.get_derniere_carte()}
\NormalTok{    Carte n°}\DecValTok{63} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB}
    \OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(r)}
    \DecValTok{3-55-63}
    \OperatorTok{>>>}\NormalTok{ r.get_nombre_tdb()}
    \DecValTok{9}
    \OperatorTok{>>>}\NormalTok{ r.ajouter(Carte(}\DecValTok{76}\NormalTok{))}
\NormalTok{    []}
    \OperatorTok{>>>}\NormalTok{ r.ajouter(Carte(}\DecValTok{82}\NormalTok{))}
\NormalTok{    []}
    \OperatorTok{>>>}\NormalTok{ r.get_nombre_tdb()}
    \DecValTok{11} 
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  Quand la sixième carte est ajoutée, les 5 premières sont renvoyées
  dans une liste et la liste des cartes de la rangées est réduite à la
  dernière carte ajoutée.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(r)}
    \DecValTok{3-55-63-76-82}
    \OperatorTok{>>>}\NormalTok{ r.ajouter(Carte(}\DecValTok{85}\NormalTok{))}
\NormalTok{    [Carte n°}\DecValTok{3} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB, Carte n°}\DecValTok{55} \OperatorTok{-} \DecValTok{7}\NormalTok{ TdB, Carte n°}\DecValTok{63} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB, Carte n°}\DecValTok{76} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB, Carte n°}\DecValTok{82} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB]}
    \OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(r)}
    \DecValTok{85}
    \OperatorTok{>>>}\NormalTok{ r.cartes}
\NormalTok{    [Carte n°}\DecValTok{85} \OperatorTok{-} \DecValTok{2}\NormalTok{ TdB]}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  On peut remplacer la liste des cartes.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ r.set_cartes([Carte(}\DecValTok{100}\NormalTok{)])}
    \OperatorTok{>>>}\NormalTok{ r.cartes}
\NormalTok{    [Carte n°}\DecValTok{100} \OperatorTok{-} \DecValTok{3}\NormalTok{ TdB]}
\end{Highlighting}
\end{Shaded}

