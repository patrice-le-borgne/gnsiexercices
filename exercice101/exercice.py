## Partie A ##

reponse_A_1 = 'a'
reponse_A_2 = 'b'
reponse_A_3 = 'c'
reponse_A_4 = 'c'
reponse_A_5 = 'b'

## Partie C ##

# Question 2.a
# Compléter les tuplés suivants (routeur, cout).

R1 = [('R2', 50), ('R3', 10), ('R4', 40), ('R5', 60), ('R6', 20), ('R7', 30), ('R8', 80)]

# Question 2.b
# Compléter le chemin suivant. Mettez bien des majuscules.
# Séparer les routes par un -. Aucun espace dans la chaine de caractères.
      
chemin = "L1-R1-R3-R6-R7-R4-R5-R8-L2"

# Question 2.c

cout_max = 40
      
