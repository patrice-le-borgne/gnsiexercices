Exercice
========

Cet exercice porte sur les réseaux et le routage. Les deux parties de
cet exercice sont indépendantes.

**Partie A**

À son domicile, une élève remarque que l'adresse IP de l'interface
réseau (carte wifi) de son ordinateur personnel est 192.168.1.14 avec le
masque 255.255.255.0.

Pour chacune des questions ci-dessous, recopier la seule bonne réponse.
1. Sous Unix, quelle instruction en ligne de commande a pu délivrer
cette information ? - `ifconfig` - `ping` - `ps` - `ls`

2.  Parmi les protocoles ci-dessous, quel est celui qui a permis
    d'attribuer automatiquement cette adresse IP ?
    a-   DNS
    b-   DHCP
    c-   TCP
    d-   HTTP

3.  Parmi les adresses IP ci-dessous, quelle est la seule possible pour
    un autre appareil connecté au même réseau ?
    a-   192.168.0.14
    b-   192.168.0.1
    c-   192.168.1.1
    d-   192.168.1.255



4.  Toujours à son domicile, l'élève consulte une page web qui prétend
    que l'adresse IP de son ordinateur est 88.168.10.210.
    a-   C'est une fausse information car son adresse IP est 192.168.1.14
        .
    b-   C'est sûrement faux car seul le fournisseur d'accès peut avoir
        connaissance de cette information.
    c-   C'est possible et cette adresse serait celle de la box vers
        Internet.
    d-   C'est possible, mais cela signifierait que l'ordinateur est
        infecté par un malware.



5.  Est-il possible qu'un ordinateur connecté au réseau du lycée possède
    la même adresse IP que l'élève à son domicile ?
    a-   Oui, à condition que les connexions n'aient pas lieu au même
        moment.
    b-   Oui, car les adresses 192.168.x.x ne sont pas routées sur
        Internet.
    c-   Oui, à condition d'utiliser un VPN.
    d   Non, car deux machines sont identifiées de manière unique par
        leur adresse IP.

**Partie B**

On représente ci-dessous un réseau dans lequel `R1`, `R2`, `R3`, `R4`,
`R5`, `R6`, `R7` et `R8` sont des routeurs. Le réseau local `L1` est
relié au routeur `R1` et le réseau local `L2` au routeur `R8`.

L1 --- R1 --- R2
       | \    | 
       |  \   | 
       |   \  | 
       |    \ | 
       R3 --- R4 --- R5
       | \    | \    |
       |  \   |  \   |
       |   \  |   \  |
       |    \ |    \ |
       R6 --- R7 --- R8 --- L2


Les liaisons sont de trois types : • Eth : Ethernet, dont la bande
passante est de 10 Mb/s ; • V1 : VDSL, dont la bande passante est de 50
Mb/s ; • V2 : VDSL2, dont la bande passante est de 100 Mb/s. On rappelle
que la bande passante d'une liaison est la quantité d'information qui
peut être transmise en bits/s.

Le tableau ci-dessous précise les types des liaisons entre les routeurs.

|---------+-------+-------+-------+-------+-------+-------|
| Liaison | R1-R2 | R1-R3 | R1-R4 | R2-R4 | R3-R4 | R3-R6 |
|---------+-------+-------+-------+-------+-------+-------|
| Type    | Eth   | V2    | Eth   | V2    | Eth   | V2    |
|---------+-------+-------+-------+-------+-------+-------|


|---------+-------+-------+-------+-------+-------+-------+-------|
| Liaison | R3-R7 | R4-R5 | R4-R7 | R4-R8 | R5-R8 | R6-R7 | R7-R8 |
|---------+-------+-------+-------+-------+-------+-------+-------|
| Type    | Eth   | V1    | V2    | Eth   | V1    | V2    | Eth   |
|---------+-------+-------+-------+-------+-------+-------+-------|

Pour tenir compte du débit des liaisons, on décide d'utiliser le
protocole OSPF (distance liée au coût minimal des liaisons) pour
effectuer le routage. Le coût $C$ d'une liaison est donné par la formule
: $C = \frac{10^9}{BP}$ où est la bande passante de la liaison en
bits/s.

1.  Vérifier que le coût d'une liaison VDSL est égal à 20.

2.  a.  Recopier la table du routeur `R1` sur votre copie en inscrivant
        les coûts des liaisons.

        |---------+------|
        | Routeur | Coût |
        |---------+------|
        | R2      |      |
        |---------+------|
        | R3      |      |
        |---------+------|
        | R4      |      |
        |---------+------|
        | R5      |      |
        |---------+------|
        | R6      |      |
        |---------+------|
        | R7      |      |
        |---------+------|
        | R8      |      |
        |---------+------|
        

    b.  Déterminer le chemin parcouru par un paquet partant du réseau L1 et
        arrivant au réseau L2, en utilisant le protocole OSPF.
        
    c.  La liaison R1-R4 est remplacée par une liaison de type ADSL avec une
        bande passante intermédiaire entre celles de type Ethernet et VDSL.
        Quel devrait être le coût maximal de cette liaison pour que des paquets
        issus du réseau L1 à destination du réseau L2 transitent par celle-ci ?
        En déduire la bande passante minimale de cette liaison.
