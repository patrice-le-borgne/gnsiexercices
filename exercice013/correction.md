# Exercice

## Q1.a

4 feuilles :

- 12
- val
- 21
- 32

## Q1.b

## Q1.c

hauteur : 4
taille : 9

## Q1.d

Val : [16, 17]

## Q2.a

12, 13, 15, 16, 18, 19, 21, 23, 32  (valeurs triées)

## Q2.b

12, 13, 16, 15, 21, 19, 32, 23, 18

## Q3.a


## Q3.b

~~~ {.python}
    racine = Noeud(18)
    racine.insere_tout([15, 13, 12, 16, 23, 19, 21, 32])
~~~


## Q3.c

bloc 3 – bloc 2
ne rentre pas dans bloc 1

## Q4

~~~ {.python}
    def recherche(self, v):
    	n = self
    	while n is not None :
    	    if v == n.v:
    	        return True
    	    elif v < n.v:
    	        n = n.ag
    	    else:
    	        n = n.ad
        return False
~~~
