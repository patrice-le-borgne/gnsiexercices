~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Pour cet exercice :

- On appelle « mot » une chaîne de caractères composée avec des caractères choisis
parmi les 26 lettres minuscules ou majuscules de l'alphabet,

- On appelle « phrase » une chaîne de caractères :
    - composée avec un ou plusieurs « mots » séparés entre eux par un seul
caractère espace `' '`,
    - se finissant :
        - soit par un point `'.'` qui est alors collé au dernier mot,
        - soit par un point d'exclamation `'!'` ou d'interrogation `'?'` qui est alors
séparé du dernier mot par un seul caractère espace `' '`.

Après avoir remarqué le lien entre le nombre de mots et le nombre de caractères espace
dans une phrase, programmer une fonction `nombre_de_mots` qui prend en paramètre une
phrase et renvoie le nombre de mots présents dans cette phrase.

Exemples :

~~~ {.python .amc file="Q_1.md" bareme="2"}
    >>> nombre_de_mots('Le point d exclamation est separe !')
    6
    >>> nombre_de_mots('Il y a un seul espace entre les mots.')
    9
~~~

~~~ {.python .hidden .test file="Q_1_a.md" bareme="1"}
    >>> nombre_de_mots('Le point d exclamation est separe !')
    6
    >>> nombre_de_mots('Il y a un seul espace entre les mots !')
    9
~~~

~~~ {.python .hidden .test file="Q_1_b.md" bareme="1"}
    >>> nombre_de_mots('Le point est collé au dernier mot.')
    7
~~~

~~~ {.python .hidden .test file="Q_1_c.md" bareme="1"}
    >>> nombre_de_mots('On a oublie le point final ')
    6
~~~

