"""
Partie A

1.a. Il faut un attribut qui n'apparaisse qu'une seule fois dans la colonne.
"""

cle_primaire_mesure = "id_mesure"

"""
1.b. id_centre est commun aux deux tables.
"""
attribut_de_jointure = "id_centre"

"""
2.a. La requète renvoie la liste des villes dont l'atitude est supérieure à 500m.
2.b.
"""

prop_Q_A_2_b = "SELECT nom_ville FROM Centres WHERE altitude>=700 AND altitude<=1200"

prop_Q_A_2_c = "SELECT longitude, nom_ville FROM Centres WHERE longitude>5 ORDER BY nom_ville"

prop_Q_A_3_b = "INSERT INTO Mesures VALUES (3650,138,'2021-11-08',11,1013,0)"

prop_Q_A_4 = "SELECT AVG(temperature) AS moyenne FROM Mesures"

"""
5. La requête renvoie les données de la (ou des) ville(s) ayant la latitude la plus basse,
autrement dit, la ville la plus au sud.
"""


prop_Q_A_6 = "SELECT DISTINCT c.nom_ville FROM Centres AS c JOIN Mesures AS m ON c.id_centre=m.id_centre WHERE m.pluviometrie >=10"

"""
Partie B

1. consommations_martin[0] = {'Année': 23, 'Consommation': 6.8, 'Date': '03/01', 'Jour': 'Mardi'}
Il s'agit d'un dictionnaire.

2.
"""

def week_end(donnees_jour):
    """
    Renvoie True si les données concernent un jour de week-end, False sinon.
    """
    jour_de_la_semaine = donnees_jour['Jour']
    return jour_de_la_semaine in ["Samedi", "Dimanche"]

def conso_maximale(consommations):
    """
    Renvoie sous forme d’un tuple la date et l’année où la consommation a été la plus forte.
    """
    date = None
    année = None
    res_maxi = 0
    for x in consommations:
        if x['Consommation'] > res_maxi:
            res_maxi = x['Consommation']
            date = x['Date']
            année = x['Année']
    return (date, année)


def somme_conso(consommations, annee):
    """
    Renvoie la somme totale des consommations en kwH lors l'année.
    """
    s = 0
    for x in consommations:
        if x['Année'] == annee:
            s += x['Consommation']
    return s

def consommations_jours(consommations):
    """
    Renvoie un dictionnaire dont les clés sont les jours de la semaine
    et les valeurs les consommations en kwH accumulées le jour
    correspondant
    """
    dico = {}
    for j in ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']:
        s = 0
        for x in consommations:
            if x['Jour'] == j:
                s += x['Consommation']
        dico[j] = s
    return dico
    
