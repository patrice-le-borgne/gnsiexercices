"""
Partie A

1.a. Proposer une clé primaire pour la relation Mesures. Pas de justification. Juste le nom de l'attribut.
"""

cle_primaire_mesure = ""

"""
1.b. Avec quel attribut peut-on faire une jointure entre la relation Centres
et la relation Mesures ?  Juste le nom de l'attribut.
"""
attribut_de_jointure = ""

"""
2.b.
"""

prop_Q_A_2_b = ""

prop_Q_A_2_c = ""

prop_Q_A_3_b = ""

prop_Q_A_4 = ""

"""
5. Pas évalué ici.

6.
"""

prop_Q_A_6 = ""

"""
Partie B

1. Pas évalué.

2.
"""

def week_end(donnees_jour):
    """
    Renvoie True si les données concernent un jour de week-end, False sinon.
    """
    pass

def conso_maximale(consommations):
    """
    Renvoie sous forme d’un tuple la date et l’année où la consommation a été la plus forte.
    """
    date = None
    année = None
    res_maxi = 0
    for x in ..............:
        if ........................:
            res_maxi = ..................
            date = ......................
            année = .....................
    return (date, année)


def somme_conso(consommations, annee):
    """
    Renvoie la somme totale des consommations en kwH lors l'année.
    """
    pass

def consommations_jours(consommations):
    """
    Renvoie un dictionnaire dont les clés sont les jours de la semaine
    et les valeurs les consommations en kwH accumulées le jour
    correspondant
    """
    pass
    
