    >>> from exercice import *
    >>> import sqlite3 
    >>> conn = sqlite3.connect('consommations.sqlite')
    >>> def initialisation():
    ...     """
    ...     Fonction qui vide la table des coureurs si elle existe puis en crée une nouvelle.
    ...     """
    ... 
    ...     curseur = conn.cursor()
    ... 
    ...     curseur.executescript("""
    ...     DROP TABLE IF EXISTS Mesures;
    ...     DROP TABLE IF EXISTS Centres;
    ...     CREATE TABLE IF NOT EXISTS "Mesures" (
    ...         "id_mesure" INTEGER,
    ...         "id_centre" INTEGER,
    ...         "date_mesure"   TEXT,
    ...         "temperature"   INTEGER,
    ...         "pression"  INTEGER,
    ...         "pluviometrie"  INTEGER
    ...     );
    ...     CREATE TABLE IF NOT EXISTS "Centres" (
    ...         "id_centre" INTEGER,
    ...         "nom_ville" TEXT,
    ...         "latitude"  REAL,
    ...         "longitude" REAL,
    ...         "altitude"  INTEGER
    ...     );
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (1566,138,'2021-10-29',8,1015,3);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (1568,213,'2021-10-29',15.1,1011,0);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2174,126,'2021-10-30',18.2,1023,0);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2200,185,'2021-10-30',5.6,989,20);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2232,459,'2021-10-31',25,1035,0);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2514,213,'2021-10-31',17.4,1020,0);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2563,126,'2021-11-01',10.1,1005,15);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2592,459,'2021-11-01',23.3,1028,2);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3425,317,'2021-11-02',9,1012,13);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3430,138,'2021-11-02',7.5,996,16);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3611,263,'2021-11-03',13.9,1005,8);
    ...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3625,126,'2021-11-03',10.8,1008,8);
    ...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (213,'Amiens',49.894,2.293,60);
    ...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (138,'Grenoble',45.185,5.723,550);
    ...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (263,'Brest',48.388,-4.49,52);
    ...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (185,'Tignes',45.469,6.909,2594);
    ...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (459,'Nice',43.706,7.262,260);
    ...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (126,'LePuy-en-Velay',45.042,3.888,744);
    ...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (317,'Gérardmer',48.073,6.879,855);
    ...     """)
    ...     conn.commit()
    >>> def requete_correcte(solution, proposition):
    ...     # Créer un cursor 
    ...     cur = conn.cursor() 
    ...     comparaison = "WITH TA AS ({}), TB AS ({}) SELECT * FROM TA EXCEPT SELECT * FROM TB UNION ALL SELECT * FROM TB EXCEPT SELECT * FROM TA;".format(solution, proposition)
    ...     # Exécution de la requete
    ...     try:
    ...         resultat = cur.execute(comparaison) 
    ...         row = cur.fetchone()
    ...         if row is None:
    ...             rc = True
    ...         else:
    ...             rc = False
    ...     except Exception:
    ...         print("Le nombre de colonnes est incorrect !")
    ...         rc = False
    ...     # Envoyer la requete 
    ...     conn.commit()
    ...     # Fermer la connexion 
    ...     conn.close
    ...     return rc
    >>> def execute(proposition):
    ...     # Créer un cursor 
    ...     cur = conn.cursor() 
    ...     # Exécution de la requete
    ...     try:
    ...         resultat = cur.execute(proposition)
    ...         rc =True
    ...     except Exception:
    ...         print("Erreur lors de l'exécution de la requête !")
    ...         rc = False
    ...     # Envoyer la requete 
    ...     conn.commit()
    ...     # Fermer la connexion 
    ...     conn.close
    ...     return rc
    >>> def verification_simple(proposition, sortie):
    ...     # Créer un cursor 
    ...     cur = conn.cursor() 
    ...     # Exécution de la requete
    ...     try:
    ...         resultat = cur.execute(proposition) 
    ...         row = cur.fetchone()
    ...         rc = row == sortie
    ...     except Exception:
    ...         print("Erreur lors de l'exécution de la requête !")
    ...         rc = False
    ...     # Envoyer la requete 
    ...     conn.commit()
    ...     # Fermer la connexion 
    ...     conn.close
    ...     return rc
    >>> solution_Q_A_2_b = "SELECT nom_ville FROM Centres WHERE altitude>=700 AND altitude<=1200"
    >>> solution_Q_A_2_c = "SELECT longitude, nom_ville FROM Centres WHERE longitude>5 ORDER BY nom_ville"
    >>> solution_Q_A_6 = "SELECT DISTINCT c.nom_ville FROM Centres AS c JOIN Mesures AS m ON c.id_centre=m.id_centre WHERE m.pluviometrie >=10"
    >>> conso = [{'Année': 23, 'Consommation': 6.8, 'Date': '03/01', 'Jour': 'Mardi'},
    ...  {'Année': 23, 'Consommation': 8.5, 'Date': '02/01', 'Jour': 'Lundi'},
    ...  {'Année': 23, 'Consommation': 7.5, 'Date': '01/01', 'Jour': 'Dimanche'},
    ...  {'Année': 22, 'Consommation': 10, 'Date': '31/12', 'Jour': 'Samedi'},
    ...  {'Année': 22, 'Consommation': 9.6, 'Date': '30/12', 'Jour': 'Vendredi'},
    ...  {'Année': 22, 'Consommation': 8, 'Date': '29/12', 'Jour': 'Jeudi'},
    ...  {'Année': 22, 'Consommation': 8.1, 'Date': '28/12', 'Jour': 'Mercredi'},
    ...  {'Année': 22, 'Consommation': 11.1, 'Date': '27/12', 'Jour': 'Mardi'},
    ...  {'Année': 22, 'Consommation': 12.5, 'Date': '26/12', 'Jour': 'Lundi'},
    ...  {'Année': 22, 'Consommation': 9.4, 'Date': '25/12', 'Jour': 'Dimanche'}]

    >>> attribut_de_jointure
    'id_centre'

 
