Exercice
========

Partie A
--------

``` {.python}
 Cet exercice porte sur les systėmes d'exploitation et la gestion des processus par un système d'exploitation.
```

Cet exercice pourra utiliser des commandes de systèmes d'exploitation de
type UNIX telles que `cd`, `1s` , `mkdir`, `rm` ,`rmd`, `mv`, `cat`.

1.  Dans un système d'exploitation de type UNIX, on considėre
    l'arborescence des fichiers suivante :

![arborescence](exo054_arborescence.png)

On souhaite, grâce à l'utilisation du terminal de commande, explorer et
modifier les répertoires et fichiers présents.

On suppose qu'on se trouve actuellement dans le dossier
`/home/morgane/lycee`

1.  Parmi les 4 propositions suivantes, donner celle correspondant à
    l'affichage obtenu lors de l'utilisation de la commande `ls` :

-   **proposition 1** : francais NSI info.txt image1.jpg
-   **proposition 2** : francais NSI
-   **proposition 3** : lycee
-   **proposition 4** : bin etc home tmp

2.  Écrire la commande qui permet, à partir de cet emplacement,
    d'atteindre le répertoire `morgane`.

<!-- -->

2.  On suppose maintenant qu'on se trouve dans le répertoire
    `/home/morgane/lycee/NSI`.

3.  Écrire la commande qui permet de créer, à cet emplacement, le
    dossier nommé `algorithmique`.

<!-- -->

2.  Écrire la commande qui permet, à partir de cet emplacement, de
    supprimer le fichier `info.txt`.

Partie B
--------

La commande UNIX `ps` présente un cliché instantané des processus en
cours d'exécution.

Avec l'option `−eo pid,ppid,stat,command`, cette commande affiche dans
l'ordre l'identifiant du processus `PID` (process identifier), le `PPID`
(parent process identifier), l'état `STAT` et le nom de la commande à
l'origine du processus.

Les valeurs du champ `STAT` indique l'état des processus :

-   `R` : processus en cours d'exécution
-   `S` : processus endormi (en attente d'une ressource)

Sur un ordinateur, on exécute la commande `ps −eo pid,ppid,stat,command`
et on obtient un affichage dont on donne ci-dessous un extrait.

``` {.python}
$ ps -eo pid,ppid,stat,command

PID   PPID  STAT  COMMAND                     
1     0     Ss    /sbin/init                  
....  ....  ..    ...                         
1912  1908  Ss    Bash                        
2014  1912  Ss    Bash                        
1920  1747  Sl    Gedit                       
2013  1912  Ss    Bash                        
2091  1593  Sl    /usr/lib/firefox/firefox    
5437  1912  Sl    python programme1.py        
5440  2013  R     python programme2.py        
5450  1912  R+    ps -eo pid,ppid,stat,command
```

À l'aide de cet affichage, répondre aux questions ci-dessous. 1. Quel
est le nom de la première commande exécutée par le système
d'exploitation lors du démarrage ?

2.  Quels sont les identifiants des processus actifs sur cet ordinateur
    au moment de l'appel de la commande `ps` ? Justifier la réponse.

<!-- -->

3.  Depuis quelle application a-t-on exécuté la commande `ps` ? Donner
    les autres commandes qui ont été exécutées à partir de cette
    application.

<!-- -->

4.  Expliquer l'ordre dans lequel les deux commandes
    `python programme1.py` et `python programme2.py` ont été exécutées.

<!-- -->

5.  Peut-on prédire que l'une des deux commandes `python programme1.py`
    et `python programme2.py` finira avant l'autre ?

Partie C
--------

On considère le réseau suivant compososé de sept routeurs.

![réseau](exo_54_reseaux.png)

On donne les tables de routage préalablement construites ci-dessous avec
le protocole RIP (Routing information Protocole). Le protocole RIP
permet de construire les tables de routage des différerents routeurs, en
indiquant pour chaque routeur, la distance, en nombre de sauts, qui le
sépare d'un autre routeur.

![Tables de routage](exo_54_tables_de_routage.png)

1.  Le routeur R2 doit envoyer un paquet de données au routeur R7 qui en
    accuse réception. Déterminer le chemin parcouru par le paquet de
    données ainsi que celui parcouru par l'accusé de réception.

<!-- -->

2.  1.Indiquer la faiblesse que présente ce réseau en cas de panne du
    routeur R4.
    2.  Proposer une solution pour y remédier.

<!-- -->

3.  **Dans cette question uniquement**, on décide de rajouter un routeur
    R8 qui sera relié aux routeurs R2 et R6.
    -   Donner une table de routage pour R8 qui minimise le nombre de
        saut.
    -   Donner une nouvelle table de routage de R2,

<!-- -->

4.  **Pour la suite de l'exercice on considèrere le réseau sans le
    routeur R8.** Il a été décidé de modifier les règles de routage de
    ce réseau en appliquant dorénavant le protocole de routage OSPF qui
    prend en compte la bande passante.

Ce protocole attribue un coût à chaque liaison afin de priviligier le
choix de certaines routes plus rapides. Plus le coût est faible, plus le
lien est intéressant.

Le coût d'une liaison est calcul par la formule :

$$\text{coût} = \dfrac{10^8 \text{bit/s}}{\text{bande passante du lien en bit/s}}$$

Voici le tableau rérérençant les coûts de liaisons en fonction du type
deliaison entre 2 routeurs :

![couts](exo_54_couts.png)

On rappelle que 1 Mb/s = 1000 kb/s = $10^6$ bit/s.

``` {.python}
- Déterminer la bande passante du FastEthernet (FE) et justifier que le coût du reseau de type Ethernet (E) est de 10.

-  On précise sur le graphe ci-dessous les types de liaison dans notre réseau:
```

![liaisons](exo_54_liaisons.png)

Le coût d'un chemin est la somme des coûts des liaisons rencontrés.
Donner en justifiant le chemin le moins coûteux pour relier R2 à R5.
Préciser le coût.
