def moyenne(tab):
    """
    Renvoye la moyenne pondérée d’une liste non vide, passée en
    paramètre, de tuples à deux éléments de la forme (`valeur`,
    `coefficient`) où `valeur` et `coefficient` sont des nombres
    positifs ou nuls.  Si la somme des coefficients est nulle, la
    fonction renvoie `None`, si la somme des coefficients est non
    nulle, la fonction renvoie, sous forme de flottant, la moyenne des
    valeurs affectées de leur coefficient.

    """
    somme = 0
    coeffs = 0
    for note, coeff in tab:
        somme += note * coeff
        coeffs += coeff
    if coeffs != 0:
        return somme / coeffs
    return None
