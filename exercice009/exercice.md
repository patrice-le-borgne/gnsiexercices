~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : structures linéaires
thème : dichotomie
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Recopier et compléter sous Python la fonction suivante en respectant la spécification. On ne recopiera pas les commentaires.

~~~ {.python .amc file="Q_1.md" bareme="4"}
def dichotomie(tab, x):
    """
    tab : tableau d’entiers trié dans l’ordre croissant
    x : nombre entier
    La fonction renvoie True si tab contient x et False sinon
    """
    debut = 0
    fin = len(tab) - 1
    while debut <= fin:
        m = ...
        if x == tab[m]:
            return ...
        if x > tab[m]:
            debut = m + 1
        else:
            fin = ...
  return ...
~~~

Exemples :

~~~ {.python .test file="Q_1.md" bareme="2"}
    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 28)
    True
    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 27)
    False
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 15)
    True
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
    >>> dichotomie([15, 16, 18, 19, 23, 24, 28, 29, 31, 33], 33)
    True
~~~


