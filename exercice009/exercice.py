def dichotomie(tab, x):
    """
    tab : tableau d’entiers trié dans l’ordre croissant
    x : nombre entier
    La fonction renvoie True si tab contient x et False sinon
    """
    debut = 0 
    fin = len(tab) - 1
    while debut <= fin:
        m = (debut + fin) // 2 ## on calcule une valeur de l'indice située "au milieu" entre debut et fin
        if x == tab[m]:
            return True
        if x > tab[m]: ## si la valeur x cherchée est située dans le tableau trié tab,
            debut = m + 1 ## alors elle s'y trouve entre les indices debut = m + 1 et fin (inchangé)
        else: ## dans ce cas, x < tab[m], donc, si la valeur x cherchée est située dans le tableau trié tab,
            fin = m - 1 ## alors elle s'y trouve entre les indices debut (inchangé) et fin = m - 1
    ## en sortie de boucle while, debut > fin, il est impossible que la valeur x soit dans le tableau tab
    return False ## on renvoie donc la valeur booléenne False
