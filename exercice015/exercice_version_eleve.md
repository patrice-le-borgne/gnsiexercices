Exercice
========

*Principaux thèmes abordés : structure de données (programmation objet)
et langages et programmation (spécification).*

Une entreprise fabrique des yaourts qui peuvent être soit nature (sans
arôme), soit aromatisés (fraise, abricot ou vanille).

Pour pouvoir traiter informatiquement les spécificités de ce produit, on
va donc créer une classe Yaourt qui possèdera un certain nombre
d'attributs :

-   Son genre : nature ou aromatisé
-   Son arôme : fraise, abricot, vanille ou aucun
-   Sa date de durabilité minimale (DDM) exprimée par un entier compris
    entre 1 et 365 (on ne gère pas les années bissextiles). Par exemple,
    si la DDM est égale à 15, la date de durabilité minimale est le 15
    janvier.

On va créer également des méthodes permettant d'interagir avec l'objet
`Yaourt` pour attribuer un arôme ou récupérer un genre par exemple. On
peut représenter cette classe par le tableau de spécifications
ci-dessous :

![La classe Yaourt](S10_20_exo4.png)

1.  On a commencé à implémenter la classe `Yaourt`. Le code ci-dessous
    sera à compléter.

``` {.python}
class Yaourt:
    """ Classe définissant un yaourt caractérisé par :
    - son arome
    - son genre
    - sa durée de durabilité minimale"""
    
    def __init__(self,arome,duree):
        ####
        # À compléter :Q.1.a
        ###
        self.__arome = arome
        self.__duree = duree
        if arome == 'aucun':
            self.__genre = 'nature'
        else:
            self.__genre = 'aromatise'

    def get_arome(self):
        # À rédiger
        pass
    
    def get_duree(self):
        return self.__duree
    
    def get_genre(self):
        return self.__genre
    
    def set_duree(self,duree):
        if duree > 0 :
            self.__duree = duree

    def set_arome(self, arome):
        # À rédiger
        pass
            
    def __set_genre(self,arome):
       if arome == 'aucun':
            self.__genre = 'nature'
       else:
            self.__genre = 'aromatise'


def creer_pile():
    pile = [ ]
    return pile

def depiler(p):
    # À rédiger
    pass

def empiler(p, Yaourt):
    # À rédiger
    pass

def est_vide(p):
    # À rédiger
    pass
```

a.  Quelles sont les assertions à prévoir pour vérifier que l'arôme et
    la durée correspondent bien à des valeurs acceptables. Il faudra
    aussi expliciter les commentaires qui seront renvoyés. Pour rappel :
    -   L'arôme doit prendre comme valeur `'fraise'`, `'abricot'`,
        `'vanille'` ou `'aucun'`.
    -   Sa date de durabilité minimale (DDM) est une valeur positive.

<!-- -->

b.  Pour créer un yaourt, on exécutera la commande suivante :

``` {.python}
>>> Mon_Yaourt = Yaourt('fraise',24)
```

Quelle valeur sera affectée à l'attribut genre associé à `Mon_Yaourt` ?

c.  Écrire en python une fonction `get_arome(self)`, renvoyant l'arôme
    du yaourt créé.

<!-- -->

2.  On appelle mutateur une méthode permettant de modifier un ou
    plusieurs attributs d'un objet.

    Sur votre copie, écrire en Python le mutateur
    `set_arome(self,arome)` permettant de modifier l'arôme du yaourt. On
    veillera à garder une cohérence entre l'arôme et le genre.

<!-- -->

3.  On veut créer une pile contenant le stock de yaourts. Pour cela il
    faut tout d'abord créer une pile vide :

``` {.python}
        >>> def creer_pile():
        ...     pile = [ ]
        ...     return pile
```

a.  Créer une fonction `empiler(p, Yaourt)` qui renvoie la pile `p`
    après avoir ajouté un objet de type `Yaourt` à la fin.
b.  Créer une fonction `depiler(p)` qui renvoie l'objet à dépiler.
c.  Créer une fonction `est_vide(p)` qui renvoie `True` si la pile est
    vide et `False` sinon.
d.  Qu'affiche le bloc de commandes suivantes ci-dessous ?

``` {.python}
        >>> mon_Yaourt1 = Yaourt('aucun',18)
        >>> mon_Yaourt2 = Yaourt('fraise',24)
        >>> ma_pile = creer_pile()
        >>> empiler(ma_pile, mon_Yaourt1)
        >>> empiler(ma_pile, mon_Yaourt 2)
        >>> print(depiler(ma_pile).get_duree())
        >>> print(est_vide(ma_pile))
```
