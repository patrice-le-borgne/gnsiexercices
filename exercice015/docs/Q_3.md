    >>> from exercice import *

    >>> mon_Yaourt1 = Yaourt('aucun',18)
    >>> mon_Yaourt2 = Yaourt('fraise',24)
    >>> ma_pile = creer_pile()
    >>> est_vide(ma_pile)
    True
    >>> empiler(ma_pile, mon_Yaourt1)
    >>> est_vide(ma_pile)
    False
    >>> empiler(ma_pile, mon_Yaourt2)
    >>> depiler(ma_pile).get_duree()
    24
    >>> est_vide(ma_pile)
    False
    >>> depiler(ma_pile).get_duree()
    18
    >>> est_vide(ma_pile)
    True

 
