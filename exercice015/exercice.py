###################################################
### prenom, nom :                               ###
###################################################

class Yaourt:
    """ Classe définissant un yaourt caractérisé par :
    - son arome
    - son genre
    - sa durée de durabilité minimale"""
    
    def __init__(self,arome,duree):
        assert arome in ['fraise', 'abricot', 'vanille', 'aucun'], "Cet arome est inconnu"
        assert duree > 0 and duree < 366, "la durée doit être comprise entre 1 et 365"
        self.__arome = arome
        self.__duree = duree
        if arome == 'aucun':
            self.__genre = 'nature'
        else:
            self.__genre = 'aromatise'

    def get_arome(self):
        return self.__arome
    
    def get_duree(self):
        return self.__duree
    
    def get_genre(self):
        return self.__genre
    
    def set_duree(self,duree):
        if duree > 0 :
            self.__duree = duree

    def set_arome(self, arome):
        assert arome in ['fraise', 'abricot', 'vanille', 'aucun'], "Cet arome est inconnu"
        self.__arome = arome
        self.__set_genre(arome)
            
    def __set_genre(self,arome):
       if arome == 'aucun':
            self.__genre = 'nature'
       else:
            self.__genre = 'aromatise'


def creer_pile():
    pile = [ ]
    return pile

def depiler(p):
    return p.pop()

def empiler(p, Yaourt):
    p.append( Yaourt)

def est_vide(p):
    return p == []
