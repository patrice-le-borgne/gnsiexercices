~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : arbres
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On souhaite développer un jeu de Scrabble. Pour pouvoir vérifier
rapidement si un mot existe, nous pouvons stocker le dictionnaire sous
forme d'arbre binaire. Dans le cadre de cet exercice, nous allons nous
limiter aux mots qui commencent par la lettre `a`.

**Partie A**

Sur les figures suivantes, les arbres sont représentés vers le bas,
les fils droits vers la droite et les fils gauches vers le bas. Ainsi
les mots `abaca` (bananier des Philippines), `abaque` et `abats`
seront stockés sous la forme suivante.

   a
   |
   b
   |
   a
   |
   c--q--t
   |  |  |
   a  u  s
      |
      e
	  
	  
1.  Quel est la taille de l'arbre ci-dessus ?

~~~ {.python .hidden .test .amc file="A_1.md" bareme="1"}
>>> taille
10
~~~

2. Pourquoi est-il pertinent de mémoriser le dictionnaire de la langue
   française sous forme d'arbre binaire ?

~~~ {.python .hidden .amc file="A_2.md" bareme="1"}
>>> pass
~~~

3. Dans la figure ci-dessous, deux mots ont été ajoutés. Quels
   sont-ils ?

   a
   |
   b
   |
   a--------b
   |        |
   c--q--t  é--e
   |  |  |     |
   a  u  s     s
      |        |
      e        s
               |
               e
    

~~~ {.python .hidden .test .amc file="A_3.md" bareme="1"}
>>> mot_1 in ["abbé", "abbesse"]
True
>>> mot_2 in ["abbé", "abbesse"]
True
~~~

4. Compléter la figure en représentant le mot `abbaye`.

~~~ {.python .hidden .test .amc file="A_4.md" bareme="1"}
>>> reponse_A_4
'b'
~~~

**Partie B**.

Vous trouverez en annexe le code source de la classe
`ArbreBinaire`. L'objet de cette partie est d'implémenter une méthode
de recherche d'un mot dans l'arbre.

1. Écrire une méthode `ajoute_a_droite(self,x)` qui ajoute `x`, s'il
   n'est pas présent, à la branche droite de l'arbre en partant de la
   racine. La méthode doit renvoyer l'arbre dont la racine contient la
   lettre `x`.

   Supposons que `a` corresponde à l'arbre binaire représenté
   ci-dessous, dont la racine est `"c"`.

   c--q--t
   |  |  |
   a  u  s
      |
      e
      

   `a.ajoute_a_droite("q")` doit renvoyer l'arbre qui contient la lettre `"q"`
   sans modifier l'arbre.

   En revanche `a.ajoute_a_droite("b")` doit ajouter la lettre `"b"` à
   l'extrémité de la branche et renvoyer l'arbre qui la
   contient. On obtiendra alors alors le suivant :

   c--q--t--b
   |  |  |
   a  u  s
      |
      e
  
~~~ {.python .hidden .test .amc file="B_1.md" bareme="2"}
>>> arbre_1 = ArbreBinaire('c', ArbreBinaire('a', None, None), None)
>>> arbre_1.racine()
'c'
>>> q = arbre_1.ajoute_a_droite('q')
>>> q.racine()
'q'
>>> arbre_1.fils_droit() == q
True
~~~


	  
2.  Écrire une méthode `ajoute_mot(self, mot)` qui ajoute le `mot` à l'arbre.

~~~ {.python .hidden .test .amc file="B_2.md" bareme="2"}
>>> pass
~~~


**Partie C**

1.  Écrire une méthode `cherche_a_droite(self, x)` qui cherche la
    présence de `x` dans la branche droite de l'arbre en partant de la
    racine. Cette méthode renverra `None` si `x` n'est pas présent
    sinon elle renverra l'arbre qui contient la valeur `x`.
	  
