    >>> from exercice import *

    >>> arbre_1 = ArbreBinaire('c', ArbreBinaire('a', None, None), None)
    >>> arbre_1.racine()
    'c'
    >>> q = arbre_1.ajoute_a_droite('q')
    >>> q.racine()
    'q'
    >>> arbre_1.fils_droit() == q
    True
    >>> t = arbre_1.ajoute_a_droite('t')
    >>> t.racine()
    't'
    >>> arbre_1.fils_droit().fils_droit() == t
    True
    >>> t_bis = arbre_1.ajoute_a_droite('t')
    >>> t_bis == t
    True

 
