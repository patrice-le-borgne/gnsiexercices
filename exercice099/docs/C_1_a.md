    >>> from exercice import *

    >>> t = ArbreBinaire('t', ArbreBinaire('s', None, None), None)
    >>> t.fils_gauche().noeud.fin = True
    >>> q = ArbreBinaire('q', ArbreBinaire('u', ArbreBinaire('e', None, None), None), t)
    >>> q.fils_gauche().fils_gauche().noeud.fin = True
    >>> c = ArbreBinaire('c',ArbreBinaire('a', None, None), q)
    >>> c.fils_droit().racine()
    'q'
    >>> c.fils_droit().fils_droit().racine()
    't'
    >>> c.fils_droit().fils_droit().fils_droit().est_vide()
    True
    >>> c.cherche_a_droite('q').racine()
    'q'
    >>> c.cherche_a_droite('t').racine()
    't'

 
