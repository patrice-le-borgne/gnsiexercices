    >>> from exercice import *

    >>> t = ArbreBinaire('t', ArbreBinaire('s', None, None), None)
    >>> t.fils_gauche().noeud.fin = True
    >>> q = ArbreBinaire('q', ArbreBinaire('u', ArbreBinaire('e', None, None), None), t)
    >>> q.fils_gauche().fils_gauche().noeud.fin = True
    >>> c = ArbreBinaire('c',ArbreBinaire('a', None, None), q)
    >>> c.fils_gauche().noeud.fin = True
    >>> a = ArbreBinaire('a',ArbreBinaire('b', ArbreBinaire('a', c, None), None), None)
    >>> a.longueur_mot_le_plus_long()
    6

 
