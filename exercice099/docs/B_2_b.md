    >>> from exercice import *

    >>> a = ArbreBinaire('a', None, None)
    >>> a.racine()
    'a'
    >>> a.ajoute('abaca')
    >>> a.fils_droit().est_vide()
    True
    >>> a.fils_gauche().racine()
    'b'
    >>> a.fils_gauche().fils_gauche().fils_gauche().noeud.fin
    False
    >>> a.fils_gauche().fils_gauche().fils_gauche().fils_gauche().noeud.fin
    True

 
