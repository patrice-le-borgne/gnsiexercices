Exercice
========

1.  a.  Tracer $G_1$ le graphe dont la matrice d'adjacence est :
        $$\left( \begin{array}{cccc} 0&1&1&0\\ 1&0&1&1\\ 1&1&0&1\\
        0&1&1&0\\ \end{array}\right)$$
    b.  Le graphe $G_1$ est-il orienté ou non orienté ?

2.  a.  Tracer $G_2$ le graphe \textbf{orienté} dont la matrice
        d'adjacence est :$$\left( \begin{array}{cccc}
        0&1&0&0\\
        0&0&1&1\\
        1&1&0&0\\
        0&1&1&0\\
        \end{array}\right)$$
    b.  Le graphe $G_2$ est-il orienté ou non orienté ?

3.  a.  Tracer $G_3$ le graphe dont le dictionnaire des prédécesseurs
        est : $$\{A:[C],B:[A,C,E],C:[A],D:[A],E:[D]\}$$
    b.  Le graphe $G_3$ est-il orienté ?
    c.  Déterminer la matrice d'adjacence de $G_3$.
