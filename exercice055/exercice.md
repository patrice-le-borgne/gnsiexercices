~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : bases de données
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
=========

On souhaite gérer un club de tennis en ligne avec la possibilité de réserver un
terrain à un créneau horaire. Le site ne gère que des réservations pour des matchs
en simple. Voici la structure de la base de données :


Relation contenant l’ensemble des joueurs du club avec leurs identifiants.

Table `joueurs`

| `id_joueur` | `nom_joueur` | `prenom_joueur` | `login` | `mdp` |
|-------------|--------------|-----------------|---------|-------|
| 1           | Dupont       | Alice           | alice   | 1234  |
| 2           | Durand       | Belina          | belina  | 5694  |
| 3           | Caron        | Camilia         | camilia | 9478  |
| 4           | Dupont       | Dorine          | dorine  | 1347  |


Relation précisant les matchs joués.

Table `matchs`

| `id_match` | `date`     | `id_creneau` | `id_terrain` | `id_joueur1` | `id_joueur2` |
|------------|------------|--------------|--------------|--------------|--------------|
| 1          | 2020-08-01 | 2            | 1            | 1            | 4            |
| 2          | 2020-08-01 | 3            | 1            | 2            | 3            |
| 3          | 2020-08-02 | 6            | 2            | 1            | 3            |
| 4          | 2020-08-02 | 7            | 2            | 2            | 4            |
| 5          | 2020-08-08 | 3            | 3            | 1            | 2            |
| 6          | 2020-08-08 | 5            | 2            | 3            | 4            |

Relation précisant les différents terrains.

Table `terrains`

| `id_terrain` | `nom_terrain` | `surface`    |
|--------------|---------------|--------------|
| 1            | stade         | terre battue |
| 2            | gymnase       | synthétique  |
| 3            | hangar        | terre battue |


Relation précisant les créneaux réservables.

Table `creneaux`

| `plage_horaire` | `id_creneau` |
|-----------------|--------------|
| 1               | 8h-9h        |
| 2               | 9h-10h       |
| 3               | 10h-11h      |
| 4               | 11h-12h      |
| 5               | 12h-13h      |
| 6               | 13h-14h      |
| 7               | 14h-15h      |
| 8               | 15h-16h      |
| 9               | 16h-17h      |
| 10              | 17h-18h      |
| 11              | 18h-19h      |
| 12              | 19h-20h      |


1.

a. Donner la clé primaire de la relation matchs.

~~~ {.python .hidden  .amc file="Q_1_a.md" bareme="1"}
>>> pass
~~~

b. La relation matchs a-t-elle une ou des clés étrangères ? Si oui quelles sont-elles ?

~~~ {.python .hidden  .amc file="Q_1_b.md" bareme="1"}
>>> pass
~~~

2. Par lecture et analyse des relations de la base de donnée.

a. Déterminer le jour et la plage horaire du match entre Durand Belina et Caron Camilia.

~~~ {.python .hidden  .amc file="Q_2_a.md" bareme="1"}
>>> pass
~~~

b. Déterminer le nom des deux joueurs qui sont les seuls à avoir joué dans le hangar.

~~~ {.python .hidden  .amc file="Q_2_b.md" bareme="1"}
>>> pass
~~~

3. Requêtes en langage SQL.

a. Écrire une requête qui renvoie les prénoms des joueurs dont le nom est ‘Dupont’.

~~~ {.python .hidden  .amc file="Q_3_a.md" bareme="1"}
>>> pass
~~~

b. Écrire une requête qui modifie le mot de passe de Dorine Dupont, son nouveau mot de passe étant 1976.

~~~ {.python .hidden  .amc file="Q_3_b.md" bareme="1"}
>>> pass
~~~

4. Écrire une requête permettant d’ajouter le nouveau membre «Zora MAGID» dont le login est « zora » et le mot de passe 2021.

~~~ {.python .hidden  .amc file="Q_4.md" bareme="1"}
>>> pass
~~~

5. Écrire une requête qui renvoie les jours où Alice joue.

~~~ {.python .hidden  .amc file="Q_5.md" bareme="1"}
>>> pass
~~~









