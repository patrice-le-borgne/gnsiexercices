Exercice
========

Une `Rangee` est une `liste` qui contient au minimum une carte et au
maximum 6 cartes.

,-------------------------------------------------------------------------------.
|Rangee                                                                         |
|-------------------------------------------------------------------------------|
|+ cartes : la liste des cartes                                                 |
|                                                                               |
|+ __init__(self, c) : une carte c doit être fournie à la création de la rangée.|
|                     Si c n'est pas une carte, cela provoque une AssertionError|
|+ __len(self)__:renvoie la longueur de la rangée.                              |
|+ get_derniere_carte(self): Renvoie la dernière carte de la rangée             |
|+ get_cartes(self): Renvoie la liste de cartes.                                |
|+ set_cartes(self, l): remplace la liste de cartes par la liste l.             |
|+ ajouter(self, c):Ajoute le carte c à la liste. Si c est la sixième           |
|                 carte, les 5 premières sont renvoyées dans une liste.         |
|+ get_nombre_tdb(self): renvoie le nombre de têtes de bœufs                    |
|                      contenues dans la rangée.                                |
|+ __str__(self): pour afficher la liste de cartes séparées par un tiret.       |
`-------------------------------------------------------------------------------'

1.  Ne nous trompons pas entre la liste et les éléments de la liste....


    >>> r = Rangee(Carte(3))
    >>> len(r)
    1
    >>> r.get_derniere_carte()
    Carte n°3 - 1 TdB
    >>> r.get_cartes()
    [Carte n°3 - 1 TdB]


2.  Tant que la rangée n'est pas pleine, elle renvoie une liste vide à
    chaque ajout.


    >>> r.ajouter(Carte(55))
    []
    >>> r.get_derniere_carte()
    Carte n°55 - 7 TdB
    >>> r.get_nombre_tdb()
    8
    >>> print(r)
    3-55
    >>> r.ajouter(Carte(63))
    []
    >>> r.get_derniere_carte()
    Carte n°63 - 1 TdB
    >>> print(r)
    3-55-63
    >>> r.get_nombre_tdb()
    9
    >>> r.ajouter(Carte(76))
    []
    >>> r.ajouter(Carte(82))
    []
    >>> r.get_nombre_tdb()
    11 


3.  Quand la sixième carte est ajoutée, les 5 premières sont renvoyées
    dans une liste et la liste des cartes de la rangées est réduite à la
    dernière carte ajoutée.


    >>> print(r)
    3-55-63-76-82
    >>> r.ajouter(Carte(85))
    [Carte n°3 - 1 TdB, Carte n°55 - 7 TdB, Carte n°63 - 1 TdB, Carte n°76 - 1 TdB, Carte n°82 - 1 TdB]
    >>> print(r)
    85
    >>> r.cartes
    [Carte n°85 - 2 TdB]


4.  On peut remplacer la liste des cartes.


    >>> r.set_cartes([Carte(100)])
    >>> r.cartes
    [Carte n°100 - 3 TdB]

