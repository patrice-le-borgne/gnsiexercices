    >>> from exercice import *
    >>> r2 = Rangee(Carte(10))

    >>> r2.ajouter(Carte(11))
    []
    >>> r2.ajouter(Carte(21))
    []
    >>> len(r2)
    3
    >>> r2.ajouter(Carte(31))
    []
    >>> r2.ajouter(Carte(41))
    []
    >>> len(r2)
    5
    >>> r2.get_derniere_carte()
    Carte n°41 - 1 TdB

 
