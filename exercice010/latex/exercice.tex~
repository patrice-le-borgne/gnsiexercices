\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

On modélise la représentation binaire d'un entier non signé par un
tableau d'entiers dont les éléments sont 0 ou 1. Par exemple, le tableau
\texttt{{[}1,\ 0,\ 1,\ 0,\ 0,\ 1,\ 1{]}} représente l'écriture binaire
de l'entier dont l'écriture décimale est

\begin{Shaded}
\begin{Highlighting}[]
\DecValTok{2}\OperatorTok{**}\DecValTok{6} \OperatorTok{+} \DecValTok{2}\OperatorTok{**}\DecValTok{4} \OperatorTok{+} \DecValTok{2}\OperatorTok{**}\DecValTok{1} \OperatorTok{+} \DecValTok{2}\OperatorTok{**}\DecValTok{0} \OperatorTok{=} \DecValTok{83}
\end{Highlighting}
\end{Shaded}

À l'aide d'un parcours séquentiel, écrire la fonction convertir
répondant aux spécifications suivantes :

\begin{Shaded}
\begin{Highlighting}[]
    \KeywordTok{def}\NormalTok{ convertir(T):}
        \CommentTok{"""}
\CommentTok{        T est un tableau d'entiers, dont les éléments sont 0 ou 1 et}
\CommentTok{        représentant un entier écrit en binaire.}
\CommentTok{        Renvoie l'entier positif dont la représentation binaire}
\CommentTok{        est donnée par le tableau T}
\CommentTok{        """}
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ convertir([}\DecValTok{1}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{1}\NormalTok{])}
    \DecValTok{83}
    \OperatorTok{>>>}\NormalTok{ convertir([}\DecValTok{1}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{0}\NormalTok{])}
    \DecValTok{130}
    \OperatorTok{>>>>} 
\end{Highlighting}
\end{Shaded}

