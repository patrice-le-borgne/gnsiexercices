~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : binaire
thème : boucles
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On modélise la représentation binaire d'un entier non signé par un tableau d'entiers dont les éléments sont 0 ou 1. Par exemple, le tableau `[1, 0, 1, 0, 0, 1, 1]` représente l'écriture binaire de l'entier dont l'écriture décimale est

~~~ {.python .amc file="Q_1.md" bareme="4"}
2**6 + 2**4 + 2**1 + 2**0 = 83
~~~

À l'aide d'un parcours séquentiel, écrire la fonction convertir répondant aux spécifications suivantes :

~~~ {.python}
    def convertir(T):
        """
        T est un tableau d'entiers, dont les éléments sont 0 ou 1 et
        représentant un entier écrit en binaire.
        Renvoie l'entier positif dont la représentation binaire
        est donnée par le tableau T
        """
~~~

~~~ {.python .test file="Q_1.md" bareme="2"}
    >>> convertir([1, 0, 1, 0, 0, 1, 1])
    83
    >>> convertir([1, 0, 0, 0, 0, 0, 1, 0])
    130
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
    >>> convertir([0, 0, 0, 0, 0])
    0
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
    >>> convertir([0, 1, 1, 1, 1, 1, 1, 1])
    127
~~~

