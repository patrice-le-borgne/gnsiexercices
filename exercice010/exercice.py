def convertir(T):
    """
    T est un tableau d'entiers, dont les éléments sont 0 ou 1 et
    représentant un entier écrit en binaire.
    Renvoie l'entier positif dont la représentation binaire
    est donnée par le tableau T
    """
    n = 0
    for c in T:
        n = 2 * n + c
    return n
