~~~ {.hidden .meta}
classe : terminale
type : oral2
chapitre : structures linéaires
thème : tris
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> from random import randint
~~~

Exercice
==========

La fonction `tri_bulles` prend en paramètre une liste `T` d’entiers non triés et renvoie la liste triée par ordre croissant.
Compléter le code Python ci-dessous qui implémente la fonction `tri_bulles`.

~~~ {.python .amc file="Q_1.md" bareme="4"}
def tri_bulles(T):
    n = len(T)
    for i in range(...,...,-1):
        for j in range(i):
            if T[j] > T[...]:
                ... = T[j]
                T[j] = T[...]
                T[j+1] = temp
    return T
~~~


~~~ {.python .hidden .test file="Q_1.md" bareme="2"}
>>> L = [randint(1, 100) for i in range(20)]
>>> L1 = sorted(L)
>>> L1 == tri_bulles(L)
True
~~~

