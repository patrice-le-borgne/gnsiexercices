Exercice
========

On souhaite coder une balle qui rebondir sur les bords d'une cadre. La
position `(x, y)` de la balle correspond à celle de son centre.

Compléter le code de la fonction `avancer(x,y)` qui renvoie la nouvelle
position de la balle une fois qu'elle a avance de **10 pixels**.

La balle peut se déplacer dans 4 directions : `'haut'`, `'droite'`,
`'bas'`, `'gauche'`. *On rappelle que l'axe des ordonnées sont orientés
vers le bas.*

``` {.python}
def avancer(direction, x, y):
    """
    Renvoie la nouvelle position de la balle une fois
    qu'elle a avancé. On rappelle que l'axe des ordonnées
    est orientés vers le bas en informatique.
    """
    if direction == "bas":
        ...............
    elif direction == "gauche":
        ...............
    elif direction == "droite":
        ...............
    else:
        ...............
    return(x,y)

```

**Exemple d'appels de la fonction**

``` {.python}
>>> avancer("droite", 200, 100)
(210, 100)
>>> avancer("gauche", 200, 100)
(190, 100)
>>> avancer("bas", 200, 100)
(200, 110)
>>> avancer("haut", 200, 100)
(200, 90)
```
