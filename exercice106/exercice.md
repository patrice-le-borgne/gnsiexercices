~~~ {.hidden .meta}
classe : seconde
type : oral2
chapitre : fonction
thème : fonction
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> global s
>>> s = 0
~~~

Exercice
========

On souhaite coder une balle qui rebondir sur les bords d'une cadre. La position `(x, y)` de la balle correspond à celle de son centre.

Compléter le code de la fonction `avancer(x,y)` qui renvoie la nouvelle position de la balle une fois qu'elle a avancé de **10 pixels**.

La balle peut se déplacer dans 4 directions : `'haut'`, `'droite'`, `'bas'`, `'gauche'`. *On rappelle que l'axe des ordonnées sont orientés vers le bas.*

~~~ {.python}
def avancer(direction, x, y):
    """
    Renvoie la nouvelle position de la balle une fois
    qu'elle a avancé. On rappelle que l'axe des ordonnées
    est orientés vers le bas en informatique.
    """
    if direction == "bas":
        ...............
    elif direction == "gauche":
        ...............
    elif direction == "droite":
        ...............
    else:
        ...............
    return(x,y)


~~~


**Exemple d'appels de la fonction**

~~~ {.python .amc file="Q_1.md" bareme="2"}
>>> avancer("droite", 200, 100)
(210, 100)
>>> avancer("gauche", 200, 100)
(190, 100)
>>> avancer("bas", 200, 100)
(200, 110)
>>> avancer("haut", 200, 100)
(200, 90)
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> avancer("droite", 300, 100)
(310, 100)
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> avancer("gauche", 300, 100)
(290, 100)
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
>>> avancer("bas", 200, 200)
(200, 210)
~~~

~~~ {.python .hidden .test file="Q_4.md" bareme="1"}
>>> avancer("haut", 200, 200)
(200, 190)
~~~
