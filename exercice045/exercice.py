def indice_capitale(indice: int) -> str:
    assert 0 <= indice < 26, "L'indice doit être entre 0 et 25"
    return chr(ord('A') + indice)

def lettre_capitale(lettre: str) -> int:
    assert lettre in [chr(i + ord('A')) for i in range(26)], "La lettre doit être dans l'alphabet latin capital"
    return ord(lettre) - ord('A')


class CodeCesar:
    def __init__(self, cle):
        self.cle = cle

    def decale(self, lettre):
        indice_1 = lettre_capitale(lettre)
        indice_2 = indice_1 + self.cle
        if indice_2 >= 26:
            indice_2 = indice_2 - 26
        if indice_2 < 0:
            indice_2 = indice_2 + 26
        nouvelle_lettre = indice_capitale(indice_2)
        return nouvelle_lettre

    def chiffre(self, texte):
        chaine = ""
        for caractere in texte:
            chaine = chaine + self.decale(caractere)
        return chaine

    def transforme(self, texte):
        self.cle = -self.cle
        message = self.cryptage(texte)
        self.cle = -self.cle
        return message

def chiffre_texte(clef, texte):
    return CodeCesar(clef).chiffre(texte)
