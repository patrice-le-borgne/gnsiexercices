\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Dans cet exercice, on étudie une méthode de chiffrement de chaines de
caractères alphabétiques. Pour des raisons historiques, cette méthode de
chiffrement est appelée « code de César ». On considère que les messages
ne contiennent que les lettres capitales de l'alphabet
\texttt{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"} et la méthode de chiffrement
utilise un nombre entier fixé appelé la clé de chiffrement.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Soit la classe \texttt{CodeCesar} définie ci-dessous :
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{class}\NormalTok{ CodeCesar:}
    \KeywordTok{def} \FunctionTok{__init__}\NormalTok{(}\VariableTok{self}\NormalTok{, cle):}
        \VariableTok{self}\NormalTok{.cle }\OperatorTok{=}\NormalTok{ cle}

    \KeywordTok{def}\NormalTok{ decale(}\VariableTok{self}\NormalTok{, lettre):}
\NormalTok{        indice_1 }\OperatorTok{=}\NormalTok{ indice_capitale(lettre)}
\NormalTok{        indice_2 }\OperatorTok{=}\NormalTok{ indice_1 }\OperatorTok{+} \VariableTok{self}\NormalTok{.cle}
        \ControlFlowTok{if}\NormalTok{ indice_2 }\OperatorTok{>=} \DecValTok{26}\NormalTok{:}
\NormalTok{            indice_2 }\OperatorTok{=}\NormalTok{ indice_2 }\OperatorTok{-} \DecValTok{26}
        \ControlFlowTok{if}\NormalTok{ indice_2 }\OperatorTok{<} \DecValTok{0}\NormalTok{:}
\NormalTok{            indice_2 }\OperatorTok{=}\NormalTok{ indice_2 }\OperatorTok{+} \DecValTok{26}
\NormalTok{        nouvelle_lettre }\OperatorTok{=}\NormalTok{ lettre_capitale(indice_2)}
        \ControlFlowTok{return}\NormalTok{ nouvelle_lettre}
\end{Highlighting}
\end{Shaded}

On dispose aussi des fonctions \texttt{indice\_capitale(indice)} et
\texttt{lettre\_capitale(lettre)} qui renvoient la lettre majuscule
correspondant à un indice donné et l'indice (la position d'une lettre)
dans l'alphabet latin usuel.

\textbf{Représenter} le résultat d'exécution du code Python suivant :

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ code }\OperatorTok{=}\NormalTok{ CodeCesar(}\DecValTok{3}\NormalTok{)}
\OperatorTok{>>>}\NormalTok{ code.decale(}\StringTok{'A'}\NormalTok{)}
\NormalTok{...}
\OperatorTok{>>>}\NormalTok{ code.decale(}\StringTok{'X'}\NormalTok{)}
\NormalTok{...}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  La méthode de chiffrement du « code de César » consiste à décaler les
  lettres du message dans l'alphabet d'un nombre de rangs fixé par la
  clé. Par exemple, avec la clé 3, toutes les lettres sont décalées de 3
  rangs vers la droite : le A devient le D, le B devient le E, etc.
\end{enumerate}

\textbf{Ajouter} une méthode \texttt{chiffre(self,\ texte)} dans la
classe \texttt{CodeCesar} définie à la question précédente, qui reçoit
en paramètre une chaîne de caractères (le message à chiffrer) et qui
renvoie une chaîne de caractères (le message chiffré).

Cette méthode \texttt{chiffre(self,\ texte)} doit chiffrer la chaîne
\texttt{texte} avec la clé de l'objet de la classe \texttt{CodeCesar}
qui a été instanciée.

Exemple :

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ code }\OperatorTok{=}\NormalTok{ CodeCesar(}\DecValTok{3}\NormalTok{)}
\OperatorTok{>>>}\NormalTok{ code.chiffre(}\StringTok{"NSI"}\NormalTok{)}
\CommentTok{'QVL'}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  \textbf{Écrire} une fonction \texttt{chiffre\_texte(clef,\ texte)} qui
  :

  \begin{itemize}
  \tightlist
  \item
    prend en argument la clef de chiffrement et le message à chiffrer ;
  \item
    instancie un objet de la classe \texttt{CodeCesar} ;
  \item
    renvoie le texte chiffré.
  \end{itemize}
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  On ajoute la méthode \texttt{transforme(texte)} à la classe
  \texttt{CodeCesar} :
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ transforme(}\VariableTok{self}\NormalTok{, texte):}
    \VariableTok{self}\NormalTok{.cle }\OperatorTok{=} \OperatorTok{-}\VariableTok{self}\NormalTok{.cle}
\NormalTok{    message }\OperatorTok{=} \VariableTok{self}\NormalTok{.cryptage(texte)}
    \VariableTok{self}\NormalTok{.cle }\OperatorTok{=} \OperatorTok{-}\VariableTok{self}\NormalTok{.cle}
    \ControlFlowTok{return}\NormalTok{ message}
\end{Highlighting}
\end{Shaded}

On exécute la ligne suivante dans une console
\texttt{CodeCesar(10).transforme("PSX")}.

Que va-t-il s'afficher ? Expliquer votre réponse.
