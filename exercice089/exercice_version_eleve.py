"""
Module Carte de six qui prend
"""


class Carte:
    """ Classe décrivant les cartes sur 6 qui prend"""
    def __init__(self, numero):
        """
        Chaque carte porte un numéro compris entre 1 et 104 ainsi
        qu'un certain nombre de têtes de bœufs qui correspondent à
        des pénalités.
        """
        pass
    
    def get_numero(self):
        """
        Renvoie le numero de la carte
        """
        pass

    def get_nombre_tdb(self):
        """
        renvoie le nombre de têtes de bœuf que la carte contient.
        """
        pass

    def __repr__(self):
        """
        La représentation dans une console.
        """
        pass

    def __str__(self):
        """
        La description de la carte en mode texte.
        """
        pass

    def __get_penalites(self):
        """Renvoie le nombre de têtes de vaches que contient la carte."""
        pass
    
    def __lt__(self, autre):
        """
        Permet la comparaison < entre la carte courante et la carte autre.
        """
        pass

    def __gt__(self, autre):
        """
        Permet la comparaison > entre la carte courante et la carte autre.
        """
        pass


if __name__ == "__main__":
    import nose
    nose.run()
