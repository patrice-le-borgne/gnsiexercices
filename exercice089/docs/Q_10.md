    >>> from exercice import *

    >>> cartes = [0]
    >>> for i in range(1, 105):
    ...    m = Carte(i)
    ...    cartes.append(m)
    ...    assert m.numero == i

    >>> cartes_pas_simples = []

    >>> m10 = Carte(16)
    >>> m20 = Carte(13)
    >>> m30 = Carte(100)

    >>> m40 = Carte(1)
    >>> m50 = Carte(81)
    >>> m40 < m50
    True
    >>> m40 > m50
    False

 
