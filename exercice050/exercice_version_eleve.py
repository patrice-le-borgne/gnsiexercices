"""
Les 2 premières fonctions vous sont fournies pour que
vous puissiez faire des tests personnels.
"""

def creer_grille_vide(nl=6, nc=7):
    """
    Crée une grille vide
    """
    return [ [0 for i in range(nc)] for j in range(nl)]

def afficher_grille(t):
    """
    Affiche la grille du puissance 4 avec des croix et des ronds.
    """
    for i in range(len(t)):
        ligne = "|"
        for j in range(len(t[0])):
            if  t[i][j] == 0:
                ligne += " |"
            elif t[i][j] == 1:
                ligne += "X|"
            else:
                ligne += "O|"
        print(ligne)
    print("-" * (len(t[0])*2 + 1))

def derniere_ligne_vide(t, colonne):
    """
    Renvoie la ligne où on peut placer le pion.
    """
    assert 0 <= colonne < len(t[0])

def ajouter_pion_colonne(t, colonne, joueur):
    """
    Ajoute le pion de joueur dans la colonne.
    Renvoie la ligne où le pion a été ajouté.
    Si la colonne était pleine, renvoie -1.
    """
    assert 0 <= colonne < len(t[0])    

def alignement_vertical(t, ligne, colonne, joueur):
    """
    Teste l'alignement vertical pour joueur 
    à partir de la case(ligne, colonne).
    """
    assert 0 <= ligne < len(t)
    assert 0 <= colonne < len(t[0])


def alignement_horizontal(t, ligne, colonne, joueur):
    """
    Teste l'alignement vertical pour joueur 
    à partir de la case(ligne, colonne).
    """
    assert 0 <= ligne < len(t)
    assert 0 <= colonne < len(t[0])


if __name__ == '__main__':
    pass
