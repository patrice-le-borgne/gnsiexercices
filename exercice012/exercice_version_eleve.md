Exercice
========

*Cet exercice traite principalement du thème « architectures
matérielles, systèmes d'exploitation et réseaux. Cet exercice mobilise
des connaissances sur le routage et sur l'évolution des architectures
des machines.*

**Partie A** : Routage dans un réseau informatique

1.  Expliquer pourquoi le protocole TCP-IP prévoit un découpage en
    paquets et une encapsulation des fichiers transférés d'un ordinateur
    à un autre via Internet.
2.  On souhaite modéliser un réseau informatique par un graphe pondéré
    pour identifier le chemin optimal pour un paquet.
    a.  Préciser ce que représentent les sommets et les arêtes du
        graphe.
    b.  Préciser si le protocole RIP utilise le nombre de sauts ou le
        délai de réception comme poids des arêtes.

**Partie B** : Système d'exploitation Un système d'exploitation doit
assurer la gestion des processus et des ressources.

1.  Dans ce contexte, expliquer et illustrer par un exemple ce qu'est
    une situation d'interblocage (deadlock).

2.  Citer des mécanismes permettant d'éviter ces situations.

**Partie C** : Architectures matérielles

**Architecture Von Neumann** *« L'architecture dite architecture de Von
Neumann est un modèle pour un ordinateur qui utilise une structure de
stockage unique pour conserver à la fois les instructions et les données
demandées ou produites par le calcul. De telles machines sont aussi
connues sous le nom d'ordinateur à programme enregistré. »*

*source : Wikipédia*

Elle décompose l'ordinateur en 4 éléments : l'unité de contrôle (appelé
aussi unité de commande), l'unité arithmétique et logique (UAL), la
mémoire et les entrées-sorties. Les deux premiers éléments sont
rassemblés dans le processeur (CPU en anglais pour Control Processing
Unit).

1.  Recopier et compléter le schéma de cette architecture ci-dessous en
    faisant apparaître les communications entre les différents éléments.

![Von Neumann](poly21_S2_exo4_1.png)

2.  Dans quel(s) élément(s) sont situés le « compteur de programme » (CP
    ou IP en anglais pour Instruction Pointer) et le « registre
    d'instruction » (RI ou IR en anglais pour Instruction Register).
    Préciser leurs rôles.

**Architecture de Harvard**

*« L'architecture de type Harvard est une conception qui sépare
physiquement la mémoire de données et la mémoire programme. L'accès à
chacune des deux mémoires s'effectue via deux bus distincts. \[...\]
L'architecture Harvard est souvent utilisée dans les processeurs
numériques de signal (DSP) et les microcontrôleurs. »*

*source : Wikipédia*

3.  Recopier et compléter le schéma de cette architecture ci-dessous et
    faire apparaître les communications entre les différents éléments.

![Harvard](poly21_S2_exo4_2.png)

4.  Expliquer ce qu'est une mémoire morte et une mémoire vive. Expliquer
    brièvement pourquoi, dans les microcontrôleurs, la mémoire programme
    est une mémoire morte.

**Partie D** : Système sur puce

*« Un "système sur une puce", souvent désigné dans la littérature
scientifique par le terme anglais "system on a chip" (d'où son
abréviation SoC), est un système complet embarqué sur une seule puce
("circuit intégré"), pouvant comprendre de la mémoire, un ou plusieurs
microprocesseurs, des périphériques d'interface, ou tout autre composant
nécessaire à la réalisation de la fonction attendue. »*

*source : Wikipédia*

1.  Citer un des avantages d'avoir plusieurs processeurs.
2.  Expliquer pourquoi les systèmes sur puces intègrent en général des
    bus ayant des vitesses de transmission différentes.
3.  Citer un des avantages d'un circuit imprimé de petite taille.
4.  Citer un des inconvénients de cette miniaturisation.
