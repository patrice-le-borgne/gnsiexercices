# Exercice

## Partie A

### Q1

Le réseau fonctionne sur le principe de la commutation de paquets. TCP
découpe le message en paquets (datagrammes). Ces paquets sont routés
par IP grâce aux en-têtes qui encapsulent le message.

### Q2a

- Le sommets représentent le nom des routeurs.
- Les arêtes représentent les connexions entre les routeurs.

### Q2b

RIP utilise le nombre de sauts entre les routeurs.

## Partie B

### Q1

L'interblocage se produit lorsque des processus concurrents s'attendent mutuellement.
Exemple d'interblocage : le processus P1 utilise la ressource R2 qui
est attendue par le processus P2 qui utilise la ressource R1,
attendue par P1.

### Q2

- Utilisation de mutex.
- Algorithme du Banquier.

## Partie C

# Q1

## Q2

- **IP** : UC. Registre qui contient l'adresse mémoire de
  l'instruction en cours d'exécution ou prochainement exécutée. Une
  fois l'instruction chargée, il est automatiquement incrémenté pour
  pointer l'instruction suivante.
- **IR** : UC. Registre qui contient l'instruction en cours
  d'exécution ou de décodage.

## Q3

## Q4

-  Mémoire morte : mémoire non volatile dont le contenu est fixé lors
de leur fabrication, qui peut être lue plusieurs fois et qui n’est pas
prévue pour être modifiée. 
-  Mémoire vive : mémoire volatile dans laquelle des données peuvent être écrites, lues et modifiées.  Un microcontrôleur exécute un programme dès qu'il est mis sous tension. Ce programme ne doit pas être modifié par un simple utilisateur.  

## Partie D

### Q1

Chaque microprocesseur peut effectuer une tâche spécialiser pour augmenter la rapidité du traitement.  

### Q2

La vitesse des différents composants (CPU, RAM, circuits d'entrées/sorties, ...) évolue en fonction de la technologie. Cette évolution n’est pas la même pour tous les composants.  

## Q3

La miniaturisation permet d’avoir des fonctions avancées de taille réduite, facile à intégrer physiquement dans des systèmes dits embarqués.

### Q4 

La puissance de calcul est généralement plus faible qu’avec une architecture classique.
