~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> a = [1, 0, 1, 0, 1, 1, 0, 1]
>>> b = [0, 1, 1, 1, 0, 1, 0, 0]
>>> c = [1, 1, 0, 1]
>>> d = [0, 0, 1, 1]
~~~

Exercice
========

L'opérateur « ou exclusif » entre deux bits renvoie 0 si les deux bits sont égaux et 1 s'ils sont
différents. Il est symbolisé par le caractère $\oplus$.
Ainsi :

- $0 \oplus 0 = 0$
- $0 \oplus 1 = 1$
- $1 \oplus 0 = 1$
- $1 \oplus 1 = 0$

On représente ici une suite de bits par un tableau contenant des 0 et des 1.

Exemples :

~~~ {.python .amc file="Q_1.md" bareme="2"}
    a = [1, 0, 1, 0, 1, 1, 0, 1]
    b = [0, 1, 1, 1, 0, 1, 0, 0]
    c = [1, 1, 0, 1]
    d = [0, 0, 1, 1]
~~~

Écrire la fonction ```ou_exclusif``` qui prend en paramètres deux tableaux de même longueur et qui renvoie
un tableau où l’élément situé à position `i` est le résultat, par l’opérateur « ou exclusif », des
éléments à la position `i` des tableaux passés en paramètres.

En considérant les quatre exemples ci-dessus, cette fonction donne :

~~~ {.python .test file="Q_1.md" bareme="1"}
    >>> ou_exclusif(a, b)
    [1, 1, 0, 1, 1, 0, 0, 1]
    >>> ou_exclusif(c, d)
    [1, 1, 1, 0]
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> ou_exclusif([1, 1, 1, 0, 1], [1, 1, 0, 0, 0])
[0, 0, 1, 0, 1]
>>> ou_exclusif([], [])
[]
~~~

