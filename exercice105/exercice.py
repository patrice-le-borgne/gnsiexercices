def impot(R):
    if R < 4412:
        return 0
    elif 4412 <= R <= 8677:
        return R * .0683 - 301.84
    elif 8677 < R <= 15274:
        return R * .1914 -1369.48
    else:
        return R * 0.2826 - 2762.47
