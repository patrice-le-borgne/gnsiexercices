Exercice
========

L'impôt $I$ sur le revenu $R$ est calculé par la formule suivante :

-   si $R < 4412$ alors $I = 0$
-   si $R \in [4412 ; 8677]$ alors $I = R \times 0,0683 - 301,84$;
-   si $R \in ]8677 ; 15274]$ alors $I = R \times 0,1914 - 1369,48$
-   si $R > 15274$ alors $I = R \times 0,2826 - 2762,47$

Compléter le fonction `impot(R)` qui renvoie le montant de l'impôt
correspondant au revenu `R`.

``` {.python}
def impot(R):
    if ............:
        return 0
    elif ......................
        return ................
    elif ......................
        return ................
    else:
        .......................
```

**Exemple d'appels de la fonction**

``` {.python}
>>> impot(3700)
0
>>> impot(8000)
244.56
>>> impot(15000)
1501.52
>>> impot(40000)
8541.53
```
