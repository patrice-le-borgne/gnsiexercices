~~~ {.hidden .meta}
classe : seconde
type : oral2
chapitre : fonction
thème : fonction
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

L'impôt $I$ sur le revenu $R$ est calculé par la formule suivante :

- si $R < 4412$ alors $I = 0$
- si $R \in [4412 ; 8677]$ alors $I = R \times 0,0683 - 301,84$; 
- si $R \in ]8677 ; 15274]$ alors $I = R \times 0,1914 - 1369,48$
- si $R > 15274$ alors $I = R \times 0,2826 - 2762,47$

Compléter le fonction `impot(R)` qui renvoie le montant de l'impôt correspondant au revenu `R`.

~~~ {.python}
def impot(R):
    if ............:
        return 0
    elif ......................
        return ................
    elif ......................
        return ................
    else:
        .......................
~~~


**Exemple d'appels de la fonction**

~~~ {.python .amc file="Q_1.md" bareme="1"}
>>> impot(3700)
0
>>> impot(8000)
244.56
>>> impot(15000)
1501.52
>>> impot(40000)
8541.53
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> impot(3700)
0
>>> impot(2700)
0
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> impot(8000)
244.56
>>> impot(7550)
213.825
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
>>> impot(15000)
1501.52
>>> impot(12641)
1050.0074
~~~

~~~ {.python .hidden .test file="Q_4.md" bareme="1"}
>>> impot(40000)
8541.53
>>> impot(80000)
19845.53
~~~
