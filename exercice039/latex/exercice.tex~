\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

L'objectif de cet exercice est de mettre en place une modélisation d'un
jeu de labyrinthe en langage Python.

On décide de représenter un labyrinthe par un tableau carré de taille
\texttt{n}, dans lequel les cases seront des \(0\) si l'on peut s'y
déplacer et des \(1\) s'il s'agit d'un mur. Voici un exemple de
représentation d'un labyrinthe :

\begin{minipage}{.350\linewidth}
\includegraphics[width=\linewidth]{laby_1_exo_2_2021.png}
\end{minipage}
\hfill
\begin{minipage}{.65\linewidth}
\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ laby}\OperatorTok{=}\NormalTok{[[}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{],}
\NormalTok{    ...       [}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{]]}
\end{Highlighting}
\end{Shaded}
\end{minipage}

L'entrée du labyrinthe se situe à la première case du tableau (celle en
haut à gauche) et la sortie du labyrinthe se trouve à la dernière case
(celle en bas à droite).

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Proposer, en langage Python, une fonction \texttt{mur()}, prenant en
  paramètre un tableau représentant un labyrinthe et deux entiers
  \texttt{i} et \texttt{j} compris entre \(0\) et \(n-1\) et qui renvoie
  un booléen indiquant la présence ou non d'un mur.

  Par exemple :
%%%%%%%%%%%%%%%%%%%%%\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ mur(laby, }\DecValTok{2}\NormalTok{, }\DecValTok{3}\NormalTok{)}
\VariableTok{True}
\OperatorTok{>>>}\NormalTok{ mur(laby, }\DecValTok{1}\NormalTok{, }\DecValTok{8}\NormalTok{)}
\VariableTok{False}
\end{Highlighting}
\end{Shaded}

\begin{minipage}{0.6\linewidth}
Un parcours dans le labyrinthe va être représenté par une liste de
cases. Il s'agit de couples \texttt{(i,j)} où \texttt{i} et \texttt{j}
correspondent respectivement aux numéros de ligne et de colonne des
cases successivement visitées au long du parcours. Ainsi, la liste
suivante

\texttt{{[}(1,4),(1,5),(1,6),(2,6),(3,6),(3,5),(3,4){]}}

correspond au parcours repéré par des étoiles ci-contre :
\end{minipage}
\hfill
\begin{minipage}{0.35\linewidth}
\includegraphics[width=\linewidth]{laby_2_exo_2_2021.png}
\end{minipage}


La liste \texttt{{[}(0,0),(1,0),(1,1),(5,1),(6,1){]}} ne peut
correspondre au parcours d'un labyrinthe car toutes les cases parcourues
successivement ne sont pas adjacentes.

%%\begin{enumerate}
%%\def\labelenumi{\arabic{enumi}.}
%%\setcounter{enumi}{1}
%%\tightlist
\item
  On considère la fonction voisine ci-dessous, écrite en langage Python,
  qui prend en paramètres deux cases données sous forme de couple.
%\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ voisine(case1, case2) :}
\NormalTok{    l1, c1 }\OperatorTok{=}\NormalTok{ case1}
\NormalTok{    l2, c2 }\OperatorTok{=}\NormalTok{ case2}
\NormalTok{    d }\OperatorTok{=}\NormalTok{ (l1}\OperatorTok{-}\NormalTok{l2)}\OperatorTok{**}\DecValTok{2} \OperatorTok{+}\NormalTok{ (c1}\OperatorTok{-}\NormalTok{c2)}\OperatorTok{**}\DecValTok{2}
    \ControlFlowTok{return}\NormalTok{ (d }\OperatorTok{==} \DecValTok{1}\NormalTok{)}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
%\def\labelenumi{\alph{enumi}.}
\item
  Après avoir remarqué que les quantités \texttt{l1-l2} et
  \texttt{c1-c2} sont des entiers, expliquer pourquoi la fonction
  voisine indique si deux cases données sous forme de tuples
  \texttt{(l,c)} sont adjacentes.
\item
  En déduire une fonction \texttt{adjacentes(l)} qui reçoit une liste de
  cases \texttt{l} et renvoie un booléen indiquant si la liste des cases
  forme une chaîne de cases adjacentes.
\end{enumerate}

Un parcours sera qualifié de \textbf{compatible} avec le labyrinthe
lorsqu'il s'agit d'une succession de cases adjacentes accessibles (non
murées). On donne la fonction \texttt{teste(cases,\ laby)} qui indique
si le chemin \texttt{cases} est un chemin possible compatible avec le
labyrinthe \texttt{laby} :

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ teste(cases, laby) :}
    \ControlFlowTok{if} \KeywordTok{not}\NormalTok{ adjacentes(cases) :}
        \ControlFlowTok{return} \VariableTok{False}
\NormalTok{    possible }\OperatorTok{=} \VariableTok{True}
\NormalTok{    i }\OperatorTok{=} \DecValTok{0}
    \ControlFlowTok{while}\NormalTok{ i }\OperatorTok{<} \BuiltInTok{len}\NormalTok{(cases) }\KeywordTok{and}\NormalTok{ possible:}
        \ControlFlowTok{if}\NormalTok{ mur(laby, cases[i][}\DecValTok{0}\NormalTok{], cases[i][}\DecValTok{1}\NormalTok{]) :}
\NormalTok{            possible }\OperatorTok{=} \VariableTok{False}
\NormalTok{        i }\OperatorTok{=}\NormalTok{ i }\OperatorTok{+} \DecValTok{1}
    \ControlFlowTok{return}\NormalTok{ possible}
\end{Highlighting}
\end{Shaded}

%\begin{enumerate}
%\def\labelenumi{\arabic{enumi}.}
%\setcounter{enumi}{2}
%\tightlist
\item
  Justifier que la boucle de la fonction précédente se termine.
\item
  En déduire une fonction \texttt{echappe(cases,\ laby)} qui indique par
  un booléen si le chemin \texttt{cases} permet d'aller de l'entrée à la
  sortie du labyrinthe \texttt{laby}.
\end{enumerate}
