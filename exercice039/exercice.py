def mur(lab, i, j):
    return lab[i][j] == 1


def voisine(case1, case2):
    l1, c1 = case1
    l2, c2 = case2
    d = (l1-l2)**2 + (c1-c2)**2
    return (d == 1)

"""
Réponse question 2.a.

La distance qui sépare une case d'une autre case située en diagonale est de racine de 2
car le calcul de d correspond à une théorème de Pythagore. d correspond au carré de
l'hypoténuse.
"""

point_2_a = 1

def adjacentes(l):
    # on parcourt la liste jusqu'à l'avant dernier élément
    for i in range(len(l)-1):
    	# si l'élement courant et son suivant ne sont pas
    	# voisins, on renvoie faux.
        if not voisine(l[i], l[i+1]):
            return False
    # si on a parcouru toute la liste sans rencontrer de return
    # c'est que toutes les cases sont adjacentes à leurs voisines.
    return True

def teste(cases, laby):
    if not adjacentes(cases) :
        return False
    ## On peut faire la suite avec une boucle for !
    possible = True
    i = 0
    while i < len(cases) and possible:
        if mur(laby, cases[i][0], cases[i][1]) :
            possible = False
        i = i + 1
    return possible

def echappe(cases, laby):
    # on teste que la première case de la liste est bien l'entrée
    # du labyrinthe
    resultat = cases[0] == (0,0)
    # puis que la dernière case est bien la sortie.
    resultat = resultat and cases[-1] == (len(laby)-1, len(laby[0])-1)
    # on vérifie ensuite que le chemin entre les 2 est valide.
    resultat = resultat and teste(cases, laby)
    return resultat
