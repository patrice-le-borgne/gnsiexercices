    >>> from exercice import *
    >>> j4 = Joueur("Albert")
    >>> j4.main = [Carte(5), Carte(7), Carte(75), Carte(77),  Carte(88),  Carte(101)]
    >>> j4.score = 12
    >>> j5 = Joueur("Claire")

    >>> j4.poser_carte(7)
    Carte n°7 - 1 TdB
    >>> len(j4.main)
    5
    >>> j4.main
    [Carte n°5 - 2 TdB, Carte n°75 - 2 TdB, Carte n°77 - 5 TdB, Carte n°88 - 5 TdB, Carte n°101 - 1 TdB]

 
