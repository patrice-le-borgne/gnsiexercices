    >>> from exercice import *
    >>> j4 = Joueur("Albert")
    >>> j4.main = [Carte(5), Carte(7), Carte(75), Carte(77),  Carte(88),  Carte(101)]
    >>> j4.score = 12
    >>> j5 = Joueur("Claire")

        >>> j = Joueur('Bob')
        >>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
        >>> len(j.main)
        3
        >>> j.poser_carte(3)
        Carte n°3 - 1 TdB
        >>> len(j.main)
        2
        >>> j2 = Joueur("Roger")
        >>> j2.prendre_cartes([Carte(1), Carte(20), Carte(30), Carte(55)])
        >>> len(j2.main)
        4
        >>> j2.poser_carte(55)
        Carte n°55 - 7 TdB
        >>> len(j2.main)
        3

 
