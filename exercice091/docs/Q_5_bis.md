    >>> from exercice import *
    >>> j4 = Joueur("Albert")
    >>> j4.main = [Carte(5), Carte(7), Carte(75), Carte(77),  Carte(88),  Carte(101)]
    >>> j4.score = 12
    >>> j5 = Joueur("Claire")

    >>> j4.poser_carte(6)
    Traceback (most recent call last):
    ...
    AssertionError
    >>> j5.poser_carte(6)
    Traceback (most recent call last):
    ...
    AssertionError

 
