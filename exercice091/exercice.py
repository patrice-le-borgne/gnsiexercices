"""
Module joueur.
"""

from carte import Carte
from random import randint

class Joueur:
    """
    La classe joueur.
    Les attributs :
      - nom : string
      - main : une liste de cartes.
      - score : le nombre de têtes de bœufs récupérees lors de la partie.
    Les méthodes :
      - __init__(self, nom="Vous"): initialise le joueur avec un nom par défaut 
        une main vide ([]) et un score nul.                                 
      - prendre_cartes(self, l) : ajoute la liste de cartes à la main. La main
        doit être triée. On vérifie qu'il s'agit bien d'une liste de cartes.
      - get_score(self) : renvoie le score.                                   
      - a_des_cartes(self) : renvoie True si le joueur a encore des cartes.   
      - est_carte_valable(self, n): Pour la version texte. Vérifie si n est le
        numéro d'une carte.                                                
      - poser_carte(n) : renvoie la carte de valeur n et l'enlève de la main. 
        Si la carte de valeur n n'est pas dans la main, une AssertionError  
        est levée.                                                          
      - poser_carte_au_hasard(self) : renvoie une carte au hasard. Utile pour 
        pour faire jouer les autres joueurs.                                
      - ajouter_penalites(self, l) : ajoute les nombres de têtes de bœufs     
        de la liste l au score.                                             
      - get_main(self) : renvoie la liste des cartes séparées par un tiret    
      -  __str__(self) : renvoie le nom du joueur et son score                 
    """

    def __init__(self, nom="Vous"):
        """
        Le constructeur.
        """
        self.nom = nom
        self.main = []
        self.score = 0

    def prendre_cartes(self, l):
        """
        Ajoute la carte à la main et classe la main.
        """
        for c  in l:
            assert isinstance(c, Carte)
        self.main = l
        self.main.sort()

    def get_score(self):
        """
        Renvoie le score du joueur.
        """
        return self.score

    def a_des_cartes(self):
        """
        Renvoie True si le joueur a encore des cartes.
        """
        return len(self.main) > 0

    def est_carte_valable(self, n):
        """
        Pour la version texte. Vérifie si n est le numéro d'une carte.
        """
        for carte_courante in self.main:
                if carte_courante.get_numero() == n:
                    return True
        return False

    def poser_carte(self, n):
        """
        renvoie la carte de valeur n et l'enlève de la main.
           Si la carte de valeur n n'est pas dans la main, une AssertionError
           est levée.
        """
        assert self.est_carte_valable(n)
        for carte_courante in self.main:
            if carte_courante.get_numero() == n:
                c = carte_courante
        self.main.remove(c)

        return c

    def poser_carte_au_hasard(self):
        """
        renvoie la carte de valeur n et l'enlève de la main.
           Si la carte de valeur n n'est pas dans la main, une AssertionError
           est levée.
        """
        return self.main.pop(randint(0, len(self.main) - 1))

    def ajouter_penalites(self, l):
        """
        ajoute les nombres de têtes de bœufs
        de la liste l au score.
        """
        for c in l:
            self.score += c.get_nombre_tdb()

    def get_main(self):
        """
        renvoie la liste des cartes séparées par un tiret
        """
        l = [ str(c) for c in self.main]
        return "-".join(l)
        
            
    def __str__(self):
        """
        renvoie le nom du joueur et son score sous la forme
        nom : score
        """
        return f"{self.nom} : {self.score}"

