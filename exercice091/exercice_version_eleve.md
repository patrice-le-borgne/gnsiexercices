Exercice
========

,--------------------------------------------------------------------------.
| Joueur                                                                   |
|--------------------------------------------------------------------------|
| + nom : string                                                           |
| + main : une liste de cartes.                                            |
| + score : le nombre de têtes de bœufs récupérees lors de la partie.      |
|                                                                          |
| +__init__(self, nom="Vous"): initialise le joueur avec un nom par défaut |
|     une main vide ([]) et un score nul.                                  |
| + prendre_cartes(self, l) : ajoute la liste de cartes à la main. La main |
|     doit être triée. On vérifie qu'il s'agit bien d'une liste de cartes. |
| + get_score(self) : renvoie le score.                                    |
| + a_des_cartes(self) : renvoie True si le joueur a encore des cartes.    |
| + est_carte_valable(self, n): Pour la version texte. Vérifie si n est le |
|      numéro d'une carte.                                                 |
| + poser_carte(n) : renvoie la carte de valeur n et l'enlève de la main.  |
|     Si la carte de valeur n n'est pas dans la main, une AssertionError   |
|     est levée.                                                           |
| + poser_carte_au_hasard(self) : renvoie une carte au hasard. Utile pour  |
|     pour faire jouer les autres joueurs.                                 |
| + ajouter_penalites(self, l) : ajoute les nombres de têtes de bœufs      |
|     de la liste l au score.                                              |
| + get_main(self) : renvoie la liste des cartes séparées par un tiret     |
| + __str__(self) : renvoie le nom du joueur et son score                  |
`--------------------------------------------------------------------------'

1. On crée un joueur. On vérifie qu'il n'a pas de carte et que son score est vierge.
    >>> j = Joueur('Bob')
    >>> j.nom
    'Bob'
    >>> j.main
    []
    >>> j.score
    0
    
2. La méthode `prendre_cartes()` sera utilisée lors de la distribution
   des cartes. Il faut vérifier qu'il n'y a que des cartes dans la
   liste.

    >>> j.prendre_cartes([3, 10, 51])
    Traceback (most recent call last):
    ...
    AssertionError
    >>> j.prendre_cartes([Carte(3), Carte(10), 51])
    Traceback (most recent call last):
    ...
    AssertionError
    >>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
    >>> j.main
    [Carte n°3 - 1 TdB, Carte n°10 - 3 TdB, Carte n°51 - 1 TdB]

3. La méthode `get_score(self)` renvoie le score du joueur.

    >>> j.get_score()
	0
	>>> j2 = Joueur("Roger")
	>>> j2.prendre_cartes([Carte(1), Carte(20), Carte(30), Carte(55)])
	>>> j2.get_score()
	0
	
4. On teste si les joueurs ont des cartes.

    >>> j.a_des_cartes()
	True
	>>> j2.a_des_cartes()
	True
	>>> j3 = Joueur("Marcel")
	>>> j3.a_des_cartes()
	False
	
5. Quand le joueur demande à poser une carte, il faut que la carte
   soit dans sa main sinon cela provoque une `AssertionError`.

    >>> j.est_carte_valable(5)
    False
    >>> j.poser_carte(5)
    Traceback (most recent call last):
    ...
    AssertionError
    >>> j.est_carte_valable(3)
    True
	
6. Si le joueur possède la carte demandée alors elle est renvoyée et le
   joueur possède une carte en moins.
   
    >>> len(j.main)
	3
	>>> j.poser_carte(3)
	Carte n°3 - 1 TdB
    >>> len(j.main)
	2
    >>> len(j2.main)
	4
	>>> j2.poser_carte(55)
	Carte n°55 - 7 TdB
    >>> len(j2.main)
	3

7.  On teste l'ajout de pénalités.

    >>> j.get_score()
	0
	>>> j.ajouter_penalites([Carte(9), Carte(11)])
    >>> j.get_score()
	6
	>>> j2.ajouter_penalites([Carte(90), Carte(33), Carte(104)])
    >>> j2.get_score()
	9
	
8. On vérifie la main des joueurs.

    >>> j.get_main()
	'10-51'
	>>> j2.get_main()
	'1-20-30'
    >>> j2.poser_carte(20)
	Carte n°20 - 3 TdB
    >>> j2.get_main()
	'1-30'
	
9. On teste la fonction `__str()__`.

    >>> str(j)
	'Bob : 6'
	>>> str(j2)
	'Roger : 9'
    >>> print(j)
	Bob : 6
	>>> print(j2)
	Roger : 9
