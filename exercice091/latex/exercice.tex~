\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

La classe \texttt{Joueur} gère un joueur qui sera caractérisé par son
nom, sa main et son score.

,--------------------------------------------------------------------------.
\textbar{} Joueur \textbar{}
\textbar{}--------------------------------------------------------------------------\textbar{}
\textbar{} + nom : string \textbar{} \textbar{} + main : une liste de
cartes. \textbar{} \textbar{} + score : le nombre de têtes de bœufs
récupérees lors de la partie. \textbar{} \textbar{} \textbar{}
\textbar{} +\textbf{init}(self, nom=``Vous''): initialise le joueur avec
un nom par défaut \textbar{} \textbar{} une main vide ({[}{]}) et un
score nul. \textbar{} \textbar{} + prendre\_cartes(self, l) : ajoute la
liste de cartes à la main. La main \textbar{} \textbar{} doit être
triée. On vérifie qu'il s'agit bien d'une liste de cartes. \textbar{}
\textbar{} + get\_score(self) : renvoie le score. \textbar{} \textbar{}
+ a\_des\_cartes(self) : renvoie True si le joueur a encore des cartes.
\textbar{} \textbar{} + est\_carte\_valable(self, n): Pour la version
texte. Vérifie si n est le \textbar{} \textbar{} numéro d'une carte.
\textbar{} \textbar{} + poser\_carte(n) : renvoie la carte de valeur n
et l'enlève de la main. \textbar{} \textbar{} Si la carte de valeur n
n'est pas dans la main, une AssertionError \textbar{} \textbar{} est
levée. \textbar{} \textbar{} + poser\_carte\_au\_hasard(self) : renvoie
une carte au hasard. Utile pour \textbar{} \textbar{} pour faire jouer
les autres joueurs. \textbar{} \textbar{} + ajouter\_penalites(self, l)
: ajoute les nombres de têtes de bœufs \textbar{} \textbar{} de la liste
l au score. \textbar{} \textbar{} + get\_main(self) : renvoie la liste
des cartes séparées par un tiret \textbar{} \textbar{} +
\textbf{str}(self) : renvoie le nom du joueur et son score \textbar{}
`--------------------------------------------------------------------------'

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  On crée un joueur. On vérifie qu'il n'a pas de carte et que son score
  est vierge.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j }\OperatorTok{=}\NormalTok{ Joueur(}\StringTok{'Bob'}\NormalTok{)}
    \OperatorTok{>>>}\NormalTok{ j.nom}
    \CommentTok{'Bob'}
    \OperatorTok{>>>}\NormalTok{ j.main}
\NormalTok{    []}
    \OperatorTok{>>>}\NormalTok{ j.score}
    \DecValTok{0}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  La méthode \texttt{prendre\_cartes()} sera utilisée lors de la
  distribution des cartes. Il faut vérifier qu'il n'y a que des cartes
  dans la liste.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j.prendre_cartes([}\DecValTok{3}\NormalTok{, }\DecValTok{10}\NormalTok{, }\DecValTok{51}\NormalTok{])}
\NormalTok{    Traceback (most recent call last):}
\NormalTok{    ...}
    \PreprocessorTok{AssertionError}
    \OperatorTok{>>>}\NormalTok{ j.prendre_cartes([Carte(}\DecValTok{3}\NormalTok{), Carte(}\DecValTok{10}\NormalTok{), }\DecValTok{51}\NormalTok{])}
\NormalTok{    Traceback (most recent call last):}
\NormalTok{    ...}
    \PreprocessorTok{AssertionError}
    \OperatorTok{>>>}\NormalTok{ j.prendre_cartes([Carte(}\DecValTok{3}\NormalTok{), Carte(}\DecValTok{10}\NormalTok{), Carte(}\DecValTok{51}\NormalTok{)])}
    \OperatorTok{>>>}\NormalTok{ j.main}
\NormalTok{    [Carte n°}\DecValTok{3} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB, Carte n°}\DecValTok{10} \OperatorTok{-} \DecValTok{3}\NormalTok{ TdB, Carte n°}\DecValTok{51} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB]}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  La méthode \texttt{get\_score(self)} renvoie le score du joueur.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j.get_score()}
    \DecValTok{0}
    \OperatorTok{>>>}\NormalTok{ j2 }\OperatorTok{=}\NormalTok{ Joueur(}\StringTok{"Roger"}\NormalTok{)}
    \OperatorTok{>>>}\NormalTok{ j2.prendre_cartes([Carte(}\DecValTok{1}\NormalTok{), Carte(}\DecValTok{20}\NormalTok{), Carte(}\DecValTok{30}\NormalTok{), Carte(}\DecValTok{55}\NormalTok{)])}
    \OperatorTok{>>>}\NormalTok{ j2.get_score()}
    \DecValTok{0}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  On teste si les joueurs ont des cartes.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j.a_des_cartes()}
    \VariableTok{True}
    \OperatorTok{>>>}\NormalTok{ j2.a_des_cartes()}
    \VariableTok{True}
    \OperatorTok{>>>}\NormalTok{ j3 }\OperatorTok{=}\NormalTok{ Joueur(}\StringTok{"Marcel"}\NormalTok{)}
    \OperatorTok{>>>}\NormalTok{ j3.a_des_cartes()}
    \VariableTok{False}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Quand le joueur demande à poser une carte, il faut que la carte soit
  dans sa main sinon cela provoque une \texttt{AssertionError}.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j.est_carte_valable(}\DecValTok{5}\NormalTok{)}
    \VariableTok{False}
    \OperatorTok{>>>}\NormalTok{ j.poser_carte(}\DecValTok{5}\NormalTok{)}
\NormalTok{    Traceback (most recent call last):}
\NormalTok{    ...}
    \PreprocessorTok{AssertionError}
    \OperatorTok{>>>}\NormalTok{ j.est_carte_valable(}\DecValTok{3}\NormalTok{)}
    \VariableTok{True}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{5}
\tightlist
\item
  Si le joueur possède la carte demandée alors elle est renvoyée et le
  joueur possède une carte en moins.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j.main)}
    \DecValTok{3}
    \OperatorTok{>>>}\NormalTok{ j.poser_carte(}\DecValTok{3}\NormalTok{)}
\NormalTok{    Carte n°}\DecValTok{3} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB}
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j.main)}
    \DecValTok{2}
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j2.main)}
    \DecValTok{4}
    \OperatorTok{>>>}\NormalTok{ j2.poser_carte(}\DecValTok{55}\NormalTok{)}
\NormalTok{    Carte n°}\DecValTok{55} \OperatorTok{-} \DecValTok{7}\NormalTok{ TdB}
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j2.main)}
    \DecValTok{3}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{6}
\tightlist
\item
  On teste l'ajout de pénalités.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j.get_score()}
    \DecValTok{0}
    \OperatorTok{>>>}\NormalTok{ j.ajouter_penalites([Carte(}\DecValTok{9}\NormalTok{), Carte(}\DecValTok{11}\NormalTok{)])}
    \OperatorTok{>>>}\NormalTok{ j.get_score()}
    \DecValTok{6}
    \OperatorTok{>>>}\NormalTok{ j2.ajouter_penalites([Carte(}\DecValTok{90}\NormalTok{), Carte(}\DecValTok{33}\NormalTok{), Carte(}\DecValTok{104}\NormalTok{)])}
    \OperatorTok{>>>}\NormalTok{ j2.get_score()}
    \DecValTok{9}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{7}
\tightlist
\item
  On vérifie la main des joueurs.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j.get_main()}
    \CommentTok{'10-51'}
    \OperatorTok{>>>}\NormalTok{ j2.get_main()}
    \CommentTok{'1-20-30'}
    \OperatorTok{>>>}\NormalTok{ j2.poser_carte(}\DecValTok{20}\NormalTok{)}
\NormalTok{    Carte n°}\DecValTok{20} \OperatorTok{-} \DecValTok{3}\NormalTok{ TdB}
    \OperatorTok{>>>}\NormalTok{ j2.get_main()}
    \CommentTok{'1-30'}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{8}
\tightlist
\item
  On teste la fonction \texttt{\_\_str()\_\_}.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>} \BuiltInTok{str}\NormalTok{(j)}
    \CommentTok{'Bob : 6'}
    \OperatorTok{>>>} \BuiltInTok{str}\NormalTok{(j2)}
    \CommentTok{'Roger : 9'}
    \OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(j)}
\NormalTok{    Bob : }\DecValTok{6}
    \OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(j2)}
\NormalTok{    Roger : }\DecValTok{9}
\end{Highlighting}
\end{Shaded}

