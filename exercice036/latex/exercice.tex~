\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Soit une image binaire représentée dans un tableau à 2 dimensions. Les
éléments \texttt{M{[}i{]}{[}j{]}}, appelés pixels, sont égaux soit à
\texttt{0} soit à \texttt{1}.

Une composante d'une image est un sous-ensemble de l'image constitué
uniquement de \texttt{1} et de \texttt{0} qui sont côte à côte, soit
horizontalement soit verticalement.

Par exemple, les composantes de

\begin{figure}
\centering
\includegraphics{s04_22_exo_2_1.png}
\caption{image}
\end{figure}

sont

\begin{figure}
\centering
\includegraphics{s04_22_exo_2_2.png}
\caption{image}
\end{figure}

On souhaite, à partir d'un pixel égal à \texttt{1} dans une image
\texttt{M}, donner la valeur \texttt{val} à tous les pixels de la
composante à laquelle appartient ce pixel.

La fonction \texttt{propager} prend pour paramètre une image \texttt{M},
deux entiers \texttt{i} et \texttt{j} et une valeur entière
\texttt{val}. Elle met à la valeur \texttt{val} tous les pixels de la
composante du pixel \texttt{M{[}i{]}{[}j{]}} s'il vaut \texttt{1} et ne
fait rien s'il vaut \texttt{0}.

Par exemple, \texttt{propager(M,2,1,3)} donne

\begin{figure}
\centering
\includegraphics{s04_22_exo_2_3.png}
\caption{image}
\end{figure}

Compléter le code récursif de la fonction \texttt{propager} donné
ci-dessous :

\begin{Shaded}
\begin{Highlighting}[]
    \KeywordTok{def}\NormalTok{ propager(M, i, j, val):}
        \ControlFlowTok{if}\NormalTok{ M[i][j]}\OperatorTok{==}\NormalTok{ ...:}
            \ControlFlowTok{return} 
    
\NormalTok{        M[i][j] }\OperatorTok{=}\NormalTok{ val}
    
        \CommentTok{# l'élément en haut fait partie de la composante}
        \ControlFlowTok{if}\NormalTok{ ((i}\DecValTok{-1}\NormalTok{) }\OperatorTok{>=} \DecValTok{0} \KeywordTok{and}\NormalTok{ M[i}\DecValTok{-1}\NormalTok{][j] }\OperatorTok{==}\NormalTok{ ...):}
\NormalTok{            propager(M, i}\DecValTok{-1}\NormalTok{, j, val)}
    
        \CommentTok{# l'élément en bas fait partie de la composante}
        \ControlFlowTok{if}\NormalTok{ ((...) }\OperatorTok{<} \BuiltInTok{len}\NormalTok{(M) }\KeywordTok{and}\NormalTok{ M[i}\OperatorTok{+}\DecValTok{1}\NormalTok{][j] }\OperatorTok{==} \DecValTok{1}\NormalTok{):}
\NormalTok{            propager(M, ..., j, val)}
    
        \CommentTok{# l'élément à gauche fait partie de la composante}
        \ControlFlowTok{if}\NormalTok{ ((...) }\OperatorTok{>=} \DecValTok{0} \KeywordTok{and}\NormalTok{ M[i][j}\DecValTok{-1}\NormalTok{] }\OperatorTok{==} \DecValTok{1}\NormalTok{):}
\NormalTok{            propager(M, i, ..., val)}
    
        \CommentTok{# l'élément à droite fait partie de la composante}
        \ControlFlowTok{if}\NormalTok{ ((...) }\OperatorTok{<} \BuiltInTok{len}\NormalTok{(M) }\KeywordTok{and}\NormalTok{ M[i][j}\OperatorTok{+}\DecValTok{1}\NormalTok{] }\OperatorTok{==} \DecValTok{1}\NormalTok{): }
\NormalTok{            propager(M, i, ..., val)}
\end{Highlighting}
\end{Shaded}

Exemple :

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ M }\OperatorTok{=}\NormalTok{ [[}\DecValTok{0}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{],[}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{],[}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{],[}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{]]}
    \OperatorTok{>>>}\NormalTok{ propager(M,}\DecValTok{2}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{3}\NormalTok{)}
    \OperatorTok{>>>}\NormalTok{ M}
\NormalTok{    [[}\DecValTok{0}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{1}\NormalTok{, }\DecValTok{0}\NormalTok{], [}\DecValTok{0}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{0}\NormalTok{, }\DecValTok{1}\NormalTok{], [}\DecValTok{3}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{0}\NormalTok{], [}\DecValTok{0}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{0}\NormalTok{]]}
\end{Highlighting}
\end{Shaded}

