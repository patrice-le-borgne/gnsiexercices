
*1.*
|-----------------+---------+-----------+---------------------------|
| N° périphérique | Adresse | Opération | Réponse de l'ordonnanceur |
|-----------------+---------+-----------+---------------------------|
|               0 | '10'    | écriture  | "OK"                      |
|               1 | '11'    | lecture   | "OK"                      |
|               2 | '10'    | lecture   | "ATT"                     |
|               3 | '10'    | écriture  | "ATT"                     |
|               0 | '12'    | lecture   | "OK"                      |
|               1 | '10'    | lecture   | "OK"                      |
|               2 | '10'    | lecture   | "OK"                      |
|               3 | '10'    | écriture  | "ATT"                     |
|-----------------+---------+-----------+---------------------------|

Il s'agit d'un nouveau tour, les lectures sont possibles, la première écriture ne l'est pas, on a déjà accédé en lecture pendant le tour à l'adresse demandée.

*2.*

- À chaque début de tour, le périphérique 0 demande à écrire à l'adresse '10' ; c'est accepté.
- Juste après, le périphérique 1 demande à lire à l'adresse '10' ; c'est refusé.

Le périphérique 1 ne pourra jamais lire l'adresse '10'.

*3.a*

- **Tour 1** : 0 ; 1 ; 2 ; 3
    - 0 peut écrire, puis
    - 1 ne peut pas lire
- **Tour 2** : 1 ; 2 ; 3 ; 0
    - 1 peut lire, puis
    - 0 ne peut pas écrire
- **Tour 3** : 2 ; 3 ; 0 ; 1
    - 0 peut écrire, puis
    - 1 ne peut pas lire
- **Tour 4** : 3 ; 0 ; 1 ; 2
    - 0 peut écrire, puis
    - 1 ne peut pas lire

*3.b.*

- Au tour 1, la valeur écrite par le périphérique 0 sera lue par le périphérique 1 au tour suivant.
- Au tour 2, rien n'est écrit par le périphérique 0.
- Au tour 3, la valeur écrite par le périphérique 0 **ne sera jamais** lue par le périphérique 1 ; en effet, une autre écriture intervient avant la prochaine lecture.
- Au tour 4, la valeur écrite par le périphérique 0 **ne sera jamais** lue par le périphérique 1 ; en effet, une autre écriture intervient avant la prochaine lecture.

Ainsi, une seule valeur sur trois sera effectivement lue. La proportion est $\frac{1}{3}$.

*4.*
|------+-----------------+---------+-----------+----------------------+------------------+---------|
| Tour | N° périphérique | Adresse | Opération | Réponse ordonnanceur | 'ATT_L'          | 'ATT_E' |
|------+-----------------+---------+-----------+----------------------+------------------+---------|
|    1 |               0 | '10'    | écriture  | "OK"                 | vide             | vide    |
|    1 |               1 | '10'    | lecture   | "ATT"                | (1, 10)          | vide    |
|    1 |               2 | '11'    | écriture  | "OK"                 | (1, 10)          | vide    |
|    1 |               3 | '11'    | lecture   | "ATT"                | (1, 10), (3, 11) | vide    |
|    2 |               1 | '10'    | lecture   | "OK"                 | (3, 11)          | vide    |
|    2 |               3 | '11'    | lecture   | "OK"                 | vide             | vide    |
|    2 |               0 | '10'    | écriture  | "ATT"                | vide             | (0, 10) |
|    2 |               2 | '12'    | écriture  | "OK"                 | vide             | (0, 10) |
|    3 |               0 | '10'    | écriture  | "OK"                 | vide             | vide    |
|    3 |               1 | '10'    | lecture   | "ATT"                | (1, 10)          | vide    |
|    3 |               2 | '11'    | écriture  | "OK"                 | (1, 10)          | vide    |
|    3 |               3 | '12'    | lecture   | "OK"                 | (1, 10)          | vide    |
|------+-----------------+---------+-----------+----------------------+------------------+---------|

*5.a.*

[[file:circuit5acorrection.png]]

*5.b.*

[[file:circuit5bcorrection.png]]

*5.a.*

[[file:circuit5ccorrection.png]]

