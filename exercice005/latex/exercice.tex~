\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Programmer la fonction \texttt{recherche}, prenant en paramètre un
tableau non vide \texttt{tab} (type list) d'entiers et un entier
\texttt{n}, et qui renvoie l'indice de la dernière occurrence de
l'élément cherché. Si l'élément n'est pas présent, la fonction renvoie
la longueur du tableau.

Exemples :

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ recherche([}\DecValTok{5}\NormalTok{, }\DecValTok{3}\NormalTok{],}\DecValTok{1}\NormalTok{)}
    \DecValTok{2}
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ recherche([}\DecValTok{2}\NormalTok{,}\DecValTok{4}\NormalTok{],}\DecValTok{2}\NormalTok{)}
    \DecValTok{0}
    \OperatorTok{>>>}\NormalTok{ recherche([}\DecValTok{2}\NormalTok{,}\DecValTok{3}\NormalTok{,}\DecValTok{5}\NormalTok{,}\DecValTok{2}\NormalTok{,}\DecValTok{4}\NormalTok{],}\DecValTok{2}\NormalTok{)}
    \DecValTok{3}
\end{Highlighting}
\end{Shaded}

