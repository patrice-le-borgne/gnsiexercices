1. 
```
G1 = {
    'A': {'B': 4, 'E': 4},
    'B': {'A': 4, 'F': 7, 'G': 5},
    'C': {'E': 8, 'D': 4},
    'D': {'C': 4, 'E': 6, 'F': 8},
    'E': {'A': 4, 'C': 8, 'D': 6},
    'F': {'B': 7, 'D': 8, 'G': 3},
    'G': {'B': 5, 'F': 3}
}
```

2. Le chemin le plus court entre les villes A et D est A-B-F-G-D.

3. Matrice d'adjacence du graphe G1 :

```
      A  B  C  D  E  F  G
A [  0  4  0  0  4  0  0 ]
B [  4  0  0  0  0  7  5 ]
C [  0  0  0  4  8  0  0 ]
D [  0  0  4  0  6  8  0 ]
E [  4  0  8  6  0  0  0 ]
F [  0  7  0  8  0  0  3 ]
G [  0  5  0  0  0  3  0 ]
```

4. Implémentation en Python du graphe G2 à l'aide d'un dictionnaire :

```python
G2 = {
    'A': ['C', 'B'],
    'B': ['A', 'I', 'G'],
    'C': ['A', 'D', 'E'],
    'D': ['C', 'E'],
    'E': ['C', 'D', 'G'],
    'H': ['A', 'I', 'G'],
    'I': ['B', 'H', 'F'],
    'F': ['I', 'G'],
    'G': ['B', 'E', 'H', 'F']
}
```

5. Parcours en largeur du graphe G2 en partant de A : A - C - B - D - E - I - G - H - F

6. La fonction `cherche_itineraires` peut être qualifiée de fonction récursive car elle s'appelle elle-même à l'intérieur de sa propre définition.

7. La fonction `cherche_itineraires` permet de trouver tous les chemins possibles entre un sommet de départ et un sommet d'arrivée dans un graphe.

8. Compléter la fonction `itineraires_court` :

```python
def itineraires_court(G, dep, arr):
    cherche_itineraires(G, dep, arr)
    tab_court = []
    mini = float('inf')
    for v in tab_itineraires:
        if len(v) < mini:
            mini = len(v)
    for v in tab_itineraires:
        if len(v) == mini:
            tab_court.append(v)
    return tab_court
```

9. Le problème décrit est dû au fait que la liste `tab_itineraires` est définie en dehors de la fonction `itineraires_court`, ce qui signifie qu'elle conserve les résultats des appels précédents. Lorsque la fonction est appelée une deuxième fois sans être réinitialisée, elle garde les anciennes valeurs. Pour éviter ce problème, il faut réinitialiser la liste `tab_itineraires` au début de la fonction `itineraires_court`.

10. Un système de gestion de base de données (SGBD) est plus pertinent que l'utilisation d'un simple fichier texte pour plusieurs raisons :

    a. Organisation des données : Un SGBD permet d'organiser les données de manière structurée et relationnelle, ce qui facilite la manipulation et l'accès aux informations. En revanche, un fichier texte ne permet pas une organisation aussi efficace des données, ce qui rend leur gestion plus complexe.
    b. Sécurité des données : Les SGBD offrent des fonctionnalités de sécurité avancées telles que l'authentification, l'autorisation d'accès et le cryptage des données. Ces fonctionnalités permettent de protéger les données sensibles contre les accès non autorisés. En revanche, un fichier texte est plus vulnérable aux attaques et manipulations malveillantes.
    c. Gestion des transactions : Les SGBD offrent des mécanismes de gestion des transactions qui garantissent l'intégrité des données et la cohérence des opérations. En cas d'échec d'une opération, le SGBD peut effectuer un rollback pour annuler les modifications effectuées, ce qui n'est pas possible avec un simple fichier texte.
    d. Performance : Les SGBD sont conçus pour gérer efficacement de grandes quantités de données, ce qui permet d'optimiser les performances et la vitesse d'accès aux informations. En revanche, un fichier texte peut devenir lent et difficile à gérer lorsque la taille des données augmente.
    e. Facilité de gestion : Les SGBD offrent des outils de gestion et d'administration qui simplifient la maintenance et la gestion des données. Ils permettent également de créer des requêtes complexes pour extraire des informations spécifiques. En revanche, la manipulation des données dans un fichier texte est souvent laborieuse et nécessite des compétences techniques avancées.

    En résumé, l'utilisation d'un SGBD offre de nombreux avantages par rapport à un simple fichier texte en termes d'organisation, de sécurité, de performances, de gestion et de facilité d'accès aux données. Il s'agit donc d'une solution plus pertinente pour gérer efficacement et en toute sécurité des données.

11. Schéma relationnel de la table `ville`:
- id (int)
- nom (varchar)
- num_dep (int)
- nombre_hab (int)
- superficie (float)

12. L'attribut `id_ville` dans la table `sport` permet de lier chaque infrastructure à la ville où elle se trouve en se référant à l'identifiant unique de la ville.

13. Le résultat de la requête SQL est : 
| nom   |
|-------|
| Nice  |

14. Requête SQL pour lister les noms des piscines présentes dans la table `sport`:
```sql
SELECT nom FROM sport WHERE type = 'piscine'
```

15. Requête SQL pour modifier la note du terrain multisport "Ballons perdus" de 6 à 7:
```sql
UPDATE sport SET note = 7 WHERE nom = 'Ballons perdus'
```

16. Requête SQL pour ajouter la ville de Toulouse dans la table `ville`:
```sql
INSERT INTO ville (id, nom, num_dep, nombre_hab, superficie) VALUES (8, 'Toulouse', 31, 471941, 118)
```

17. Requête SQL pour lister les noms des murs d'escalade disponibles à Annecy:
```sql
SELECT nom FROM sport WHERE type = 'mur d’escalade' AND id_ville = 1
```
