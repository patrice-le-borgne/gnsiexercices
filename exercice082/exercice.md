~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : arbres
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> a = Arbre(1)
>>> a.fg = Arbre(4)
>>> a.fd = Arbre(0)
>>> a.fd.fd = Arbre(7)
>>> b = Arbre(0)
>>> b.fg = Arbre(1)
>>> b.fd = Arbre(2)
>>> b.fg.fg = Arbre(3)
>>> b.fd.fg = Arbre(4)
>>> b.fd.fd = Arbre(5)
>>> b.fd.fg.fd = Arbre(6)
~~~

Exercice
==========

Un arbre binaire est implémenté par la classe `Arbre` donnée ci-dessous.
Les attributs `fg` et `fd` prennent pour valeurs des instances de la classe `Arbre` ou `None`.

~~~ {.python}
class Arbre:
    def __init__(self, etiquette):
        self.v = etiquette
        self.fg = None
        self.fd = None
~~~

![image1](arbre082.png){: .center}

L’arbre ci-dessus sera donc implémenté de la manière suivante :

~~~ {.python .amc file="Q_1.md" bareme="4"}
a = Arbre(1)
a.fg = Arbre(4)
a.fd = Arbre(0)
a.fd.fd = Arbre(7)
~~~

Écrire une fonction récursive `taille` prenant en paramètre une instance `a` de la classe
`Arbre` et qui renvoie la taille de l’arbre que cette instance implémente.

Écrire de même une fonction récursive `hauteur` prenant en paramètre une instance `a`
de la classe `Arbre` et qui renvoie la hauteur de l’arbre que cette instance implémente.

Si un arbre a un seul nœud, sa taille et sa hauteur sont égales à 1.
S’il est vide, sa taille et sa hauteur sont égales à 0.

Tester les deux fonctions sur l’arbre représenté ci-dessous :

![image](arbre082_bis.png){: .center}

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> taille(a)
4
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> taille(b)
7
>>> taille(Arbre(None))
1
>>> taille(None)
0
~~~

~~~ {.python .hidden .test file="Q_3.md" bareme="1"}
>>> hauteur(a)
3
~~~

~~~ {.python .hidden .test file="Q_4.md" bareme="1"}
>>> hauteur(b)
4
>>> hauteur(Arbre(None))
1
>>> hauteur(None)
0
~~~


