    >>> from exercice import *
    >>> from joueur import Joueur
    >>> from jeu import Jeu
    >>> from plateau import Plateau
    >>> from carte import Carte
    >>> from unittest.mock import patch

    >>> with patch ('jeu.Jeu.piocher', side_effect = 4* [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
    ...     partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
    ...     with patch('joueur.randint', return_value = 0):
    ...         with patch ('builtins.input', side_effect=4* ["72", "25", "97", "90", "62", "80", "14", "33", "83", "92"]):
    ...             partie.run()
    ###############
    Nouvelle manche
    ###############
    <BLANKLINE>
    Jean : 0
    Bob : 0
    Roger : 0
    Maurice : 0
    <BLANKLINE>
    [1] : 39
    [1] : 81
    [1] : 74
    [2] : 35
    <BLANKLINE>
    14-25-33-62-72-80-83-90-92-97
    On place la carte de Bob : 1.
    Bob ramasse la rangée 1.
    On place la carte de Roger : 3.
    On place la carte de Maurice : 6.
    On place votre carte : 72.
    <BLANKLINE>
    Jean : 0
    Bob : 1
    Roger : 0
    Maurice : 0
    <BLANKLINE>
    [3] : 1-3-6
    [1] : 81
    [1] : 74
    [3] : 35-72
    <BLANKLINE>
    14-25-33-62-80-83-90-92-97
    On place la carte de Roger : 10.
    On place la carte de Maurice : 21.
    On place la carte de Bob : 22.
    Bob ramasse la rangée 1.
    On place votre carte : 25.
    <BLANKLINE>
    Jean : 0
    Bob : 8
    Roger : 0
    Maurice : 0
    <BLANKLINE>
    [7] : 22-25
    [1] : 81
    [1] : 74
    [3] : 35-72
    <BLANKLINE>
    ...
    Jean : 6
    Bob : 72
    Roger : 42
    Maurice : 93
    <BLANKLINE>
    [5] : 33
    [2] : 89-92
    [4] : 14-47-78-83
    [1] : 104
    <BLANKLINE>
    <BLANKLINE>

 
