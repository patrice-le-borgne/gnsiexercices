    >>> from exercice import *
    >>> from joueur import Joueur
    >>> from jeu import Jeu
    >>> from plateau import Plateau
    >>> from carte import Carte
    >>> from unittest.mock import patch

    >>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
    ...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
    ...    print(partie.joueurs[0].get_main())
    14-25-33-62-72-80-83-90-92-97

    >>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
    ...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
    ...    print(partie.joueurs[1].get_main())
    1-22-29-30-37-42-47-84-85-89

    >>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
    ...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
    ...    print(partie.joueurs[2].get_main())
    3-10-16-44-52-55-56-77-78-88

    >>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
    ...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
    ...    print(partie.joueurs[3].get_main())
    6-21-40-61-63-76-86-94-101-104

    >>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
    ...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
    ...    print(partie.plateau)
    [1] : 39
    [1] : 81
    [1] : 74
    [2] : 35
    <BLANKLINE>

 
