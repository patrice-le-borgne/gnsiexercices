"""
Module partie
"""
from joueur import Joueur
from jeu import Jeu
from plateau import Plateau

class Partie:
    """
    Classe Partie
    """

    def __init__(self, l):
        """
        Il faut fournir une liste de 4 noms.
        """
        assert len(l) == 4
        for nom in l:
            assert isinstance(nom, str)
        self.joueurs = [Joueur(n) for n in l]
        self.plateau = None
        self.setup()

    def setup(self):
        """
        On mélange le jeu de cartes. On les distribue aux 4 joueurs
        puis on en place 4 sur le plateau.
        """
        jeu =Jeu()
        for j in self.joueurs:
            j.prendre_cartes([jeu.piocher() for i in range(10)])
        liste_cartes = [jeu.piocher() for i in range(4)]
        self.plateau = Plateau(liste_cartes)

        
    def __str__(self):
        """
        Renvoie les scores des joueurs, le plateau puis la main du joueur.
        """
        liste_joueurs = [str(j)  for j in self.joueurs]
        texte = "\n".join(liste_joueurs)
        texte += "\n\n"
        texte += str(self.plateau) + "\n"
        texte += str(self.joueurs[0].get_main())
        return texte

    def est_terminee(self):
        """
        Teste si un joueur a dépassé 66.
        """
        for j in self.joueurs:
            if j.get_score() >= 66:
                return True
        return False

    def run(self):
        while not self.est_terminee():
        # on mélange le jeu et on distribue les cartes.
            self.setup()
            print("###############\nNouvelle manche\n###############\n")
            print(self)
            # tant que le joueur a des cartes
            while self.joueurs[0].a_des_cartes():
                # il saisit au clavier le numéro de la carte qu'il veut poser
                n = 0
                while not self.joueurs[0].est_carte_valable(n):
                    n = int(input("Quelle carte voulez-vous poser ?"))
                # on range les choix dans une liste. Les 3 autres joueurs
                # jouent au hasard.
                choix = [(0, self.joueurs[0].poser_carte(n))]
                for i in range(1, 4):
                    choix.append((i, self.joueurs[i].poser_carte_au_hasard()))
                # on trie cette liste de choix car les cartes sont placées en
                # commençant par la plus petite.
                choix = sorted(choix, key=lambda c: c[1])
                # on place les cartes sur le plateau
                for i, c in choix:
                    i_rangee, l = self.plateau.ajouter(c)
                    if i == 0:
                        print(f"On place votre carte : {c}.")
                    else:
                        print(f"On place la carte de {self.joueurs[i].nom} : {c}.")
                    if l != []:
                        if self.joueurs[i].nom == "Vous":
                            print(f"{self.joueurs[i].nom} ramassez la rangée {i_rangee + 1}.")
                        else:
                            print(f"{self.joueurs[i].nom} ramasse la rangée {i_rangee + 1}.")
                    self.joueurs[i].ajouter_penalites(l)
                print()
                print(self)

if __name__ == '__main__':
    import nose
    nose.run()


