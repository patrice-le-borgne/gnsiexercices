~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : listes de listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> from joueur import Joueur
>>> from jeu import Jeu
>>> from plateau import Plateau
>>> from carte import Carte
>>> from unittest.mock import patch
~~~

Exercice
========

La classe Partie gère le bon déroulement de la partie. On rappelle
qu'une partie s'arrête dès qu'un joueur a dépassé les 66 points à
l'issue d'une manche.  Elle se joue généralement en plusieurs manches.

Pour une manche, on distribue à chaque joueur 10 cartes et on dispose
4 cartes sur le plateau.  Toute manche commencée doit être
terminée. 

Le vainqueur de la partie est celui qui a le moins de
points.


,----------------------------------------------------------------------------------------------.
|Partie                                                                                        |
|----------------------------------------------------------------------------------------------|
|+ joueurs : la liste des joueurs (classe Joueur). Le premier de la liste est le joueur humain.|
|          Les 3 autres sont des joueurs numériques.                                           |
|+ plateau : le plateau de jeu (Classe Plateau)                                                |
|----------------------------------------------------------------------------------------------|
|+ __init__(self, l) : l est une liste de 4 noms.                                              |
|+ setup(self) : mélange le jeu complet de cartes, distribue les cartes aux 4 joueurs          |
|              puis en place 4 sur le plateau.                                                 |
|+ __str__(self): renvoie les scores des joueurs, le plateau puis la main du joueur.           |
|+ est_terminee(self): teste si un joueur a plus de 66 points.                                 |
|+ run(self) : la partie en elle-même.                                                         |
`----------------------------------------------------------------------------------------------'

Le déroulement de la partie est le suivant :

```
afficher la partie;

tant que la partie n'est pas terminée :
	afficher "Nouvelle manche";
	mélanger et distribuer les cartes;

tant que le joueur a des cartes :
	n=0;
	tant que la carte saisie par le joueur n'est pas valable :
		n = carte jouée par le joueur
	
	on range les cartes jouées par les joueurs 
	dans une liste de tuplés (indice joueur, carte);
	
	on trie cette liste dans l'ordre croissant;
	(utiliser la fonction sorted avec le paramètre key.)


	on parcourt les cartes jouées : 
		on place la carte sur le plateau;
		( on rappelle que la méthode ajouter de Plateau
		renvoie l'indice de la rangée où est placée la carte
		ainsi que les listes des cartes ramassées.)

	    on affiche quelle carte a été placée et
		à quel joueur elle appartient
		
		si la carte placée est la 6ème ?
		    alors afficher quel joueur ramasse quelle rangée
			      ajouter les pénalités au joueur;
	afficher la partie;
```


1. La partie s'initialise avec une liste de 4 noms sous forme de `str`, sinon cela provode une `AssertionError` :

~~~ {.python .test .amc file="Q_1.md" bareme="1"}
>>> partie = Partie([1, 2, 3, 4])
Traceback (most recent call last):
...
AssertionError
>>> partie = Partie(["Jean", "Bob", "Roger"])
Traceback (most recent call last):
...
AssertionError
>>> partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
<BLANKLINE>
~~~

2. Une manche débute par la distribution des cartes et la mise en place du plateau (une carte par rangée).

  Les tests suivants supposent que vous avez bien utilisé la méthode
  `piocher` de la classe `Jeu` pour distribuer les
  cartes. De plus, le premier joueur de la liste est l'humain.


~~~ {.python .test .amc file="Q_2.md" bareme="1"}
>>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
...    print(partie.joueurs[0].get_main())
14-25-33-62-72-80-83-90-92-97

>>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
...    print(partie.joueurs[1].get_main())
1-22-29-30-37-42-47-84-85-89

>>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
...    print(partie.joueurs[2].get_main())
3-10-16-44-52-55-56-77-78-88

>>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
...    print(partie.joueurs[3].get_main())
6-21-40-61-63-76-86-94-101-104

>>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
...    print(partie.plateau)
[1] : 39
[1] : 81
[1] : 74
[2] : 35
<BLANKLINE>
~~~

3. En mode console, on veut pouvoir afficher les scores, le plateau et la main du joueur avec une simple `print`. En implémentant le méthode `__str__`, on doit obtenir l'affichage suivant. Le `<BLANKLINE>` ne sert que pour les tests. Il précise qu'il y a une ligne vide en mode normal.

~~~ {.python .test .amc file="Q_3.md" bareme="1"}
>>> with patch ('jeu.Jeu.piocher', side_effect = [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
...    partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
...    print(partie)
Jean : 0
Bob : 0
Roger : 0
Maurice : 0
<BLANKLINE>
[1] : 39
[1] : 81
[1] : 74
[2] : 35
<BLANKLINE>
14-25-33-62-72-80-83-90-92-97
~~~

4. Le test suivant court-circuite certaines fonctions pour distribuer et jouer toujours les mêmes cartes. Pour que le joueur puisse suivre ce qu'il se passe, nous publions au fur et à mesure des messages qui peuvent être :

- "On place votre carte : (numero de la carte)."
- "On place la carte de (joueur) : (numero de la carte)."
- "Vous ramassez la rangée : (numero de la rangée)."
- "(joueur) ramasse la rangée : (numero de la rangée)."

Vous remarquerez que le début d'une nouvelle manche est indiqué par :

~~~ {.python}
###############
Nouvelle manche
###############
~~~

Le test suivant a été écrit pour la méthode `poser_une_carte_au_hasard` de la classe `Joueur` suivante :

~~~ {.python}
    def poser_carte_au_hasard(self):
        """
        renvoie la carte de valeur n et l'enlève de la main.
        """
        return self.main.pop(randint(0, len(self.main) - 1))
~~~


~~~ {.python .test .amc file="Q_4.md" bareme="7"}
>>> with patch ('jeu.Jeu.piocher', side_effect = 4* [Carte(72), Carte(25), Carte(97), Carte(90), Carte(62), Carte(80), Carte(14), Carte(33), Carte(83), Carte(92), Carte(47), Carte(22), Carte(85), Carte(1), Carte(29), Carte(84), Carte(42), Carte(30), Carte(89), Carte(37), Carte(55), Carte(77), Carte(16), Carte(10), Carte(44), Carte(78), Carte(3), Carte(52), Carte(88), Carte(56), Carte(104), Carte(6), Carte(63), Carte(101) , Carte(21), Carte(40), Carte(76), Carte(61), Carte(94), Carte(86), Carte(39), Carte(81), Carte(74), Carte(35)]):
...     partie = Partie(["Jean", "Bob", "Roger", "Maurice"])
...     with patch('joueur.randint', return_value = 0):
...         with patch ('builtins.input', side_effect=4* ["72", "25", "97", "90", "62", "80", "14", "33", "83", "92"]):
...             partie.run()
###############
Nouvelle manche
###############
<BLANKLINE>
Jean : 0
Bob : 0
Roger : 0
Maurice : 0
<BLANKLINE>
[1] : 39
[1] : 81
[1] : 74
[2] : 35
<BLANKLINE>
14-25-33-62-72-80-83-90-92-97
On place la carte de Bob : 1.
Bob ramasse la rangée 1.
On place la carte de Roger : 3.
On place la carte de Maurice : 6.
On place votre carte : 72.
<BLANKLINE>
Jean : 0
Bob : 1
Roger : 0
Maurice : 0
<BLANKLINE>
[3] : 1-3-6
[1] : 81
[1] : 74
[3] : 35-72
<BLANKLINE>
14-25-33-62-80-83-90-92-97
On place la carte de Roger : 10.
On place la carte de Maurice : 21.
On place la carte de Bob : 22.
Bob ramasse la rangée 1.
On place votre carte : 25.
<BLANKLINE>
Jean : 0
Bob : 8
Roger : 0
Maurice : 0
<BLANKLINE>
[7] : 22-25
[1] : 81
[1] : 74
[3] : 35-72
<BLANKLINE>
...
Jean : 6
Bob : 72
Roger : 42
Maurice : 93
<BLANKLINE>
[5] : 33
[2] : 89-92
[4] : 14-47-78-83
[1] : 104
<BLANKLINE>
<BLANKLINE>
~~~



