# Questions préliminaires

"""
1. Expliquer pourquoi le couple `(1, 3)` est une inversion dans le tableau `[4, 8, 3, 7]`.


"""
score_QP1 = 1


"""
2. Justifier que le couple `(2, 3)` n’en est pas une.


"""
score_QP2 = 1


## Partie A

# Q.A.1

def fonction1(tab, i):
    nb_elem = len(tab)
    cpt = 0
    for j in range(i+1, nb_elem):
        if tab[j] < tab[i]:
            cpt += 1
    return cpt

# Q.A.2
def nombre_inversions(tab):
    compteur = 0
    for i in range(len(tab) - 1):
        compteur += fonction1(tab,i)
    return compteur

# Q.A.3
# Choisir la bonne réponse :
# a. O(n)    b.O(n.log(n))    c. O(n^2)

reponse_A_3 = "c"


## Partie B

# Q.B.1
# Choisir la bonne réponse :
# a. tri par sélection    b.tri à bulles    c. tri fusion

reponse_B_1 = "c"

## Q.B.2
def moitie_gauche(tab):
    n = len(tab)
    if n %2 == 0:
        return tab[:n//2]
    else:
        return tab[:n//2+1]


## Q.B.3
def moitie_droite(tab):
    n = len(tab)
    if n %2 == 0:
        return tab[n//2:]
    else:
        return tab[n//2+1:]


## Q.B.4
def nb_inv_tab(l1, l2):
    compteur = 0
    for e1 in l1:
        for e2 in l2:
            if e1 > e2:
                compteur += 1
    return compteur

def tri(l):
    l.sort()

def nombre_inversions_rec(tab):
    if len(tab) <=1:
        return 0
    else:
        md = moitie_droite(tab)
        mg = moitie_gauche(tab)
        nbg = nombre_inversions_rec(mg)
        nbd = nombre_inversions_rec(md)                 
        tri(md)
        tri(mg)
        return nbg +nbd + nb_inv_tab(mg,md)
