~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Écrire une fonction `recherche` qui prend en paramètres `caractere`, un caractère, et
`mot`, une chaîne de caractères, et qui renvoie le nombre d’occurrences de `caractere`
dans `mot`, c’est-à-dire le nombre de fois où `caractere` apparaît dans `mot`.

Exemples :

~~~ {.python .amc .test file="Q_1.md" bareme="2"}
    >>> recherche('e', "sciences")
    2
    >>> recherche('i',"mississippi")
    4
    >>> recherche('a',"mississippi")
    0
~~~

